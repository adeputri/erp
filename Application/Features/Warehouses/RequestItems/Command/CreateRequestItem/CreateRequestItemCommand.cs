﻿using MediatR;
using SkeletonApi.Shared;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Warehouses.RequestItems.Command.CreateRequestItem
{
    public record CreateRequestItemCommand : IRequest<Result<Guid>>
    {
     

        [JsonPropertyName("qty_request")]
        public int QtyRequest { get; set; }
        [JsonPropertyName("item_id")]
        public Guid ItemId { get; set; }

        [JsonPropertyName("project_id")]
        public Guid ProjectId { get; set; }

        [JsonPropertyName("request_employee_id")]
        public Guid RequestEmployeeId { get; set; }

        [JsonPropertyName("pic_employee_id")]
        public Guid PICEmployeeId { get; set; }
    }
}
