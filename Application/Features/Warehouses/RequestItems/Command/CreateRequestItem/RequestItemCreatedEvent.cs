﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Warehouses.RequestItems.Command.CreateRequestItem
{
    public class RequestItemCreatedEvent : BaseEvent
    {
        public RequestItemWarehouse RequestItemWarehouse { get; set; }

        public RequestItemCreatedEvent(RequestItemWarehouse requestItemWarehouse)
        {
            RequestItemWarehouse = requestItemWarehouse;
        }
    }
}
