﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Common.Exceptions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Warehouses.RequestItems.Command.CreateRequestItem
{
    internal class CreateRequestItemHandler : IRequestHandler<CreateRequestItemCommand, Result<Guid>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IMasterProjectRepository _masterProjectRepository;
        private readonly IMapper _mapper;

        public CreateRequestItemHandler(IUnitOfWork unitOfWork,
            IEmployeeRepository employeeRepository,
            IItemRepository itemRepository,
            IMasterProjectRepository masterProjectRepository,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _employeeRepository = employeeRepository;
            _itemRepository = itemRepository;
            _masterProjectRepository = masterProjectRepository;
            _mapper = mapper;
        }

        public async Task<Result<Guid>> Handle(CreateRequestItemCommand request, CancellationToken cancellationToken)
        {
            var item = await _itemRepository.GetItemAsync(request.ItemId);
            if (item == null)
            {
                ItemNotFoundException.Throw(request.ItemId);
            }

            var employeeRequest = await _employeeRepository.GetEmployeeAsync(request.RequestEmployeeId);
            if (employeeRequest == null)
            {
                EmployeeNotFoundException.Throw(request.RequestEmployeeId);
            }

            var employeePIC = await _employeeRepository.GetEmployeeAsync(request.PICEmployeeId);
            if (employeePIC == null)
            {
                EmployeeNotFoundException.Throw(request.PICEmployeeId);
            }

            var project = await _masterProjectRepository.GetProjectById(request.ProjectId);
            if (project == null)
            {
                ProjectNotFoundException.Throw(request.ProjectId);
            }

            var ModelData = new RequestItemWarehouse
            {
                ItemId = item.Id,
                RequestEmployeeId = employeeRequest.Id,
                PICEmployeeId = employeePIC.Id,
                QtyRequest = request.QtyRequest,
                ProjectId = project.Id,
                IsFinish = false,
            };

            await _unitOfWork.Repository<RequestItemWarehouse>().AddAsync(ModelData);
            ModelData.AddDomainEvent(new RequestItemCreatedEvent(ModelData));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Guid>.SuccessAsync(ModelData.Id, "Model Created");
        }
    }
}