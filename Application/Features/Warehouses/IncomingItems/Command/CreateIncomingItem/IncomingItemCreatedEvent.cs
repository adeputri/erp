﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Warehouses.IncomingItems.Command.CreateIncomingItem
{
    public class IncomingItemCreatedEvent : BaseEvent
    {
        public Warehouse Warehouse { get; set; }

        public IncomingItemCreatedEvent(Warehouse warehouse)
        {
            Warehouse = warehouse;
        }
    }
}
