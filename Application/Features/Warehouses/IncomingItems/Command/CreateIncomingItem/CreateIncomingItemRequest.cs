﻿using MediatR;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SkeletonApi.Shared;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Warehouses.IncomingItems.Command.CreateIncomingItem
{
    public record CreateIncomingItemRequest : IRequest<Result<Guid>>
    {
        [JsonPropertyName("barcode_item")]
        public string BarcodeItem { get; set; }

        [JsonPropertyName("qty_item")]
        public int QtyItem { get; set; }

        [JsonPropertyName("project_id")]
        public Guid ProjectId { get; set; }

        [JsonPropertyName("request_employee_id")]
        public Guid RequestEmployeeId { get; set; }

        [Column("pic_employee_id")]
        public Guid PICEmployeeId { get; set; }

        [BindNever]
        public string PathDocument { get; set; }

        public string Note { get; set; }
    }
}
