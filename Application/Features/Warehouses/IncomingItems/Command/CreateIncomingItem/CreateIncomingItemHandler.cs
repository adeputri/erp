﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Common.Exceptions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Warehouses.IncomingItems.Command.CreateIncomingItem
{
    internal class CreateIncomingItemHandler : IRequestHandler<CreateIncomingItemRequest, Result<Guid>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IItemRepository _itemRepository;
        private readonly IMasterProjectRepository _masterProjectRepository;
        private readonly IMapper _mapper;

        public CreateIncomingItemHandler(IEmployeeRepository employeeRepository,
            IItemRepository itemRepository,
            IMapper mapper,
            IUnitOfWork unitOfWork,
            IMasterProjectRepository masterProjectRepository)
        {
            _employeeRepository = employeeRepository;
            _itemRepository = itemRepository;
            _unitOfWork = unitOfWork;
            _masterProjectRepository = masterProjectRepository;
        }

        public async Task<Result<Guid>> Handle(CreateIncomingItemRequest request, CancellationToken cancellationToken)
        {
            var itemId = await _itemRepository.GetIdByBarcodeAsync(request.BarcodeItem);
            if (itemId == Guid.Empty)
            {
                BarcodeItemNotFoundException.Throw(request.BarcodeItem);
            }

            var employeeRequest = await _employeeRepository.GetEmployeeAsync(request.RequestEmployeeId);
            if (employeeRequest == null)
            {
                EmployeeNotFoundException.Throw(request.RequestEmployeeId);
            }

            var employeePIC = await _employeeRepository.GetEmployeeAsync(request.PICEmployeeId);
            if (employeePIC == null)
            {
                EmployeeNotFoundException.Throw(request.RequestEmployeeId);
            }

            var project = await _masterProjectRepository.GetProjectById(request.ProjectId);
            if (project == null)
            {
                ProjectNotFoundException.Throw(request.ProjectId);
            }

            var ModelData = new Warehouse
            {
                ItemId = itemId,
                RequestEmployeeId = employeeRequest.Id,
                PICEmployeeId = employeePIC.Id,
                QtyItem = request.QtyItem,
                ProjectId = project.Id,
                Note = request.Note,
                PathDocument = request.PathDocument,
            };

            await _unitOfWork.Repository<Warehouse>().AddAsync(ModelData);
            ModelData.AddDomainEvent(new IncomingItemCreatedEvent(ModelData));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Guid>.SuccessAsync(ModelData.Id, "Model Created");
        }
    }
}