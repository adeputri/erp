﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Purchases.PPN
{
    public record PpnDto
    {
        [JsonPropertyName("payment_status")]
        public string PaymentStatus { get; set; }
        [JsonPropertyName("supplier_id")]
        public Guid SupplierId { get; set; }
        [JsonPropertyName("project_id")]
        public Guid ProjectId { get; set; }
        [JsonPropertyName("pre_order_date")]
        public DateTime PreOrderDate { get; set; }
        [JsonPropertyName("pre_order_number")]
        public string PreOrderNumber { get; set; }
        [JsonPropertyName("spk_number")]
        public string SpkNumber { get; set; }
        [JsonPropertyName("pi_number")]
        public string PiNumber { get; set; }
        [JsonPropertyName("inv_date")]
        public DateTime InvDate { get; set; }
        [JsonPropertyName("inv_receipt_date")]
        public DateTime InvRecieptDate { get; set; }
        [JsonPropertyName("inv_number")]
        public string InvNumber { get; set; }
        [JsonPropertyName("faktur_date")]
        public DateTime FakturDate { get; set; }
        [JsonPropertyName("faktur_number")]
        public string FakturNumber { get; set; }
        [JsonPropertyName("travel_doc_date")]
        public DateTime TravelDocDate { get; set; }
        [JsonPropertyName("travel_doc_number")]
        public string TravelDocNumber { get; set; }
        public string Masa { get; set; }
        public double Dpp { get; set; }
        public double Ppn { get; set; }
        public double Discount { get; set; }
        public double Pph23 { get; set; }
        public string Note { get; set; }
        [JsonPropertyName("paid_of_date")]
        public DateTime PaidOfDate { get; set; }
        [JsonPropertyName("file_path")]
        public string FilePath { get; set; }
    }
    public sealed record CreatePpnResponseDto : PpnDto { }
}