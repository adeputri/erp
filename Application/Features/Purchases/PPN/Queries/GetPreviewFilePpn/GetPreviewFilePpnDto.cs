﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPreviewFilePpn
{
    public class GetPreviewFilePpnDto : IMapFrom<GetPreviewFilePpnDto>
    {
        [JsonPropertyName("mime_type")]
        public string? MimeType { get; set; }

        [JsonPropertyName("path")]
        public string? Path { get; set; }

        public byte[] Content { get; set; }
    }
}