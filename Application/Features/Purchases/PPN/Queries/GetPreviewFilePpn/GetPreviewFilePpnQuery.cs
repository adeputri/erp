﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPreviewFilePpn
{
    public record GetPreviewFilePpnQuery : IRequest<Result<GetPreviewFilePpnDto>>
    {
        public string Path { get; set; }
        public GetPreviewFilePpnQuery(string path)
        {
            Path = path;
        }
    }

    internal class GetPreviewFileQueryHandler : IRequestHandler<GetPreviewFilePpnQuery, Result<GetPreviewFilePpnDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPpnRepository _ppnRepository;

        public GetPreviewFileQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IPpnRepository ppnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _ppnRepository = ppnRepository;
        }

        public async Task<Result<GetPreviewFilePpnDto>> Handle(GetPreviewFilePpnQuery query, CancellationToken cancellationToken)
        {
            var file = await _ppnRepository.GetFile(query.Path);
            return await Result<GetPreviewFilePpnDto>.SuccessAsync(file, "Successfully fetch data");
        }
    }
}