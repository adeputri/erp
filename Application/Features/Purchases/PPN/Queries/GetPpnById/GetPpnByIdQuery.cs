﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnById
{
    public record GetPpnByIdQuery : IRequest<Result<GetPpnByIdDto>>
    {
        public string UniqeCode { get; set; }
        public GetPpnByIdQuery(string uniqeCode)
        {
            UniqeCode = uniqeCode;
        }
    }

    internal class GetCashAtBankByIdQueryHandler : IRequestHandler<GetPpnByIdQuery, Result<GetPpnByIdDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPpnRepository _ppnRepository;

        public GetCashAtBankByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IPpnRepository ppnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _ppnRepository = ppnRepository;
        }

        public async Task<Result<GetPpnByIdDto>> Handle(GetPpnByIdQuery query, CancellationToken cancellationToken)
        {
            var dt = await _ppnRepository.GetById(query.UniqeCode);
            if (dt == null)
            {
                return await Result<GetPpnByIdDto>.FailureAsync("Unique code not found");
            }

            return await Result<GetPpnByIdDto>.SuccessAsync(dt, "Successfully fetch data");
        }
    }
}