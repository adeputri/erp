﻿using ClosedXML.Excel;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnWithPagination;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Queries.Download
{
    public class DownloadPpnToExcel
    {
        private readonly PaginatedResult<GetPpnWithPaginationDto> data;

        public DownloadPpnToExcel(PaginatedResult<GetPpnWithPaginationDto> dt)
        {
            data = dt;
        }

        public void GetListExcel(ref byte[] _content, ref string FileName)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Sheet1");

                worksheet.Cell(1, 1).Value = "File Path";
                worksheet.Cell(1, 2).Value = "Payment Status";
                worksheet.Cell(1, 3).Value = "Supplier Name";
                worksheet.Cell(1, 4).Value = "NPWP Supplier";
                worksheet.Cell(1, 5).Value = "Project Name";
                worksheet.Cell(1, 6).Value = "Number SPK";
                worksheet.Cell(1, 7).Value = "PI Number";
                worksheet.Cell(1, 8).Value = "FP Date";
                worksheet.Cell(1, 9).Value = "Faktur Number";
                worksheet.Cell(1, 10).Value = "Travel Doc Date";
                worksheet.Cell(1, 11).Value = "Travel Doc Number";
                worksheet.Cell(1, 12).Value = "Masa";
                worksheet.Cell(1, 13).Value = "Total";
                worksheet.Cell(1, 14).Value = "Note";
                worksheet.Cell(1, 15).Value = "Paid of Date";
                worksheet.Cell(1, 16).Value = "DPP";
                worksheet.Cell(1, 17).Value = "PPN";
                worksheet.Cell(1, 18).Value = "PPH23";
                worksheet.Cell(1, 19).Value = "Discount";
                worksheet.Cell(1, 20).Value = "Approval Status";
                worksheet.Cell(1, 21).Value = "Update At";
                worksheet.Cell(1, 22).Value = "Created At";
                worksheet.Cell(1, 23).Value = "Created By";
                worksheet.Cell(1, 24).Value = "Pre Order Number";
                worksheet.Cell(1, 25).Value = "Pre Order Date";
                worksheet.Cell(1, 26).Value = "Inv Number";
                worksheet.Cell(1, 27).Value = "Inv Date";
                worksheet.Cell(1, 28).Value = "Inv Receipt Date";

                int currentRow = 2;

                foreach (var item in data.Data)
                {
                    var dataPo = item.DataPo;
                    var dataInv = item.DataInv;

                    int maxRows = Math.Max(dataPo.Count, dataInv.Count);

                    for (int j = 0; j < maxRows; j++)
                    {
                        worksheet.Cell(currentRow, 1).Value = item.FilePath;
                        worksheet.Cell(currentRow, 2).Value = item.PaymentStatus;
                        worksheet.Cell(currentRow, 3).Value = item.SupplierName;
                        worksheet.Cell(currentRow, 4).Value = item.SupplierNpwp;
                        worksheet.Cell(currentRow, 5).Value = item.ProjectName;
                        worksheet.Cell(currentRow, 6).Value = item.NumberSpk;
                        worksheet.Cell(currentRow, 7).Value = item.PiNumber;
                        worksheet.Cell(currentRow, 8).Value = item.FpDate;
                        worksheet.Cell(currentRow, 9).Value = item.FakturNumber;
                        worksheet.Cell(currentRow, 10).Value = item.TravelDocDate;
                        worksheet.Cell(currentRow, 11).Value = item.TravelDocNumber;
                        worksheet.Cell(currentRow, 12).Value = item.Masa;
                        worksheet.Cell(currentRow, 13).Value = item.Total;
                        worksheet.Cell(currentRow, 14).Value = item.Note;
                        worksheet.Cell(currentRow, 15).Value = item.PaidOfDate;
                        worksheet.Cell(currentRow, 16).Value = item.Dpp;
                        worksheet.Cell(currentRow, 17).Value = item.Ppn;
                        worksheet.Cell(currentRow, 18).Value = item.Pph23;
                        worksheet.Cell(currentRow, 19).Value = item.Discount;
                        worksheet.Cell(currentRow, 20).Value = item.ApprovalStatus;
                        worksheet.Cell(currentRow, 21).Value = item.UpdateAt;
                        worksheet.Cell(currentRow, 22).Value = item.CreatedAt;
                        worksheet.Cell(currentRow, 23).Value = item.CreatedBy;

                        if (j < dataPo.Count)
                        {
                            worksheet.Cell(currentRow, 24).Value = dataPo[j].PreOrderNumber;
                            worksheet.Cell(currentRow, 25).Value = dataPo[j].PreOrderDate;
                        }

                        if (j < dataInv.Count)
                        {
                            worksheet.Cell(currentRow, 26).Value = dataInv[j].InvNumber;
                            worksheet.Cell(currentRow, 27).Value = dataInv[j].InvDate;
                            worksheet.Cell(currentRow, 28).Value = dataInv[j].InvRecieptDate;
                        }

                        currentRow++;
                    }
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    _content = stream.ToArray();
                    FileName = $"Ppn_{DateTime.Now:ddMMyyyy}.xlsx";
                }
            }
        }
    }
}