﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnWithPagination
{
    public record GetPpnWithPaginationQuery : IRequest<PaginatedResult<GetPpnWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public GetPpnWithPaginationQuery()
        {
        }
        public GetPpnWithPaginationQuery(int pageNumber, int pageSize, string searchterm, int month, int year)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
            Month = month;
            Year = year;
        }
    }

    internal class GetCashAtBankWithPaginationQueryHandler : IRequestHandler<GetPpnWithPaginationQuery, PaginatedResult<GetPpnWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPpnRepository _ppnRepository;

        public GetCashAtBankWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IPpnRepository ppnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _ppnRepository = ppnRepository;
        }

        public async Task<PaginatedResult<GetPpnWithPaginationDto>> Handle(GetPpnWithPaginationQuery query, CancellationToken cancellationToken)
        {
            var data = await _ppnRepository.GetAll(query.SearchTerm, query.Month, query.Year);
            return await data.ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}