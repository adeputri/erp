﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnWithPagination
{
    public class GetPpnWithPaginationDto : IMapFrom<GetPpnWithPaginationDto>
    {
        [JsonPropertyName("unique_code")]
        public string UniqueCode { get; set; }

        [JsonPropertyName("file_path")]
        public string FilePath { get; set; }

        [JsonPropertyName("payment_status")]
        public string PaymentStatus { get; set; }

        [JsonPropertyName("supplier_id")]
        public Guid SupplierId { get; set; }

        [JsonPropertyName("supplier_name")]
        public string SupplierName { get; set; }

        [JsonPropertyName("npwp_supplier")]
        public string SupplierNpwp { get; set; }

        [JsonPropertyName("project_id")]
        public Guid ProjectId { get; set; }

        [JsonPropertyName("project_name")]
        public string ProjectName { get; set; }

        public string NumberSpk { get; set; }

        [JsonPropertyName("pi_number")]
        public string PiNumber { get; set; }

        [JsonPropertyName("fp_date")]
        public DateTime FpDate { get; set; }

        [JsonPropertyName("faktur_number")]
        public string FakturNumber { get; set; }

        [JsonPropertyName("travel_doc_date")]
        public DateTime TravelDocDate { get; set; }

        [JsonPropertyName("travel_doc_number")]
        public string TravelDocNumber { get; set; }

        [JsonPropertyName("masa")]
        public string Masa { get; set; }

        [JsonPropertyName("total")]
        public double Total { get; set; }

        [JsonPropertyName("note")]
        public string Note { get; set; }

        [JsonPropertyName("paid_of_date")]
        public DateTime PaidOfDate { get; set; }

        [JsonPropertyName("dpp")]
        public double Dpp { get; set; }

        [JsonPropertyName("ppn")]
        public double Ppn { get; set; }

        [JsonPropertyName("pph23")]
        public double Pph23 { get; set; }

        [JsonPropertyName("discount")]
        public double? Discount { get; set; }

        [JsonPropertyName("approval")]
        public string Approval { get; set; }

        [JsonPropertyName("approval_status")]
        public string ApprovalStatus { get; set; }

        [JsonPropertyName("update_at")]
        public DateTime UpdateAt { get; set; }

        [JsonPropertyName("created_at")]
        public string CreatedAt { get; set; }

        [JsonPropertyName("created_by")]
        public string CreatedBy { get; set; }

        [JsonPropertyName("data_po")]
        public List<DataPreOrder> DataPo { get; set; }

        [JsonPropertyName("data_inv")]
        public List<DataInvoice> DataInv { get; set; }
    }

    public class DataPreOrder
    {
        [JsonPropertyName("pre_order_number")]
        public string PreOrderNumber { get; set; }

        [JsonPropertyName("pre_order_date")]
        public string PreOrderDate { get; set; }
    }

    public class DataInvoice
    {
        [JsonPropertyName("inv_date")]
        public string InvDate { get; set; }

        [JsonPropertyName("inv_receipt_date")]
        public string InvRecieptDate { get; set; }

        [JsonPropertyName("inv_number")]
        public string InvNumber { get; set; }
    }
}