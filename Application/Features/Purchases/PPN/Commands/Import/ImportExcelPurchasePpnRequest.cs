﻿using MediatR;
using Microsoft.AspNetCore.Http;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.Import
{
    public record ImportExcelPurchasePpnRequest : IRequest<Result<CreatePpnResponseDto>>
    {
        public IFormFile File { get; set; }

        public ImportExcelPurchasePpnRequest(IFormFile formFile)
        {
            File = formFile;
        }
    }

    public class PpnImportDto
    {
        public string UniqueCode { get; set; }
        public string PaymentStatus { get; set; }
        public Guid SupplierId { get; set; }
        public Guid ProjectId { get; set; }
        public string PiNumber { get; set; }
        public DateTime? FpDate { get; set; }
        public string FakturNumber { get; set; }
        public DateTime? TravelDocDate { get; set; }
        public string TravelDocNumber { get; set; }
        public string Masa { get; set; }
        public double Dpp { get; set; }
        public double Ppn { get; set; }
        public string? Note { get; set; }
        public DateTime? PaidOfDate { get; set; }
        public double Pph23 { get; set; }
        public double? Discount { get; set; }
        public string ApprovalStatus { get; set; }
        public string CreatedBy { get; set; }
        public List<DataInv> DataInv { get; set; }
        public List<DataPo> DataPo { get; set; }
    }
}