﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.Import
{
    internal class ImportPurchasePpnCommandHandler : IRequestHandler<ImportExcelPurchasePpnRequest, Result<CreatePpnResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPpnRepository _ppnRepository;
        public ImportPurchasePpnCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IPpnRepository pnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _ppnRepository = pnRepository;
        }
            
        public ImportPurchasePpnCommandHandler()
        {
        }

        public async Task<Result<CreatePpnResponseDto>> Handle(ImportExcelPurchasePpnRequest request, CancellationToken cancellationToken)
        {
            var import = await _ppnRepository.UploadExcel(request.File);
            foreach (var data in import)
            {
                var ppnCreate = _mapper.Map<PurchasePpn>(data);
                var ppnResponse = _mapper.Map<CreatePpnResponseDto>(ppnCreate);

                foreach (var item in data.DataPo)
                {
                    ppnCreate.PreOrderDate = item.PreOrderDate ?? null; 
                    ppnCreate.PreOrderNumber = item.PreOrderNumber ?? null; 

                    foreach (var entity in data.DataInv)
                    {
                        ppnCreate.Id = Guid.NewGuid();
                        ppnCreate.InvDate = entity.InvDate ?? null;
                        ppnCreate.InvNumber = entity.InvNumber ?? null;
                        ppnCreate.InvReceiptDate = entity.InvRecieptDate ?? null; 

                        await _unitOfWork.Repository<PurchasePpn>().AddAsync(ppnCreate);
                        ppnCreate.AddDomainEvent(new PpnCreatedEvent(ppnCreate));
                        await _unitOfWork.Save(cancellationToken);
                    }
                }
            }
            return await Result<CreatePpnResponseDto>.SuccessAsync("success");
        }
    }
}