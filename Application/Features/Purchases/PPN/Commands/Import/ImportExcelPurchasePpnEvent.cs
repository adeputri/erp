﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.Import
{
    public class ImportExcelPurchasePpnEvent : BaseEvent
    {
        public PurchasePpn PurchasePpn { get; set; }

        public ImportExcelPurchasePpnEvent(PurchasePpn purchasePpn)
        {
            PurchasePpn = purchasePpn;
        }
    }
}