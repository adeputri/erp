﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.ConfirmApprovePpn
{
    internal class ConfirmApproveCashAtBankCommanHandler : IRequestHandler<ConfirmApproveCashAtBankRequest, Result<PurchasePpn>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ConfirmApproveCashAtBankCommanHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<PurchasePpn>> Handle(ConfirmApproveCashAtBankRequest request, CancellationToken cancellationToken)
        {
            var checkCashAtBank = await _unitOfWork.Repository<PurchasePpn>().FindByCondition(o => o.UpdatedAt.Value.Month == request.Month && o.UpdatedAt.Value.Year == request.Year).ToListAsync();
            if (checkCashAtBank.Count() != 0)
            {
                foreach (var data in checkCashAtBank)
                {
                    data.ApprovalBy = request.ApprovalBy;
                    data.ApprovalStatus = "Approved";
                    await _unitOfWork.Repository<PurchasePpn>().UpdateAsync(data);
                    data.AddDomainEvent(new ApproveCashAtBankConfirmEvent(data));
                    await _unitOfWork.Save(cancellationToken);
                }
                return await Result<PurchasePpn>.SuccessAsync("Success");
            }
            return await Result<PurchasePpn>.FailureAsync("Not found");
        }
    }
}