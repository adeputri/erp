﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.ConfirmApprovePpn
{
    public class ApproveCashAtBankConfirmEvent : BaseEvent
    {
        public PurchasePpn CashAtBank { get; set; }

        public ApproveCashAtBankConfirmEvent(PurchasePpn cashAtBank)
        {
            CashAtBank = cashAtBank;
        }
    }
}