﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.ConfirmApprovePpn
{
    public class ConfirmApproveCashAtBankRequest : IRequest<Result<PurchasePpn>>
    {
        [JsonPropertyName("month")]
        public int Month { get; set; }

        [JsonPropertyName("year")]
        public int Year { get; set; }

        [JsonPropertyName("approval_by")]
        public string ApprovalBy { get; set; }
    }
}