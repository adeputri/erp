﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn
{
    public class PpnCreatedEvent : BaseEvent
    {
        public PurchasePpn PurchasePpn { get; set; }

        public PpnCreatedEvent(PurchasePpn purchasePpn)
        {
            PurchasePpn = purchasePpn;
        }
    }
}