﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn
{
    internal class CreatePpnCommandHandler : IRequestHandler<CreatePpnRequest, Result<CreatePpnResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPpnRepository _ppnRepository;

        public CreatePpnCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IPpnRepository ppnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _ppnRepository = ppnRepository;
        }

        public async Task<Result<CreatePpnResponseDto>> Handle(CreatePpnRequest request, CancellationToken cancellationToken)
        {
            var ppnCreate = _mapper.Map<PurchasePpn>(request);
            var ppnResponse = _mapper.Map<CreatePpnResponseDto>(ppnCreate);

            if (request.File != null)
            {
                var checkFile = await _ppnRepository.UploadFile(request.File);
                ppnCreate.FilePath = checkFile.Item2.ToString();
                if (checkFile.Item1 == false)
                {
                    return await Result<CreatePpnResponseDto>.FailureAsync(ppnResponse, "Check your file");
                }
            }
            ppnCreate.UniqueCode = DateTime.UtcNow.ToString("yyMMddhhmmss");
            ppnCreate.ApprovalStatus = "Waiting";

            foreach (var item in request.DataPo)
            {
                ppnCreate.PreOrderDate = item.PreOrderDate.Value;
                ppnCreate.PreOrderNumber = item.PreOrderNumber;
                foreach (var data in request.DataInv)
                {
                    ppnCreate.Id = Guid.NewGuid();
                    ppnCreate.InvDate = data.InvDate.Value;
                    ppnCreate.InvNumber = data.InvNumber;
                    ppnCreate.InvReceiptDate = data.InvRecieptDate.Value;
                    await _unitOfWork.Repository<PurchasePpn>().AddAsync(ppnCreate);
                    ppnCreate.AddDomainEvent(new PpnCreatedEvent(ppnCreate));
                    await _unitOfWork.Save(cancellationToken);
                }
            }
            return await Result<CreatePpnResponseDto>.SuccessAsync(ppnResponse, "Created.");
        }
    }
}