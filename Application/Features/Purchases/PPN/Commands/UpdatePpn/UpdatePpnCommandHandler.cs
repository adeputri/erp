﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.DeletePpn;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.UpdatePpn
{
    internal class UpdatePpnCommandHandler : IRequestHandler<UpdatePpnRequest, Result<PurchasePpn>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPpnRepository _cashAtBankRepository;

        public UpdatePpnCommandHandler(IMapper mapper, IUnitOfWork unitOfWork, IPpnRepository cashAtBankRepository)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _cashAtBankRepository = cashAtBankRepository;
        }

        public async Task<Result<PurchasePpn>> Handle(UpdatePpnRequest request, CancellationToken cancellationToken)
        {
            var cashAtBankCreate = _mapper.Map<PurchasePpn>(request);
            var bankResponse = _mapper.Map<CreatePpnResponseDto>(cashAtBankCreate);

            var validateCashAtBank = await _unitOfWork.Repository<PurchasePpn>().FindByCondition(p => p.UniqueCode == request.UniqueCode).ToListAsync();
            if (validateCashAtBank == null)
            {
                return await Result<PurchasePpn>.FailureAsync("Id not found");
            }

            foreach (var data in validateCashAtBank)
            {
                await _unitOfWork.Repository<PurchasePpn>().DeleteAsync(data);
                data.AddDomainEvent(new PpnDeletedEvent(data));
                await _unitOfWork.Save(cancellationToken);
            }
            if (request.File != null)
            {
                var upload = await _cashAtBankRepository.UploadFile(request.File);
                cashAtBankCreate.FilePath = request.FileName +","+ upload.Item2;
            }
            else
            {
               cashAtBankCreate.FilePath = request.FileName;
            }
            cashAtBankCreate.UniqueCode = DateTime.UtcNow.ToString("yyMMddhhmmss");
            cashAtBankCreate.ApprovalStatus = "Waiting";

            foreach (var item in request.DataPo)
            {
                cashAtBankCreate.PreOrderDate = item.PreOrderDate.Value;
                cashAtBankCreate.PreOrderNumber = item.PreOrderNumber;
                foreach (var dt in request.DataInv)
                {
                    cashAtBankCreate.Id = Guid.NewGuid();
                    cashAtBankCreate.InvDate = dt.InvDate.Value;
                    cashAtBankCreate.InvReceiptDate = dt.InvRecieptDate.Value;
                    cashAtBankCreate.InvNumber = dt.InvNumber;
                    cashAtBankCreate.CreatedAt = DateTime.UtcNow;
                    cashAtBankCreate.UpdatedAt = DateTime.UtcNow;
                    await _unitOfWork.Repository<PurchasePpn>().AddAsync(cashAtBankCreate);
                    cashAtBankCreate.AddDomainEvent(new PpnCreatedEvent(cashAtBankCreate));
                    await _unitOfWork.Save(cancellationToken);
                }
            }
            return await Result<PurchasePpn>.SuccessAsync(cashAtBankCreate, "Success");
        }
    }
}