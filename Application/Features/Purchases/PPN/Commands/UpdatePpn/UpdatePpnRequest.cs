﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.UpdatePpn
{
    public class UpdatePpnRequest : IRequest<Result<PurchasePpn>>
    {
        public Guid Id { get; set; }

        [BindProperty(Name = "unique_code")]
        public string UniqueCode { get; set; }

        [BindProperty(Name = "file")]
        public FileDto File { get; set; }
        [BindProperty(Name = "file_name")]
        public string FileName { get; set; }
        [BindProperty(Name = "payment_status")]
        public string PaymentStatus { get; set; }

        [BindProperty(Name = "supplier_id")]
        public Guid SupplierId { get; set; }

        [BindProperty(Name = "project_id")]
        public Guid ProjectId { get; set; }

        [BindProperty(Name = "pi_number")]
        public string PiNumber { get; set; }

        [BindProperty(Name = "fp_date")]
        public DateTime FpDate { get; set; }

        [BindProperty(Name = "faktur_number")]
        public string FakturNumber { get; set; }

        [BindProperty(Name = "travel_doc_date")]
        public DateTime TravelDocDate { get; set; }

        [BindProperty(Name = "travel_doc_number")]
        public string TravelDocNumber { get; set; }

        [BindProperty(Name = "masa")]
        public string Masa { get; set; }

        [BindProperty(Name = "dpp")]
        public double Dpp { get; set; }

        [BindProperty(Name = "ppn")]
        public double Ppn { get; set; }

        [BindProperty(Name = "note")]
        public string Note { get; set; }

        [BindProperty(Name = "paid_of_date")]
        public DateTime PaidOfDate { get; set; }

        [BindProperty(Name = "pph23")]
        public double Pph23 { get; set; }

        [BindProperty(Name = "discount")]
        public double Discount { get; set; }

        [BindProperty(Name = "created_by")]
        public string CreatedBy { get; set; }

        [BindProperty(Name = "data_invoice")]
        public string DataInvoice { get; set; }

        [BindProperty(Name = "data_pre_order")]
        public string DataPreOrder { get; set; }

        [NotMapped]
        public List<DataInv> DataInv { get; set; }

        [NotMapped]
        public List<DataPo> DataPo { get; set; }
    }
}