﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.UpdatePpn
{
    public class PpnUpdateEvent : BaseEvent
    {
        public PurchasePpn PurchasePpn { get; set; }

        public PpnUpdateEvent(PurchasePpn purchasePpn)
        {
            PurchasePpn = purchasePpn;
        }
    }
}