﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.DeletePpn
{
    public class PpnDeletedEvent : BaseEvent
    {
        public PurchasePpn PurchasePpn { get; set; }

        public PpnDeletedEvent(PurchasePpn purchasePpn)
        {
            PurchasePpn = purchasePpn;
        }
    }
}