﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.DeletePpn
{
    public class DeletePpnRequest : IRequest<Result<string>>, IMapFrom<PurchasePpn>
    {
        public string UniqueCode { get; set; }

        public DeletePpnRequest()
        { }

        public DeletePpnRequest(string uniqueCode)
        {
            UniqueCode = uniqueCode;
        }
    }
}