﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.PPN.Commands.DeletePpn
{
    internal class DeletePpnCommandHandler : IRequestHandler<DeletePpnRequest, Result<string>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeletePpnCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<string>> Handle(DeletePpnRequest request, CancellationToken cancellationToken)
        {
            var teamDelete = await _unitOfWork.Repository<PurchasePpn>().FindByCondition(p => p.UniqueCode == request.UniqueCode).ToListAsync();
            foreach (var team in teamDelete)
            {
                if (team != null)
                {
                    team.DeletedAt = DateTime.UtcNow;

                    await _unitOfWork.Repository<PurchasePpn>().UpdateAsync(team);
                    team.AddDomainEvent(new PpnDeletedEvent(team));
                    await _unitOfWork.Save(cancellationToken);

                }
            }
            return await Result<string>.SuccessAsync("Deleted.");
        }
    }
}