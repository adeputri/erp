﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetPreviewFileNonPpn
{
    public record GetPreviewFileNonPpnQuery : IRequest<Result<GetPreviewFileNonPpnDto>>
    {
        public string Path { get; set; }
        public GetPreviewFileNonPpnQuery(string path)
        {
            Path = path;
        }
    }

    internal class GetPreviewFileNonPpnQueryHandler : IRequestHandler<GetPreviewFileNonPpnQuery, Result<GetPreviewFileNonPpnDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly INonPpnRepository _nonPpnRepository;

        public GetPreviewFileNonPpnQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, INonPpnRepository nonPpnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _nonPpnRepository = nonPpnRepository;
        }

        public async Task<Result<GetPreviewFileNonPpnDto>> Handle(GetPreviewFileNonPpnQuery query, CancellationToken cancellationToken)
        {
            var file = await _nonPpnRepository.GetFile(query.Path);
            return await Result<GetPreviewFileNonPpnDto>.SuccessAsync(file, "Successfully fetch data");
        }
    }
}