﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetPreviewFileNonPpn
{
    public class GetPreviewFileNonPpnDto
    {
        [JsonPropertyName("mime_type")]
        public string? MimeType { get; set; }

        [JsonPropertyName("path")]
        public string? Path { get; set; }

        public byte[] Content { get; set; }
    }
}