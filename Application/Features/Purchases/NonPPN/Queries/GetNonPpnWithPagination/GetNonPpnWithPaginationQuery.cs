﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnWithPagination
{
    public record GetNonPpnWithPaginationQuery : IRequest<PaginatedResult<GetNonPpnWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public Guid CategoryId { get; set; }
        public GetNonPpnWithPaginationQuery()
        {
        }
        public GetNonPpnWithPaginationQuery(int pageNumber, int pageSize, string searchterm, int month, int year, Guid categoryId)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
            Month = month;
            Year = year;
            CategoryId = categoryId;
        }
    }

    internal class GetNonPpnWithPaginationQueryHandler : IRequestHandler<GetNonPpnWithPaginationQuery, PaginatedResult<GetNonPpnWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly INonPpnRepository _nonPpnRepository;

        public GetNonPpnWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, INonPpnRepository nonPpnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _nonPpnRepository = nonPpnRepository;
        }

        public async Task<PaginatedResult<GetNonPpnWithPaginationDto>> Handle(GetNonPpnWithPaginationQuery query, CancellationToken cancellationToken)
        {
            var data = await _nonPpnRepository.GetNonPpnWithPagination(query.SearchTerm, query.Month, query.Year, query.CategoryId);
            return await data.ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}