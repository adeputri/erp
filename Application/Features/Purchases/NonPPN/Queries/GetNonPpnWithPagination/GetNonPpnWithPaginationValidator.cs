﻿using FluentValidation;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnWithPagination
{
    public class GetNonPpnWithPaginationValidator : AbstractValidator<GetNonPpnWithPaginationQuery>
    {
        public GetNonPpnWithPaginationValidator()
        {
            RuleFor(x => x.PageNumber)
               .GreaterThanOrEqualTo(1)
               .WithMessage("PageNumber at least greater than or equal to 1.");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageSize at least greater than or equal to 1.");
        }
    }
}