﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnById
{
    public class GetNonPpnByIdDto
    {
        public Guid Id { get; set; }

        [JsonPropertyName("bon_date")]
        public string BonDate { get; set; }

        [JsonPropertyName("bon_number")]
        public string? BonNumber { get; set; }

        [JsonPropertyName("code_id")]
        public Guid? CodeId { get; set; }

        [JsonPropertyName("code_name")]
        public string? CodeName { get; set; }

        [JsonPropertyName("item")]
        public string? Item { get; set; }

        [JsonPropertyName("transaction")]
        public string? Transaction { get; set; }

        [JsonPropertyName("debit")]
        public double? Debit { get; set; }

        [JsonPropertyName("kredit")]
        public double? Kredit { get; set; }

        [JsonPropertyName("saldo")]
        public double? Saldo { get; set; }

        [JsonPropertyName("payment_method")]
        public string? PaymentMethod { get; set; }

        [JsonPropertyName("note")]
        public string? Note { get; set; }

        [JsonPropertyName("project_id")]
        public Guid? MasterProjectId { get; set; }

        [JsonPropertyName("project_name")]
        public string? MasterProjectName { get; set; }

        [JsonPropertyName("category_id")]
        public Guid? CategoryId { get; set; }

        [JsonPropertyName("category_name")]
        public string? CategoryName { get; set; }

        [JsonPropertyName("approval_status")]
        public string? ApprovalStatus { get; set; }

        public string Type { get; set; }

        [JsonPropertyName("file_path")]
        public string[] FilePath { get; set; }
    }
}