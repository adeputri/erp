﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnById
{
    public record GetNonPpnByIdQuery : IRequest<Result<GetNonPpnByIdDto>>
    {
        public Guid Id { get; set; }
        public GetNonPpnByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    internal class GetNonPpnByIdQueryHandler : IRequestHandler<GetNonPpnByIdQuery, Result<GetNonPpnByIdDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly INonPpnRepository _nonPpnRepository;

        public GetNonPpnByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, INonPpnRepository nonPpnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _nonPpnRepository = nonPpnRepository;
        }

        public async Task<Result<GetNonPpnByIdDto>> Handle(GetNonPpnByIdQuery query, CancellationToken cancellationToken)
        {
            var dt = await _nonPpnRepository.GetNonPpnById(query.Id);
            if (dt == null)
            {
                return await Result<GetNonPpnByIdDto>.FailureAsync("Id not found");
            }
            return await Result<GetNonPpnByIdDto>.SuccessAsync(dt, "Successfully fetch data");
        }
    }
}