﻿using ClosedXML.Excel;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnWithPagination;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Queries.Download
{
    public class DownloadNonPpnToExcel
    {
        private readonly PaginatedResult<GetNonPpnWithPaginationDto> data;

        public DownloadNonPpnToExcel(PaginatedResult<GetNonPpnWithPaginationDto> pg)
        {
            data = pg;
        }

        public void GetListExcel(ref byte[] _content, ref string FileName)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Sheet1");

                worksheet.Cell(1, 1).Value = "Category";
                worksheet.Cell(1, 2).Value = "Bon Date";
                worksheet.Cell(1, 3).Value = "Bon Number";
                worksheet.Cell(1, 4).Value = "Code";
                worksheet.Cell(1, 5).Value = "Item";
                worksheet.Cell(1, 6).Value = "Transaction";
                worksheet.Cell(1, 7).Value = "Debit";
                worksheet.Cell(1, 8).Value = "Kredit";
                worksheet.Cell(1, 9).Value = "Saldo";
                worksheet.Cell(1, 10).Value = "Payment Method";
                worksheet.Cell(1, 11).Value = "Note";
                worksheet.Cell(1, 12).Value = "Project";
                worksheet.Cell(1, 13).Value = "Status";

                for (int i = 0; i < data.Data.Count(); i++)
                {
                    worksheet.Cell(i + 2, 1).Value = data.Data.ElementAt(i).CategoryName;
                    worksheet.Cell(i + 2, 2).Value = data.Data.ElementAt(i).BonDate;
                    worksheet.Cell(i + 2, 3).Value = data.Data.ElementAt(i).BonNumber;
                    worksheet.Cell(i + 2, 4).Value = data.Data.ElementAt(i).CodeName;
                    worksheet.Cell(i + 2, 5).Value = data.Data.ElementAt(i).Item;
                    worksheet.Cell(i + 2, 6).Value = data.Data.ElementAt(i).Transaction;
                    worksheet.Cell(i + 2, 7).Value = data.Data.ElementAt(i).Debit;
                    worksheet.Cell(i + 2, 8).Value = data.Data.ElementAt(i).Kredit;
                    worksheet.Cell(i + 2, 9).Value = data.Data.ElementAt(i).Saldo;
                    worksheet.Cell(i + 2, 10).Value = data.Data.ElementAt(i).PaymentMethod;
                    worksheet.Cell(i + 2, 11).Value = data.Data.ElementAt(i).Note;
                    worksheet.Cell(i + 2, 12).Value = data.Data.ElementAt(i).MasterProjectName;
                    worksheet.Cell(i + 2, 13).Value = data.Data.ElementAt(i).ApprovalStatus;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    _content = stream.ToArray();
                    FileName = $"Non_Ppn_{DateTime.Now.ToString("ddMMyyyy")}.xlsx";
                }
            }
        }
    }
}