﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnInformation
{
    public class GetNonPpnInformationDto
    {
        [JsonPropertyName("saldo_awal")]
        public double? SaldoAwal { get; set; }

        [JsonPropertyName("debit")]
        public double? Debit { get; set; }

        [JsonPropertyName("kredit")]
        public double? Kredit { get; set; }

        [JsonPropertyName("saldo_akhir")]
        public double? SaldoAkhir { get; set; }
    }
}