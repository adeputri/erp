﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnInformation
{
    public record GetNonPpnInformationQuery : IRequest<Result<GetNonPpnInformationDto>>
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public Guid CategoryId { get; set; }
        public GetNonPpnInformationQuery(int month, int year, Guid categoryId)
        {
            Month = month;
            Year = year;
            CategoryId = categoryId;
        }
    }

    internal class GetNonPpnInformationQueryHandler : IRequestHandler<GetNonPpnInformationQuery, Result<GetNonPpnInformationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly INonPpnRepository _nonPpnRepository;

        public GetNonPpnInformationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, INonPpnRepository nonPppnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _nonPpnRepository = nonPppnRepository;
        }

        public async Task<Result<GetNonPpnInformationDto>> Handle(GetNonPpnInformationQuery query, CancellationToken cancellationToken)
        {
            var dt = await _nonPpnRepository.GetNonPpnInformation(query.Month, query.Year, query.CategoryId);
            return await Result<GetNonPpnInformationDto>.SuccessAsync(dt, "Successfully fetch data");
        }
    }
}