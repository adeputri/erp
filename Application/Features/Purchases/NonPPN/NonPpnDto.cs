﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN
{
    public record NonPpnDto
    {
        [JsonPropertyName("project_id")]
        public Guid? ProjectId { get; set; }

        [JsonPropertyName("code_id")]
        public Guid? CodeId { get; set; }

        [JsonPropertyName("category_id")]
        public Guid? CategoryId { get; set; }

        [JsonPropertyName("bon_date")]
        public DateTime? BonDate { get; set; }

        [JsonPropertyName("bon_number")]
        public string? BonNumber { get; set; }

        [JsonPropertyName("transaction")]
        public string? Transaction { get; set; }

        [JsonPropertyName("debit")]
        public double? Debit { get; set; }

        [JsonPropertyName("kredit")]
        public double? Kredit { get; set; }

        [JsonPropertyName("payment_method")]
        public string? PaymentMethod { get; set; }

        [JsonPropertyName("note")]
        public string? Note { get; set; }

        [JsonPropertyName("file_path")]
        public string? FilePath { get; set; }
    }
    public sealed record CreateNonPpnResponseDto : NonPpnDto { }
}