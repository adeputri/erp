﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN.Commands.CreateReportNonPPN;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import
{
    internal class ImportExcelNonPpnCommandHandler : IRequestHandler<ImportExcelNonPpnRequest, Result<CreateNonPpnResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly INonPpnRepository _nonPpnRepository;

        public ImportExcelNonPpnCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, INonPpnRepository ppnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _nonPpnRepository = ppnRepository;
        }

        public async Task<Result<CreateNonPpnResponseDto>> Handle(ImportExcelNonPpnRequest request, CancellationToken cancellationToken)
        {
            var dataImport = await _nonPpnRepository.UploadExcel(request.File);
            foreach (var item in dataImport)
            {
                var nonPpnCreate = _mapper.Map<PurchaseNonPpn>(item);
                var nonPpnResponse = _mapper.Map<CreateNonPpnResponseDto>(item);

                nonPpnCreate.ApprovalStatus = "Waiting";
                await _unitOfWork.Repository<PurchaseNonPpn>().AddAsync(nonPpnCreate);
                nonPpnCreate.AddDomainEvent(new NonPpnCreatedEvent(nonPpnCreate));
            }
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateNonPpnResponseDto>.SuccessAsync("Created.");
        }
    }
}