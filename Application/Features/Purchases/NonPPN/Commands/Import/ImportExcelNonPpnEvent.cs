﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import
{
    public class ImportExcelNonPpnEvent : BaseEvent
    {
        public PurchaseNonPpn PurchaseNonPpn { get; set; }

        public ImportExcelNonPpnEvent(PurchaseNonPpn purchaseNonPpn)
        {
            PurchaseNonPpn = purchaseNonPpn;
        }
    }
}