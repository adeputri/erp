﻿using MediatR;
using Microsoft.AspNetCore.Http;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import
{
    public class ImportExcelNonPpnRequest : IRequest<Result<CreateNonPpnResponseDto>>
    {
        public IFormFile File { get; set; }

        public ImportExcelNonPpnRequest(IFormFile file)
        {
            File = file;
        }
    }

    public class NonPpnImportDto
    {
        public Guid? MasterProjectId { get; set; }
        public Guid? CodeId { get; set; }
        public Guid? CategoryId { get; set; }
        public DateTime? BonDate { get; set; }
        public string? BonNumber { get; set; }
        public string? Transaction { get; set; }
        public double? Debit { get; set; }
        public double? Kredit { get; set; }
        public string? PaymentMethod { get; set; }
        public string? Note { get; set; }
        public string CreatedBy { get; set; }
        public IFormFile[] FilePath { get; set; }
        public string? Type { get; set; }
    }
}