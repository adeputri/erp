﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Commands.DeleteNonPpn
{
    internal class DeleteNonPpnCommandHandler : IRequestHandler<DeleteNonPpnRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteNonPpnCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteNonPpnRequest request, CancellationToken cancellationToken)
        {
            var bankDelete = await _unitOfWork.Repository<PurchaseNonPpn>().GetByIdAsync(request.Id);
            if (bankDelete != null)
            {
                bankDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<PurchaseNonPpn>().UpdateAsync(bankDelete);
                bankDelete.AddDomainEvent(new NonPpnDeletedEvent(bankDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(bankDelete.Id, "Non ppn deleted.");
            }
            return await Result<Guid>.FailureAsync("Id non ppn not found");
        }
    }
}