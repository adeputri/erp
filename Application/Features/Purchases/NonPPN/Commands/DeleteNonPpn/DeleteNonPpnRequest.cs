﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Commands.DeleteNonPpn
{
    public class DeleteNonPpnRequest : IRequest<Result<Guid>>, IMapFrom<PurchaseNonPpn>
    {
        public Guid Id { get; set; }

        public DeleteNonPpnRequest(Guid id)
        {
            Id = id;
        }

        public DeleteNonPpnRequest()
        {
        }
    }
}