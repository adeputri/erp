﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Commands.DeleteNonPpn
{
    public class NonPpnDeletedEvent : BaseEvent
    {
        public PurchaseNonPpn PurchaseNonPpn { get; set; }

        public NonPpnDeletedEvent(PurchaseNonPpn purchaseNonPpn)
        {
            PurchaseNonPpn = purchaseNonPpn;
        }
    }
}