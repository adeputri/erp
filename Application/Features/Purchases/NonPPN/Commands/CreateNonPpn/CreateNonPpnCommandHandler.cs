﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN.Commands.CreateReportNonPPN
{
    internal class CreateNonPpnCommandHandler : IRequestHandler<CreateNonPpnRequest, Result<CreateNonPpnResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly INonPpnRepository _nonPpnRepository;

        public CreateNonPpnCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, INonPpnRepository ppnRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _nonPpnRepository = ppnRepository;
        }

        public async Task<Result<CreateNonPpnResponseDto>> Handle(CreateNonPpnRequest request, CancellationToken cancellationToken)
        {
            var nonPpnCreate = _mapper.Map<PurchaseNonPpn>(request);
            var nonPpnResponse = _mapper.Map<CreateNonPpnResponseDto>(nonPpnCreate);
            if (request.File != null)
            {
                var checkFile = await _nonPpnRepository.UploadFile(request.File);
                nonPpnCreate.FilePath = checkFile.Item2.ToString();
                if (checkFile.Item1 == false)
                {
                    return await Result<CreateNonPpnResponseDto>.FailureAsync("Check your file");
                }
            }
            nonPpnCreate.ApprovalStatus = "Waiting";
            await _unitOfWork.Repository<PurchaseNonPpn>().AddAsync(nonPpnCreate);
            nonPpnCreate.AddDomainEvent(new NonPpnCreatedEvent(nonPpnCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateNonPpnResponseDto>.SuccessAsync(nonPpnResponse, "Created.");
        }
    }
}