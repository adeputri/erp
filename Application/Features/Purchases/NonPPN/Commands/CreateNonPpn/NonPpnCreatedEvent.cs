﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN.Commands.CreateReportNonPPN
{
    public class NonPpnCreatedEvent : BaseEvent
    {
        public PurchaseNonPpn PurchaseNonPpn { get; set; }

        public NonPpnCreatedEvent(PurchaseNonPpn purchaseNonPpn)
        {
            PurchaseNonPpn = purchaseNonPpn;
        }
    }
}