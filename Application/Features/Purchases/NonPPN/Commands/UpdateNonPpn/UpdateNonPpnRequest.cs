﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Commands.UpdateNonPpn
{
    public class UpdateNonPpnRequest : IRequest<Result<PurchaseNonPpn>>
    {
        [BindProperty(Name = "id")]
        public Guid Id { get; set; }

        [BindProperty(Name = "master_project_id")]
        public Guid? MasterProjectId { get; set; }

        [BindProperty(Name = "code_id")]
        public Guid? CodeId { get; set; }

        [BindProperty(Name = "category_id")]
        public Guid? CategoryId { get; set; }

        [BindProperty(Name = "bon_date")]
        public DateTime? BonDate { get; set; }

        [BindProperty(Name = "bon_number")]
        public string? BonNumber { get; set; }

        [BindProperty(Name = "transaction")]
        public string? Transaction { get; set; }

        [BindProperty(Name = "debit")]
        public double? Debit { get; set; }

        [BindProperty(Name = "kredit")]
        public double? Kredit { get; set; }

        [BindProperty(Name = "payment_method")]
        public string? PaymentMethod { get; set; }

        [BindProperty(Name = "note")]
        public string? Note { get; set; }

        [BindProperty(Name = "created_by")]
        public string CreatedBy { get; set; }

        [BindProperty(Name = "file")]
        public FileDto File { get; set; }
    }
}