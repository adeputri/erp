﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Commands.UpdateNonPpn
{
    public class NonPpnUpdateEvent : BaseEvent
    {
        public PurchaseNonPpn PurchaseNonPpn { get; set; }

        public NonPpnUpdateEvent(PurchaseNonPpn purchaseNonPpn)
        {
            PurchaseNonPpn = purchaseNonPpn;
        }
    }
}