﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Purchases.NonPPN.Commands.UpdateNonPpn
{
    internal class UpdateNonPpnCommandHandler : IRequestHandler<UpdateNonPpnRequest, Result<PurchaseNonPpn>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly INonPpnRepository _nonPpnRepository;

        public UpdateNonPpnCommandHandler(IMapper mapper, IUnitOfWork unitOfWork, INonPpnRepository nonPpnRepository)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _nonPpnRepository = nonPpnRepository;
        }

        public async Task<Result<PurchaseNonPpn>> Handle(UpdateNonPpnRequest request, CancellationToken cancellationToken)
        {
            var nonPpnValidate = await _unitOfWork.Repository<PurchaseNonPpn>().GetByIdAsync(request.Id);
            if (nonPpnValidate == null)
            {
                return await Result<PurchaseNonPpn>.FailureAsync("Id not found");
            }
            if (request.File != null)
            {
                var checkFile = await _nonPpnRepository.UploadFile(request.File);
                nonPpnValidate.FilePath = checkFile.Item2.ToString();
                if (checkFile.Item1 == false)
                {
                    return await Result<PurchaseNonPpn>.FailureAsync("Check your file");
                }
            }
            nonPpnValidate.MasterProjectId = request.MasterProjectId;
            nonPpnValidate.CodeId = request.CodeId;
            nonPpnValidate.CategoryId = request.CategoryId;
            nonPpnValidate.BonDate = request.BonDate;
            nonPpnValidate.BonNumber = request.BonNumber;
            nonPpnValidate.Transaction = request.Transaction;
            nonPpnValidate.Debit = request.Debit;
            nonPpnValidate.Kredit = request.Kredit;
            nonPpnValidate.PaymentMethod = request.PaymentMethod;
            nonPpnValidate.Note = request.Note;
            nonPpnValidate.CreatedBy = request.CreatedBy;
            nonPpnValidate.ApprovalStatus = "Waiting";

            await _unitOfWork.Repository<PurchaseNonPpn>().UpdateAsync(nonPpnValidate);
            nonPpnValidate.AddDomainEvent(new NonPpnUpdateEvent(nonPpnValidate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<PurchaseNonPpn>.SuccessAsync("Success");
        }
    }
}