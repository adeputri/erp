﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Teams.Commands.CreateTeam
{
    public class TeamCreatedEvent : BaseEvent
    {
        public Team Team { get; set; }

        public TeamCreatedEvent(Team team)
        {
            Team = team;
        }
    }
}