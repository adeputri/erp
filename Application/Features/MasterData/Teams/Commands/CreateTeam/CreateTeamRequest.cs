﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Teams.Commands.CreateTeam
{
    public class CreateTeamRequest : IRequest<Result<CreateTeamResponseDto>>
    {
        [JsonPropertyName("team_name")]
        public string TeamName { get; set; }
    }
}