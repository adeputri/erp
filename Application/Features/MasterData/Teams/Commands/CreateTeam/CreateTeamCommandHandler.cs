﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Teams.Commands.CreateTeam
{
    internal class CreateTeamCommandHandler : IRequestHandler<CreateTeamRequest, Result<CreateTeamResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ITeamRepository _teamRepository;

        public CreateTeamCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, ITeamRepository teamRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _teamRepository = teamRepository;
        }

        public async Task<Result<CreateTeamResponseDto>> Handle(CreateTeamRequest request, CancellationToken cancellationToken)
        {
            var positionCreate = _mapper.Map<Team>(request);
            var positionResponse = _mapper.Map<CreateTeamResponseDto>(positionCreate);

            var validateData = await _teamRepository.ValidateData(positionCreate);

            if (validateData != true)
            {
                return await Result<CreateTeamResponseDto>.FailureAsync(positionResponse, "Data already exist");
            }

            positionCreate.CreatedAt = DateTime.UtcNow;
            positionCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<Team>().AddAsync(positionCreate);
            positionCreate.AddDomainEvent(new TeamCreatedEvent(positionCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateTeamResponseDto>.SuccessAsync(positionResponse, "Team created.");
        }
    }
}