﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Teams.Commands.DeleteTeam
{
    public class TeamDeletedEvent : BaseEvent
    {
        public Team Team { get; set; }

        public TeamDeletedEvent(Team team)
        {
            Team = team;
        }
    }
}