﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Teams.Commands.DeleteTeam
{
    public class DeleteTeamRequest : IRequest<Result<Guid>>, IMapFrom<Team>
    {
        public Guid Id { get; set; }

        public DeleteTeamRequest(Guid id)
        {
            Id = id;
        }

        public DeleteTeamRequest()
        {
        }
    }
}