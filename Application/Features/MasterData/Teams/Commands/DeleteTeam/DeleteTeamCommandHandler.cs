﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Teams.Commands.DeleteTeam
{
    internal class DeleteTeamCommandHandler : IRequestHandler<DeleteTeamRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteTeamCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteTeamRequest request, CancellationToken cancellationToken)
        {
            var teamDelete = await _unitOfWork.Repository<Team>().GetByIdAsync(request.Id);
            if (teamDelete != null)
            {
                teamDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<Team>().UpdateAsync(teamDelete);
                teamDelete.AddDomainEvent(new TeamDeletedEvent(teamDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(teamDelete.Id, "Team deleted.");
            }
            return await Result<Guid>.FailureAsync("Team not found");
        }
    }
}