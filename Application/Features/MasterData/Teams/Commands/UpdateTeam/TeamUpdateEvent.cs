﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Teams.Commands.UpdateTeam
{
    public class TeamUpdateEvent : BaseEvent
    {
        public Team Team { get; set; }

        public TeamUpdateEvent(Team team)
        {
            Team = team;
        }
    }
}