﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Teams.Commands.UpdateTeam
{
    internal class UpdateTeamCommandHandler : IRequestHandler<UpdateTeamRequest, Result<Team>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateTeamCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Team>> Handle(UpdateTeamRequest request, CancellationToken cancellationToken)
        {
            var validateTeam = await _unitOfWork.Repository<Team>().GetByIdAsync(request.Id);
            if (validateTeam == null)
            {
                return await Result<Team>.FailureAsync("Team not found");
            }
            validateTeam.UpdatedAt = DateTime.UtcNow;
            validateTeam.TeamName = request.TeamName;

            await _unitOfWork.Repository<Team>().UpdateAsync(validateTeam);
            validateTeam.AddDomainEvent(new TeamUpdateEvent(validateTeam));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Team>.SuccessAsync("Success");
        }
    }
}