﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Teams.Commands.UpdateTeam
{
    public class UpdateTeamRequest : IRequest<Result<Team>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("team_name")]
        public string TeamName { get; set; }
    }
}