﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Teams
{
    public record TeamDto
    {
        [JsonPropertyName("team_name")]
        public string TeamName { get; set; }
    }
    public sealed record CreateTeamResponseDto : TeamDto { }
}