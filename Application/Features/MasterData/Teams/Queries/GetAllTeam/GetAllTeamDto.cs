﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Teams.Queries.GetAllTeam
{
    public class GetAllTeamDto : IMapFrom<Team>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("team_name")]
        public string TeamName { get; set; }
    }
}