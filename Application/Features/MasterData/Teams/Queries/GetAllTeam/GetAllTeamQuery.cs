﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Teams.Queries.GetAllTeam
{
    public record GetAllTeamQuery : IRequest<Result<List<GetAllTeamDto>>>;

    internal class GetAllTeamQueryHandler : IRequestHandler<GetAllTeamQuery, Result<List<GetAllTeamDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllTeamQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllTeamDto>>> Handle(GetAllTeamQuery query, CancellationToken cancellationToken)
        {
            var team = await _unitOfWork.Repository<Team>().FindByCondition(o => o.DeletedAt == null)
                           .ProjectTo<GetAllTeamDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllTeamDto>>.SuccessAsync(team, "Successfully fetch data");
        }
    }
}