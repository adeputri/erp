﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Teams.Queries.GetTeamWithPagination
{
    public record GetTeamWithPaginationQuery : IRequest<PaginatedResult<GetTeamWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetTeamWithPaginationQuery()
        {
        }
        public GetTeamWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetTeamWithPaginationQueryHandler : IRequestHandler<GetTeamWithPaginationQuery, PaginatedResult<GetTeamWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetTeamWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetTeamWithPaginationDto>> Handle(GetTeamWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Team>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.TeamName.ToLower() == query.SearchTerm.ToLower()))
                           .OrderByDescending(x => x.UpdatedAt)
                           .ProjectTo<GetTeamWithPaginationDto>(_mapper.ConfigurationProvider)
                           .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}