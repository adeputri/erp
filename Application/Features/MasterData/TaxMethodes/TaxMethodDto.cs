﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes
{
    public record TaxMethodDto
    {
        [JsonPropertyName("supplier_name")]
        public string SupplierName { get; set; }

        [JsonPropertyName("npwp")]
        public string Npwp { get; set; }
    }
    public sealed record CreateTaxMethodResponseDto : TaxMethodDto { }
}