﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Queries.GetAllTaxMethod
{
    public class GetAllTaxMethodDto
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}