﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Queries.GetAllTaxMethod
{
    public record GetAllTaxMethodQuery : IRequest<Result<List<GetAllTaxMethodDto>>>;

    internal class GetAllTaxMethodQueryHandler : IRequestHandler<GetAllTaxMethodQuery, Result<List<GetAllTaxMethodDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllTaxMethodQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllTaxMethodDto>>> Handle(GetAllTaxMethodQuery query, CancellationToken cancellationToken)
        {
            var team = await _unitOfWork.Repository<TaxMethode>().FindByCondition(o => o.DeletedAt == null)
                           .ProjectTo<GetAllTaxMethodDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllTaxMethodDto>>.SuccessAsync(team, "Successfully fetch data");
        }
    }
}