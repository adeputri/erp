﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Queries.GetAllTaxMethodWithPagination
{
    public class GetTaxMethodWithPaginationDto : IMapFrom<TaxMethode>
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}