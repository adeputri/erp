﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Queries.GetAllTaxMethodWithPagination
{
    public record GetTaxMethodWithPaginationQuery : IRequest<PaginatedResult<GetTaxMethodWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetTaxMethodWithPaginationQuery()
        {
        }
        public GetTaxMethodWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetAllTaskMethodWithPaginationQueryHandler : IRequestHandler<GetTaxMethodWithPaginationQuery, PaginatedResult<GetTaxMethodWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllTaskMethodWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetTaxMethodWithPaginationDto>> Handle(GetTaxMethodWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<TaxMethode>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.Status.ToLower() == query.SearchTerm.ToLower()))
                           .OrderByDescending(x => x.UpdatedAt)
                           .ProjectTo<GetTaxMethodWithPaginationDto>(_mapper.ConfigurationProvider)
                           .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}