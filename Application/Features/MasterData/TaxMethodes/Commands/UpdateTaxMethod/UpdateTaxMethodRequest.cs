﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.UpdateTaxMethod
{
    public class UpdateTaxMethodRequest : IRequest<Result<TaxMethode>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}