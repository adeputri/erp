﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.UpdateTaxMethod
{
    internal class UpdateTaxMethodCommandHandler : IRequestHandler<UpdateTaxMethodRequest, Result<TaxMethode>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateTaxMethodCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<TaxMethode>> Handle(UpdateTaxMethodRequest request, CancellationToken cancellationToken)
        {
            var validateTaxMethod = await _unitOfWork.Repository<TaxMethode>().GetByIdAsync(request.Id);
            if (validateTaxMethod == null)
            {
                return await Result<TaxMethode>.FailureAsync("Not found");
            }
            validateTaxMethod.UpdatedAt = DateTime.UtcNow;
            validateTaxMethod.Status = request.Status;

            await _unitOfWork.Repository<TaxMethode>().UpdateAsync(validateTaxMethod);
            validateTaxMethod.AddDomainEvent(new TaxMethodUpdateEvent(validateTaxMethod));
            await _unitOfWork.Save(cancellationToken);

            return await Result<TaxMethode>.SuccessAsync("Success");
        }
    }
}