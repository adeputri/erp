﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.UpdateTaxMethod
{
    public class TaxMethodUpdateEvent : BaseEvent
    {
        public TaxMethode TaxMethode { get; set; }

        public TaxMethodUpdateEvent(TaxMethode taxMethode)
        {
            TaxMethode = taxMethode;
        }
    }
}