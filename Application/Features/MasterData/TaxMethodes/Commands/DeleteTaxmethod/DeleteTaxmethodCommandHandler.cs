﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.CreateTaxMethod;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.DeleteTaxmethod
{
    internal class DeleteTaxmethodCommandHandler : IRequestHandler<DeleteTaxmethodRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteTaxmethodCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteTaxmethodRequest request, CancellationToken cancellationToken)
        {
            var projectDelete = await _unitOfWork.Repository<TaxMethode>().GetByIdAsync(request.Id);
            if (projectDelete != null)
            {
                projectDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<TaxMethode>().UpdateAsync(projectDelete);
                projectDelete.AddDomainEvent(new TaxMethodCreatedEvent(projectDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(projectDelete.Id, "Deleted.");
            }
            return await Result<Guid>.FailureAsync("Not found");
        }
    }
}