﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.DeleteTaxmethod
{
    public class TaxMethodDeletedEvent : BaseEvent
    {
        public TaxMethode TaxMethode { get; set; }

        public TaxMethodDeletedEvent(TaxMethode taxMethode)
        {
            TaxMethode = taxMethode;
        }
    }
}