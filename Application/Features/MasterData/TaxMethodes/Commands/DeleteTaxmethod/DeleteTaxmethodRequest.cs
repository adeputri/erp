﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.DeleteTaxmethod
{
    public class DeleteTaxmethodRequest : IRequest<Result<Guid>>, IMapFrom<TaxMethode>
    {
        public Guid Id { get; set; }

        public DeleteTaxmethodRequest(Guid id)
        {
            Id = id;
        }

        public DeleteTaxmethodRequest()
        {
        }
    }
}