﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.CreateTaxMethod
{
    internal class CreateTaxMethodCommandhandler : IRequestHandler<CreateTaxMethodRequest, Result<CreateTaxMethodResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ITaxMethodRepository _taxRepository;

        public CreateTaxMethodCommandhandler(IUnitOfWork unitOfWork, IMapper mapper, ITaxMethodRepository taxRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _taxRepository = taxRepository;
        }

        public async Task<Result<CreateTaxMethodResponseDto>> Handle(CreateTaxMethodRequest request, CancellationToken cancellationToken)
        {
            var positionCreate = _mapper.Map<TaxMethode>(request);
            var positionResponse = _mapper.Map<CreateTaxMethodResponseDto>(positionCreate);

            var validateData = await _taxRepository.Validatedata(positionCreate);

            if (validateData != true)
            {
                return await Result<CreateTaxMethodResponseDto>.FailureAsync(positionResponse, "Data already exist");
            }

            positionCreate.CreatedAt = DateTime.UtcNow;
            positionCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<TaxMethode>().AddAsync(positionCreate);
            positionCreate.AddDomainEvent(new TaxMethodCreatedEvent(positionCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateTaxMethodResponseDto>.SuccessAsync(positionResponse, "Created.");
        }
    }
}