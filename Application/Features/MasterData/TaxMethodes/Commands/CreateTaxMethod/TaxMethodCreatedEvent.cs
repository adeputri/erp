﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.CreateTaxMethod
{
    public class TaxMethodCreatedEvent : BaseEvent
    {
        public TaxMethode TaxMethode { get; set; }

        public TaxMethodCreatedEvent(TaxMethode taxMethode)
        {
            TaxMethode = taxMethode;
        }
    }
}