﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.CreateTaxMethod
{
    public class CreateTaxMethodRequest : IRequest<Result<CreateTaxMethodResponseDto>>
    {
        [JsonPropertyName("supplier_name")]
        public string SupplierName { get; set; }

        [JsonPropertyName("npwp")]
        public string Npwp { get; set; }
    }
}