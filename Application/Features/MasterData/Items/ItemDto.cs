﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Items
{
    public record ItemDto
    {
        [JsonPropertyName("barcode_item")]
        public string BarcodeItem { get; set; }
        [JsonPropertyName("item_name")]
        public string ItemName { get; set; }
        [JsonPropertyName("unit_id")]
        public Guid MasterUnitId { get; set; }
    }
    public sealed record CreateItemResponseDto : ItemDto { }
}