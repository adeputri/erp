﻿using FluentValidation;

namespace SkeletonApi.Application.Features.MasterData.Items.Queries.GetItemWithPagination
{
    public class GetItemWithPaginationValidator : AbstractValidator<GetItemWithPaginationQuery>
    {
        public GetItemWithPaginationValidator()
        {
            RuleFor(x => x.PageNumber)
             .GreaterThanOrEqualTo(1)
             .WithMessage("PageNumber at least greater than or equal to 1.");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageSize at least greater than or equal to 1.");
        }
    }
}