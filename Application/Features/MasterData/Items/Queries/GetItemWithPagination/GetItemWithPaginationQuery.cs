﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Items.Queries.GetItemWithPagination
{
    public record GetItemWithPaginationQuery : IRequest<PaginatedResult<GetItemWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetItemWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetItemWithPaginationQuery()
        {
        }
    }

    internal class GetItemWithPaginationQueryHandler : IRequestHandler<GetItemWithPaginationQuery, PaginatedResult<GetItemWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetItemWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetItemWithPaginationDto>> Handle(GetItemWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Item>()
                .Entities.Where(o => (o.DeletedAt == null) && (query.SearchTerm == null || o.BarcodeItem.ToLower().Contains(query.SearchTerm.ToLower()) || o.Merk.Name.ToLower().Contains(query.SearchTerm.ToLower())))
                .AsNoTracking()
                .Include(o => o.MasterUnit)
                .Include(o => o.Merk)
                .Select(o => new GetItemWithPaginationDto
                {
                    Id = o.Id,
                    BarcodeItem = o.BarcodeItem,
                    ItemName = o.Name,
                    Type = o.Type,
                    UnitId = o.MasterUnitId,
                    UnitName = o.MasterUnit.UnitName,
                    MerkId = o.MerkId,
                    MerkName = o.Merk.Name,
                    UpdatedAt = o.UpdatedAt
                }).OrderByDescending(p => p.UpdatedAt).ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}