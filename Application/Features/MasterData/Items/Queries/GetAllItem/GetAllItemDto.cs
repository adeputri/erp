﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Items.Queries.GetAllItem
{
    public class GetAllItemDto : IMapFrom<Item>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("barcode_item")]
        public string BarcodeItem { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        [JsonPropertyName("master_unit_id")]
        public Guid MasterUnitId { get; set; }

        [JsonPropertyName("unit_name")]
        public string UnitName { get; set; }

        [JsonPropertyName("merk_id")]
        public Guid MerkId { get; set; }

        [JsonPropertyName("merk_name")]
        public string MerkName { get; set; }

        [JsonPropertyName("updated_at")]
        public DateTime? UpdatedAt { get; set; }
    }
}