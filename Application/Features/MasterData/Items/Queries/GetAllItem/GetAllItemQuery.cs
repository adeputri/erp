﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Items.Queries.GetAllItem
{
    public record GetAllItemQuery : IRequest<Result<List<GetAllItemDto>>>;

    internal class GetAllPaymentScheduleQueryHandler : IRequestHandler<GetAllItemQuery, Result<List<GetAllItemDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllPaymentScheduleQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllItemDto>>> Handle(GetAllItemQuery query, CancellationToken cancellationToken)
        {
            var items = await _unitOfWork.Repository<Item>()
                .Entities.Where(o => o.DeletedAt == null)
                .AsNoTracking()
                .Include(o => o.MasterUnit)
                .Include(o => o.Merk)
                .Select(o => new GetAllItemDto
                {
                    Id = o.Id,
                    BarcodeItem = o.BarcodeItem,
                    Name = o.Name,
                    Type = o.Type,
                    MasterUnitId = o.MasterUnitId,
                    UnitName = o.MasterUnit.UnitName,
                    MerkId = o.MerkId,
                    MerkName = o.Merk.Name,
                    UpdatedAt = o.UpdatedAt
                }).OrderByDescending(o => o.UpdatedAt).
                ToListAsync(cancellationToken);

            return await Result<List<GetAllItemDto>>.SuccessAsync(items, "Successfully fetch data");
        }
    }
}