﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Items.Queries.GetItemByBarcode
{
    public class GetItemByBarcodeDto
    {
        [JsonPropertyName("barcode_item")]
        public string BarcodeItem { get; set; }

        public string Type { get; set; }

        [JsonPropertyName("item_name")]
        public string ItemName { get; set; }

        [JsonPropertyName("unit_id")]
        public Guid UnitId { get; set; }

        [JsonPropertyName("unit_name")]
        public string UnitName { get; set; }

        [JsonPropertyName("merk_id")]
        public Guid MerkId { get; set; }

        [JsonPropertyName("merk_name")]
        public string MerkName { get; set; }

        [JsonPropertyName("updated_at")]
        public DateTime? UpdatedAt { get; set; }
    }
}