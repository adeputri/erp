﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Items.Queries.GetItemByBarcode
{
    internal class GetItemByBarcodeHandler : IRequestHandler<GetItemByBarcodeQuery, Result<GetItemByBarcodeDto>>
    {
        private readonly IUnitOfWork _unitOfWork;

        public GetItemByBarcodeHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<GetItemByBarcodeDto>> Handle(GetItemByBarcodeQuery request, CancellationToken cancellationToken)
        {
            var result = await _unitOfWork.Repository<Item>()
                  .Entities
                  .AsNoTracking()
                  .Include(o => o.MasterUnit)
                  .Include(o => o.Merk)
                  .Where(o => o.BarcodeItem == request.Barcode)
                  .Select(o => new GetItemByBarcodeDto
                  {
                      BarcodeItem = o.BarcodeItem,
                      ItemName = o.Name,
                      Type = o.Type,
                      UnitId = o.MasterUnitId,
                      UnitName = o.MasterUnit.UnitName,
                      MerkId = o.MerkId,
                      MerkName = o.Merk.Name,
                      UpdatedAt = o.UpdatedAt
                  }).FirstOrDefaultAsync(cancellationToken);

            if (result == null)
            {
                //ItemNotFoundException.Throw(request.Barcode);
            }

            return await Result<GetItemByBarcodeDto>.SuccessAsync(result, "Succesfully fetch data");
        }
    }
}