﻿using MediatR;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Items.Queries.GetItemByBarcode
{
    public record GetItemByBarcodeQuery(string Barcode) : IRequest<Result<GetItemByBarcodeDto>>;
}