﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Items.Commands.DeleteItem
{
    public class DeleteItemRequest : IRequest<Result<string>>, IMapFrom<Item>
    {
        public Guid Id { get; set; }

        public DeleteItemRequest(Guid id)
        {
            Id = id;
        }

        public DeleteItemRequest()
        {
        }
    }
}