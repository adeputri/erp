﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Items.Commands.DeleteItem
{
    public class ItemDeletedEvent : BaseEvent
    {
        public Item Item { get; set; }

        public ItemDeletedEvent(Item item)
        {
            Item = item;
        }
    }
}