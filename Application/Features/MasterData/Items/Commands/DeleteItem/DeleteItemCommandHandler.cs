﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Features.MasterData.Items.Commands.CreateItem;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Items.Commands.DeleteItem
{
    internal class DeleteItemCommandHandler : IRequestHandler<DeleteItemRequest, Result<string>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteItemCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<string>> Handle(DeleteItemRequest request, CancellationToken cancellationToken)
        {
            var paymentScheduleDelete = await _unitOfWork.Repository<Item>().GetByIdAsync(request.Id);
            if (paymentScheduleDelete != null)
            {
                paymentScheduleDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<Item>().UpdateAsync(paymentScheduleDelete);
                paymentScheduleDelete.AddDomainEvent(new ItemCreatedEvent(paymentScheduleDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<string>.SuccessAsync(paymentScheduleDelete.BarcodeItem, "deleted.");
            }
            return await Result<string>.FailureAsync("not found");
        }
    }
}