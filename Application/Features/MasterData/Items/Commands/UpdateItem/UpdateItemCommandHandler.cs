﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Items.Commands.UpdateItem
{
    internal class UpdateItemCommandHandler : IRequestHandler<UpdateItemRequest, Result<Item>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateItemCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Item>> Handle(UpdateItemRequest request, CancellationToken cancellationToken)
        {
            var validateItem = await _unitOfWork.Repository<Item>().GetByIdAsync(request.Id);
            if (validateItem == null)
            {
                return await Result<Item>.FailureAsync("Item not found");
            }
            validateItem.BarcodeItem = request.BarcodeItem;
            validateItem.Name = request.Name;
            validateItem.Type = request.Type;
            validateItem.MasterUnitId = request.MasterUnitId;
            validateItem.MerkId = request.MerkId;

            await _unitOfWork.Repository<Item>().UpdateAsync(validateItem);
            validateItem.AddDomainEvent(new ItemUpdateEvent(validateItem));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Item>.SuccessAsync(validateItem, "Success");
        }
    }
}