﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Items.Commands.UpdateItem
{
    public class ItemUpdateEvent : BaseEvent
    {
        public Item Item { get; set; }

        public ItemUpdateEvent(Item item)
        {
            Item = item;
        }
    }
}