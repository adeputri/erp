﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Items.Commands.UpdateItem
{
    public class UpdateItemRequest : IRequest<Result<Item>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("barcode_item")]
        public string BarcodeItem { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("master_unit_id")]
        public Guid MasterUnitId { get; set; }

        [JsonPropertyName("merk_id")]
        public Guid MerkId { get; set; }
    }
}