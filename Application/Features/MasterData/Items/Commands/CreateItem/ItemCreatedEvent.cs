﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Items.Commands.CreateItem
{
    public class ItemCreatedEvent : BaseEvent
    {
        public Item Item { get; set; }

        public ItemCreatedEvent(Item item)
        {
            Item = item;
        }
    }
}