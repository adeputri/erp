﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Common.Exceptions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Items.Commands.CreateItem
{
    internal class CreateItemCommandHandler : IRequestHandler<CreateItemRequest, Result<CreateItemResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IItemRepository _itemRepository;

        public CreateItemCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IItemRepository itemRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _itemRepository = itemRepository;
        }

        public async Task<Result<CreateItemResponseDto>> Handle(CreateItemRequest request, CancellationToken cancellationToken)
        {
            var item = _mapper.Map<Item>(request);

            var validateData = await _itemRepository.ValidateData(item);

            if (validateData != true)
            {
                ItemAlreadyExistsException.Throw(request.BarcodeItem);
            }

            await _unitOfWork.Repository<Item>().AddAsync(item);
            item.AddDomainEvent(new ItemCreatedEvent(item));
            await _unitOfWork.Save(cancellationToken);

            var response = _mapper.Map<CreateItemResponseDto>(item);
            return await Result<CreateItemResponseDto>.SuccessAsync(response, "Item created.");
        }
    }
}