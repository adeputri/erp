﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.CreateBankBranch
{
    public class CreateBankBranchRequest : IRequest<Result<CreateBankBranchResponseDto>>
    {
        [JsonPropertyName("branch_bank_name")]
        public string BranchBankName { get; set; }

        [JsonPropertyName("bank_id")]
        public Guid BankId { get; set; }
    }
}