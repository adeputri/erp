﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.CreateBankBranch
{
    internal class CreateBankBranchCommandHandler : IRequestHandler<CreateBankBranchRequest, Result<CreateBankBranchResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IBankBranchRepository _bankBranchRepository;

        public CreateBankBranchCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IBankBranchRepository bankBranchRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _bankBranchRepository = bankBranchRepository;
        }

        public async Task<Result<CreateBankBranchResponseDto>> Handle(CreateBankBranchRequest request, CancellationToken cancellationToken)
        {
            var bankCreate = _mapper.Map<BranchBank>(request);
            var bankResponse = _mapper.Map<CreateBankBranchResponseDto>(bankCreate);

            var validateData = await _bankBranchRepository.ValidateData(bankCreate);

            if (validateData != true)
            {
                return await Result<CreateBankBranchResponseDto>.FailureAsync(bankResponse, "Data already exist");
            }

            bankCreate.CreatedAt = DateTime.UtcNow;
            bankCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<BranchBank>().AddAsync(bankCreate);
            bankCreate.AddDomainEvent(new BankBranchCreatedEVent(bankCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateBankBranchResponseDto>.SuccessAsync(bankResponse, "Bank Branch created.");
        }
    }
}