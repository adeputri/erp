﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.CreateBankBranch
{
    public class BankBranchCreatedEVent : BaseEvent
    {
        public BranchBank BranchBank { get; set; }

        public BankBranchCreatedEVent(BranchBank branchBank)
        {
            BranchBank = branchBank;
        }
    }
}