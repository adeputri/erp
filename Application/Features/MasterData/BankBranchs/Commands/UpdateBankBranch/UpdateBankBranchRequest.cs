﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.UpdateBankBranch
{
    public class UpdateBankBranchRequest : IRequest<Result<BranchBank>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("branch_bank_name")]
        public string BranchBankName { get; set; }

        [JsonPropertyName("bank_id")]
        public Guid BankId { get; set; }
    }
}