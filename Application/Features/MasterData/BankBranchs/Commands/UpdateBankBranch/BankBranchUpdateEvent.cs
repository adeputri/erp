﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.UpdateBankBranch
{
    public class BankBranchUpdateEvent : BaseEvent
    {
        public BranchBank BranchBank { get; set; }

        public BankBranchUpdateEvent(BranchBank branchBank)
        {
            BranchBank = branchBank;
        }
    }
}