﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.UpdateBankBranch
{
    internal class UpdateBankBranchCommandHandler : IRequestHandler<UpdateBankBranchRequest, Result<BranchBank>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateBankBranchCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<BranchBank>> Handle(UpdateBankBranchRequest request, CancellationToken cancellationToken)
        {
            var bankValidate = await _unitOfWork.Repository<BranchBank>().GetByIdAsync(request.Id);
            if (bankValidate == null)
            {
                return await Result<BranchBank>.FailureAsync("Bank not found");
            }
            bankValidate.UpdatedAt = DateTime.UtcNow;
            bankValidate.BranchBankName = request.BranchBankName;
            bankValidate.BankId = request.BankId;

            await _unitOfWork.Repository<BranchBank>().UpdateAsync(bankValidate);
            bankValidate.AddDomainEvent(new BankBranchUpdateEvent(bankValidate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<BranchBank>.SuccessAsync("Success");
        }
    }
}