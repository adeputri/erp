﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.DeleteBankBranch
{
    public class BankBranchDeletedEvent : BaseEvent
    {
        public BranchBank BranchBank { get; set; }

        public BankBranchDeletedEvent(BranchBank branchBank)
        {
            BranchBank = branchBank;
        }
    }
}