﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.DeleteBankBranch
{
    public class DeleteBankBranchRequest : IRequest<Result<Guid>>, IMapFrom<BranchBank>
    {
        public Guid Id { get; set; }

        public DeleteBankBranchRequest(Guid id)
        {
            Id = id;
        }

        public DeleteBankBranchRequest()
        {
        }
    }
}