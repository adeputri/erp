﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Queries.GetAllBankBranch
{
    public record GetAllBankBranchQuery : IRequest<Result<List<GetAllBankBranchDto>>>;

    internal class GetAllBankBranchQueryHandler : IRequestHandler<GetAllBankBranchQuery, Result<List<GetAllBankBranchDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllBankBranchQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllBankBranchDto>>> Handle(GetAllBankBranchQuery query, CancellationToken cancellationToken)
        {
            var bank = await _unitOfWork.Repository<BranchBank>().FindByCondition(o => o.DeletedAt == null)
                           .ProjectTo<GetAllBankBranchDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllBankBranchDto>>.SuccessAsync(bank, "Successfully fetch data");
        }
    }
}