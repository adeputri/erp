﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Queries.GetAllBankBranch
{
    public class GetAllBankBranchDto : IMapFrom<BranchBank>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("branch_bank_name")]
        public string BranchBankName { get; set; }
    }
}