﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Queries.GetBankBranchWithPagination
{
    public record GetBankBranchWithPaginationQuery : IRequest<PaginatedResult<GetBankBranchWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public GetBankBranchWithPaginationQuery()
        {
        }

        public GetBankBranchWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetBankBranchWithPaginationQueryHandler : IRequestHandler<GetBankBranchWithPaginationQuery, PaginatedResult<GetBankBranchWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetBankBranchWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetBankBranchWithPaginationDto>> Handle(GetBankBranchWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<BranchBank>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.BranchBankName.ToLower() == query.SearchTerm.ToLower()))
            .OrderByDescending(x => x.UpdatedAt).Select(o => new GetBankBranchWithPaginationDto
            {
                Id = o.Id,
                BranchBankName = o.BranchBankName,
                BankName = o.Bank.BankName,
                UpdateAt = o.UpdatedAt.Value
            })
            .ProjectTo<GetBankBranchWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}