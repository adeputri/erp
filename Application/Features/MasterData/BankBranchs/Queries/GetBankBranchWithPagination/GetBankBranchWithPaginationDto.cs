﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs.Queries.GetBankBranchWithPagination
{
    public class GetBankBranchWithPaginationDto : IMapFrom<GetBankBranchWithPaginationDto>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("branch_bank_name")]
        public string BranchBankName { get; set; }

        [JsonPropertyName("bank_name")]
        public string BankName { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime UpdateAt { get; set; }
    }
}