﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.BankBranchs
{
    public record BankBranchDto
    {
        [JsonPropertyName("branch_bank_name")]
        public string BranchBankName { get; set; }

        [JsonPropertyName("bank_id")]
        public Guid BankId { get; set; }
    }
    public sealed record CreateBankBranchResponseDto : BankBranchDto { }
}