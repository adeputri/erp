﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses
{
    public record PaymentStatusDto
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
    public sealed record CreatePaymentStatusResponseDto : PaymentStatusDto { }
}