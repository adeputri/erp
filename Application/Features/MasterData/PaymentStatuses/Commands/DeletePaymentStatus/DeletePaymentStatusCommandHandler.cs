﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.DeletePaymentStatus
{
    internal class DeletePaymentStatusCommandHandler : IRequestHandler<DeletePaymentStatusRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeletePaymentStatusCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeletePaymentStatusRequest request, CancellationToken cancellationToken)
        {
            var paymentStatusDelete = await _unitOfWork.Repository<PaymentStatus>().GetByIdAsync(request.Id);
            if (paymentStatusDelete != null)
            {
                paymentStatusDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<PaymentStatus>().UpdateAsync(paymentStatusDelete);
                paymentStatusDelete.AddDomainEvent(new PaymentStatusDeletedEvent(paymentStatusDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(paymentStatusDelete.Id, "deleted.");
            }
            return await Result<Guid>.FailureAsync("not found");
        }
    }
}