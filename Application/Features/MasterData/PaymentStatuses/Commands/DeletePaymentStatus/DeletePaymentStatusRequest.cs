﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.DeletePaymentStatus
{
    public class DeletePaymentStatusRequest : IRequest<Result<Guid>>, IMapFrom<PaymentStatus>
    {
        public Guid Id { get; set; }

        public DeletePaymentStatusRequest(Guid id)
        {
            Id = id;
        }

        public DeletePaymentStatusRequest()
        {
        }
    }
}