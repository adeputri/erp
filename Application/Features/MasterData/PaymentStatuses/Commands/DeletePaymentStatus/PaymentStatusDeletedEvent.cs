﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.DeletePaymentStatus
{
    public class PaymentStatusDeletedEvent : BaseEvent
    {
        public PaymentStatus PaymentStatus { get; set; }

        public PaymentStatusDeletedEvent(PaymentStatus paymentStatus)
        {
            PaymentStatus = paymentStatus;
        }
    }
}