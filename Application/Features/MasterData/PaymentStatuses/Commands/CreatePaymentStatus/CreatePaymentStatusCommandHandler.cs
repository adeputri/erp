﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.CreatePaymentStatus
{
    internal class CreatePaymentStatusCommandHandler : IRequestHandler<CreatePaymentStatusRequest, Result<CreatePaymentStatusResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPaymentStatusRepository _paymentStatusRepository;

        public CreatePaymentStatusCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IPaymentStatusRepository paymentStatusRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _paymentStatusRepository = paymentStatusRepository;
        }

        public async Task<Result<CreatePaymentStatusResponseDto>> Handle(CreatePaymentStatusRequest request, CancellationToken cancellationToken)
        {
            var paymentStatusCreate = _mapper.Map<PaymentStatus>(request);
            var paymentStatusResponse = _mapper.Map<CreatePaymentStatusResponseDto>(paymentStatusCreate);

            var validateData = await _paymentStatusRepository.ValidateData(paymentStatusCreate);

            if (validateData != true)
            {
                return await Result<CreatePaymentStatusResponseDto>.FailureAsync(paymentStatusResponse, "Data already exist");
            }

            paymentStatusCreate.CreatedAt = DateTime.UtcNow;
            paymentStatusCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<PaymentStatus>().AddAsync(paymentStatusCreate);
            paymentStatusCreate.AddDomainEvent(new PaymentStatusCreatedEvent(paymentStatusCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreatePaymentStatusResponseDto>.SuccessAsync(paymentStatusResponse, "Created.");
        }
    }
}