﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.CreatePaymentStatus
{
    public class PaymentStatusCreatedEvent : BaseEvent
    {
        public PaymentStatus PaymentStatus { get; set; }

        public PaymentStatusCreatedEvent(PaymentStatus paymentStatus)
        {
            PaymentStatus = paymentStatus;
        }
    }
}