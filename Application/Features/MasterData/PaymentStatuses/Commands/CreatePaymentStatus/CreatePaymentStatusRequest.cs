﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.CreatePaymentStatus
{
    public class CreatePaymentStatusRequest : IRequest<Result<CreatePaymentStatusResponseDto>>
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}