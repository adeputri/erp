﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.UpdatePaymentStatus
{
    internal class UpdatePaymentStatusCommandHandler : IRequestHandler<UpdatePaymentStatusRequest, Result<PaymentStatus>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdatePaymentStatusCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<PaymentStatus>> Handle(UpdatePaymentStatusRequest request, CancellationToken cancellationToken)
        {
            var validatePaymentStatus = await _unitOfWork.Repository<PaymentStatus>().GetByIdAsync(request.Id);
            if (validatePaymentStatus == null)
            {
                return await Result<PaymentStatus>.FailureAsync("not found");
            }
            validatePaymentStatus.UpdatedAt = DateTime.UtcNow;
            validatePaymentStatus.Status = request.Status;

            await _unitOfWork.Repository<PaymentStatus>().UpdateAsync(validatePaymentStatus);
            validatePaymentStatus.AddDomainEvent(new PaymentStatusUpdateEvent(validatePaymentStatus));
            await _unitOfWork.Save(cancellationToken);

            return await Result<PaymentStatus>.SuccessAsync(validatePaymentStatus, "Success");
        }
    }
}