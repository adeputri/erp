﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.UpdatePaymentStatus
{
    public class PaymentStatusUpdateEvent : BaseEvent
    {
        public PaymentStatus PaymentStatus { get; set; }

        public PaymentStatusUpdateEvent(PaymentStatus paymentStatus)
        {
            PaymentStatus = paymentStatus;
        }
    }
}