﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.UpdatePaymentStatus
{
    public class UpdatePaymentStatusRequest : IRequest<Result<PaymentStatus>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}