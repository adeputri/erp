﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Queries.GetAllPaymentStatus
{
    public record GetAllPaymentStatusQuery : IRequest<Result<List<GetAllPaymentStatusDto>>>;

    internal class GetAllPaymentScheduleQueryHandler : IRequestHandler<GetAllPaymentStatusQuery, Result<List<GetAllPaymentStatusDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllPaymentScheduleQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllPaymentStatusDto>>> Handle(GetAllPaymentStatusQuery query, CancellationToken cancellationToken)
        {
            var employee = await _unitOfWork.Repository<PaymentStatus>().FindByCondition(o => o.DeletedAt == null)
                            .ProjectTo<GetAllPaymentStatusDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetAllPaymentStatusDto>>.SuccessAsync(employee, "Successfully fetch data");
        }
    }
}