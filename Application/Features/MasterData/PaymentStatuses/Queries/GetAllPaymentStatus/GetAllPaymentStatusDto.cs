﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PaymentStatuses.Queries.GetAllPaymentStatus
{
    public class GetAllPaymentStatusDto : IMapFrom<PaymentStatus>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}