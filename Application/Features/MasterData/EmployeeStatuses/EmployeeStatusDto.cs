﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses
{
    public record EmployeeStatusDto
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
    public sealed record CreateEmployeeStatusResponseDto : EmployeeStatusDto { }
}