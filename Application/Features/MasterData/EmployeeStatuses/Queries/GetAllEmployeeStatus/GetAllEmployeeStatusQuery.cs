﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Queries.GetAllEmployeeStatus
{
    public record GetAllEmployeeStatusQuery : IRequest<Result<List<GetAllEmployeeStatusDto>>>;

    internal class GetAllEmployeeStatusQueryHandler : IRequestHandler<GetAllEmployeeStatusQuery, Result<List<GetAllEmployeeStatusDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllEmployeeStatusQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllEmployeeStatusDto>>> Handle(GetAllEmployeeStatusQuery query, CancellationToken cancellationToken)
        {
            var employee = await _unitOfWork.Repository<EmployeeStatus>().FindByCondition(o => o.DeletedAt == null)
                            .ProjectTo<GetAllEmployeeStatusDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetAllEmployeeStatusDto>>.SuccessAsync(employee, "Successfully fetch data");
        }
    }
}