﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Queries.GetAllEmployeeStatus
{
    public class GetAllEmployeeStatusDto : IMapFrom<EmployeeStatus>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}