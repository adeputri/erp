﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Queries.GetEmployeeStatusWithPagination
{
    public record GetEmployeeStatusWithPaginationQuery : IRequest<PaginatedResult<GetEmployeeStatusWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetEmployeeStatusWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetEmployeeStatusWithPaginationQuery()
        {
        }
    }

    internal class GetEmployeeStatusWithPaginationQueryHandler : IRequestHandler<GetEmployeeStatusWithPaginationQuery, PaginatedResult<GetEmployeeStatusWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetEmployeeStatusWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetEmployeeStatusWithPaginationDto>> Handle(GetEmployeeStatusWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<EmployeeStatus>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.Status.ToLower() == query.SearchTerm.ToLower()))
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetEmployeeStatusWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}