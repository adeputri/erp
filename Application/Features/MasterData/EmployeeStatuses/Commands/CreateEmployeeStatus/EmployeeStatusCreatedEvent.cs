﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.CreateEmployeesStatus
{
    public class EmployeeStatusCreatedEvent : BaseEvent
    {
        public EmployeeStatus EmployeeStatus { get; set; }

        public EmployeeStatusCreatedEvent(EmployeeStatus employeeStatus)
        {
            EmployeeStatus = employeeStatus;
        }
    }
}