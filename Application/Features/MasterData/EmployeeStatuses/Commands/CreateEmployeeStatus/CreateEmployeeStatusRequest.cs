﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.CreateEmployeesStatus
{
    public class CreateEmployeeStatusRequest : IRequest<Result<CreateEmployeeStatusResponseDto>>
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}