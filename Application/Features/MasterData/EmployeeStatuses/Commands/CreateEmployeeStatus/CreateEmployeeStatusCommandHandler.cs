﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.CreateEmployeesStatus
{
    internal class CreateEmployeeStatusCommandHandler : IRequestHandler<CreateEmployeeStatusRequest, Result<CreateEmployeeStatusResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEmployeeStatusRepository _employeeStatusRepository;

        public CreateEmployeeStatusCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IEmployeeStatusRepository employeeStatusRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _employeeStatusRepository = employeeStatusRepository;
        }

        public async Task<Result<CreateEmployeeStatusResponseDto>> Handle(CreateEmployeeStatusRequest request, CancellationToken cancellationToken)
        {
            var EmployeeStatusCreate = _mapper.Map<EmployeeStatus>(request);
            var employeeStatusResponse = _mapper.Map<CreateEmployeeStatusResponseDto>(EmployeeStatusCreate);

            var validateData = await _employeeStatusRepository.ValidateData(EmployeeStatusCreate);

            if (validateData != true)
            {
                return await Result<CreateEmployeeStatusResponseDto>.FailureAsync(employeeStatusResponse, "Data already exist");
            }

            EmployeeStatusCreate.CreatedAt = DateTime.UtcNow;
            EmployeeStatusCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<EmployeeStatus>().AddAsync(EmployeeStatusCreate);
            EmployeeStatusCreate.AddDomainEvent(new EmployeeStatusCreatedEvent(EmployeeStatusCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateEmployeeStatusResponseDto>.SuccessAsync(employeeStatusResponse, "Employee Status created.");
        }
    }
}