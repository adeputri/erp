﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.UpdateEmployeeStatus
{
    internal class UpdateEmployeeStatusCommandHandler : IRequestHandler<UpdateEmployeeStatusRequest, Result<EmployeeStatus>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateEmployeeStatusCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<EmployeeStatus>> Handle(UpdateEmployeeStatusRequest request, CancellationToken cancellationToken)
        {
            var validateEmployeeStatus = await _unitOfWork.Repository<EmployeeStatus>().GetByIdAsync(request.Id);
            if (validateEmployeeStatus == null)
            {
                return await Result<EmployeeStatus>.FailureAsync("Employee status not found");
            }
            validateEmployeeStatus.UpdatedAt = DateTime.UtcNow;
            validateEmployeeStatus.Status = request.Status;

            await _unitOfWork.Repository<EmployeeStatus>().UpdateAsync(validateEmployeeStatus);
            validateEmployeeStatus.AddDomainEvent(new EmployeeStatusUpdateEvent(validateEmployeeStatus));
            await _unitOfWork.Save(cancellationToken);

            return await Result<EmployeeStatus>.SuccessAsync("Success");
        }
    }
}