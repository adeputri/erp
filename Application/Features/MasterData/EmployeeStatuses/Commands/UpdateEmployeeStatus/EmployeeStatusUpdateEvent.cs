﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.UpdateEmployeeStatus
{
    public class EmployeeStatusUpdateEvent : BaseEvent
    {
        public EmployeeStatus EmployeeStatus { get; set; }

        public EmployeeStatusUpdateEvent(EmployeeStatus employeeStatus)
        {
            EmployeeStatus = employeeStatus;
        }
    }
}