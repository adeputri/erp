﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.DeleteEmployeeStatus
{
    public class EmployeeStatusDeletedEvent : BaseEvent
    {
        public EmployeeStatus EmployeeStatus { get; set; }

        public EmployeeStatusDeletedEvent(EmployeeStatus employeeStatus)
        {
            EmployeeStatus = employeeStatus;
        }
    }
}