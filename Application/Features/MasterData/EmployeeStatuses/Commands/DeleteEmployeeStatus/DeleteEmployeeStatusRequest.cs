﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.DeleteEmployeeStatus
{
    public class DeleteEmployeeStatusRequest : IRequest<Result<Guid>>, IMapFrom<EmployeeStatus>
    {
        public Guid Id { get; set; }

        public DeleteEmployeeStatusRequest(Guid id)
        {
            Id = id;
        }

        public DeleteEmployeeStatusRequest()
        {
        }
    }
}