﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.DeleteEmployeeStatus
{
    internal class DeleteEmployeeStatusCommandHandler : IRequestHandler<DeleteEmployeeStatusRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteEmployeeStatusCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteEmployeeStatusRequest request, CancellationToken cancellationToken)
        {
            var employeeStatusDelete = await _unitOfWork.Repository<EmployeeStatus>().GetByIdAsync(request.Id);
            if (employeeStatusDelete != null)
            {
                employeeStatusDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<EmployeeStatus>().UpdateAsync(employeeStatusDelete);
                employeeStatusDelete.AddDomainEvent(new EmployeeStatusDeletedEvent(employeeStatusDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(employeeStatusDelete.Id, "Employee status deleted.");
            }
            return await Result<Guid>.FailureAsync("Employee status not found");
        }
    }
}