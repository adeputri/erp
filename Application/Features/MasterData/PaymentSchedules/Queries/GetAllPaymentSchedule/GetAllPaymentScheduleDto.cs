﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Queries.GetAllPaymentSchedule
{
    public class GetAllPaymentScheduleDto : IMapFrom<PaymentSchedule>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("schedule")]
        public string Schedule { get; set; }
    }
}