﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Queries.GetAllPaymentSchedule
{
    public record GetAllPaymentScheduleQuery : IRequest<Result<List<GetAllPaymentScheduleDto>>>;

    internal class GetAllPaymentScheduleQueryHandler : IRequestHandler<GetAllPaymentScheduleQuery, Result<List<GetAllPaymentScheduleDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllPaymentScheduleQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllPaymentScheduleDto>>> Handle(GetAllPaymentScheduleQuery query, CancellationToken cancellationToken)
        {
            var employee = await _unitOfWork.Repository<PaymentSchedule>().FindByCondition(o => o.DeletedAt == null)
                            .ProjectTo<GetAllPaymentScheduleDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetAllPaymentScheduleDto>>.SuccessAsync(employee, "Successfully fetch data");
        }
    }
}