﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Queries.GetPaymentScheduleWithPagination
{
    public record GetPaymentShceduleWithPaginationQuery : IRequest<PaginatedResult<GetPaymentShceduleWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetPaymentShceduleWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetPaymentShceduleWithPaginationQuery()
        {
        }
    }

    internal class GetPaymentShceduleWithPaginationQueryHandler : IRequestHandler<GetPaymentShceduleWithPaginationQuery, PaginatedResult<GetPaymentShceduleWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPaymentShceduleWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetPaymentShceduleWithPaginationDto>> Handle(GetPaymentShceduleWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<PaymentSchedule>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.Schedule.ToLower() == query.SearchTerm.ToLower()))
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetPaymentShceduleWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}