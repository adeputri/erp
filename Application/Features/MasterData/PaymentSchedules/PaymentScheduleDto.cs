﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules
{
    public record PaymentScheduleDto
    {
        [JsonPropertyName("schedule")]
        public string Schedule { get; set; }
    }
    public sealed record CreatePaymentScheduleResponseDto : PaymentScheduleDto { }
}