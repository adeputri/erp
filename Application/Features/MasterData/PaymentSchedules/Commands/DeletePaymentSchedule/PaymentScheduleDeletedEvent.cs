﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.DeletePaymentSchedule
{
    public class PaymentScheduleDeletedEvent : BaseEvent
    {
        public PaymentSchedule PaymentSchedule { get; set; }

        public PaymentScheduleDeletedEvent(PaymentSchedule paymentSchedule)
        {
            PaymentSchedule = paymentSchedule;
        }
    }
}