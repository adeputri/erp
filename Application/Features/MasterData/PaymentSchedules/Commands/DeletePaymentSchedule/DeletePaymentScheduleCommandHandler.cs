﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.DeletePaymentSchedule
{
    internal class DeletePaymentScheduleCommandHandler : IRequestHandler<DeletePaymentScheduleRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeletePaymentScheduleCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeletePaymentScheduleRequest request, CancellationToken cancellationToken)
        {
            var paymentScheduleDelete = await _unitOfWork.Repository<PaymentSchedule>().GetByIdAsync(request.Id);
            if (paymentScheduleDelete != null)
            {
                paymentScheduleDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<PaymentSchedule>().UpdateAsync(paymentScheduleDelete);
                paymentScheduleDelete.AddDomainEvent(new PaymentScheduleDeletedEvent(paymentScheduleDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(paymentScheduleDelete.Id, "deleted.");
            }
            return await Result<Guid>.FailureAsync("not found");
        }
    }
}