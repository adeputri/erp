﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.DeletePaymentSchedule
{
    public class DeletePaymentScheduleRequest : IRequest<Result<Guid>>, IMapFrom<PaymentSchedule>
    {
        public Guid Id { get; set; }

        public DeletePaymentScheduleRequest(Guid id)
        {
            Id = id;
        }

        public DeletePaymentScheduleRequest()
        {
        }
    }
}