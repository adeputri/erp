﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.CreatePaymentSchedule
{
    public class PaymentScheduleCreatedEVent : BaseEvent
    {
        public PaymentSchedule PaymentSchedule { get; set; }

        public PaymentScheduleCreatedEVent(PaymentSchedule paymentSchedule)
        {
            PaymentSchedule = paymentSchedule;
        }
    }
}