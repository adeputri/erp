﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.CreatePaymentSchedule
{
    internal class CreatePaymentScheduleCommandHandler : IRequestHandler<CreatePaymentScheduleRequest, Result<CreatePaymentScheduleResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPaymentScheduleRepository _paymentScheduleRepository;

        public CreatePaymentScheduleCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IPaymentScheduleRepository paymentScheduleRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _paymentScheduleRepository = paymentScheduleRepository;
        }

        public async Task<Result<CreatePaymentScheduleResponseDto>> Handle(CreatePaymentScheduleRequest request, CancellationToken cancellationToken)
        {
            var EmployeeStatusCreate = _mapper.Map<PaymentSchedule>(request);
            var employeeStatusResponse = _mapper.Map<CreatePaymentScheduleResponseDto>(EmployeeStatusCreate);

            var validateData = await _paymentScheduleRepository.ValidateData(EmployeeStatusCreate);

            if (validateData != true)
            {
                return await Result<CreatePaymentScheduleResponseDto>.FailureAsync(employeeStatusResponse, "Data already exist");
            }

            EmployeeStatusCreate.CreatedAt = DateTime.UtcNow;
            EmployeeStatusCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<PaymentSchedule>().AddAsync(EmployeeStatusCreate);
            EmployeeStatusCreate.AddDomainEvent(new PaymentScheduleCreatedEVent(EmployeeStatusCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreatePaymentScheduleResponseDto>.SuccessAsync(employeeStatusResponse, "created.");
        }
    }
}