﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.CreatePaymentSchedule
{
    public class CreatePaymentScheduleRequest : IRequest<Result<CreatePaymentScheduleResponseDto>>
    {
        [JsonPropertyName("schedule")]
        public string Schedule { get; set; }
    }
}