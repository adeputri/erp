﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.UpdatePaymentSchedule
{
    public class PaymentScheduleUpdateEvent : BaseEvent
    {
        public PaymentSchedule PaymentSchedule { get; set; }

        public PaymentScheduleUpdateEvent(PaymentSchedule paymentSchedule)
        {
            PaymentSchedule = paymentSchedule;
        }
    }
}