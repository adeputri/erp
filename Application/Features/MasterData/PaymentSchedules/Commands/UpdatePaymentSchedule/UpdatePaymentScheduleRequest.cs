﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.UpdatePaymentSchedule
{
    public class UpdatePaymentScheduleRequest : IRequest<Result<PaymentSchedule>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("schedule")]
        public string Schedule { get; set; }
    }
}