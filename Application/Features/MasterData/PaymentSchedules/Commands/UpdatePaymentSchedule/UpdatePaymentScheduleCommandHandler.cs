﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.UpdatePaymentSchedule
{
    internal class UpdatePaymentScheduleCommandHandler : IRequestHandler<UpdatePaymentScheduleRequest, Result<PaymentSchedule>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdatePaymentScheduleCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<PaymentSchedule>> Handle(UpdatePaymentScheduleRequest request, CancellationToken cancellationToken)
        {
            var validateEmployeeStatus = await _unitOfWork.Repository<PaymentSchedule>().GetByIdAsync(request.Id);
            if (validateEmployeeStatus == null)
            {
                return await Result<PaymentSchedule>.FailureAsync("not found");
            }
            validateEmployeeStatus.UpdatedAt = DateTime.UtcNow;
            validateEmployeeStatus.Schedule = request.Schedule;

            await _unitOfWork.Repository<PaymentSchedule>().UpdateAsync(validateEmployeeStatus);
            validateEmployeeStatus.AddDomainEvent(new PaymentScheduleUpdateEvent(validateEmployeeStatus));
            await _unitOfWork.Save(cancellationToken);

            return await Result<PaymentSchedule>.SuccessAsync(validateEmployeeStatus, "Success");
        }
    }
}