﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Employees.Queries.GetAllEmployee
{
    public record GetAllEmployeeQuery : IRequest<Result<List<GetAllEmployeeDto>>>;

    internal class GetAllEmployeeQueriesHandler : IRequestHandler<GetAllEmployeeQuery, Result<List<GetAllEmployeeDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllEmployeeQueriesHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllEmployeeDto>>> Handle(GetAllEmployeeQuery query, CancellationToken cancellationToken)
        {
            var employee = await _unitOfWork.Repository<Employee>().FindByCondition(o => o.DeletedAt == null)
                            .Select(e => new GetAllEmployeeDto { Id = e.Id, EmployeeName = e.FirstName + " " + e.LastName })
                           .ProjectTo<GetAllEmployeeDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllEmployeeDto>>.SuccessAsync(employee, "Successfully fetch data");
        }
    }
}