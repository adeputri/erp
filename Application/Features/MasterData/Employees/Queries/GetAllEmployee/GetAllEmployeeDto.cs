﻿using SkeletonApi.Application.Common.Mappings;

namespace SkeletonApi.Application.Features.MasterData.Employees.Queries.GetAllEmployee
{
    public class GetAllEmployeeDto : IMapFrom<GetAllEmployeeDto>
    {
        public Guid Id { get; set; }
        public string EmployeeName { get; set; }
    }
}