﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Units.Commands.DeleteUnit
{
    internal class DeleteUnitCommandHandler : IRequestHandler<DeleteUnitRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteUnitCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteUnitRequest request, CancellationToken cancellationToken)
        {
            var teamDelete = await _unitOfWork.Repository<MasterUnit>().GetByIdAsync(request.Id);
            if (teamDelete != null)
            {
                teamDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterUnit>().UpdateAsync(teamDelete);
                teamDelete.AddDomainEvent(new UnitDeletedEvent(teamDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(teamDelete.Id, "Deleted.");
            }
            return await Result<Guid>.FailureAsync("Not found");
        }
    }
}