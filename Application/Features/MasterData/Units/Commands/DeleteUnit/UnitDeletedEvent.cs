﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Units.Commands.DeleteUnit
{
    public class UnitDeletedEvent : BaseEvent
    {
        public MasterUnit MasterUnit { get; set; }

        public UnitDeletedEvent(MasterUnit masterUnit)
        {
            MasterUnit = masterUnit;
        }
    }
}