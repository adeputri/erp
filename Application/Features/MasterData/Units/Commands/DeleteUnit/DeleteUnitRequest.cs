﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Units.Commands.DeleteUnit
{
    public class DeleteUnitRequest : IRequest<Result<Guid>>, IMapFrom<MasterUnit>
    {
        public Guid Id { get; set; }

        public DeleteUnitRequest(Guid id)
        {
            Id = id;
        }

        public DeleteUnitRequest()
        {
        }
    }
}