﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Units.Commands.UpdateUnit
{
    public class UnitUpdateEvent : BaseEvent
    {
        public MasterUnit MasterUnit { get; set; }

        public UnitUpdateEvent(MasterUnit masterUnit)
        {
            MasterUnit = masterUnit;
        }
    }
}