﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Units.Commands.UpdateUnit
{
    public class UpdateUnitRequest : IRequest<Result<MasterUnit>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("unit_name")]
        public string UnitName { get; set; }
    }
}