﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Units.Commands.UpdateUnit
{
    internal class UpdateUnitCommandHandler : IRequestHandler<UpdateUnitRequest, Result<MasterUnit>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateUnitCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<MasterUnit>> Handle(UpdateUnitRequest request, CancellationToken cancellationToken)
        {
            var validateTeam = await _unitOfWork.Repository<MasterUnit>().GetByIdAsync(request.Id);
            if (validateTeam == null)
            {
                return await Result<MasterUnit>.FailureAsync("Team not found");
            }
            validateTeam.UpdatedAt = DateTime.UtcNow;
            validateTeam.UnitName = request.UnitName;

            await _unitOfWork.Repository<MasterUnit>().UpdateAsync(validateTeam);
            validateTeam.AddDomainEvent(new UnitUpdateEvent(validateTeam));
            await _unitOfWork.Save(cancellationToken);

            return await Result<MasterUnit>.SuccessAsync("Success");
        }
    }
}