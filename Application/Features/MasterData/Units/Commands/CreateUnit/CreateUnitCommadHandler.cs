﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Units.Commands.CreateUnit
{
    internal class CreateUnitCommadHandler : IRequestHandler<CreateUnitRequest, Result<CreateUnitResponnseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IUnitRepository _unitRepository;

        public CreateUnitCommadHandler(IUnitOfWork unitOfWork, IMapper mapper, IUnitRepository unitRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _unitRepository = unitRepository;
        }

        public async Task<Result<CreateUnitResponnseDto>> Handle(CreateUnitRequest request, CancellationToken cancellationToken)
        {
            var positionCreate = _mapper.Map<MasterUnit>(request);
            var positionResponse = _mapper.Map<CreateUnitResponnseDto>(positionCreate);

            var validateData = await _unitRepository.ValidateData(positionCreate);

            if (validateData != true)
            {
                return await Result<CreateUnitResponnseDto>.FailureAsync(positionResponse, "Data already exist");
            }

            positionCreate.CreatedAt = DateTime.UtcNow;
            positionCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<MasterUnit>().AddAsync(positionCreate);
            positionCreate.AddDomainEvent(new UnitCreateEvent(positionCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateUnitResponnseDto>.SuccessAsync(positionResponse, "Created.");
        }
    }
}