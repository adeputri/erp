﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Units.Commands.CreateUnit
{
    public class CreateUnitRequest : IRequest<Result<CreateUnitResponnseDto>>
    {
        [JsonPropertyName("unit_name")]
        public string UnitName { get; set; }
    }
}