﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Units.Commands.CreateUnit
{
    public class UnitCreateEvent : BaseEvent
    {
        public MasterUnit Unit { get; set; }

        public UnitCreateEvent(MasterUnit unit)
        {
            Unit = unit;
        }
    }
}