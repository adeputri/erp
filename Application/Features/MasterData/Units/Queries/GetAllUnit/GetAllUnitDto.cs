﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Units.Queries.GetAllUnit
{
    public class GetAllUnitDto : IMapFrom<MasterUnit>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("unit_name")]
        public string UnitName { get; set; }
    }
}