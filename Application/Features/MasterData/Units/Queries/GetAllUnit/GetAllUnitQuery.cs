﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Units.Queries.GetAllUnit
{
    public record GetAllUnitQuery : IRequest<Result<List<GetAllUnitDto>>>;

    internal class GetAllTeamQueryHandler : IRequestHandler<GetAllUnitQuery, Result<List<GetAllUnitDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllTeamQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllUnitDto>>> Handle(GetAllUnitQuery query, CancellationToken cancellationToken)
        {
            var team = await _unitOfWork.Repository<MasterUnit>().FindByCondition(o => o.DeletedAt == null)
                           .ProjectTo<GetAllUnitDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllUnitDto>>.SuccessAsync(team, "Successfully fetch data");
        }
    }
}