﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Units.Queries.GetUnitWithPagination
{
    public record GetUnitWithPaginationQuery : IRequest<PaginatedResult<GetUnitWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetUnitWithPaginationQuery()
        {
        }
        public GetUnitWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetUnitWithPaginationQueryHandler : IRequestHandler<GetUnitWithPaginationQuery, PaginatedResult<GetUnitWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetUnitWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetUnitWithPaginationDto>> Handle(GetUnitWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<MasterUnit>()
                .FindByCondition(x => (x.DeletedAt == null) &&
                (query.SearchTerm == null || x.UnitName.ToLower().Contains(query.SearchTerm.ToLower())))
                .Select(o => new GetUnitWithPaginationDto
                {
                    Id = o.Id,
                    UnitName = o.UnitName,
                    UpdatedAt = o.UpdatedAt.Value.AddHours(7)
                })
                .OrderByDescending(x => x.UpdatedAt)
                .ProjectTo<GetUnitWithPaginationDto>(_mapper.ConfigurationProvider)
                .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}