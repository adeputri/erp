﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Units
{
    public record UnitDto
    {
        [JsonPropertyName("unit_name")]
        public string UnitName { get; set; }
    }
    public sealed record CreateUnitResponnseDto : UnitDto { }
}