﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Customers.Queries.GetCustomerWithPagination
{
    public record GetCustomerWithPaginationQuery : IRequest<PaginatedResult<GetCustomerWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public GetCustomerWithPaginationQuery()
        {
        }

        public GetCustomerWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetCustomerWithPaginationQueryHandler : IRequestHandler<GetCustomerWithPaginationQuery, PaginatedResult<GetCustomerWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetCustomerWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetCustomerWithPaginationDto>> Handle(GetCustomerWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Customer>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.CustomerName.ToLower() == query.SearchTerm.ToLower()))
            .Select(o => new GetCustomerWithPaginationDto
            {
                Id = o.Id,
                CustomerName = o.CustomerName,
                Npwp = o.Npwp,
                Address = o.Address,
                ContactPerson = o.ContactPerson,
                PhoneNumber = o.PhoneNumber,
                UpdatedAt = o.UpdatedAt.Value.AddHours(7)
            })
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetCustomerWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}