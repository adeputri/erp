﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Customers.Queries.GetAllCustomer
{
    public record GetAllCustomerQuery : IRequest<Result<List<GetAllCustomerDto>>>;

    internal class GetAllCustomerQueryHandler : IRequestHandler<GetAllCustomerQuery, Result<List<GetAllCustomerDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllCustomerQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllCustomerDto>>> Handle(GetAllCustomerQuery query, CancellationToken cancellationToken)
        {
            var customer = await _unitOfWork.Repository<Customer>().FindByCondition(o => o.DeletedAt == null)
                            .ProjectTo<GetAllCustomerDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetAllCustomerDto>>.SuccessAsync(customer, "Successfully fetch data");
        }
    }
}