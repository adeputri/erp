﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Customers.Commands.CreateCustomer
{
    public class CustomerCreatedEvent : BaseEvent
    {
        public Customer Customer { get; set; }

        public CustomerCreatedEvent(Customer customer)
        {
            Customer = customer;
        }
    }
}