﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Customers.Commands.CreateCustomer
{
    internal class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerRequest, Result<CreateCustomerResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepository;

        public CreateCustomerCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, ICustomerRepository breakeRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _customerRepository = breakeRepository;
        }

        public async Task<Result<CreateCustomerResponseDto>> Handle(CreateCustomerRequest request, CancellationToken cancellationToken)
        {
            var customerCreate = _mapper.Map<Customer>(request);
            var customerResponse = _mapper.Map<CreateCustomerResponseDto>(customerCreate);

            var validateData = await _customerRepository.ValidateData(customerCreate);

            if (validateData != true)
            {
                return await Result<CreateCustomerResponseDto>.FailureAsync(customerResponse, "Data already exist");
            }

            await _unitOfWork.Repository<Customer>().AddAsync(customerCreate);
            customerCreate.AddDomainEvent(new CustomerCreatedEvent(customerCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateCustomerResponseDto>.SuccessAsync(customerResponse, "Customer created.");
        }
    }
}