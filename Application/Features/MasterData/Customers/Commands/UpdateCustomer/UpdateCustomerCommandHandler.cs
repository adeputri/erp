﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Customers.Commands.UpdateCustomer
{
    internal class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerRequest, Result<Customer>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateCustomerCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Customer>> Handle(UpdateCustomerRequest request, CancellationToken cancellationToken)
        {
            var validateCustomer = await _unitOfWork.Repository<Customer>().GetByIdAsync(request.Id);
            if (validateCustomer == null)
            {
                return await Result<Customer>.FailureAsync("Customer not found");
            }
            validateCustomer.UpdatedAt = DateTime.UtcNow;
            validateCustomer.CustomerName = request.CustomerName;
            validateCustomer.Npwp = request.Npwp;
            validateCustomer.Address = request.Address;
            validateCustomer.PhoneNumber = request.PhoneNumber;
            validateCustomer.ContactPerson = request.ContactPerson;

            await _unitOfWork.Repository<Customer>().UpdateAsync(validateCustomer);
            validateCustomer.AddDomainEvent(new CustomerUpdateEvent(validateCustomer));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Customer>.SuccessAsync("Success");
        }
    }
}