﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Customers.Commands.UpdateCustomer
{
    public class CustomerUpdateEvent : BaseEvent
    {
        public Customer Customer { get; set; }

        public CustomerUpdateEvent(Customer customer)
        {
            Customer = customer;
        }
    }
}