﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Customers.Commands.UpdateCustomer
{
    public class UpdateCustomerRequest : IRequest<Result<Customer>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("customer_name")]
        public string CustomerName { get; set; }

        [JsonPropertyName("npwp")]
        public string Npwp { get; set; }

        [JsonPropertyName("address")]
        public string Address { get; set; }

        [JsonPropertyName("contact_person")]
        public string ContactPerson { get; set; }

        [JsonPropertyName("phone_number")]
        public string PhoneNumber { get; set; }
    }
}