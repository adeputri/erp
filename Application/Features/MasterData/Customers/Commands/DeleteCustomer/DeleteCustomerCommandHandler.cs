﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Customers.Commands.DeleteCustomer
{
    internal class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteCustomerCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteCustomerRequest request, CancellationToken cancellationToken)
        {
            var customerDelete = await _unitOfWork.Repository<Customer>().GetByIdAsync(request.Id);
            if (customerDelete != null)
            {
                customerDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<Customer>().UpdateAsync(customerDelete);
                customerDelete.AddDomainEvent(new CustomerDeletedEvent(customerDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(customerDelete.Id, "Customer deleted.");
            }
            return await Result<Guid>.FailureAsync("Customer not found");
        }
    }
}