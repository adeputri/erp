﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Customers.Commands.DeleteCustomer
{
    public class CustomerDeletedEvent : BaseEvent
    {
        public Customer Customer { get; set; }

        public CustomerDeletedEvent(Customer customer)
        {
            Customer = customer;
        }
    }
}