﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Customers.Commands.DeleteCustomer
{
    public class DeleteCustomerRequest : IRequest<Result<Guid>>, IMapFrom<Customer>
    {
        public Guid Id { get; set; }

        public DeleteCustomerRequest(Guid id)
        {
            Id = id;
        }

        public DeleteCustomerRequest()
        {
        }
    }
}