﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Customers
{
    public record CustomerDto
    {
        [JsonPropertyName("customer_name")]
        public string CustomerName { get; set; }
        [JsonPropertyName("npwp")]
        public string Npwp { get; set; }
        [JsonPropertyName("contact_person")]
        public string ContactPerson { get; set; }
        [JsonPropertyName("phone_number")]
        public string PhoneNumber { get; set; }
    }
    public sealed record CreateCustomerResponseDto : CustomerDto { }
}