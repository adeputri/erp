﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Merks.Queries.GetAllMerk
{
    public record GetAllMerkQuery : IRequest<Result<List<GetAllMerkDto>>>;

    internal class GetAllMerkQueryHandler : IRequestHandler<GetAllMerkQuery, Result<List<GetAllMerkDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllMerkQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllMerkDto>>> Handle(GetAllMerkQuery query, CancellationToken cancellationToken)
        {
            var project = await _unitOfWork.Repository<Merk>()
                .Entities.Where(o => o.DeletedAt == null)
                .AsNoTracking()
                .Select(o => new GetAllMerkDto
                {
                    Id = o.Id,
                    Name = o.Name,
                    CreatedAt = o.UpdatedAt.Value.AddHours(7),
                }).ToListAsync(cancellationToken);

            return await Result<List<GetAllMerkDto>>.SuccessAsync(project, "Successfully fetch data");
        }
    }
}