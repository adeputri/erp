﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Merks.Queries.GetMerkWithPagination
{
    public class GetMerkWithPaginationDto : IMapFrom<GetMerkWithPaginationDto>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime CreatedAt { get; set; }
    }
}