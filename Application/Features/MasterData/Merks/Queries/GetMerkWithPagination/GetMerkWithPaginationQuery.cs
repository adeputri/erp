﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Merks.Queries.GetMerkWithPagination
{
    public record GetMerkWithPaginationQuery : IRequest<PaginatedResult<GetMerkWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetMerkWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetMerkWithPaginationQuery()
        {
        }
    }

    internal class GetMerkWithPaginationQueryHandler : IRequestHandler<GetMerkWithPaginationQuery, PaginatedResult<GetMerkWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetMerkWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetMerkWithPaginationDto>> Handle(GetMerkWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Merk>()
                .Entities
                .AsNoTracking()
                .Where(o => (o.DeletedAt == null) && (query.SearchTerm == null || o.Name.ToLower().Contains(query.SearchTerm.ToLower())))
                .Select(o => new GetMerkWithPaginationDto
                {
                    Id = o.Id,
                    Name = o.Name,
                    CreatedAt = o.UpdatedAt.Value.AddHours(7),
                }).OrderByDescending(o => o.CreatedAt).
                ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}