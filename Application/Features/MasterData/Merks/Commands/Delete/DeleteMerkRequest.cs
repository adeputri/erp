﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Merks.Commands.Delete
{
    public class DeleteMerkRequest : IRequest<Result<Guid>>, IMapFrom<Merk>
    {
        public Guid Id { get; set; }

        public DeleteMerkRequest()
        { }

        public DeleteMerkRequest(Guid id)
        {
            Id = id;
        }
    }
}