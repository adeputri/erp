﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Merks.Commands.Delete
{
    public class MerkDeletedEvent : BaseEvent
    {
        public Merk Merk { get; set; }

        public MerkDeletedEvent(Merk merk)
        {
            Merk = merk;
        }
    }
}