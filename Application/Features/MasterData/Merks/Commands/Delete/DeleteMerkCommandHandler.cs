﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Merks.Commands.Delete
{
    internal class DeleteMerkCommandHandler : IRequestHandler<DeleteMerkRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteMerkCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteMerkRequest request, CancellationToken cancellationToken)
        {
            var merkDelete = await _unitOfWork.Repository<Merk>().GetByIdAsync(request.Id);
            if (merkDelete != null)
            {
                merkDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<Merk>().UpdateAsync(merkDelete);
                merkDelete.AddDomainEvent(new MerkDeletedEvent(merkDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(merkDelete.Id, "Merk deleted.");
            }
            return await Result<Guid>.FailureAsync("Merk not found");
        }
    }
}