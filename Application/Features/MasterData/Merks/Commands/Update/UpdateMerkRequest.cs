﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Merks.Commands.Update
{
    public class UpdateMerkRequest : IRequest<Result<Merk>>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}