﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Merks.Commands.Update
{
    internal class UpdateMerkCommandHandler : IRequestHandler<UpdateMerkRequest, Result<Merk>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateMerkCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Merk>> Handle(UpdateMerkRequest request, CancellationToken cancellationToken)
        {
            var validateProject = await _unitOfWork.Repository<Merk>().GetByIdAsync(request.Id);

            if (validateProject == null)
            {
                return await Result<Merk>.FailureAsync("Merk not found");
            }
            validateProject.Name = request.Name;
            await _unitOfWork.Repository<Merk>().UpdateAsync(validateProject);
            validateProject.AddDomainEvent(new MerkUpdateEvent(validateProject));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Merk>.SuccessAsync("Success");
        }
    }
}