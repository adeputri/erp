﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Merks.Commands.Update
{
    public class MerkUpdateEvent : BaseEvent
    {
        public Merk Merk { get; set; }

        public MerkUpdateEvent(Merk merk)
        {
            Merk = merk;
        }
    }
}