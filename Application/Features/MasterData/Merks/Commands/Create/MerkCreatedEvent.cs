﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Merks.Commands.CreateMerk
{
    public class MerkCreatedEvent : BaseEvent
    {
        public Merk Merk { get; set; }

        public MerkCreatedEvent(Merk merk)
        {
            Merk = merk;
        }
    }
}