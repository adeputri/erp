﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Features.MasterData.Customers;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Application.Interfaces.Repositories.Configuration;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Merks.Commands.CreateMerk
{
    internal class CreateMerkCommandHandler : IRequestHandler<CreateMerkRequest, Result<CreateMerkResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IMerkRepository _merkRepository;

        public CreateMerkCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IMerkRepository merkRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _merkRepository = merkRepository;
        }

        public async Task<Result<CreateMerkResponseDto>> Handle(CreateMerkRequest request, CancellationToken cancellationToken)
        {
            var merkCreate = _mapper.Map<Merk>(request);
            var merkResponse = _mapper.Map<CreateMerkResponseDto>(merkCreate);
            var validateData = await _merkRepository.ValidateData(merkCreate);
            if (validateData != true)
            {
                return await Result<CreateMerkResponseDto>.FailureAsync(merkResponse, "Data already exist");
            }
            await _unitOfWork.Repository<Merk>().AddAsync(merkCreate);
            merkCreate.AddDomainEvent(new MerkCreatedEvent(merkCreate));
            await _unitOfWork.Save(cancellationToken);

            var response = _mapper.Map<CreateMerkResponseDto>(merkCreate);
            return await Result<CreateMerkResponseDto>.SuccessAsync(response, "Created");
        }
    }
}