﻿using MediatR;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Merks.Commands.CreateMerk
{
    public class CreateMerkRequest : IRequest<Result<CreateMerkResponseDto>>
    {
        public string Name { get; set; }
    }
}