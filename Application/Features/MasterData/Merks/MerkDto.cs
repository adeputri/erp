﻿namespace SkeletonApi.Application.Features.MasterData.Merks
{
    public record MerkDto
    {
        public string Name { get; set; }
    }
    public sealed record CreateMerkResponseDto : MerkDto { }
}