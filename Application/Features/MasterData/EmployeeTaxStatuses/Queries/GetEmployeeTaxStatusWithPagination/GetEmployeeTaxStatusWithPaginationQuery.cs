﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Queries.GetEmployeeTaxStatusWithPagination
{
    public record GetEmployeeTaxStatusWithPaginationQuery : IRequest<PaginatedResult<GetEmployeeTaxStatusWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetEmployeeTaxStatusWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetEmployeeTaxStatusWithPaginationQuery()
        {
        }
    }

    internal class GetEmployeeTaxStatusWithPaginationQueryHandler : IRequestHandler<GetEmployeeTaxStatusWithPaginationQuery, PaginatedResult<GetEmployeeTaxStatusWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetEmployeeTaxStatusWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetEmployeeTaxStatusWithPaginationDto>> Handle(GetEmployeeTaxStatusWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<EmployeeTaxStatus>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.Status.ToLower() == query.SearchTerm.ToLower()))
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetEmployeeTaxStatusWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}