﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Queries.GetAllEmployeeTaxStatus
{
    public record GetAllEmployeeTaxStatusQuery : IRequest<Result<List<GetAllEmployeeTaxStatusDto>>>;

    internal class GetAllEmployeeTaxStatusQueryHandler : IRequestHandler<GetAllEmployeeTaxStatusQuery, Result<List<GetAllEmployeeTaxStatusDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllEmployeeTaxStatusQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllEmployeeTaxStatusDto>>> Handle(GetAllEmployeeTaxStatusQuery query, CancellationToken cancellationToken)
        {
            var employee = await _unitOfWork.Repository<EmployeeTaxStatus>().FindByCondition(o => o.DeletedAt == null)
                          .ProjectTo<GetAllEmployeeTaxStatusDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllEmployeeTaxStatusDto>>.SuccessAsync(employee, "Successfully fetch data");
        }
    }
}