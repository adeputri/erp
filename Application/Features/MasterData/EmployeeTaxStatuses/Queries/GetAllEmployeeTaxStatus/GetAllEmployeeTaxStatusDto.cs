﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Queries.GetAllEmployeeTaxStatus
{
    public class GetAllEmployeeTaxStatusDto : IMapFrom<EmployeeTaxStatus>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}