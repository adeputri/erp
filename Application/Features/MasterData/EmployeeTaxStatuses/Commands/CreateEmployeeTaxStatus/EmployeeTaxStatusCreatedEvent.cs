﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.CreateEmployeeTaxStatus
{
    public class EmployeeTaxStatusCreatedEvent : BaseEvent
    {
        public EmployeeTaxStatus EmployeeTaxStatus { get; set; }

        public EmployeeTaxStatusCreatedEvent(EmployeeTaxStatus employeeTax)
        {
            EmployeeTaxStatus = employeeTax;
        }
    }
}