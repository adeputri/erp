﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.CreateEmployeeTaxStatus
{
    internal class CreateEmployeeTaxStatusCommandHandler : IRequestHandler<CreateEmployeeTaxStatusRequest, Result<CreateEmployeeTaxStatusResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEmployeeTaxStatusRespository _employeeStatusRepository;

        public CreateEmployeeTaxStatusCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IEmployeeTaxStatusRespository employeeStatusRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _employeeStatusRepository = employeeStatusRepository;
        }

        public async Task<Result<CreateEmployeeTaxStatusResponseDto>> Handle(CreateEmployeeTaxStatusRequest request, CancellationToken cancellationToken)
        {
            var EmployeeStatusCreate = _mapper.Map<EmployeeTaxStatus>(request);
            var employeeStatusResponse = _mapper.Map<CreateEmployeeTaxStatusResponseDto>(EmployeeStatusCreate);

            var validateData = await _employeeStatusRepository.ValidateData(EmployeeStatusCreate);

            if (validateData != true)
            {
                return await Result<CreateEmployeeTaxStatusResponseDto>.FailureAsync(employeeStatusResponse, "Data already exist");
            }

            EmployeeStatusCreate.CreatedAt = DateTime.UtcNow;
            EmployeeStatusCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<EmployeeTaxStatus>().AddAsync(EmployeeStatusCreate);
            EmployeeStatusCreate.AddDomainEvent(new EmployeeTaxStatusCreatedEvent(EmployeeStatusCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateEmployeeTaxStatusResponseDto>.SuccessAsync(employeeStatusResponse, "Employee tax status Status created.");
        }
    }
}