﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.CreateEmployeeTaxStatus
{
    public class CreateEmployeeTaxStatusRequest : IRequest<Result<CreateEmployeeTaxStatusResponseDto>>
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}