﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.DeleteEmployeeTaxStatus
{
    internal class DeleteEmployeeTaxStatusCommandHandler : IRequestHandler<DeleteEmployeeTaxStatusRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteEmployeeTaxStatusCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteEmployeeTaxStatusRequest request, CancellationToken cancellationToken)
        {
            var employeeStatusDelete = await _unitOfWork.Repository<EmployeeTaxStatus>().GetByIdAsync(request.Id);
            if (employeeStatusDelete != null)
            {
                employeeStatusDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<EmployeeTaxStatus>().UpdateAsync(employeeStatusDelete);
                employeeStatusDelete.AddDomainEvent(new EmployeeTaxStatusDeletedEvent(employeeStatusDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(employeeStatusDelete.Id, "Employee tax status deleted.");
            }
            return await Result<Guid>.FailureAsync("Employee tax status not found");
        }
    }
}