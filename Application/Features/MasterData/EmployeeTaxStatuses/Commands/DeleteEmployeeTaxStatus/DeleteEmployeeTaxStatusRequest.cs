﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.DeleteEmployeeTaxStatus
{
    public class DeleteEmployeeTaxStatusRequest : IRequest<Result<Guid>>, IMapFrom<EmployeetaxStatusDto>
    {
        public Guid Id { get; set; }

        public DeleteEmployeeTaxStatusRequest(Guid id)
        {
            Id = id;
        }

        public DeleteEmployeeTaxStatusRequest()
        {
        }
    }
}