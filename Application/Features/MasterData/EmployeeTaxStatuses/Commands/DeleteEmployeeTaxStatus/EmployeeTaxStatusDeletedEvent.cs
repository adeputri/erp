﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.DeleteEmployeeTaxStatus
{
    public class EmployeeTaxStatusDeletedEvent : BaseEvent
    {
        public EmployeeTaxStatus EmployeeTaxStatus { get; set; }

        public EmployeeTaxStatusDeletedEvent(EmployeeTaxStatus employeeTaxStatus)
        {
            EmployeeTaxStatus = employeeTaxStatus;
        }
    }
}