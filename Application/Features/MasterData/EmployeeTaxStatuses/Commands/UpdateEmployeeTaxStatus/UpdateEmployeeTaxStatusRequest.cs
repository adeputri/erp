﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.UpdateEmployeeTaxStatus
{
    public class UpdateEmployeeTaxStatusRequest : IRequest<Result<EmployeeTaxStatus>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}