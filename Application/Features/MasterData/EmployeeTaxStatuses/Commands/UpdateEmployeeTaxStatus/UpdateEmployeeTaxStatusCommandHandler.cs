﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.UpdateEmployeeTaxStatus
{
    internal class UpdateEmployeeTaxStatusCommandHandler : IRequestHandler<UpdateEmployeeTaxStatusRequest, Result<EmployeeTaxStatus>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateEmployeeTaxStatusCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<EmployeeTaxStatus>> Handle(UpdateEmployeeTaxStatusRequest request, CancellationToken cancellationToken)
        {
            var validateEmployeeStatus = await _unitOfWork.Repository<EmployeeTaxStatus>().GetByIdAsync(request.Id);
            if (validateEmployeeStatus == null)
            {
                return await Result<EmployeeTaxStatus>.FailureAsync("Employee tax status not found");
            }
            validateEmployeeStatus.UpdatedAt = DateTime.UtcNow;
            validateEmployeeStatus.Status = request.Status;

            await _unitOfWork.Repository<EmployeeTaxStatus>().UpdateAsync(validateEmployeeStatus);
            validateEmployeeStatus.AddDomainEvent(new EmployeeTaxStatusUpdateEvent(validateEmployeeStatus));
            await _unitOfWork.Save(cancellationToken);

            return await Result<EmployeeTaxStatus>.SuccessAsync("Success");
        }
    }
}