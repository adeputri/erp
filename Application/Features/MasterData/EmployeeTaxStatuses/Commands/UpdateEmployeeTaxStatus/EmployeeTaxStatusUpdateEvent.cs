﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.UpdateEmployeeTaxStatus
{
    public class EmployeeTaxStatusUpdateEvent : BaseEvent
    {
        public EmployeeTaxStatus EmployeeTaxStatus { get; set; }

        public EmployeeTaxStatusUpdateEvent(EmployeeTaxStatus employeeTaxStatus)
        {
            EmployeeTaxStatus = employeeTaxStatus;
        }
    }
}