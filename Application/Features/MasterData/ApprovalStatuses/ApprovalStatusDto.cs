﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses
{
    public record ApprovalStatusDto
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
    public sealed record CreateApprovalStatusResponseDto : ApprovalStatusDto { }
}