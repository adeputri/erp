﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Queries.GetApprovalStatusWithPagination
{
    public record GetApprovalStatusWithPaginationQuery : IRequest<PaginatedResult<GetApprovalStatusWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public GetApprovalStatusWithPaginationQuery()
        {
        }

        public GetApprovalStatusWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetApprovalStatusWithPaginationQueryHandler : IRequestHandler<GetApprovalStatusWithPaginationQuery, PaginatedResult<GetApprovalStatusWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetApprovalStatusWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetApprovalStatusWithPaginationDto>> Handle(GetApprovalStatusWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<ApprovalStatus>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.Status.ToLower() == query.SearchTerm.ToLower()))
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetApprovalStatusWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}