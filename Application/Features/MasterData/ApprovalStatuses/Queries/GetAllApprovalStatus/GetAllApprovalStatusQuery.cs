﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Queries.GetAllApprovalStatus
{
    public record GetAllApprovalStatusQuery : IRequest<Result<List<GetAllApprovalStatusDto>>>;

    internal class GetAllBankQueryHandler : IRequestHandler<GetAllApprovalStatusQuery, Result<List<GetAllApprovalStatusDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllBankQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllApprovalStatusDto>>> Handle(GetAllApprovalStatusQuery query, CancellationToken cancellationToken)
        {
            var bank = await _unitOfWork.Repository<ApprovalStatus>().FindByCondition(o => o.DeletedAt == null)
                           .ProjectTo<GetAllApprovalStatusDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllApprovalStatusDto>>.SuccessAsync(bank, "Successfully fetch data");
        }
    }
}