﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Queries.GetAllApprovalStatus
{
    public class GetAllApprovalStatusDto : IMapFrom<ApprovalStatus>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}