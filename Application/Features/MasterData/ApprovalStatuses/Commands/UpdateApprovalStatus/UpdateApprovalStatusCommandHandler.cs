﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.UpdateApprovalStatus
{
    internal class UpdateApprovalStatusCommandHandler : IRequestHandler<UpdateApprovalStatusRequest, Result<ApprovalStatus>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateApprovalStatusCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<ApprovalStatus>> Handle(UpdateApprovalStatusRequest request, CancellationToken cancellationToken)
        {
            var approvalValidate = await _unitOfWork.Repository<ApprovalStatus>().GetByIdAsync(request.Id);
            if (approvalValidate == null)
            {
                return await Result<ApprovalStatus>.FailureAsync("Approval not found");
            }
            approvalValidate.UpdatedAt = DateTime.UtcNow;
            approvalValidate.Status = request.Status;

            await _unitOfWork.Repository<ApprovalStatus>().UpdateAsync(approvalValidate);
            approvalValidate.AddDomainEvent(new ApprovalStatusUpdateEvent(approvalValidate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<ApprovalStatus>.SuccessAsync("Success");
        }
    }
}