﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.UpdateApprovalStatus
{
    public class ApprovalStatusUpdateEvent : BaseEvent
    {
        public ApprovalStatus ApprovalStatus { get; set; }

        public ApprovalStatusUpdateEvent(ApprovalStatus approvalStatus)
        {
            ApprovalStatus = approvalStatus;
        }
    }
}