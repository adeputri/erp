﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.UpdateApprovalStatus
{
    public class UpdateApprovalStatusRequest : IRequest<Result<ApprovalStatus>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}