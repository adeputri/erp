﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.DeleteApprovalStatus
{
    public class ApprovalStatusDeletedEvent : BaseEvent
    {
        public ApprovalStatus ApprovalStatus { get; set; }

        public ApprovalStatusDeletedEvent(ApprovalStatus approvalStatus)
        {
            ApprovalStatus = approvalStatus;
        }
    }
}