﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.DeleteApprovalStatus
{
    public class DeleteApprovalStatusRequest : IRequest<Result<Guid>>, IMapFrom<ApprovalStatus>
    {
        public Guid Id { get; set; }

        public DeleteApprovalStatusRequest(Guid id)
        {
            Id = id;
        }

        public DeleteApprovalStatusRequest()
        {
        }
    }
}