﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.DeleteApprovalStatus
{
    internal class DeleteApprovalStatusCommandHandler : IRequestHandler<DeleteApprovalStatusRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteApprovalStatusCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteApprovalStatusRequest request, CancellationToken cancellationToken)
        {
            var bankDelete = await _unitOfWork.Repository<ApprovalStatus>().GetByIdAsync(request.Id);
            if (bankDelete != null)
            {
                bankDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<ApprovalStatus>().UpdateAsync(bankDelete);
                bankDelete.AddDomainEvent(new ApprovalStatusDeletedEvent(bankDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(bankDelete.Id, "Deleted.");
            }
            return await Result<Guid>.FailureAsync("Not found");
        }
    }
}