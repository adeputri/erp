﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.CreateApprovalStatus
{
    public class CreateApprovalStatusRequest : IRequest<Result<CreateApprovalStatusResponseDto>>
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}