﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.CreateApprovalStatus
{
    public class ApprovalStatusCreatedEvent : BaseEvent
    {
        public ApprovalStatus ApprovalStatus { get; set; }

        public ApprovalStatusCreatedEvent(ApprovalStatus approvalStatus)
        {
            ApprovalStatus = approvalStatus;
        }
    }
}