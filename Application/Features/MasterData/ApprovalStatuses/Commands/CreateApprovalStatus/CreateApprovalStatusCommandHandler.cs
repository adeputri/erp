﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.CreateApprovalStatus
{
    internal class CreateApprovalStatusCommandHandler : IRequestHandler<CreateApprovalStatusRequest, Result<CreateApprovalStatusResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IApprovalStatusRepository _approvalRepository;

        public CreateApprovalStatusCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IApprovalStatusRepository approvalRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _approvalRepository = approvalRepository;
        }

        public async Task<Result<CreateApprovalStatusResponseDto>> Handle(CreateApprovalStatusRequest request, CancellationToken cancellationToken)
        {
            var approvalCreate = _mapper.Map<ApprovalStatus>(request);
            var bankResponse = _mapper.Map<CreateApprovalStatusResponseDto>(approvalCreate);

            var validateData = await _approvalRepository.Validatedata(approvalCreate);

            if (validateData != true)
            {
                return await Result<CreateApprovalStatusResponseDto>.FailureAsync(bankResponse, "Data already exist");
            }

            approvalCreate.CreatedAt = DateTime.UtcNow;
            approvalCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<ApprovalStatus>().AddAsync(approvalCreate);
            approvalCreate.AddDomainEvent(new ApprovalStatusCreatedEvent(approvalCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateApprovalStatusResponseDto>.SuccessAsync(bankResponse, "Created.");
        }
    }
}