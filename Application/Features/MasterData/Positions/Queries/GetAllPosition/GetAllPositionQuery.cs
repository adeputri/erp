﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Positions.Queries.GetAllPosition
{
    public record GetAllPositionQuery : IRequest<Result<List<GetAllPositionDto>>>;

    internal class GetAllPositionQueryHandler : IRequestHandler<GetAllPositionQuery, Result<List<GetAllPositionDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllPositionQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllPositionDto>>> Handle(GetAllPositionQuery query, CancellationToken cancellationToken)
        {
            var position = await _unitOfWork.Repository<Position>().FindByCondition(o => o.DeletedAt == null)
                            .ProjectTo<GetAllPositionDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetAllPositionDto>>.SuccessAsync(position, "Successfully fetch data");
        }
    }
}