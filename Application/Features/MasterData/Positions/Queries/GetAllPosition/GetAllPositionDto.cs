﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Positions.Queries.GetAllPosition
{
    public class GetAllPositionDto : IMapFrom<Position>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("position_name")]
        public string PositionName { get; set; }
    }
}