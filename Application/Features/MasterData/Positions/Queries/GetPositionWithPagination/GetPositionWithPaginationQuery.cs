﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Positions.Queries.GetPositionWithPagination
{
    public record GetPositionWithPaginationQuery : IRequest<PaginatedResult<GetPositionWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetPositionWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetPositionWithPaginationQuery()
        {
        }
    }

    internal class GetPositionWithPaginationQueryHandler : IRequestHandler<GetPositionWithPaginationQuery, PaginatedResult<GetPositionWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPositionWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetPositionWithPaginationDto>> Handle(GetPositionWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Position>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.PositionName.ToLower() == query.SearchTerm.ToLower()))
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetPositionWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}