﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Positions.Commands.UpdatePosition
{
    internal class UpdatePositionCommandHandler : IRequestHandler<UpdatePositionRequest, Result<Position>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdatePositionCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Position>> Handle(UpdatePositionRequest request, CancellationToken cancellationToken)
        {
            var validatePosition = await _unitOfWork.Repository<Position>().GetByIdAsync(request.Id);
            if (validatePosition == null)
            {
                return await Result<Position>.FailureAsync("Position not found");
            }
            validatePosition.UpdatedAt = DateTime.UtcNow;
            validatePosition.PositionName = request.PositionName;

            await _unitOfWork.Repository<Position>().UpdateAsync(validatePosition);
            validatePosition.AddDomainEvent(new PositionUpdateEvent(validatePosition));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Position>.SuccessAsync("Success");
        }
    }
}