﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Positions.Commands.UpdatePosition
{
    public class UpdatePositionRequest : IRequest<Result<Position>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("position_name")]
        public string PositionName { get; set; }
    }
}