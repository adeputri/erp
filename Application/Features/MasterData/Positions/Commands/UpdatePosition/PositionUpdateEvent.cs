﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Positions.Commands.UpdatePosition
{
    public class PositionUpdateEvent : BaseEvent
    {
        public Position Position { get; set; }

        public PositionUpdateEvent(Position position)
        {
            Position = position;
        }
    }
}