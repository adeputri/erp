﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Positions.Commands.DeletePosition
{
    public class DeletePositionRequest : IRequest<Result<Guid>>, IMapFrom<Position>
    {
        public Guid Id { get; set; }

        public DeletePositionRequest(Guid id)
        {
            Id = id;
        }

        public DeletePositionRequest()
        {
        }
    }
}