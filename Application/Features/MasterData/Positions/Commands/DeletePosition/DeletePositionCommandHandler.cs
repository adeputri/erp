﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Positions.Commands.DeletePosition
{
    internal class DeletePositionCommandHandler : IRequestHandler<DeletePositionRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeletePositionCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeletePositionRequest request, CancellationToken cancellationToken)
        {
            var positionDelete = await _unitOfWork.Repository<Position>().GetByIdAsync(request.Id);
            if (positionDelete != null)
            {
                positionDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<Position>().UpdateAsync(positionDelete);
                positionDelete.AddDomainEvent(new PositionDeletedEvent(positionDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(positionDelete.Id, "Position deleted.");
            }
            return await Result<Guid>.FailureAsync("Position not found");
        }
    }
}