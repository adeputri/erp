﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Positions.Commands.DeletePosition
{
    public class PositionDeletedEvent : BaseEvent
    {
        public Position Position { get; set; }

        public PositionDeletedEvent(Position position)
        {
            Position = position;
        }
    }
}