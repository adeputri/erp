﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Positions.Commands.CreatePositions
{
    public class PositionCreatedEvent : BaseEvent
    {
        public Position Position { get; set; }

        public PositionCreatedEvent(Position position)
        {
            Position = position;
        }
    }
}