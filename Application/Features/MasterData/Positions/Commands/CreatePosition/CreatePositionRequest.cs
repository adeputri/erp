﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Positions.Commands.CreatePositions
{
    public class CreatePositionRequest : IRequest<Result<CreatePositionResponseDto>>
    {
        [JsonPropertyName("position_name")]
        public string PositionName { get; set; }
    }
}