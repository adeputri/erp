﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Positions.Commands.CreatePositions
{
    internal class CreatePositionCommandHandler : IRequestHandler<CreatePositionRequest, Result<CreatePositionResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPositionRepository _positionRepository;

        public CreatePositionCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IPositionRepository positionRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _positionRepository = positionRepository;
        }

        public async Task<Result<CreatePositionResponseDto>> Handle(CreatePositionRequest request, CancellationToken cancellationToken)
        {
            var positionCreate = _mapper.Map<Position>(request);
            var positionResponse = _mapper.Map<CreatePositionResponseDto>(positionCreate);

            var validateData = await _positionRepository.ValidateData(positionCreate);

            if (validateData != true)
            {
                return await Result<CreatePositionResponseDto>.FailureAsync(positionResponse, "Data already exist");
            }

            positionCreate.CreatedAt = DateTime.UtcNow;
            positionCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<Position>().AddAsync(positionCreate);
            positionCreate.AddDomainEvent(new PositionCreatedEvent(positionCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreatePositionResponseDto>.SuccessAsync(positionResponse, "Position created.");
        }
    }
}