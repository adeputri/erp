﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Positions
{
    public record PositionDto
    {
        [JsonPropertyName("position_name")]
        public string PositionName { get; set; }
    }
    public sealed record CreatePositionResponseDto : PositionDto { }
}