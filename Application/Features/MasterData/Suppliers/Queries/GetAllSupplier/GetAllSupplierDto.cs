﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Queries.GetAllSupplier
{
    public class GetAllSupplierDto : IMapFrom<Supplier>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("supplier_name")]
        public string SupplierName { get; set; }

        [JsonPropertyName("npwp")]
        public string Npwp { get; set; }

        [JsonPropertyName("address")]
        public string Address { get; set; }
    }
}