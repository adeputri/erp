﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Queries.GetAllSupplier
{
    public record GetAllSupplierQuery : IRequest<Result<List<GetAllSupplierDto>>>;

    internal class GetAllSupplierQueryHandler : IRequestHandler<GetAllSupplierQuery, Result<List<GetAllSupplierDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllSupplierQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllSupplierDto>>> Handle(GetAllSupplierQuery query, CancellationToken cancellationToken)
        {
            var project = await _unitOfWork.Repository<Supplier>().FindByCondition(o => o.DeletedAt == null).AsSplitQuery()
                          .ProjectTo<GetAllSupplierDto>(_mapper.ConfigurationProvider)
                          .ToListAsync(cancellationToken);

            return await Result<List<GetAllSupplierDto>>.SuccessAsync(project, "Successfully fetch data");
        }
    }
}