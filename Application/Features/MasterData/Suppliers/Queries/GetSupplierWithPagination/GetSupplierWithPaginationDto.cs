﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Queries.GetSupplierWithPagination
{
    public class GetSupplierWithPaginationDto : IMapFrom<GetSupplierWithPaginationDto>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("supplier_name")]
        public string SupplierName { get; set; }

        [JsonPropertyName("npwp")]
        public string Npwp { get; set; }

        [JsonPropertyName("address")]
        public string Address { get; set; }

        [JsonPropertyName("contact_person")]
        public string ContactPerson { get; set; }

        [JsonPropertyName("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime UpdatedAt { get; set; }
    }
}