﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Queries.GetSupplierWithPagination
{
    public record GetSupplierWithPaginationQuery : IRequest<PaginatedResult<GetSupplierWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetSupplierWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetSupplierWithPaginationQuery()
        {
        }
    }

    internal class GetSupplierWithPaginationQueryHandler : IRequestHandler<GetSupplierWithPaginationQuery, PaginatedResult<GetSupplierWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetSupplierWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetSupplierWithPaginationDto>> Handle(GetSupplierWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Supplier>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.SupplierName.ToLower().Contains(query.SearchTerm.ToLower())))
                            .Select(o => new GetSupplierWithPaginationDto
                            {
                                Id = o.Id,
                                SupplierName = o.SupplierName,
                                Npwp = o.Npwp,
                                Address = o.Address,
                                ContactPerson = o.ContactPerson,
                                PhoneNumber = o.PhoneNumber,
                                UpdatedAt = o.UpdatedAt.Value.AddHours(7),
                            })
                           .OrderByDescending(x => x.UpdatedAt)
                           .ProjectTo<GetSupplierWithPaginationDto>(_mapper.ConfigurationProvider)
                           .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}