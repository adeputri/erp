﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Suppliers
{
    public record SupplierDto
    {
        [JsonPropertyName("supplier_name")]
        public string SupplierName { get; set; }

        [JsonPropertyName("npwp")]
        public string Npwp { get; set; }
        [JsonPropertyName("address")]
        public string Address { get; set; }
        [JsonPropertyName("contact_person")]
        public string ContactPerson { get; set; }
        [JsonPropertyName("phone_number")]
        public string PhoneNumber { get; set; }
    }
    public sealed record CreateSupplierResponseDto : SupplierDto { }
}