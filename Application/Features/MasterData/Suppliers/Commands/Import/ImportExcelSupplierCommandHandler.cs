﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN.Commands.CreateReportNonPPN;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN;
using SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Commands.Import
{
    internal class ImportExcelSupplierCommandHandler : IRequestHandler<ImportExcelSupplierRequest, Result<CreateSupplierResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISupplierRepository _supplierRepository;

        public ImportExcelSupplierCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, ISupplierRepository supplierRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _supplierRepository = supplierRepository;
        }

        public async Task<Result<CreateSupplierResponseDto>> Handle(ImportExcelSupplierRequest request, CancellationToken cancellationToken)
        {
            var dataImport = await _supplierRepository.UploadExcel(request.File);
            foreach (var item in dataImport)
            {
                var supplierCreate = _mapper.Map<Supplier>(item);
                var supplierResponse = _mapper.Map<CreateSupplierResponseDto>(item);
                var validateData = await _supplierRepository.ValidateData(supplierCreate);

                if (validateData != true)
                {
                    return await Result<CreateSupplierResponseDto>.FailureAsync(supplierResponse, "Data already exist");
                }
                await _unitOfWork.Repository<Supplier>().AddAsync(supplierCreate);
                supplierCreate.AddDomainEvent(new SupplierImportExcelEvent(supplierCreate));
            }
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateSupplierResponseDto>.SuccessAsync("Created.");
        }
    }
}
