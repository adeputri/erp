﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Commands.Import
{
    public class SupplierImportExcelEvent : BaseEvent
    {
        public Supplier Supplier { get; set; }

        public SupplierImportExcelEvent(Supplier supplier)
        {
            Supplier = supplier;
        }
    }
}