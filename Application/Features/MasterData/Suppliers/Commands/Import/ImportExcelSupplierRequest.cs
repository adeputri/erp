﻿using MediatR;
using Microsoft.AspNetCore.Http;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Commands.Import
{
    public class ImportExcelSupplierRequest : IRequest<Result<CreateSupplierResponseDto>>
    {
        public IFormFile File { get; set; }

        public ImportExcelSupplierRequest(IFormFile file)
        {
            File = file;
        }
    }

    public class SupplierImportDto
    {
        public string SupplierName { get; set; }
        public string Npwp { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string PhoneNumber { get; set; }
    }
}