﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Commands.CreateSupplier
{
    internal class CreateSupplierCommandHandler : IRequestHandler<CreateSupplierRequest, Result<CreateSupplierResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ISupplierRepository _supplierRepository;

        public CreateSupplierCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, ISupplierRepository supplierRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _supplierRepository = supplierRepository;
        }

        public async Task<Result<CreateSupplierResponseDto>> Handle(CreateSupplierRequest request, CancellationToken cancellationToken)
        {
            var positionCreate = _mapper.Map<Supplier>(request);
            var positionResponse = _mapper.Map<CreateSupplierResponseDto>(positionCreate);

            var validateData = await _supplierRepository.ValidateData(positionCreate);

            if (validateData != true)
            {
                return await Result<CreateSupplierResponseDto>.FailureAsync(positionResponse, "Data already exist");
            }

            await _unitOfWork.Repository<Supplier>().AddAsync(positionCreate);
            positionCreate.AddDomainEvent(new CreateSupplierCreatedEvent(positionCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateSupplierResponseDto>.SuccessAsync(positionResponse, "Created.");
        }
    }
}