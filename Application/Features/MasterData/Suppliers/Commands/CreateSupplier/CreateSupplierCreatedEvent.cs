﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Commands.CreateSupplier
{
    public class CreateSupplierCreatedEvent : BaseEvent
    {
        public Supplier Supplier { get; set; }

        public CreateSupplierCreatedEvent(Supplier supplier)
        {
            Supplier = supplier;
        }
    }
}