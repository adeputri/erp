﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Commands.UpdateSupplier
{
    internal class UpdateSupplierCommandHandler : IRequestHandler<UpdateSupplierRequest, Result<Supplier>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateSupplierCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Supplier>> Handle(UpdateSupplierRequest request, CancellationToken cancellationToken)
        {
            var validateSupplier = await _unitOfWork.Repository<Supplier>().GetByIdAsync(request.Id);
            if (validateSupplier == null)
            {
                return await Result<Supplier>.FailureAsync("Not found");
            }
            validateSupplier.SupplierName = request.SupplierName;
            validateSupplier.Npwp = request.Npwp;
            validateSupplier.Address = request.Address;
            validateSupplier.PhoneNumber = request.PhoneNumber;
            validateSupplier.ContactPerson = request.ContactPerson;

            await _unitOfWork.Repository<Supplier>().UpdateAsync(validateSupplier);
            validateSupplier.AddDomainEvent(new SupplierUpdateEvent(validateSupplier));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Supplier>.SuccessAsync("Success");
        }
    }
}