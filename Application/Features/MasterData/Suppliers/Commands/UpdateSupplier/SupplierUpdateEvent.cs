﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Commands.UpdateSupplier
{
    public class SupplierUpdateEvent : BaseEvent
    {
        public Supplier Supplier { get; set; }

        public SupplierUpdateEvent(Supplier supplier)
        {
            Supplier = supplier;
        }
    }
}