﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Commands.DeleteSupplier
{
    public class SupplierDeletedEvent : BaseEvent
    {
        public Supplier Supplier { get; set; }

        public SupplierDeletedEvent(Supplier supplier)
        {
            Supplier = supplier;
        }
    }
}