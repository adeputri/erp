﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Suppliers.Commands.DeleteSupplier
{
    internal class DeleteSupplierCommandHandler : IRequestHandler<DeleteSupplierRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteSupplierCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteSupplierRequest request, CancellationToken cancellationToken)
        {
            var projectDelete = await _unitOfWork.Repository<Supplier>().GetByIdAsync(request.Id);
            if (projectDelete != null)
            {
                projectDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<Supplier>().UpdateAsync(projectDelete);
                projectDelete.AddDomainEvent(new SupplierDeletedEvent(projectDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(projectDelete.Id, "Deleted.");
            }
            return await Result<Guid>.FailureAsync("Not found");
        }
    }
}