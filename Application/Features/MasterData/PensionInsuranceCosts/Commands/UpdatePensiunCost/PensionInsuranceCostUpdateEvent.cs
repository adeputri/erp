﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Commands.UpdatePensiunCost
{
    public class PensionInsuranceCostUpdateEvent : BaseEvent
    {
        public PensionInsuranceCost PensionInsuranceCost { get; set; }

        public PensionInsuranceCostUpdateEvent(PensionInsuranceCost pensionInsuranceCost)
        {
            PensionInsuranceCost = pensionInsuranceCost;
        }
    }
}