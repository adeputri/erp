﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Commands.UpdatePensiunCost
{
    internal class UpdatePensionInsuranceCostCommandHandler : IRequestHandler<UpdatePensionInsuranceCostRequest, Result<PensionInsuranceCost>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdatePensionInsuranceCostCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<PensionInsuranceCost>> Handle(UpdatePensionInsuranceCostRequest request, CancellationToken cancellationToken)
        {
            var validate = await _unitOfWork.Repository<PensionInsuranceCost>().GetByIdAsync(request.Id);
            if (validate == null)
            {
                return await Result<PensionInsuranceCost>.FailureAsync("Not found");
            }
            validate.UpdatedAt = DateTime.UtcNow;
            validate.Status = request.Status;

            await _unitOfWork.Repository<PensionInsuranceCost>().UpdateAsync(validate);
            validate.AddDomainEvent(new PensionInsuranceCostUpdateEvent(validate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<PensionInsuranceCost>.SuccessAsync("Success");
        }
    }
}