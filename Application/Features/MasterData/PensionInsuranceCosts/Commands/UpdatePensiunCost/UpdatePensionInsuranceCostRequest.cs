﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Commands.UpdatePensiunCost
{
    public class UpdatePensionInsuranceCostRequest : IRequest<Result<PensionInsuranceCost>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}