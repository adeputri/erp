﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PensiunCosts.Commands.CreatePensiunCost
{
    public class PensionInsuranceCostCreatedEvent : BaseEvent
    {
        public PensionInsuranceCost PensionInsuranceCost { get; set; }

        public PensionInsuranceCostCreatedEvent(PensionInsuranceCost pensionInsuranceCost)
        {
            PensionInsuranceCost = pensionInsuranceCost;
        }
    }
}