﻿using MediatR;
using SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PensiunCosts.Commands.CreatePensiunCost
{
    public class CreatePensionInsuranceCostsRequest : IRequest<Result<CreatePensionInsuranceCostResponseDto>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}