﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PensiunCosts.Commands.CreatePensiunCost
{
    internal class CreatePensionInsuranceCostsCommandHandler : IRequestHandler<CreatePensionInsuranceCostsRequest, Result<CreatePensionInsuranceCostResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPensionInsuranceCostRepository _pensionRepository;

        public CreatePensionInsuranceCostsCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IPensionInsuranceCostRepository pensionRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _pensionRepository = pensionRepository;
        }

        public async Task<Result<CreatePensionInsuranceCostResponseDto>> Handle(CreatePensionInsuranceCostsRequest request, CancellationToken cancellationToken)
        {
            var paymentStatusCreate = _mapper.Map<PensionInsuranceCost>(request);
            var paymentStatusResponse = _mapper.Map<CreatePensionInsuranceCostResponseDto>(paymentStatusCreate);

            var validateData = await _pensionRepository.ValidateData(paymentStatusCreate);

            if (validateData != true)
            {
                return await Result<CreatePensionInsuranceCostResponseDto>.FailureAsync(paymentStatusResponse, "Data already exist");
            }

            paymentStatusCreate.CreatedAt = DateTime.UtcNow;
            paymentStatusCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<PensionInsuranceCost>().AddAsync(paymentStatusCreate);
            paymentStatusCreate.AddDomainEvent(new PensionInsuranceCostCreatedEvent(paymentStatusCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreatePensionInsuranceCostResponseDto>.SuccessAsync(paymentStatusResponse, "Created.");
        }
    }
}