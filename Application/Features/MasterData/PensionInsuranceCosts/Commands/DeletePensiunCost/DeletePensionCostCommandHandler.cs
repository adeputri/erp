﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Commands.DeletePensiunCost
{
    internal class DeletePensionCostCommandHandler : IRequestHandler<DeletePensionCostRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeletePensionCostCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeletePensionCostRequest request, CancellationToken cancellationToken)
        {
            var pensionCostDelete = await _unitOfWork.Repository<PensionInsuranceCost>().GetByIdAsync(request.Id);
            if (pensionCostDelete != null)
            {
                pensionCostDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<PensionInsuranceCost>().UpdateAsync(pensionCostDelete);
                pensionCostDelete.AddDomainEvent(new PensionCostDeleteEvent(pensionCostDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(pensionCostDelete.Id, "deleted.");
            }
            return await Result<Guid>.FailureAsync("not found");
        }
    }
}