﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Commands.DeletePensiunCost
{
    public class PensionCostDeleteEvent : BaseEvent
    {
        public PensionInsuranceCost PensionInsuranceCost { get; set; }

        public PensionCostDeleteEvent(PensionInsuranceCost pensionInsuranceCost)
        {
            PensionInsuranceCost = pensionInsuranceCost;
        }
    }
}