﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Commands.DeletePensiunCost
{
    public class DeletePensionCostRequest : IRequest<Result<Guid>>, IMapFrom<PensionInsuranceCost>
    {
        public Guid Id { get; set; }

        public DeletePensionCostRequest(Guid id)
        {
            Id = id;
        }

        public DeletePensionCostRequest()
        {
        }
    }
}