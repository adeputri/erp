﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Queries.GetPensionInsuranceCostWtihPagination
{
    public class GetPensionInsuranceCostWtihPaginationDto : IMapFrom<PensionInsuranceCost>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime UpdateAt { get; set; }
    }
}