﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Queries.GetPensionInsuranceCostWtihPagination
{
    public record GetPensionInsuranceCostWtihPaginationQuery : IRequest<PaginatedResult<GetPensionInsuranceCostWtihPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetPensionInsuranceCostWtihPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetPensionInsuranceCostWtihPaginationQuery()
        {
        }
    }

    internal class GetPensionInsuranceCostWtihPaginationQueryHandler : IRequestHandler<GetPensionInsuranceCostWtihPaginationQuery, PaginatedResult<GetPensionInsuranceCostWtihPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPensionInsuranceCostWtihPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetPensionInsuranceCostWtihPaginationDto>> Handle(GetPensionInsuranceCostWtihPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<PensionInsuranceCost>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.Status.ToLower() == query.SearchTerm.ToLower()))
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetPensionInsuranceCostWtihPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}