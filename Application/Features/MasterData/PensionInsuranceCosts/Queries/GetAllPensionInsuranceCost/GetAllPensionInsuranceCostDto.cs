﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Queries.GetAllPensionInsuranceCost
{
    public class GetAllPensionInsuranceCostDto : IMapFrom<PensionInsuranceCost>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}