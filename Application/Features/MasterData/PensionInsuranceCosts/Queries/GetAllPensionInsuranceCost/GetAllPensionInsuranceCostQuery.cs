﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Queries.GetAllPensionInsuranceCost
{
    public record GetAllPensionInsuranceCostQuery : IRequest<Result<List<GetAllPensionInsuranceCostDto>>>;

    internal class GetAllPensionInsuranceCostQueryHandler : IRequestHandler<GetAllPensionInsuranceCostQuery, Result<List<GetAllPensionInsuranceCostDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllPensionInsuranceCostQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllPensionInsuranceCostDto>>> Handle(GetAllPensionInsuranceCostQuery query, CancellationToken cancellationToken)
        {
            var pensionCost = await _unitOfWork.Repository<PensionInsuranceCost>().FindByCondition(o => o.DeletedAt == null)
                            .ProjectTo<GetAllPensionInsuranceCostDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllPensionInsuranceCostDto>>.SuccessAsync(pensionCost, "Successfully fetch data");
        }
    }
}