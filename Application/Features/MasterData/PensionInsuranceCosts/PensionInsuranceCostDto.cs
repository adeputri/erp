﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts
{
    public record PensionInsuranceCostDto
    {
        public Guid Id { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
    public sealed record CreatePensionInsuranceCostResponseDto : PensionInsuranceCostDto { }
}