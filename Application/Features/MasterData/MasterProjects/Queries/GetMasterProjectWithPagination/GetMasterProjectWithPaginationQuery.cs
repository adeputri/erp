﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetMasterProjectWithPagination
{
    public record GetMasterProjectWithPaginationQuery : IRequest<PaginatedResult<GetMasterProjectWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetMasterProjectWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetMasterProjectWithPaginationQuery()
        {
        }
    }

    internal class GetProjectWithPaginationQueryHandler : IRequestHandler<GetMasterProjectWithPaginationQuery, PaginatedResult<GetMasterProjectWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetProjectWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetMasterProjectWithPaginationDto>> Handle(GetMasterProjectWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<MasterProject>()
                .Entities
                .AsNoTracking()
                .Include(o => o.Customer)
                .Where(o => query.SearchTerm == null || o.ProjectName.Contains(query.SearchTerm, StringComparison.OrdinalIgnoreCase))
                .Select(o => new GetMasterProjectWithPaginationDto
                {
                    Id = o.Id,
                    ProjectName = o.ProjectName,
                    CustomerId = o.CustomerId,
                    CustomerName = o.Customer.CustomerName,
                    UpdatedAt = o.UpdatedAt.Value.AddHours(7),
                }).OrderByDescending(o => o.UpdatedAt).
                ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}