﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetMasterProjectWithPagination
{
    public class GetMasterProjectWithPaginationDto : IMapFrom<MasterProject>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("project_name")]
        public string ProjectName { get; set; }
        [JsonPropertyName("customer_id")]
        public Guid CustomerId { get; set; }
        [JsonPropertyName("customer_name")]
        public string CustomerName { get; set; }
        [JsonPropertyName("updated_at")]
        public DateTime? UpdatedAt { get; set; }
    }
}