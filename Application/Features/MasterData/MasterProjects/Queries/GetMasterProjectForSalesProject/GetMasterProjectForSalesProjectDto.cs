﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetMasterProjectForSalesProject
{
    public class GetMasterProjectForSalesProjectDto : IMapFrom<MasterProject>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("project_name")]
        public string ProjectName { get; set; }
        [JsonPropertyName("customer_name")]
        public string CustomerName { get; set; }
    }
}