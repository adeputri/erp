﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetMasterProjectForSalesProject
{
    public record GetMasterProjectForSalesProjectQuery : IRequest<Result<List<GetMasterProjectForSalesProjectDto>>>;

    internal class GetAllProjectQueryHandler : IRequestHandler<GetMasterProjectForSalesProjectQuery, Result<List<GetMasterProjectForSalesProjectDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllProjectQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetMasterProjectForSalesProjectDto>>> Handle(GetMasterProjectForSalesProjectQuery query, CancellationToken cancellationToken)
        {
            var idMasterProject = await _unitOfWork.Repository<Project>()
                     .Entities
                     .Select(b => b.MasterProjectId)
                     .ToListAsync(cancellationToken);

            var project = await _unitOfWork.Repository<MasterProject>()
                .FindByCondition(o => !idMasterProject.Contains(o.Id))
                .AsNoTracking()
                .Include(o => o.Customer)
                .Select(o => new GetMasterProjectForSalesProjectDto
                {
                    Id = o.Id,
                    ProjectName = o.ProjectName,
                    CustomerName = o.Customer.CustomerName
                }).ToListAsync(cancellationToken);

            return await Result<List<GetMasterProjectForSalesProjectDto>>.SuccessAsync(project, "Successfully fetch data");
        }
    }
}