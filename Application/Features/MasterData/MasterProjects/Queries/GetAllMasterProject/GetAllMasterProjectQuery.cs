﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetAllMasterProject
{
    public record GetAllMasterProjectQuery : IRequest<Result<List<GetAllMasterProjectDto>>>;

    internal class GetAllProjectQueryHandler : IRequestHandler<GetAllMasterProjectQuery, Result<List<GetAllMasterProjectDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllProjectQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllMasterProjectDto>>> Handle(GetAllMasterProjectQuery query, CancellationToken cancellationToken)
        {
            var project = await _unitOfWork.Repository<MasterProject>()
                .Entities
                .AsNoTracking()
                .Include(o => o.Customer)
                .Select(o => new GetAllMasterProjectDto
                {
                    Id = o.Id,
                    CustomerName = o.Customer.CustomerName,
                    ProjectName = o.ProjectName,
                    UpdatedAt = o.UpdatedAt,
                }).ToListAsync(cancellationToken);

            return await Result<List<GetAllMasterProjectDto>>.SuccessAsync(project, "Successfully fetch data");
        }
    }
}