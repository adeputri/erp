﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetCustomerByIdMasterProject
{
    public record GetCustomerByIdMasterProjectQuery : IRequest<Result<GetCustomerByIdMasterProjectDto>>
    {
        public Guid Id { get; set; }
        public GetCustomerByIdMasterProjectQuery(Guid id)
        {
            Id = id;
        }
    }

    internal class GetCustomerByIdMasterProjectQueryHandler : IRequestHandler<GetCustomerByIdMasterProjectQuery, Result<GetCustomerByIdMasterProjectDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetCustomerByIdMasterProjectQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<GetCustomerByIdMasterProjectDto>> Handle(GetCustomerByIdMasterProjectQuery query, CancellationToken cancellationToken)
        {
            var customer = await _unitOfWork.Repository<MasterProject>().FindByCondition(o => (o.DeletedAt == null) && (o.Id == query.Id))
                            .Select(p => new GetCustomerByIdMasterProjectDto
                            {
                                CustomerName = p.Customer.CustomerName,
                            }).Distinct()
                            .ProjectTo<GetCustomerByIdMasterProjectDto>(_mapper.ConfigurationProvider)
                            .FirstOrDefaultAsync(cancellationToken);

            return await Result<GetCustomerByIdMasterProjectDto>.SuccessAsync(customer, "Successfully fetch data");
        }
    }
}