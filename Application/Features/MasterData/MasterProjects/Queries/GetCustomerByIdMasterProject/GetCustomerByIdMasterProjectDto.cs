﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetCustomerByIdMasterProject
{
    public class GetCustomerByIdMasterProjectDto : IMapFrom<GetCustomerByIdMasterProjectDto>
    {
        [JsonPropertyName("customer_name")]
        public string CustomerName { get; set; }
    }
}