﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.CreateMasterProject
{
    public class CreateMasterProjectRequest : IRequest<Result<CreateProjectResponseDto>>
    {
        [JsonPropertyName("project_name")]
        public string ProjectName { get; set; }
        [JsonPropertyName("customer_id")]
        public Guid CustomerId { get; set; }
    }
}