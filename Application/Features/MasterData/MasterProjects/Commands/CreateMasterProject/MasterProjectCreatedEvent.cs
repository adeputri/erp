﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.CreateMasterProject
{
    public class MasterProjectCreatedEvent : BaseEvent
    {
        public MasterProject Project { get; set; }

        public MasterProjectCreatedEvent(MasterProject project)
        {
            Project = project;
        }
    }
}