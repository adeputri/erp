﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Common.Exceptions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.CreateMasterProject
{
    internal class CreateMasterProjectCommandHandler : IRequestHandler<CreateMasterProjectRequest, Result<CreateProjectResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IMasterProjectRepository _masterProjectRepository;

        public CreateMasterProjectCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IMasterProjectRepository masterProjectRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _masterProjectRepository = masterProjectRepository;
        }

        public async Task<Result<CreateProjectResponseDto>> Handle(CreateMasterProjectRequest request, CancellationToken cancellationToken)
        {
            var project = _mapper.Map<MasterProject>(request);

            var validateData = await _masterProjectRepository.ValidateData(project);

            if (validateData != true)
            {
                ProjectAlreadyExistsException.Throw(request.ProjectName, request.CustomerId);
            }

            await _unitOfWork.Repository<MasterProject>().AddAsync(project);
            project.AddDomainEvent(new MasterProjectCreatedEvent(project));
            await _unitOfWork.Save(cancellationToken);

            var response = _mapper.Map<CreateProjectResponseDto>(project);
            return await Result<CreateProjectResponseDto>.SuccessAsync(response, "Master Project Created");
        }
    }
}