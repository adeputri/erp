﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.UpdateMasterProject
{
    public class MasterProjectUpdateEvent : BaseEvent
    {
        public MasterProject Project { get; set; }

        public MasterProjectUpdateEvent(MasterProject project)
        {
            Project = project;
        }
    }
}