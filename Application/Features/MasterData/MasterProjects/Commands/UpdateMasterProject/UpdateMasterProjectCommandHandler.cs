﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.UpdateMasterProject
{
    internal class UpdateMasterProjectCommandHandler : IRequestHandler<UpdateMasterProjectRequest, Result<UpdateProjectResponseDto>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateMasterProjectCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<UpdateProjectResponseDto>> Handle(UpdateMasterProjectRequest request, CancellationToken cancellationToken)
        {
            var validateProject = await _unitOfWork.Repository<MasterProject>().GetByIdAsync(request.Id);

            if (validateProject == null)
            {
                return await Result<UpdateProjectResponseDto>.FailureAsync("Project not found");
            }

            validateProject.UpdatedAt = DateTime.UtcNow;
            validateProject.ProjectName = request.ProjectName;
            validateProject.CustomerId = request.CustomerId;

            await _unitOfWork.Repository<MasterProject>().UpdateAsync(validateProject);
            validateProject.AddDomainEvent(new MasterProjectUpdateEvent(validateProject));
            await _unitOfWork.Save(cancellationToken);

            return await Result<UpdateProjectResponseDto>.SuccessAsync("Success");
        }
    }
}