﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.UpdateMasterProject
{
    public class UpdateMasterProjectRequest : IRequest<Result<UpdateProjectResponseDto>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("project_name")]
        public string ProjectName { get; set; }
        [JsonPropertyName("customer_id")]
        public Guid CustomerId { get; set; }
    }
}