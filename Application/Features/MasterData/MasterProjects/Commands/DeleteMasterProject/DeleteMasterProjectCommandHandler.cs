﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.DeleteMasterProject
{
    internal class DeleteMasterProjectCommandHandler : IRequestHandler<DeleteMasterProjectRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteMasterProjectCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteMasterProjectRequest request, CancellationToken cancellationToken)
        {
            var projectDelete = await _unitOfWork.Repository<MasterProject>().GetByIdAsync(request.Id);
            if (projectDelete != null)
            {
                projectDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<MasterProject>().UpdateAsync(projectDelete);
                projectDelete.AddDomainEvent(new MasterProjectDeletedEvent(projectDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(projectDelete.Id, "Project deleted.");
            }
            return await Result<Guid>.FailureAsync("Project not found");
        }
    }
}