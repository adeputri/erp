﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.DeleteMasterProject
{
    public class DeleteMasterProjectRequest : IRequest<Result<Guid>>, IMapFrom<MasterProject>
    {
        public Guid Id { get; set; }

        public DeleteMasterProjectRequest(Guid id)
        {
            Id = id;
        }

        public DeleteMasterProjectRequest()
        {
        }
    }
}