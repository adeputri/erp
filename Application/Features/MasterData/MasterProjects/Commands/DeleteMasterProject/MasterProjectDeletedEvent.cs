﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.DeleteMasterProject
{
    public class MasterProjectDeletedEvent : BaseEvent
    {
        public MasterProject Project { get; set; }

        public MasterProjectDeletedEvent(MasterProject project)
        {
            Project = project;
        }
    }
}