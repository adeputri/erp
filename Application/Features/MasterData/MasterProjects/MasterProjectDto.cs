﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.MasterProjects
{
    public record MasterProjectDto
    {
        [JsonPropertyName("project_name")]
        public string ProjectName { get; set; }
        [JsonPropertyName("customer_name")]
        public string CustomerName { get; set; }
    }
    public sealed record CreateProjectResponseDto : MasterProjectDto { }
    public sealed record UpdateProjectResponseDto : MasterProjectDto { }
}