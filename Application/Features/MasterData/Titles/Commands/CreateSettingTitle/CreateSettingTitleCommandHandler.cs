﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Title.Commands.CreateSettingTitle
{
    internal class CreateSettingTitleCommandHandler : IRequestHandler<CreateSettingTitleRequest, Result<CreateSettingTitleResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateSettingTitleCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<CreateSettingTitleResponseDto>> Handle(CreateSettingTitleRequest request, CancellationToken cancellationToken)
        {
            var titleCreate = _mapper.Map<SettingTitle>(request);
            var positionResponse = _mapper.Map<CreateSettingTitleResponseDto>(titleCreate);

            await _unitOfWork.Repository<SettingTitle>().AddAsync(titleCreate);
            titleCreate.AddDomainEvent(new SettingTitleCreatedEvent(titleCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateSettingTitleResponseDto>.SuccessAsync(positionResponse, "Created.");
        }
    }
}