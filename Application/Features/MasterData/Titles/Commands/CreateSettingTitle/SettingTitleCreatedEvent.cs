﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Title.Commands.CreateSettingTitle
{
    public class SettingTitleCreatedEvent : BaseEvent
    {
        public SettingTitle SettingTitle { get; set; }

        public SettingTitleCreatedEvent(SettingTitle settingTitle)
        {
            SettingTitle = settingTitle;
        }
    }
}