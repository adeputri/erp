﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Title.Commands.CreateSettingTitle
{
    public class CreateSettingTitleRequest : IRequest<Result<CreateSettingTitleResponseDto>>
    {
        [JsonPropertyName("title_name")]
        public string TitleName { get; set; }
    }
}