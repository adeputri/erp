﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Title.Commands.UpdateSettingTitle
{
    public class UpdateSettingTitleRequest : IRequest<Result<SettingTitle>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("title_name")]
        public string TitleName { get; set; }
    }
}