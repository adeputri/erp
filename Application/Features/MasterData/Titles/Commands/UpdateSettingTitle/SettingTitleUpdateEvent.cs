﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Title.Commands.UpdateSettingTitle
{
    public class SettingTitleUpdateEvent : BaseEvent
    {
        public SettingTitle SettingTitle { get; set; }

        public SettingTitleUpdateEvent(SettingTitle settingTitle)
        {
            SettingTitle = settingTitle;
        }
    }
}