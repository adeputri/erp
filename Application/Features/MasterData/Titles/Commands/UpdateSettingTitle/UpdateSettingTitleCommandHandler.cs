﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Title.Commands.UpdateSettingTitle
{
    internal class UpdateSettingTitleCommandHandler : IRequestHandler<UpdateSettingTitleRequest, Result<SettingTitle>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateSettingTitleCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<SettingTitle>> Handle(UpdateSettingTitleRequest request, CancellationToken cancellationToken)
        {
            var validateTitle = await _unitOfWork.Repository<SettingTitle>().GetByIdAsync(request.Id);
            if (validateTitle == null)
            {
                return await Result<SettingTitle>.FailureAsync("Team not found");
            }
            validateTitle.TitleName = request.TitleName;

            await _unitOfWork.Repository<SettingTitle>().UpdateAsync(validateTitle);
            validateTitle.AddDomainEvent(new SettingTitleUpdateEvent(validateTitle));
            await _unitOfWork.Save(cancellationToken);

            return await Result<SettingTitle>.SuccessAsync("Success");
        }
    }
}