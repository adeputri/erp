﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Title
{
    public record SettingTitleDto
    {
        [JsonPropertyName("title_name")]
        public string TitleName { get; set; }
    }
    public sealed record CreateSettingTitleResponseDto : SettingTitleDto { }
}