﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Queries.GetEmployeeLevelWithPagination
{
    public class GetEmployeeLevelWithPaginationDto : IMapFrom<EmployeeLevel>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("level")]
        public string Level { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime UpdateAt { get; set; }
    }
}