﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Queries.GetEmployeeLevelWithPagination
{
    public record GetEmployeeLevelWithPaginationQuery : IRequest<PaginatedResult<GetEmployeeLevelWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public GetEmployeeLevelWithPaginationQuery()
        {
        }

        public GetEmployeeLevelWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetEmployeeLevelWithPaginationQueryHandler : IRequestHandler<GetEmployeeLevelWithPaginationQuery, PaginatedResult<GetEmployeeLevelWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetEmployeeLevelWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetEmployeeLevelWithPaginationDto>> Handle(GetEmployeeLevelWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<EmployeeLevel>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.Level.ToLower() == query.SearchTerm.ToLower()))
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetEmployeeLevelWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}