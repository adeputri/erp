﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Queries.GetAllEmployeeLevel
{
    public class GetAllEmployeeLevelDto : IMapFrom<EmployeeLevel>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("level")]
        public string Level { get; set; }
    }
}