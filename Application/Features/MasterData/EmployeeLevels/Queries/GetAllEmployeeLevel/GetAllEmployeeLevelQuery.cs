﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Queries.GetAllEmployeeLevel
{
    public record GetAllEmployeeLevelQuery : IRequest<Result<List<GetAllEmployeeLevelDto>>>;

    internal class GetAllEmployeeLevelQueryHandler : IRequestHandler<GetAllEmployeeLevelQuery, Result<List<GetAllEmployeeLevelDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllEmployeeLevelQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllEmployeeLevelDto>>> Handle(GetAllEmployeeLevelQuery query, CancellationToken cancellationToken)
        {
            var employeeLevel = await _unitOfWork.Repository<EmployeeLevel>().FindByCondition(o => o.DeletedAt == null)
                           .ProjectTo<GetAllEmployeeLevelDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllEmployeeLevelDto>>.SuccessAsync(employeeLevel, "Successfully fetch data");
        }
    }
}