﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels
{
    public record EmployeeLevelDto
    {
        [JsonPropertyName("level")]
        public string Level { get; set; }
    }
    public sealed record CreateEmployeeLevelResponseDto : EmployeeLevelDto { }
}