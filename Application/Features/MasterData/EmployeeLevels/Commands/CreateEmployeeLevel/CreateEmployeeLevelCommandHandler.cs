﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.CreateEmployeeLevel
{
    internal class CreateEmployeeLevelCommandHandler : IRequestHandler<CreateEmployeeLevelRequest, Result<CreateEmployeeLevelResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IEmployeeLevelRepository _employeeLevelRepository;

        public CreateEmployeeLevelCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IEmployeeLevelRepository employeeLevelRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _employeeLevelRepository = employeeLevelRepository;
        }

        public async Task<Result<CreateEmployeeLevelResponseDto>> Handle(CreateEmployeeLevelRequest request, CancellationToken cancellationToken)
        {
            var employeeCreate = _mapper.Map<EmployeeLevel>(request);
            var customerResponse = _mapper.Map<CreateEmployeeLevelResponseDto>(employeeCreate);

            var validateData = await _employeeLevelRepository.ValidateData(employeeCreate);

            if (validateData != true)
            {
                return await Result<CreateEmployeeLevelResponseDto>.FailureAsync(customerResponse, "Data already exist");
            }

            employeeCreate.CreatedAt = DateTime.UtcNow;
            employeeCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<EmployeeLevel>().AddAsync(employeeCreate);
            employeeCreate.AddDomainEvent(new EmployeeLevelCreatedEvent(employeeCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateEmployeeLevelResponseDto>.SuccessAsync(customerResponse, "Employee Level created.");
        }
    }
}