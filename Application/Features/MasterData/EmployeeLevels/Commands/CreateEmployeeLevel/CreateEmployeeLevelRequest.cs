﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.CreateEmployeeLevel
{
    public class CreateEmployeeLevelRequest : IRequest<Result<CreateEmployeeLevelResponseDto>>
    {
        [JsonPropertyName("level")]
        public string Level { get; set; }
    }
}