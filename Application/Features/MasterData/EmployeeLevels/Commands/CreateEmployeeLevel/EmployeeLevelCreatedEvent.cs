﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.CreateEmployeeLevel
{
    public class EmployeeLevelCreatedEvent : BaseEvent
    {
        public EmployeeLevel EmployeeLevel { get; set; }

        public EmployeeLevelCreatedEvent(EmployeeLevel employeeLevel)
        {
            EmployeeLevel = employeeLevel;
        }
    }
}