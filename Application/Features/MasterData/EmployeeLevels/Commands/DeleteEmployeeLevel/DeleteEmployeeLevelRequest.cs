﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.DeleteEmployeeLevel
{
    public class DeleteEmployeeLevelRequest : IRequest<Result<Guid>>, IMapFrom<EmployeeLevel>
    {
        public Guid Id { get; set; }

        public DeleteEmployeeLevelRequest(Guid id)
        {
            Id = id;
        }

        public DeleteEmployeeLevelRequest()
        {
        }
    }
}