﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.DeleteEmployeeLevel
{
    internal class DeleteEmployeeLevelCommandHandler : IRequestHandler<DeleteEmployeeLevelRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteEmployeeLevelCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteEmployeeLevelRequest request, CancellationToken cancellationToken)
        {
            var employeeDelete = await _unitOfWork.Repository<EmployeeLevel>().GetByIdAsync(request.Id);
            if (employeeDelete != null)
            {
                employeeDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<EmployeeLevel>().UpdateAsync(employeeDelete);
                employeeDelete.AddDomainEvent(new EmployeeLevelDeletedEvent(employeeDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(employeeDelete.Id, "Employee level deleted.");
            }
            return await Result<Guid>.FailureAsync("Employee level not found");
        }
    }
}