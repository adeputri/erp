﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.DeleteEmployeeLevel
{
    public class EmployeeLevelDeletedEvent : BaseEvent
    {
        public EmployeeLevel EmployeeLevel { get; set; }

        public EmployeeLevelDeletedEvent(EmployeeLevel employeeLevel)
        {
            EmployeeLevel = employeeLevel;
        }
    }
}