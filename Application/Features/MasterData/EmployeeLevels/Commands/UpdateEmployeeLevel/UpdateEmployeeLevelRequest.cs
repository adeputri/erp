﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.UpdateEmployeeLevel
{
    public class UpdateEmployeeLevelRequest : IRequest<Result<EmployeeLevel>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("level")]
        public string Level { get; set; }
    }
}