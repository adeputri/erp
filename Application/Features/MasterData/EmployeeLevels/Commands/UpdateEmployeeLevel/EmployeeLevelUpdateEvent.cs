﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.UpdateEmployeeLevel
{
    public class EmployeeLevelUpdateEvent : BaseEvent
    {
        public EmployeeLevel EmployeeLevel { get; set; }

        public EmployeeLevelUpdateEvent(EmployeeLevel employeeLevel)
        {
            EmployeeLevel = employeeLevel;
        }
    }
}