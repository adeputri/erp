﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.UpdateEmployeeLevel
{
    internal class UpdateEmployeeLevelCommandHandler : IRequestHandler<UpdateEmployeeLevelRequest, Result<EmployeeLevel>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateEmployeeLevelCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<EmployeeLevel>> Handle(UpdateEmployeeLevelRequest request, CancellationToken cancellationToken)
        {
            var validateEmployeeLevel = await _unitOfWork.Repository<EmployeeLevel>().GetByIdAsync(request.Id);
            if (validateEmployeeLevel == null)
            {
                return await Result<EmployeeLevel>.FailureAsync("Employee level not found");
            }
            validateEmployeeLevel.UpdatedAt = DateTime.UtcNow;
            validateEmployeeLevel.Level = request.Level;

            await _unitOfWork.Repository<EmployeeLevel>().UpdateAsync(validateEmployeeLevel);
            validateEmployeeLevel.AddDomainEvent(new EmployeeLevelUpdateEvent(validateEmployeeLevel));
            await _unitOfWork.Save(cancellationToken);

            return await Result<EmployeeLevel>.SuccessAsync("Success");
        }
    }
}