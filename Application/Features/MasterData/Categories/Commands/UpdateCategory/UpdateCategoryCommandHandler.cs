﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Categories.Commands.UpdateCategory
{
    internal class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryRequest, Result<Category>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateCategoryCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Category>> Handle(UpdateCategoryRequest request, CancellationToken cancellationToken)
        {
            var validateCustomer = await _unitOfWork.Repository<Category>().GetByIdAsync(request.Id);
            if (validateCustomer == null)
            {
                return await Result<Category>.FailureAsync("Category not found");
            }
            validateCustomer.CategoryName = request.CategoryName;

            await _unitOfWork.Repository<Category>().UpdateAsync(validateCustomer);
            validateCustomer.AddDomainEvent(new CategoryUpdateEvent(validateCustomer));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Category>.SuccessAsync("Success");
        }
    }
}