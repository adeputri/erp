﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Categories.Commands.UpdateCategory
{
    public class CategoryUpdateEvent : BaseEvent
    {
        public Category Category { get; set; }

        public CategoryUpdateEvent(Category category)
        {
            Category = category;
        }
    }
}