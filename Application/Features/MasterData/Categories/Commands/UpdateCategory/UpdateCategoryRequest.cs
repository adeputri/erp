﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Categories.Commands.UpdateCategory
{
    public class UpdateCategoryRequest : IRequest<Result<Category>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("category_name")]
        public string CategoryName { get; set; }
    }
}