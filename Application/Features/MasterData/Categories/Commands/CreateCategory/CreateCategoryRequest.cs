﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Categories.Commands.CreateCategory
{
    public class CreateCategoryRequest : IRequest<Result<CreateCategoryResponseDto>>
    {
        [JsonPropertyName("category_name")]
        public string CategoryName { get; set; }
    }
}