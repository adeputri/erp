﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Categories.Commands.CreateCategory
{
    internal class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryRequest, Result<CreateCategoryResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICategoryRepository _categoryRepository;

        public CreateCategoryCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, ICategoryRepository categoryRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _categoryRepository = categoryRepository;
        }

        public async Task<Result<CreateCategoryResponseDto>> Handle(CreateCategoryRequest request, CancellationToken cancellationToken)
        {
            var categoryCreate = _mapper.Map<Category>(request);
            var categoryResponse = _mapper.Map<CreateCategoryResponseDto>(categoryCreate);

            var validateData = await _categoryRepository.ValidateData(categoryCreate);

            if (validateData != true)
            {
                return await Result<CreateCategoryResponseDto>.FailureAsync(categoryResponse, "Data already exist");
            }

            await _unitOfWork.Repository<Category>().AddAsync(categoryCreate);
            categoryCreate.AddDomainEvent(new CategoryCreatedEvent(categoryCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateCategoryResponseDto>.SuccessAsync(categoryResponse, "Category created.");
        }
    }
}