﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Categories.Commands.CreateCategory
{
    public class CategoryCreatedEvent : BaseEvent
    {
        public Category Category { get; set; }

        public CategoryCreatedEvent(Category category)
        {
            Category = category;
        }
    }
}