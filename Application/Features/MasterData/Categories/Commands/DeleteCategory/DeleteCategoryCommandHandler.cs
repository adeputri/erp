﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Categories.Commands.DeleteCategory
{
    internal class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteCategoryCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteCategoryRequest request, CancellationToken cancellationToken)
        {
            var customerDelete = await _unitOfWork.Repository<Category>().GetByIdAsync(request.Id);
            if (customerDelete != null)
            {
                customerDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<Category>().UpdateAsync(customerDelete);
                customerDelete.AddDomainEvent(new CategoryDeletedEvent(customerDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(customerDelete.Id, "Deleted.");
            }
            return await Result<Guid>.FailureAsync("Id not found");
        }
    }
}