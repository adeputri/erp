﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Categories.Commands.DeleteCategory
{
    public class DeleteCategoryRequest : IRequest<Result<Guid>>, IMapFrom<Category>
    {
        public Guid Id { get; set; }

        public DeleteCategoryRequest(Guid id)
        {
            Id = id;
        }

        public DeleteCategoryRequest()
        {
        }
    }
}