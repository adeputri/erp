﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Categories.Commands.DeleteCategory
{
    public class CategoryDeletedEvent : BaseEvent
    {
        public Category Category { get; set; }

        public CategoryDeletedEvent(Category category)
        {
            Category = category;
        }
    }
}