﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Categories
{
    public record CategoryDto
    {
        [JsonPropertyName("category_name")]
        public string CategoryName { get; set; }
    }
    public sealed record CreateCategoryResponseDto : CategoryDto { }
}