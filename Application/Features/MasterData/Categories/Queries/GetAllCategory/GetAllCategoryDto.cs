﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Categories.Queries.GetAllCategory
{
    public class GetAllCategoryDto : IMapFrom<Category>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("category_name")]
        public string CategoryName { get; set; }

        [JsonPropertyName("created_at")]
        public string UpdateAt { get; set; }
    }
}