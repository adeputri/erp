﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Categories.Queries.GetAllCategory
{
    public record GetAllCategoryQuery : IRequest<Result<List<GetAllCategoryDto>>>;

    internal class GetAllCategoryQueryHandler : IRequestHandler<GetAllCategoryQuery, Result<List<GetAllCategoryDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllCategoryQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllCategoryDto>>> Handle(GetAllCategoryQuery query, CancellationToken cancellationToken)
        {
            var category = await _unitOfWork.Repository<Category>().FindByCondition(o => o.DeletedAt == null)
                            .ProjectTo<GetAllCategoryDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetAllCategoryDto>>.SuccessAsync(category, "Successfully fetch data");
        }
    }
}