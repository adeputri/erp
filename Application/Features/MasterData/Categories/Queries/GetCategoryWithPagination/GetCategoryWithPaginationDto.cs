﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Categories.Queries.GetCategoryWithPagination
{
    public class GetCategoryWithPaginationDto : IMapFrom<GetCategoryWithPaginationDto>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("category_name")]
        public string CategoryName { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime UpdatedAt { get; set; }
    }
}