﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Categories.Queries.GetCategoryWithPagination
{
    public record GetCategoryWithPaginationQuery : IRequest<PaginatedResult<GetCategoryWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public GetCategoryWithPaginationQuery()
        {
        }

        public GetCategoryWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetCategoryWithPaginationQueryHandler : IRequestHandler<GetCategoryWithPaginationQuery, PaginatedResult<GetCategoryWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetCategoryWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetCategoryWithPaginationDto>> Handle(GetCategoryWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Category>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.CategoryName.ToLower().Contains(query.SearchTerm.ToLower())))
                .Select(o => new GetCategoryWithPaginationDto
                {
                    Id = o.Id,
                    CategoryName = o.CategoryName,
                    UpdatedAt = o.UpdatedAt.Value.AddHours(7)
                })
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetCategoryWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}