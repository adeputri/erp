﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.DeleteProrateSetting
{
    public class ProrateSettingDeletedEvent : BaseEvent
    {
        public ProrateSetting ProrateSetting { get; set; }

        public ProrateSettingDeletedEvent(ProrateSetting prorateSetting)
        {
            ProrateSetting = prorateSetting;
        }
    }
}