﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.DeleteProrateSetting
{
    public class DeleteProrateSettingRequest : IRequest<Result<Guid>>, IMapFrom<ProrateSetting>
    {
        public Guid Id { get; set; }

        public DeleteProrateSettingRequest(Guid id)
        {
            Id = id;
        }

        public DeleteProrateSettingRequest()
        {
        }
    }
}