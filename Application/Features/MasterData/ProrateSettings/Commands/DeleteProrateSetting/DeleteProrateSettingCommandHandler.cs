﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.DeleteProrateSetting
{
    internal class DeleteProrateSettingCommandHandler : IRequestHandler<DeleteProrateSettingRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteProrateSettingCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteProrateSettingRequest request, CancellationToken cancellationToken)
        {
            var projectDelete = await _unitOfWork.Repository<ProrateSetting>().GetByIdAsync(request.Id);
            if (projectDelete != null)
            {
                projectDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<ProrateSetting>().UpdateAsync(projectDelete);
                projectDelete.AddDomainEvent(new ProrateSettingDeletedEvent(projectDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(projectDelete.Id, "Deleted.");
            }
            return await Result<Guid>.FailureAsync("Not found");
        }
    }
}