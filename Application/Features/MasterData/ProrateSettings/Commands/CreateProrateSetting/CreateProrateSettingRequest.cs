﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.CreateProrateSetting
{
    public class CreateProrateSettingRequest : IRequest<Result<CreateProrateSettingResponseDto>>
    {
        [JsonPropertyName("seting")]
        public string Setting { get; set; }
    }
}