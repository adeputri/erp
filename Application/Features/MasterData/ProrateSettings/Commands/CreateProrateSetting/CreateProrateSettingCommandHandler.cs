﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.CreateProrateSetting
{
    internal class CreateProrateSettingCommandHandler : IRequestHandler<CreateProrateSettingRequest, Result<CreateProrateSettingResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProrateSettingRespository _prorateRepository;

        public CreateProrateSettingCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IProrateSettingRespository prorateRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _prorateRepository = prorateRepository;
        }

        public async Task<Result<CreateProrateSettingResponseDto>> Handle(CreateProrateSettingRequest request, CancellationToken cancellationToken)
        {
            var positionCreate = _mapper.Map<ProrateSetting>(request);
            var positionResponse = _mapper.Map<CreateProrateSettingResponseDto>(positionCreate);

            var validateData = await _prorateRepository.ValidateData(positionCreate);

            if (validateData != true)
            {
                return await Result<CreateProrateSettingResponseDto>.FailureAsync(positionResponse, "Data already exist");
            }

            positionCreate.CreatedAt = DateTime.UtcNow;
            positionCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<ProrateSetting>().AddAsync(positionCreate);
            positionCreate.AddDomainEvent(new ProrateSettingCreatedEvent(positionCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateProrateSettingResponseDto>.SuccessAsync(positionResponse, "Prorate setting created.");
        }
    }
}