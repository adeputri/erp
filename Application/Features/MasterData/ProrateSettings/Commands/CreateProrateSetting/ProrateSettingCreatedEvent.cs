﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.CreateProrateSetting
{
    public class ProrateSettingCreatedEvent : BaseEvent
    {
        public ProrateSetting ProrateSetting { get; set; }

        public ProrateSettingCreatedEvent(ProrateSetting prorateSetting)
        {
            ProrateSetting = prorateSetting;
        }
    }
}