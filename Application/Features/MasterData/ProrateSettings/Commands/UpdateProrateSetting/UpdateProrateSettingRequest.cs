﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.UpdateProrateSetting
{
    public class UpdateProrateSettingRequest : IRequest<Result<ProrateSetting>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("setting")]
        public string Setting { get; set; }
    }
}