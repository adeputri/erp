﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.UpdateProrateSetting
{
    public class ProrateSettingUpdateEvent : BaseEvent
    {
        public ProrateSetting ProrateSetting { get; set; }

        public ProrateSettingUpdateEvent(ProrateSetting prorateSetting)
        {
            ProrateSetting = prorateSetting;
        }
    }
}