﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.UpdateProrateSetting
{
    internal class UpdateProrateSettingCommandHandler : IRequestHandler<UpdateProrateSettingRequest, Result<ProrateSetting>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateProrateSettingCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<ProrateSetting>> Handle(UpdateProrateSettingRequest request, CancellationToken cancellationToken)
        {
            var validateProrate = await _unitOfWork.Repository<ProrateSetting>().GetByIdAsync(request.Id);
            if (validateProrate == null)
            {
                return await Result<ProrateSetting>.FailureAsync("Not found");
            }
            validateProrate.UpdatedAt = DateTime.UtcNow;
            validateProrate.Setting = request.Setting;

            await _unitOfWork.Repository<ProrateSetting>().UpdateAsync(validateProrate);
            validateProrate.AddDomainEvent(new ProrateSettingUpdateEvent(validateProrate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<ProrateSetting>.SuccessAsync("Success");
        }
    }
}