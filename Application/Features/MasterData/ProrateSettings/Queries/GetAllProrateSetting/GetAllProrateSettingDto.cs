﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Queries.GetAllProrateSetting
{
    public class GetAllProrateSettingDto : IMapFrom<ProrateSetting>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("setting")]
        public string Setting { get; set; }
    }
}