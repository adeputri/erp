﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Queries.GetAllProrateSetting
{
    public record GetAllProrateSettingQuery : IRequest<Result<List<GetAllProrateSettingDto>>>;

    internal class GetAllProjectQueryHandler : IRequestHandler<GetAllProrateSettingQuery, Result<List<GetAllProrateSettingDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllProjectQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllProrateSettingDto>>> Handle(GetAllProrateSettingQuery query, CancellationToken cancellationToken)
        {
            var project = await _unitOfWork.Repository<ProrateSetting>().FindByCondition(o => o.DeletedAt == null)
                           .ProjectTo<GetAllProrateSettingDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllProrateSettingDto>>.SuccessAsync(project, "Successfully fetch data");
        }
    }
}