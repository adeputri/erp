﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Queries.GetProrateSettingWithPagination
{
    public record GetProrateSettingWithPaginationQuery : IRequest<PaginatedResult<GetProrateSettingWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetProrateSettingWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetProrateSettingWithPaginationQuery()
        {
        }
    }

    internal class GetProrateSettingWithPaginationQueryHandler : IRequestHandler<GetProrateSettingWithPaginationQuery, PaginatedResult<GetProrateSettingWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetProrateSettingWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetProrateSettingWithPaginationDto>> Handle(GetProrateSettingWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<ProrateSetting>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.Setting.ToLower() == query.SearchTerm.ToLower()))
                           .OrderByDescending(x => x.UpdatedAt)
                           .ProjectTo<GetProrateSettingWithPaginationDto>(_mapper.ConfigurationProvider)
                           .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}