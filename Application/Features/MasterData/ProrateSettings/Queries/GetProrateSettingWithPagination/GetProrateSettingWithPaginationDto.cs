﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.ProrateSettings.Queries.GetProrateSettingWithPagination
{
    public class GetProrateSettingWithPaginationDto : IMapFrom<ProrateSetting>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("setting")]
        public string Setting { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime UpdateAt { get; set; }
    }
}