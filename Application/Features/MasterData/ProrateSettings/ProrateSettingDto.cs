﻿namespace SkeletonApi.Application.Features.MasterData.ProrateSettings
{
    public record ProrateSettingDto
    {
        public Guid Id { get; set; }
        public string Setting { get; set; }
    }
    public sealed record CreateProrateSettingResponseDto : ProrateSettingDto { }
}