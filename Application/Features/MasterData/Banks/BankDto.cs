﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Banks
{
    public record BankDto
    {
        [JsonPropertyName("bank_name")]
        public string BankName { get; set; }
    }
    public sealed record CreateBankResponseDto : BankDto { }
}