﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Banks.Commands.DeleteBank
{
    public class BankDeletedEvent : BaseEvent
    {
        public Bank Bank { get; set; }

        public BankDeletedEvent(Bank bank)
        {
            Bank = bank;
        }
    }
}