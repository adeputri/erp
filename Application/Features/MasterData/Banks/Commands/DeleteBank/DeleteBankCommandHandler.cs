﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Banks.Commands.DeleteBank
{
    internal class DeleteBankCommandHandler : IRequestHandler<DeleteBankRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteBankCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteBankRequest request, CancellationToken cancellationToken)
        {
            var bankDelete = await _unitOfWork.Repository<Bank>().GetByIdAsync(request.Id);
            if (bankDelete != null)
            {
                bankDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<Bank>().UpdateAsync(bankDelete);
                bankDelete.AddDomainEvent(new BankDeletedEvent(bankDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(bankDelete.Id, "Bank deleted.");
            }
            return await Result<Guid>.FailureAsync("Bank not found");
        }
    }
}