﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Banks.Commands.DeleteBank
{
    public class DeleteBankRequest : IRequest<Result<Guid>>, IMapFrom<BranchBank>
    {
        public Guid Id { get; set; }

        public DeleteBankRequest(Guid id)
        {
            Id = id;
        }

        public DeleteBankRequest()
        {
        }
    }
}