﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Banks.Commands.UpdateBank
{
    public class BankUpdateEvent : BaseEvent
    {
        public Bank Bank { get; set; }

        public BankUpdateEvent(Bank bank)
        {
            Bank = bank;
        }
    }
}