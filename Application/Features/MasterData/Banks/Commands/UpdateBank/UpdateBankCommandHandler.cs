﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Banks.Commands.UpdateBank
{
    internal class UpdateBankCommandHandler : IRequestHandler<UpdateBankRequest, Result<Bank>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateBankCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Bank>> Handle(UpdateBankRequest request, CancellationToken cancellationToken)
        {
            var bankValidate = await _unitOfWork.Repository<Bank>().GetByIdAsync(request.Id);
            if (bankValidate == null)
            {
                return await Result<Bank>.FailureAsync("Bank not found");
            }
            bankValidate.UpdatedAt = DateTime.UtcNow;
            bankValidate.BankName = request.BankName;

            await _unitOfWork.Repository<Bank>().UpdateAsync(bankValidate);
            bankValidate.AddDomainEvent(new BankUpdateEvent(bankValidate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<Bank>.SuccessAsync("Success");
        }
    }
}