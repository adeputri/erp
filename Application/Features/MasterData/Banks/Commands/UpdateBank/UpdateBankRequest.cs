﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Banks.Commands.UpdateBank
{
    public class UpdateBankRequest : IRequest<Result<Bank>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("bank_name")]
        public string BankName { get; set; }
    }
}