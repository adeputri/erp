﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Banks.Commands.CreateBank
{
    internal class CreateBankCommandHandler : IRequestHandler<CreateBankRequest, Result<CreateBankResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IBankRepository _bankRepository;

        public CreateBankCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IBankRepository bankRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _bankRepository = bankRepository;
        }

        public async Task<Result<CreateBankResponseDto>> Handle(CreateBankRequest request, CancellationToken cancellationToken)
        {
            var bankCreate = _mapper.Map<Bank>(request);
            var bankResponse = _mapper.Map<CreateBankResponseDto>(bankCreate);

            var validateData = await _bankRepository.ValidateData(bankCreate);

            if (validateData != true)
            {
                return await Result<CreateBankResponseDto>.FailureAsync(bankResponse, "Data already exist");
            }

            bankCreate.CreatedAt = DateTime.UtcNow;
            bankCreate.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<Bank>().AddAsync(bankCreate);
            bankCreate.AddDomainEvent(new BankCreatedEvent(bankCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateBankResponseDto>.SuccessAsync(bankResponse, "Bank created.");
        }
    }
}