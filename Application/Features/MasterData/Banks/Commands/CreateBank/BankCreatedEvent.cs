﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.Banks.Commands.CreateBank
{
    public class BankCreatedEvent : BaseEvent
    {
        public Bank Bank { get; set; }

        public BankCreatedEvent(Bank bank)
        {
            Bank = bank;
        }
    }
}