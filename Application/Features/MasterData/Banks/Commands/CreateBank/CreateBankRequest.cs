﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Banks.Commands.CreateBank
{
    public class CreateBankRequest : IRequest<Result<CreateBankResponseDto>>
    {
        [JsonPropertyName("bank_name")]
        public string BankName { get; set; }
    }
}