﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Banks.Queries.GetAllBank
{
    public record GetAllBankQuery : IRequest<Result<List<GetAllBankDto>>>;

    internal class GetAllBankQueryHandler : IRequestHandler<GetAllBankQuery, Result<List<GetAllBankDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllBankQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllBankDto>>> Handle(GetAllBankQuery query, CancellationToken cancellationToken)
        {
            var bank = await _unitOfWork.Repository<Bank>().FindByCondition(o => o.DeletedAt == null)
                           .ProjectTo<GetAllBankDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllBankDto>>.SuccessAsync(bank, "Successfully fetch data");
        }
    }
}