﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.Banks.Queries.GetAllBank
{
    public class GetAllBankDto : IMapFrom<Bank>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("bank_name")]
        public string BankName { get; set; }
    }
}