﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.Banks.Queries.GetBankWithPagination
{
    public record GetBankWithPaginationQuery : IRequest<PaginatedResult<GetBankWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public GetBankWithPaginationQuery()
        {
        }

        public GetBankWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetBankWithPaginationQueryHandler : IRequestHandler<GetBankWithPaginationQuery, PaginatedResult<GetBankWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetBankWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetBankWithPaginationDto>> Handle(GetBankWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<Bank>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.BankName.ToLower() == query.SearchTerm.ToLower()))
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetBankWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}