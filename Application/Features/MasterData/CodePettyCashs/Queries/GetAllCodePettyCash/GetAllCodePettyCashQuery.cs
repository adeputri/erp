﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Queries.GetAllCodePettyCash
{
    public record GetAllCodePettyCashQuery : IRequest<Result<List<GetAllCodePettyCashDto>>>;

    internal class GetAllCodePettyCashQueryHandler : IRequestHandler<GetAllCodePettyCashQuery, Result<List<GetAllCodePettyCashDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllCodePettyCashQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllCodePettyCashDto>>> Handle(GetAllCodePettyCashQuery query, CancellationToken cancellationToken)
        {
            var code = await _unitOfWork.Repository<CodePettyCash>().FindByCondition(o => o.DeletedAt == null)
                           .ProjectTo<GetAllCodePettyCashDto>(_mapper.ConfigurationProvider)
                           .ToListAsync(cancellationToken);

            return await Result<List<GetAllCodePettyCashDto>>.SuccessAsync(code, "Successfully fetch data");
        }
    }
}