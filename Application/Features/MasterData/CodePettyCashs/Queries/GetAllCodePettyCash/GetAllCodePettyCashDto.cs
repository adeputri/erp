﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Queries.GetAllCodePettyCash
{
    public class GetAllCodePettyCashDto : IMapFrom<CodePettyCash>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("code_name")]
        public string CodeName { get; set; }

        [JsonPropertyName("item")]
        public string Item { get; set; }
    }
}