﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Queries.GetPettyCashWithPagination
{
    public record GetPettyCashWithPaginationQuery : IRequest<PaginatedResult<GetPettyCashWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }

        public GetPettyCashWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
        public GetPettyCashWithPaginationQuery()
        {
        }
    }

    internal class GetPettyCashWithPaginationQueryHandler : IRequestHandler<GetPettyCashWithPaginationQuery, PaginatedResult<GetPettyCashWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetPettyCashWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetPettyCashWithPaginationDto>> Handle(GetPettyCashWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<CodePettyCash>().FindByCondition(x => (x.DeletedAt == null) && (query.SearchTerm == null || x.Item.ToLower() == query.SearchTerm.ToLower()))
                .Select(o => new GetPettyCashWithPaginationDto
                {
                    Id = o.Id,
                    CodeName = o.CodeName,
                    Item = o.Item,
                    UpdatedAt = o.UpdatedAt.Value.AddHours(7)
                })
            .OrderByDescending(x => x.UpdatedAt)
            .ProjectTo<GetPettyCashWithPaginationDto>(_mapper.ConfigurationProvider)
            .ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}