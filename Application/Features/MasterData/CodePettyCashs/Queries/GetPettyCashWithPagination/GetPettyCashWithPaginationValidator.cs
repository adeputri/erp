﻿using FluentValidation;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Queries.GetPettyCashWithPagination
{
    public class GetPettyCashWithPaginationValidator : AbstractValidator<GetPettyCashWithPaginationQuery>
    {
        public GetPettyCashWithPaginationValidator()
        {
            RuleFor(x => x.PageNumber)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageNumber at least greater than or equal to 1.");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageSize at least greater than or equal to 1.");
        }
    }
}