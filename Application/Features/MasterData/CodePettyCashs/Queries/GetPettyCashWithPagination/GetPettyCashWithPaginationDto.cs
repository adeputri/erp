﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Queries.GetPettyCashWithPagination
{
    public class GetPettyCashWithPaginationDto : IMapFrom<GetPettyCashWithPaginationDto>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("code_name")]
        public string CodeName { get; set; }

        [JsonPropertyName("item")]
        public string Item { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime UpdatedAt { get; set; }
    }
}