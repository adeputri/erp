﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs
{
    public record CodePettyCashDto
    {
        [JsonPropertyName("code_name")]
        public string CodeName { get; set; }
        [JsonPropertyName("item")]
        public string Item { get; set; }
    }
    public sealed record CreateCodePettyCashResponseDto : CodePettyCashDto { }
}