﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.DeleteCodePettyCash
{
    public class DeleteCodePettyCashRequest : IRequest<Result<Guid>>, IMapFrom<CodePettyCash>
    {
        public Guid Id { get; set; }

        public DeleteCodePettyCashRequest(Guid id)
        {
            Id = id;
        }

        public DeleteCodePettyCashRequest()
        {
        }
    }
}