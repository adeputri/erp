﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.DeleteCodePettyCash
{
    internal class DeleteCodePettyCashCommandHandler : IRequestHandler<DeleteCodePettyCashRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteCodePettyCashCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteCodePettyCashRequest request, CancellationToken cancellationToken)
        {
            var positionDelete = await _unitOfWork.Repository<CodePettyCash>().GetByIdAsync(request.Id);
            if (positionDelete != null)
            {
                positionDelete.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<CodePettyCash>().UpdateAsync(positionDelete);
                positionDelete.AddDomainEvent(new CodePettyCashDeletedEvent(positionDelete));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(positionDelete.Id, "Position deleted.");
            }
            return await Result<Guid>.FailureAsync("Position not found");
        }
    }
}