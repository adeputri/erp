﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.DeleteCodePettyCash
{
    public class CodePettyCashDeletedEvent : BaseEvent
    {
        public CodePettyCash CodePettyCash { get; set; }

        public CodePettyCashDeletedEvent(CodePettyCash codePettyCash)
        {
            CodePettyCash = codePettyCash;
        }
    }
}