﻿using MediatR;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.UpdateCodePettyCash
{
    public class UpdatePettyCashRequest : IRequest<Result<CodePettyCash>>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("code_name")]
        public string CodeName { get; set; }

        [JsonPropertyName("item")]
        public string Item { get; set; }
    }
}