﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.UpdateCodePettyCash
{
    internal class UpdatePettyCashCommandHandler : IRequestHandler<UpdatePettyCashRequest, Result<CodePettyCash>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdatePettyCashCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<CodePettyCash>> Handle(UpdatePettyCashRequest request, CancellationToken cancellationToken)
        {
            var validatePosition = await _unitOfWork.Repository<CodePettyCash>().GetByIdAsync(request.Id);
            if (validatePosition == null)
            {
                return await Result<CodePettyCash>.FailureAsync("Id not found");
            }
            validatePosition.UpdatedAt = DateTime.UtcNow;

            await _unitOfWork.Repository<CodePettyCash>().UpdateAsync(validatePosition);
            validatePosition.AddDomainEvent(new PettyCashUpdateEvent(validatePosition));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CodePettyCash>.SuccessAsync("Success");
        }
    }
}