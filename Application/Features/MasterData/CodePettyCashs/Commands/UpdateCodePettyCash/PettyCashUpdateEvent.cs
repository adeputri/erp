﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.UpdateCodePettyCash
{
    public class PettyCashUpdateEvent : BaseEvent
    {
        public CodePettyCash CodePettyCash { get; set; }

        public PettyCashUpdateEvent(CodePettyCash codePettyCash)
        {
            CodePettyCash = codePettyCash;
        }
    }
}