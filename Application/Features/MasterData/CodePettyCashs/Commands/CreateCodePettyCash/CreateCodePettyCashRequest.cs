﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.CreateCodePettyCash
{
    public class CreateCodePettyCashRequest : IRequest<Result<CreateCodePettyCashResponseDto>>
    {
        [JsonPropertyName("code_name")]
        public string CodeName { get; set; }

        [JsonPropertyName("item")]
        public string Item { get; set; }
    }
}