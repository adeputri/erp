﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.CreateCodePettyCash
{
    internal class CreateCodePettyCashCommandHandler : IRequestHandler<CreateCodePettyCashRequest, Result<CreateCodePettyCashResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IPettyCashRepository _pettyCashRepository;

        public CreateCodePettyCashCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IPettyCashRepository pettyCashRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _pettyCashRepository = pettyCashRepository;
        }

        public async Task<Result<CreateCodePettyCashResponseDto>> Handle(CreateCodePettyCashRequest request, CancellationToken cancellationToken)
        {
            var pettyCashCreate = _mapper.Map<CodePettyCash>(request);
            var pettyCashResponse = _mapper.Map<CreateCodePettyCashResponseDto>(pettyCashCreate);

            var validateData = await _pettyCashRepository.ValidateDate(pettyCashCreate);

            if (validateData != true)
            {
                return await Result<CreateCodePettyCashResponseDto>.FailureAsync(pettyCashResponse, "Data already exist");
            }

            await _unitOfWork.Repository<CodePettyCash>().AddAsync(pettyCashCreate);
            pettyCashCreate.AddDomainEvent(new CodePettyCashCreatedEvent(pettyCashCreate));
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateCodePettyCashResponseDto>.SuccessAsync(pettyCashResponse, "Created.");
        }
    }
}