﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.CreateCodePettyCash
{
    public class CodePettyCashCreatedEvent : BaseEvent
    {
        public CodePettyCash CodePettyCash { get; set; }

        public CodePettyCashCreatedEvent(CodePettyCash codePettyCash)
        {
            CodePettyCash = codePettyCash;
        }
    }
}