﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Commands.CreateNonProject
{
    public class CreateNonProjectRequest : IRequest<Result<CreateNonProjectResponseDto>>
    {
        [JsonPropertyName("project_id")]
        public Guid ProjectId { get; set; }

        [JsonPropertyName("item_id")]
        public Guid ItemId { get; set; }

        [JsonPropertyName("pic_id")]
        public Guid PicId { get; set; }

        [JsonPropertyName("status_item")]
        public string StatusItem { get; set; }

        [JsonPropertyName("order_date")]
        public DateOnly OrderDate { get; set; }

        [JsonPropertyName("order_qty")]
        public int OrderQty { get; set; }

        [JsonPropertyName("etd")]
        public DateOnly Etd { get; set; }

        [JsonPropertyName("send_date")]
        public DateOnly SendDate { get; set; }

        [JsonPropertyName("send_qty")]
        public int SendQty { get; set; }

        [JsonPropertyName("balance")]
        public int BalanceQty { get; set; }
    }
}