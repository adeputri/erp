﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Commands.CreateNonProject
{
    public class NonProjectCreatedEvent : BaseEvent
    {
        public NonProject NonProject { get; set; }
        public NonProjectCreatedEvent(NonProject nonProject)
        {
            NonProject = nonProject;
        }
    }
}
