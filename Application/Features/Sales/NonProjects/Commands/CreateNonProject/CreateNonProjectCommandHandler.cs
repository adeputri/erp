﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Commands.CreateNonProject
{
    internal class CreateNonProjectCommandHandler : IRequestHandler<CreateNonProjectRequest, Result<CreateNonProjectResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateNonProjectCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<CreateNonProjectResponseDto>> Handle(CreateNonProjectRequest request, CancellationToken cancellationToken)
        {
            var item = _mapper.Map<NonProject>(request);

            await _unitOfWork.Repository<NonProject>().AddAsync(item);
            item.AddDomainEvent(new NonProjectCreatedEvent(item));
            await _unitOfWork.Save(cancellationToken);

            var response = _mapper.Map<CreateNonProjectResponseDto>(item);
            return await Result<CreateNonProjectResponseDto>.SuccessAsync(response, "created.");
        }
    }
}