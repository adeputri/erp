﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Commands.UpdateNonProject
{
    public class NonProjectUpdateEvent : BaseEvent
    {
        public NonProject NonProject { get; set; }

        public NonProjectUpdateEvent(NonProject nonProject)
        {
            NonProject = nonProject;
        }
    }
}