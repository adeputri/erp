﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Commands.UpdateNonProject
{
    internal class UpdateNonProjectCommandHandler : IRequestHandler<UpdateNonProjectRequest, Result<NonProject>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateNonProjectCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<NonProject>> Handle(UpdateNonProjectRequest request, CancellationToken cancellationToken)
        {
            var validateNonProject = await _unitOfWork.Repository<NonProject>().GetByIdAsync(request.Id);
            if (validateNonProject == null)
            {
                return await Result<NonProject>.FailureAsync("not found");
            }
            validateNonProject.ProjectId = request.ProjectId;
            validateNonProject.ItemId = request.ItemId;
            validateNonProject.PicId = request.PicId;
            validateNonProject.StatusItem = request.StatusItem;
            validateNonProject.OrderDate = request.OrderDate;
            validateNonProject.OrderQty = request.OrderQty;
            validateNonProject.Etd = request.Etd;
            validateNonProject.SendDate = request.SendDate;
            validateNonProject.SendQty = request.SendQty;
            validateNonProject.BalanceQty = request.BalanceQty;

            await _unitOfWork.Repository<NonProject>().UpdateAsync(validateNonProject);
            validateNonProject.AddDomainEvent(new NonProjectUpdateEvent(validateNonProject));
            await _unitOfWork.Save(cancellationToken);

            return await Result<NonProject>.SuccessAsync(validateNonProject, "Success");
        }
    }
}