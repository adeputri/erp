﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Commands.DeleteNonProject
{
    internal class DeleteNonProjectCommandHandler : IRequestHandler<DeleteNonProjectRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteNonProjectCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteNonProjectRequest request, CancellationToken cancellationToken)
        {
            var nonProject = await _unitOfWork.Repository<NonProject>().GetByIdAsync(request.Id);
            if (nonProject != null)
            {
                nonProject.DeletedAt = DateTime.UtcNow;

                await _unitOfWork.Repository<NonProject>().UpdateAsync(nonProject);
                nonProject.AddDomainEvent(new NonProjectDeletedEvent(nonProject));
                await _unitOfWork.Save(cancellationToken);

                return await Result<Guid>.SuccessAsync(nonProject.Id, "deleted.");
            }
            return await Result<Guid>.FailureAsync("not found");
        }
    }
}