﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Commands.DeleteNonProject
{
    public class DeleteNonProjectRequest : IRequest<Result<Guid>>, IMapFrom<NonProject>
    {
        public Guid Id { get; set; }

        public DeleteNonProjectRequest(Guid id)
        {
            Id = id;
        }

        public DeleteNonProjectRequest()
        {
        }
    }
}