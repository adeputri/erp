﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Commands.DeleteNonProject
{
    public class NonProjectDeletedEvent : BaseEvent
    {
        public NonProject NonProject { get; set; }

        public NonProjectDeletedEvent(NonProject nonProject)
        {
            NonProject = nonProject;
        }
    }
}