﻿using SkeletonApi.Application.Common.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Queries.GetNonProjectById
{
    public class GetNonProjectByIdDto : IMapFrom<GetNonProjectByIdDto>
    {
        public Guid Id { get; set; }
        [JsonPropertyName("project_id")]
        public Guid ProjectId { get; set; }

        [JsonPropertyName("number_spk")]
        public string NumberSpk { get; set; }

        [JsonPropertyName("item_id")]
        public Guid ItemId { get; set; }

        [JsonPropertyName("item_name")]
        public string ItemName { get; set; }

        [JsonPropertyName("status_item")]
        public string StatusItem { get; set; }

        [JsonPropertyName("order_date")]
        public DateOnly OrderDate { get; set; }

        [JsonPropertyName("order_qty")]
        public int OrderQty { get; set; }

        public string Unit { get; set; }

        [JsonPropertyName("etd")]
        public DateOnly Etd { get; set; }

        [JsonPropertyName("send_date")]
        public DateOnly SendDate { get; set; }

        [JsonPropertyName("send_qty")]
        public int SendQty { get; set; }

        [JsonPropertyName("balance")]
        public int BalanceQty { get; set; }

        [JsonPropertyName("customer_name")]
        public string CustomerName { get; set; }

        [JsonPropertyName("pic_id")]
        public Guid PicId { get; set; }

        [JsonPropertyName("pic_name")]
        public string PicName { get; set; }
        [JsonPropertyName("last_updated")]
        public DateTime LastUpdated { get; set; }
    }
}
