﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Queries.GetNonProjectById
{
    public record GetNonProjectByIdQuery : IRequest<Result<GetNonProjectByIdDto>>
    {
        public Guid Id { get; set; }
        public GetNonProjectByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    internal class GetProjectByIdQueryHandler : IRequestHandler<GetNonProjectByIdQuery, Result<GetNonProjectByIdDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetProjectByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<GetNonProjectByIdDto>> Handle(GetNonProjectByIdQuery query, CancellationToken cancellationToken)
        {
            var project = await _unitOfWork.Repository<NonProject>().FindByCondition(o => (o.DeletedAt == null) && (o.Id == query.Id))
                          .Select(o => new GetNonProjectByIdDto
                          {
                              Id = o.Id,
                              ProjectId = o.ProjectId,
                              NumberSpk = o.Project.NumberSpk,
                              ItemId = o.ItemId,
                              ItemName = o.Item.Name,
                              StatusItem = o.StatusItem,
                              OrderDate = o.OrderDate,
                              OrderQty = o.OrderQty,
                              Unit = o.Item.MasterUnit.UnitName,
                              Etd = o.Etd,
                              SendDate = o.SendDate,
                              SendQty = o.SendQty,
                              BalanceQty = o.BalanceQty,
                              CustomerName = o.Project.MasterProject.Customer.CustomerName,
                              PicId = o.PicId,
                              PicName = o.Employee.FirstName + " " + o.Employee.LastName,
                              LastUpdated = o.UpdatedAt.Value.AddHours(7)
                          })
                          .ProjectTo<GetNonProjectByIdDto>(_mapper.ConfigurationProvider)
                          .FirstOrDefaultAsync(cancellationToken);

            return await Result<GetNonProjectByIdDto>.SuccessAsync(project, "Successfully fetch data");
        }
    }
}