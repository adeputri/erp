﻿using FluentValidation;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Queries.GetNonProjectWithPagination
{
    public class GetNonProjectWithPaginationValidator : AbstractValidator<GetNonProjectWithPaginationQuery>
    {
        public GetNonProjectWithPaginationValidator()
        {
            RuleFor(x => x.PageNumber)
           .GreaterThanOrEqualTo(1)
           .WithMessage("PageNumber at least greater than or equal to 1.");

            RuleFor(x => x.PageSize)
                .GreaterThanOrEqualTo(1)
                .WithMessage("PageSize at least greater than or equal to 1.");
        }
    }
}