﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Features.MasterData.Items.Queries.GetItemWithPagination;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.NonProjects.Queries.GetNonProjectWithPagination
{
    public record GetNonProjectWithPaginationQuery : IRequest<PaginatedResult<GetNonProjectWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public GetNonProjectWithPaginationQuery(int pageNumber, int pageSize, string searchterm, DateTime start, DateTime end)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
            Start = start;
            End = end;
        }
        public GetNonProjectWithPaginationQuery()
        {
        }
    }

    internal class GetItemWithPaginationQueryHandler : IRequestHandler<GetNonProjectWithPaginationQuery, PaginatedResult<GetNonProjectWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetItemWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<PaginatedResult<GetNonProjectWithPaginationDto>> Handle(GetNonProjectWithPaginationQuery query, CancellationToken cancellationToken)
        {
            return await _unitOfWork.Repository<NonProject>()
               .FindByCondition(p => (p.DeletedAt == null) &&
                            (string.IsNullOrEmpty(query.SearchTerm) ||
                             p.Item.Name.Contains(query.SearchTerm) ||
                             p.StatusItem.Contains(query.SearchTerm) ||
                             p.Project.MasterProject.Customer.CustomerName.Contains(query.SearchTerm)) &&
                            (query.Start == DateTime.MinValue || p.UpdatedAt.Value >= query.Start) &&
                            (query.End == DateTime.MinValue || p.UpdatedAt.Value <= query.End)
                        )
                .AsNoTracking()
                .Select(o => new GetNonProjectWithPaginationDto
                {
                    Id = o.Id,
                    ProjectId = o.ProjectId,
                    NumberSpk = o.Project.NumberSpk,
                    ItemId = o.ItemId,
                    ItemName = o.Item.Name,
                    StatusItem = o.StatusItem,
                    OrderDate = o.OrderDate,
                    OrderQty = o.OrderQty,
                    Unit = o.Item.MasterUnit.UnitName,
                    Etd = o.Etd,
                    SendDate = o.SendDate,
                    SendQty = o.SendQty,
                    BalanceQty = o.BalanceQty,
                    CustomerName = o.Project.MasterProject.Customer.CustomerName,
                    PicId = o.PicId,
                    PicName = o.Employee.FirstName + " " + o.Employee.LastName,
                    LastUpdated = o.UpdatedAt.Value.AddHours(7)
                }).OrderByDescending(p => p.LastUpdated).ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}