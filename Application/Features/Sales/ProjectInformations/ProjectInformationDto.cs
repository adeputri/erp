﻿using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations
{
    public record ProjectInformationDto
    {
        [JsonPropertyName("number_spk")]
        public string NumberSpk { get; set; }

        [JsonPropertyName("master_project_id")]
        public Guid MasterProjectId { get; set; }

        [JsonPropertyName("project_start_date")]
        public DateOnly ProjectStartDate { get; set; }

        [JsonPropertyName("project_end_date")]
        public DateOnly ProjectEndDate { get; set; }

        [JsonPropertyName("project_status")]
        public string ProjectStatus { get; set; }

        [JsonPropertyName("pic_id")]
        public Guid PicId { get; set; }
    }
    public sealed record CreateProjectInformationResponseDto : ProjectInformationDto { }
}