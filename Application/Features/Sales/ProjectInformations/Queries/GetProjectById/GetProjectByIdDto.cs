﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.Queries.GetProjectById
{
    public class GetProjectByIdDto : IMapFrom<GetProjectByIdDto>
    {
        public Guid Id { get; set; }

        [JsonPropertyName("number_spk")]
        public string NumberSpk { get; set; }

        [JsonPropertyName("master_project_id")]
        public Guid MasterProjectId { get; set; }

        [JsonPropertyName("project_name")]
        public string ProjectName { get; set; }

        [JsonPropertyName("progress")]
        public decimal Progress { get; set; }

        [JsonPropertyName("project_status")]
        public string ProjectStatus { get; set; }

        [JsonPropertyName("start_project")]
        public DateOnly StartProject { get; set; }

        [JsonPropertyName("end_project")]
        public DateOnly EndProject { get; set; }

        [JsonPropertyName("due_date")]
        public string DueDate { get; set; }

        [JsonPropertyName("customer_name")]
        public string CustomerName { get; set; }

        [JsonPropertyName("pic_id")]
        public Guid PicId { get; set; }

        [JsonPropertyName("pic_name")]
        public string PicName { get; set; }
    }
}