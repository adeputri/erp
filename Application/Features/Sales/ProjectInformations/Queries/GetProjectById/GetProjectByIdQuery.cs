﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.Queries.GetProjectById
{
    public record GetProjectByIdQuery : IRequest<Result<GetProjectByIdDto>>
    {
        public Guid Id { get; set; }
        public GetProjectByIdQuery(Guid id)
        {
            Id = id;
        }
    }

    internal class GetProjectByIdQueryHandler : IRequestHandler<GetProjectByIdQuery, Result<GetProjectByIdDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetProjectByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<GetProjectByIdDto>> Handle(GetProjectByIdQuery query, CancellationToken cancellationToken)
        {
            var project = await _unitOfWork.Repository<Project>().FindByCondition(o => (o.DeletedAt == null) && (o.Id == query.Id))
                          .Select(p => new GetProjectByIdDto
                          {
                              Id = p.Id,
                              NumberSpk = p.NumberSpk,
                              MasterProjectId = p.MasterProjectId,
                              ProjectName = p.MasterProject.ProjectName,
                              StartProject = p.ProjectStartDate,
                              EndProject = p.ProjectEndDate,
                              CustomerName = p.MasterProject.Customer.CustomerName,
                              PicId = p.PicId,
                              PicName = p.Employee.FirstName + " " + p.Employee.LastName,
                          }).Distinct()
                          .ProjectTo<GetProjectByIdDto>(_mapper.ConfigurationProvider)
                          .FirstOrDefaultAsync(cancellationToken);

            return await Result<GetProjectByIdDto>.SuccessAsync(project, "Successfully fetch data");
        }
    }
}