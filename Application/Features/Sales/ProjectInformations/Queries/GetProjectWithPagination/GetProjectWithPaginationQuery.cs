﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Extensions;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Queries.GetProjectWithPagination
{
    public record GetProjectWithPaginationQuery : IRequest<PaginatedResult<GetProjectWithPaginationDto>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchTerm { get; set; }
        public GetProjectWithPaginationQuery()
        {
        }

        public GetProjectWithPaginationQuery(int pageNumber, int pageSize, string searchterm)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            SearchTerm = searchterm;
        }
    }

    internal class GetProjectWithPaginationQueryHandler : IRequestHandler<GetProjectWithPaginationQuery, PaginatedResult<GetProjectWithPaginationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProjectRepository _projectRepository;

        public GetProjectWithPaginationQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IProjectRepository projectRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _projectRepository = projectRepository;
        }

        public async Task<PaginatedResult<GetProjectWithPaginationDto>> Handle(GetProjectWithPaginationQuery query, CancellationToken cancellationToken)
        {
            var data = await _projectRepository.GetProgress();
            return await data.ToPaginatedListAsync(query.PageNumber, query.PageSize, cancellationToken);
        }
    }
}