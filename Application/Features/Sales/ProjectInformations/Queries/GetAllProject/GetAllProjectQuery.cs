﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.Sales.ProjectInformations.Queries.GetAllProject;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Queries.GetAllProject
{
    public record GetAllProjectQuery : IRequest<Result<List<GetAllProjectDto>>>;

    internal class GetAllProjectQueryHandler : IRequestHandler<GetAllProjectQuery, Result<List<GetAllProjectDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public GetAllProjectQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Result<List<GetAllProjectDto>>> Handle(GetAllProjectQuery query, CancellationToken cancellationToken)
        {
            var project = await _unitOfWork.Repository<Project>().FindByCondition(o => o.DeletedAt == null)
                            .Select(p => new GetAllProjectDto
                            {
                                Id = p.Id,
                                ProjectName = p.MasterProject.ProjectName,
                                NumberSpk = p.NumberSpk,
                                CustomerName = p.MasterProject.Customer.CustomerName
                            })
                            .ProjectTo<GetAllProjectDto>(_mapper.ConfigurationProvider)
                            .ToListAsync(cancellationToken);

            return await Result<List<GetAllProjectDto>>.SuccessAsync(project, "Successfully fetch data");
        }
    }
}