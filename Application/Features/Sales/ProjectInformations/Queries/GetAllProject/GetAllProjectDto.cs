﻿using SkeletonApi.Application.Common.Mappings;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.Queries.GetAllProject
{
    public class GetAllProjectDto : IMapFrom<GetAllProjectDto>
    {
        public Guid Id { get; set; }
        [JsonPropertyName("project_name")]
        public string ProjectName { get; set; }

        [JsonPropertyName("number_spk")]
        public string NumberSpk { get; set; }
        [JsonPropertyName("customer_name")]
        public string CustomerName { get; set; }
    }
}