﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Commands.CreateMainTask
{
    public class MainTaskCreatedEvent : BaseEvent
    {
        public SectionProject SectionProject { get; set; }

        public MainTaskCreatedEvent(SectionProject sectionProject)
        {
            SectionProject = sectionProject;
        }
    }

    public class TaskProjectCreatedEvent : BaseEvent
    {
        public TaskProject SectionProject { get; set; }

        public TaskProjectCreatedEvent(TaskProject sectionProject)
        {
            SectionProject = sectionProject;
        }
    }
}