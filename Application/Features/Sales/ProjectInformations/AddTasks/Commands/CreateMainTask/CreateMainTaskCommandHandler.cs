﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Commands.CreateMainTask
{
    internal class CreateMainTaskCommandHandler : IRequestHandler<CreateMainTaskRequest, Result<CreateMainTaskResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CreateMainTaskCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<Result<CreateMainTaskResponseDto>> Handle(CreateMainTaskRequest request, CancellationToken cancellationToken)
        {
            var mainTaskResponse = _mapper.Map<CreateMainTaskResponseDto>(request);

            // Find existing sections related to the project
            var existingSections = await _unitOfWork.Repository<SectionProject>()
                                    .FindByCondition(s => s.ProjectId == request.ProjectId)
                                    .ToListAsync();
            foreach (var section in existingSections)
            {
                // Find tasks related to the section
                var relatedTasks = await _unitOfWork.Repository<TaskProject>()
                                                    .FindByCondition(t => t.SectionProjectId == section.Id)
                                                    .ToListAsync();

                // Delete related tasks
                foreach (var task in relatedTasks)
                {
                    await _unitOfWork.Repository<TaskProject>().DeleteAsync(task);
                    task.AddDomainEvent(new TaskProjectCreatedEvent(task)); // Assuming you have a TaskProjectDeletedEvent
                }

                // Delete the section
                await _unitOfWork.Repository<SectionProject>().DeleteAsync(section);
                section.AddDomainEvent(new MainTaskCreatedEvent(section)); // Assuming you have a SectionProjectDeletedEvent
            }

            // Save changes after deletion
            await _unitOfWork.Save(cancellationToken);

            // Add new sections and tasks
            foreach (var section in request.CreateSections)
            {
                var newSection = new SectionProject
                {
                    SectionName = section.SectionName,
                    ProjectId = request.ProjectId
                };

                await _unitOfWork.Repository<SectionProject>().AddAsync(newSection);
                newSection.AddDomainEvent(new MainTaskCreatedEvent(newSection));

                foreach (var task in section.CreateTasks)
                {
                    var newTask = new TaskProject
                    {
                        TaskName = task.TaskName,
                        Status = task.Status,
                        AssignTo = task.AssignTo,
                        DueDate = task.DueDate,
                        SectionProjectId = newSection.Id // Set the foreign key
                    };

                    await _unitOfWork.Repository<TaskProject>().AddAsync(newTask);
                    newTask.AddDomainEvent(new TaskProjectCreatedEvent(newTask));
                }
            }

            // Save changes after adding new sections and tasks
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateMainTaskResponseDto>.SuccessAsync(mainTaskResponse, "Created.");
        }
    }
}