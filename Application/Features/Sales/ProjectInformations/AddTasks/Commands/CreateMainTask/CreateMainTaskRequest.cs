﻿using MediatR;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Commands.CreateMainTask
{
    public class CreateMainTaskRequest : IRequest<Result<CreateMainTaskResponseDto>>
    {
        [JsonPropertyName("project_id")]
        public Guid ProjectId { get; set; }

        [JsonPropertyName("create_section")]
        public List<CreateSection> CreateSections { get; set; }
    }

    public class CreateSection
    {
        [JsonPropertyName("section_name")]
        public string SectionName { get; set; }

        [JsonPropertyName("create_task")]
        public List<CreateTask> CreateTasks { get; set; }
    }

    public class CreateTask
    {
        [JsonPropertyName("task_name")]
        public string TaskName { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("assign_to")]
        public string AssignTo { get; set; }

        [JsonPropertyName("due_date")]
        public string DueDate { get; set; }
    }
}