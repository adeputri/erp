﻿using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Commands.CreateMainTask;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Queries.GetAllTaskByIdProject
{
    public class GetAllTaskByIdProjectDto : IMapFrom<GetAllTaskByIdProjectDto>
    {
        [JsonPropertyName("project_id")]
        public Guid ProjectId { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("create_section")]
        public List<CreateSection> CreateSections { get; set; }
    }
}