﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Queries.GetAllTaskByIdProject
{
    public record GetAllTaskByIdProjectQuery : IRequest<Result<IEnumerable<GetAllTaskByIdProjectDto>>>
    {
        public Guid ProjectId { get; set; }
        public GetAllTaskByIdProjectQuery(Guid projectId)
        {
            ProjectId = projectId;
        }
    }

    internal class GetAllTaskByIdProjectQueryHandler : IRequestHandler<GetAllTaskByIdProjectQuery, Result<IEnumerable<GetAllTaskByIdProjectDto>>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProjectRepository _projectRepository;

        public GetAllTaskByIdProjectQueryHandler(IUnitOfWork unitOfWork, IMapper mapper, IProjectRepository projectRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _projectRepository = projectRepository;
        }

        public async Task<Result<IEnumerable<GetAllTaskByIdProjectDto>>> Handle(GetAllTaskByIdProjectQuery query, CancellationToken cancellationToken)
        {
            var data = await _projectRepository.GetAllTaskByIdProject(query.ProjectId);

            return await Result<IEnumerable<GetAllTaskByIdProjectDto>>.SuccessAsync(data, "Successfully fetch data");
        }
    }
}