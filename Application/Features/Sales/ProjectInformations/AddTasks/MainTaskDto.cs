﻿using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Commands.CreateMainTask;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks
{
    public record MainTaskDto
    {
        [JsonPropertyName("project_id")]
        public Guid ProjectId { get; set; }
        [JsonPropertyName("create_section")]
        public List<CreateSection> CreateSections { get; set; }
    }
    public sealed record CreateMainTaskResponseDto : MainTaskDto { }
}