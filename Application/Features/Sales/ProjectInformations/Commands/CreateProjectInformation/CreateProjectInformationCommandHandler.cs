﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Features.Sales.ProjectInformations;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.CreateProjectInformation
{
    internal class CreateProjectInformationCommandHandler : IRequestHandler<CreateProjectInformationRequest, Result<CreateProjectInformationResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProjectRepository _projectRepository;

        public CreateProjectInformationCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IProjectRepository projectRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _projectRepository = projectRepository;
        }

        public async Task<Result<CreateProjectInformationResponseDto>> Handle(CreateProjectInformationRequest request, CancellationToken cancellationToken)
        {
            var projectCreate = _mapper.Map<Project>(request);
            var projectResponse = _mapper.Map<CreateProjectInformationResponseDto>(projectCreate);
            var validateData = await _projectRepository.ValidateData(projectCreate);
            if (validateData != true)
            {
                return await Result<CreateProjectInformationResponseDto>.FailureAsync(projectResponse, "Data already exist");
            }
            await _unitOfWork.Repository<Project>().AddAsync(projectCreate);
            projectCreate.AddDomainEvent(new ProjectInformationCreatedEvent(projectCreate));
            await _unitOfWork.Save(cancellationToken);

            return Result<CreateProjectInformationResponseDto>.Success(projectResponse, "Created.");
        }
    }
}