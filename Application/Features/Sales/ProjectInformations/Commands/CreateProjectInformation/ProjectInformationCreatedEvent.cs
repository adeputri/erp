﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.CreateProjectInformation
{
    public class ProjectInformationCreatedEvent : BaseEvent
    {
        public Project Project { get; set; }

        public ProjectInformationCreatedEvent(Project project)
        {
            Project = project;
        }
    }
}