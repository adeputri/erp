﻿using MediatR;
using Microsoft.AspNetCore.Http;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.Commands.Import
{
    public class ImportExcelProjectRequest : IRequest<Result<CreateProjectInformationResponseDto>>
    {
        public IFormFile File { get; set; }

        public ImportExcelProjectRequest(IFormFile file)
        {
            File = file;
        }
    }

    public class ProjectImportDto
    {
        public string NumberSpk { get; set; }
        public Guid MasterProjectId { get; set; }
        public DateOnly? ProjectStartDate { get; set; }
        public DateOnly? ProjectEndDate { get; set; }
        public Guid PicId { get; set; }
    }
}