﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.Commands.Import
{
    public class ProjectImportExcelEvent : BaseEvent
    {
        public Project Project { get; set; }

        public ProjectImportExcelEvent(Project project)
        {
            Project = project;
        }
    }
}