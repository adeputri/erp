﻿using AutoMapper;
using MediatR;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Sales.ProjectInformations.Commands.Import
{
    internal class ImportExcelProjectCommandHandler : IRequestHandler<ImportExcelProjectRequest, Result<CreateProjectInformationResponseDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProjectRepository _projectRepository;

        public ImportExcelProjectCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IProjectRepository projectRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _projectRepository = projectRepository;
        }

        public async Task<Result<CreateProjectInformationResponseDto>> Handle(ImportExcelProjectRequest request, CancellationToken cancellationToken)
        {
            var dataImport = await _projectRepository.UploadExcel(request.File);
            foreach (var item in dataImport)
            {
                var projectCreate = _mapper.Map<Project>(item);
                var projectResponse = _mapper.Map<CreateProjectInformationResponseDto>(item);
                var validateData = await _projectRepository.ValidateData(projectCreate);
                if (validateData != true)
                {
                    return await Result<CreateProjectInformationResponseDto>.FailureAsync(projectResponse, "Data already exist");
                }
                await _unitOfWork.Repository<Project>().AddAsync(projectCreate);
                projectCreate.AddDomainEvent(new ProjectImportExcelEvent(projectCreate));
            }
            await _unitOfWork.Save(cancellationToken);

            return await Result<CreateProjectInformationResponseDto>.SuccessAsync("Created.");
        }
    }
}