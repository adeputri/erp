﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.DeleteProjectInformation
{
    public class ProjectInformationDeletedEvent : BaseEvent
    {
        public Project Project { get; set; }

        public ProjectInformationDeletedEvent(Project project)
        {
            Project = project;
        }
    }
}