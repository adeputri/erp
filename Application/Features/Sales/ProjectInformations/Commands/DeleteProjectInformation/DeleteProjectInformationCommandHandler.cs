﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.DeleteProjectInformation
{
    internal class DeleteProjectInformationCommandHandler : IRequestHandler<DeleteProjectInformationRequest, Result<Guid>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteProjectInformationCommandHandler(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result<Guid>> Handle(DeleteProjectInformationRequest request, CancellationToken cancellationToken)
        {
            var projectDelete = await _unitOfWork.Repository<Project>().FindByCondition(o => o.Id == request.Id).ToListAsync();
            if (projectDelete != null)
            {
                foreach (var project in projectDelete)
                {
                    project.DeletedAt = DateTime.UtcNow;

                    await _unitOfWork.Repository<Project>().UpdateAsync(project);
                    project.AddDomainEvent(new ProjectInformationDeletedEvent(project));
                    await _unitOfWork.Save(cancellationToken);
                }
                return await Result<Guid>.SuccessAsync(request.Id, "Deleted.");
            }
            return await Result<Guid>.FailureAsync("Not found");
        }
    }
}