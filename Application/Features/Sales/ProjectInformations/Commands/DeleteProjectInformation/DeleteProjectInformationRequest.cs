﻿using MediatR;
using SkeletonApi.Application.Common.Mappings;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.DeleteProjectInformation
{
    public class DeleteProjectInformationRequest : IRequest<Result<Guid>>, IMapFrom<Project>
    {
        public Guid Id { get; set; }

        public DeleteProjectInformationRequest(Guid id)
        {
            Id = id;
        }

        public DeleteProjectInformationRequest()
        {
        }
    }
}