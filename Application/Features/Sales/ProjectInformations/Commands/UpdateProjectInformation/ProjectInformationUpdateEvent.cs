﻿using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.UpdateProjectInformation
{
    public class ProjectInformationUpdateEvent : BaseEvent
    {
        public Project Project { get; set; }

        public ProjectInformationUpdateEvent(Project project)
        {
            Project = project;
        }
    }
}