﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.CreateProjectInformation;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.UpdateProjectInformation
{
    internal class UpdateProjectInformationCommandHandler : IRequestHandler<UpdateProjectInformationRequest, Result<Project>>
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProjectRepository _projectRepository;

        public UpdateProjectInformationCommandHandler(IMapper mapper, IUnitOfWork unitOfWork, IProjectRepository projectRepository)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _projectRepository = projectRepository;
        }

        public async Task<Result<Project>> Handle(UpdateProjectInformationRequest request, CancellationToken cancellationToken)
        {
            var projectValidate = await _unitOfWork.Repository<Project>().FindByCondition(k => k.Id == request.Id).FirstOrDefaultAsync();
            //var checkMasterProjectId = await _projectRepository.CheckMasterProjectId(request.MasterProjectId);
            //if(checkMasterProjectId == false)
            //{
            //    return await Result<Project>.FailureAsync("Master Id Project Already Exist");
            //}
            if (projectValidate != null)
            {
                projectValidate.NumberSpk = request.NumberSpk;
                projectValidate.ProjectStartDate = request.ProjectStartDate;
                projectValidate.ProjectEndDate = request.ProjectEndDate;
                projectValidate.PicId = request.PicId;
                projectValidate.MasterProjectId = request.MasterProjectId;

                await _unitOfWork.Repository<Project>().UpdateAsync(projectValidate);
                projectValidate.AddDomainEvent(new ProjectInformationCreatedEvent(projectValidate));
                await _unitOfWork.Save(cancellationToken);
                return await Result<Project>.SuccessAsync("Success");
            }
            return await Result<Project>.SuccessAsync("Id not found");
        }
    }
}