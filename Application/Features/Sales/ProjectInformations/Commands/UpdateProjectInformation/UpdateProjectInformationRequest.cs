﻿using MediatR;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.CreateProjectInformation;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json.Serialization;

namespace SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.UpdateProjectInformation
{
    public class UpdateProjectInformationRequest : IRequest<Result<Project>>
    {
        [JsonPropertyName("id")]
        public Guid Id { get; set; }

        [JsonPropertyName("number_spk")]
        public string NumberSpk { get; set; }

        [JsonPropertyName("master_project_id")]
        public Guid MasterProjectId { get; set; }

        [JsonPropertyName("project_start_date")]
        public DateOnly ProjectStartDate { get; set; }

        [JsonPropertyName("project_end_date")]
        public DateOnly ProjectEndDate { get; set; }

        [JsonPropertyName("pic_id")]
        public Guid PicId { get; set; }
    }
}