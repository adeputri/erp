﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IEmployeeRepository
    {
        Task<Employee> GetEmployeeAsync(Guid id);
    }
}
