﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IEmployeeTaxStatusRespository
    {
        Task<bool> ValidateData(EmployeeTaxStatus employeeTaxStatus);
    }
}