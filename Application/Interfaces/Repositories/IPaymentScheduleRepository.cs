﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IPaymentScheduleRepository
    {
        Task<bool> ValidateData(PaymentSchedule paymentSchedule);
    }
}