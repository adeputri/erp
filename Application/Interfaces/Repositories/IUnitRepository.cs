﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IUnitRepository
    {
        Task<bool> ValidateData(MasterUnit masterUnit);
    }
}