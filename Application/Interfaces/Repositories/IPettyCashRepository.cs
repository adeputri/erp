﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IPettyCashRepository
    {
        Task<bool> ValidateDate(CodePettyCash codePettyCash);
    }
}