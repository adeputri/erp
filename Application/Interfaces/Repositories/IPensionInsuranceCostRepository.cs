﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IPensionInsuranceCostRepository
    {
        Task<bool> ValidateData(PensionInsuranceCost pensionInsuranceCost);
    }
}