﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IEmployeeLevelRepository
    {
        Task<bool> ValidateData(EmployeeLevel employeeLevel);
    }
}