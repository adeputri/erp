﻿using Microsoft.AspNetCore.Http;
using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Queries.GetAllTaskByIdProject;
using SkeletonApi.Application.Features.Sales.ProjectInformations.Commands.Import;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Queries.GetProjectWithPagination;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IProjectRepository
    {
        Task<IEnumerable<GetAllTaskByIdProjectDto>> GetAllTaskByIdProject(Guid projectId);

        Task<List<GetProjectWithPaginationDto>> GetProgress();

        Task<bool> CheckMasterProjectId(Guid MasterProjectId);

        Task<List<ProjectImportDto>> UploadExcel(IFormFile file);
        Task<bool> ValidateData(Project supplier);
    }
}