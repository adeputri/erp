﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IPaymentStatusRepository
    {
        Task<bool> ValidateData(PaymentStatus paymentStatus);
    }
}