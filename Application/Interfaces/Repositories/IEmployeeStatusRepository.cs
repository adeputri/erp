﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IEmployeeStatusRepository
    {
        Task<bool> ValidateData(EmployeeStatus employeeStatus);
    }
}