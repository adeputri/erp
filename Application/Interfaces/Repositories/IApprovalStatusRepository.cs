﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IApprovalStatusRepository
    {
        Task<bool> Validatedata(ApprovalStatus approvalStatus);
    }
}