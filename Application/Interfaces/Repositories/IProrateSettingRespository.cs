﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IProrateSettingRespository
    {
        Task<bool> ValidateData(ProrateSetting prorateSetting);
    }
}