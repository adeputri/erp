﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface ITeamRepository
    {
        Task<bool> ValidateData(Team team);
    }
}