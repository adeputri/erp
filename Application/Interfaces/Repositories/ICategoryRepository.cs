﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface ICategoryRepository
    {
        Task<bool> ValidateData(Category category);
    }
}