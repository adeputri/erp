﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface ITaxMethodRepository
    {
        Task<bool> Validatedata(TaxMethode taxMethode);
    }
}