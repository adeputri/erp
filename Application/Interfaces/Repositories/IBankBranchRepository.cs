﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IBankBranchRepository
    {
        Task<bool> ValidateData(BranchBank branchBank);
    }
}