﻿using Microsoft.AspNetCore.Http;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.Import;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnById;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnWithPagination;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPreviewFilePpn;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IPpnRepository
    {
        Task<(bool, string)> UploadFile(FileDto file);

        Task<GetPreviewFilePpnDto> GetFile(string path);

        Task<List<GetPpnWithPaginationDto>> GetAll(string searchTerm, int month, int year);

        Task<List<PpnImportDto>> UploadExcel(IFormFile file);

        Task<GetPpnByIdDto> GetById(string uniqeCode);
    }
}