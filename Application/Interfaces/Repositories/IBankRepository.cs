﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IBankRepository
    {
        Task<bool> ValidateData(Bank bank);
    }
}