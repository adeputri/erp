﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface ICustomerRepository
    {
        Task<bool> ValidateData(Customer customer);
    }
}