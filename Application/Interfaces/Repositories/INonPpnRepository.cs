﻿using Microsoft.AspNetCore.Http;
using SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnById;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnInformation;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnWithPagination;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetPreviewFileNonPpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface INonPpnRepository
    {
        Task<(bool, string)> UploadFile(FileDto file);

        Task<GetPreviewFileNonPpnDto> GetFile(string path);

        Task<List<GetNonPpnWithPaginationDto>> GetNonPpnWithPagination(string searchterm, int month, int year, Guid categoryId);

        Task<GetNonPpnInformationDto> GetNonPpnInformation(int month, int year, Guid categoryId);

        Task<List<NonPpnImportDto>> UploadExcel(IFormFile file);

        Task<GetNonPpnByIdDto> GetNonPpnById(Guid id);
    }
}