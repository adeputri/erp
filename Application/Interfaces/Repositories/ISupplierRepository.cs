﻿using Microsoft.AspNetCore.Http;
using SkeletonApi.Application.Features.MasterData.Suppliers.Commands.Import;
using SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface ISupplierRepository
    {
        Task<bool> ValidateData(Supplier supplier);
        Task<List<SupplierImportDto>> UploadExcel(IFormFile file);
    }
}