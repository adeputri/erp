﻿using SkeletonApi.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Interfaces.Repositories.Configuration
{
    public interface IMerkRepository
    {
        Task<bool> ValidateData(Merk bank);
    }
}