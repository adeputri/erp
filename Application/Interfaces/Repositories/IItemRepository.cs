﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces.Repositories
{
    public interface IItemRepository
    {
        Task<Item> GetItemAsync(Guid id);
        Task<Guid> GetIdByBarcodeAsync(string barcode);
        Task<bool> ValidateData(Item item);
    }
}