﻿using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Application.Interfaces
{
    public interface IPositionRepository
    {
        Task<bool> ValidateData(Position position);
    }
}