﻿namespace SkeletonApi.Application.Common.Exceptions
{
    public sealed class FailedAuthenticationException : UnauthorizedException
    {
        public FailedAuthenticationException(string message)
        : base(message)
        {
        }
    }
}