﻿namespace SkeletonApi.Application.Common.Exceptions
{
    public class SupplierNameNotFoundException : NotFoundException
    {
        private const string _message = "Supplier with name: {0} not found.";

        public SupplierNameNotFoundException(string name) : base(string.Format(_message, name))
        {
        }

        public static void Throw(string name)
        {
            throw new SupplierNameNotFoundException(name);
        }
    }
}