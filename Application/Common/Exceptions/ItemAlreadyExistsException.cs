﻿using System.Diagnostics.CodeAnalysis;

namespace SkeletonApi.Application.Common.Exceptions
{
    public class ItemAlreadyExistsException : BadRequestException
    {
        private const string _message = "A item already exists for barcode {0}.";

        public ItemAlreadyExistsException(string barcode) : base(string.Format(_message, barcode))
        {
        }

        [DoesNotReturn]
        public static void Throw(string barcode)
        {
            throw new ItemAlreadyExistsException(barcode);
        }
    }
}
