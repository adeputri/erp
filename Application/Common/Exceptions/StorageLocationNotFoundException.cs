﻿using System.Diagnostics.CodeAnalysis;

namespace SkeletonApi.Application.Common.Exceptions
{
    public class StorageLocationNotFoundException : NotFoundException
    {
        private const string _message = "Storage Location with id: {0} not found.";

        public StorageLocationNotFoundException(Guid id) : base(string.Format(_message, id))
        {
        }

        [DoesNotReturn]
        public static void Throw(Guid id)
        {
            throw new StorageLocationNotFoundException(id);
        }
    }
}
