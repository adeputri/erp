﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Common.Exceptions
{
    public class CategoryNotFoundException : NotFoundException
    {
        private const string _message = "Category: {0} not found.";

        public CategoryNotFoundException(string category) : base(string.Format(_message, category))
        {
        }

        public static void Throw(string code)
        {
            throw new CategoryNotFoundException(code);
        }
    }
}
