﻿namespace SkeletonApi.Application.Common.Exceptions
{
    public abstract class UnauthorizedException : Exception
    {
        public UnauthorizedException(string message)
        : base(message)
        { }
    }
}