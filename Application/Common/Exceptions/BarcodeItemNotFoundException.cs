﻿using System.Diagnostics.CodeAnalysis;

namespace SkeletonApi.Application.Common.Exceptions
{
    public class BarcodeItemNotFoundException : NotFoundException
    {
        private const string _message = "Item with Barcode {0} not found.";

        public BarcodeItemNotFoundException(string barcode) : base(string.Format(_message, barcode))
        {
        }

        [DoesNotReturn]
        public static void Throw(string barcode)
        {
            throw new BarcodeItemNotFoundException(barcode);
        }
    }
}
