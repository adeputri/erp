﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonApi.Application.Common.Exceptions
{
    public class EmployeeNameNotFound : NotFoundException
    {
        private const string _message = "Employee with name: {0} not found.";

        public EmployeeNameNotFound(string name) : base(string.Format(_message, name))
        {
        }

        public static void Throw(string name)
        {
            throw new EmployeeNameNotFound(name);
        }
    }
}
