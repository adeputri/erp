﻿namespace SkeletonApi.Application.Common.Exceptions
{
    public class SpkNumberNotFoundException : NotFoundException
    {
        private const string _message = "Spk number: {0} not found.";

        public SpkNumberNotFoundException(string name) : base(string.Format(_message, name))
        {
        }

        public static void Throw(string name)
        {
            throw new SpkNumberNotFoundException(name);
        }
    }
}