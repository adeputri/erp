﻿namespace SkeletonApi.Application.Common.Exceptions
{
    public class ProjectNotFoundException : NotFoundException
    {
        private const string _message = "Project with id: {0} not found.";

        public ProjectNotFoundException(Guid id) : base(string.Format(_message, id))
        {
        }

        public static void Throw(Guid id)
        {
            throw new ProjectNotFoundException(id);
        }
    }
}
