﻿using System.Diagnostics.CodeAnalysis;

namespace SkeletonApi.Application.Common.Exceptions
{
    public class EmployeeNotFoundException : NotFoundException
    {
        private const string _message = "Employee with id: {0} not found.";

        public EmployeeNotFoundException(Guid id) : base(string.Format(_message, id))
        {
        }

        [DoesNotReturn]
        public static void Throw(Guid id)
        {
            throw new EmployeeNotFoundException(id);
        }
    }
}
