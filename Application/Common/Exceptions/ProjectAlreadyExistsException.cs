﻿using System.Diagnostics.CodeAnalysis;

namespace SkeletonApi.Application.Common.Exceptions
{
    public class ProjectAlreadyExistsException : BadRequestException
    {
        private const string _message = "A project already exists for Project {0} and Customer {1}.";

        public ProjectAlreadyExistsException(string project, Guid customerId) : base(string.Format(_message, project, customerId))
        {
        }

        [DoesNotReturn]
        public static void Throw(string project, Guid customerId)
        {
            throw new ProjectAlreadyExistsException(project, customerId);
        }
    }
}
