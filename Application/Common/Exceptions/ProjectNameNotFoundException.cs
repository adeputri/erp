﻿namespace SkeletonApi.Application.Common.Exceptions
{
    public class ProjectNameNotFoundException : NotFoundException
    {
        private const string _message = "Project with name: {0} not found.";

        public ProjectNameNotFoundException(string name) : base(string.Format(_message, name))
        {
        }

        public static void Throw(string name)
        {
            throw new ProjectNameNotFoundException(name);
        }
    }
}