﻿namespace SkeletonApi.Application.Common.Exceptions
{
    public class CodeNotFoundException : NotFoundException
    {
        private const string _message = "Code: {0} not found.";

        public CodeNotFoundException(string code) : base(string.Format(_message, code))
        {
        }

        public static void Throw(string code)
        {
            throw new CodeNotFoundException(code);
        }
    }
}