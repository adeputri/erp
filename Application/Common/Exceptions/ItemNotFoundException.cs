﻿namespace SkeletonApi.Application.Common.Exceptions
{
    public class ItemNotFoundException : NotFoundException
    {
        private const string _message = "Item with id: {0} not found.";

        public ItemNotFoundException(Guid id) : base(string.Format(_message, id))
        {
        }

        public static void Throw(Guid id)
        {
            throw new ItemNotFoundException(id);
        }
    }
}
