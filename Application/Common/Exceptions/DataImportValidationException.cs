﻿namespace SkeletonApi.Application.Common.Exceptions
{
    public sealed class DataImportValidationException : BadRequestException
    {
        public DataImportValidationException()
        : base("Invalid data request. The amount of data is not the same.")
        {
        }
    }
}