﻿using AutoMapper;
using SkeletonApi.Application.Features.Accounts;
using SkeletonApi.Application.Features.Accounts.Profiles.Commands.CreateAccount;
using SkeletonApi.Application.Features.ManagementUser.Permissions;
using SkeletonApi.Application.Features.ManagementUser.Permissions.Commands.CreatePermissions;
using SkeletonApi.Application.Features.ManagementUser.Permissions.Commands.UpdatePermissions;
using SkeletonApi.Application.Features.ManagementUser.Roles;
using SkeletonApi.Application.Features.ManagementUser.Roles.Commands.CreateRoles;
using SkeletonApi.Application.Features.ManagementUser.Users.Commands.CreateUser;
using SkeletonApi.Application.Features.MasterData.ApprovalStatuses;
using SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.CreateApprovalStatus;
using SkeletonApi.Application.Features.MasterData.BankBranchs;
using SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.CreateBankBranch;
using SkeletonApi.Application.Features.MasterData.Banks;
using SkeletonApi.Application.Features.MasterData.Banks.Commands.CreateBank;
using SkeletonApi.Application.Features.MasterData.Categories;
using SkeletonApi.Application.Features.MasterData.Categories.Commands.CreateCategory;
using SkeletonApi.Application.Features.MasterData.Customers;
using SkeletonApi.Application.Features.MasterData.Customers.Commands.CreateCustomer;
using SkeletonApi.Application.Features.MasterData.EmployeeLevels;
using SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.CreateEmployeeLevel;
using SkeletonApi.Application.Features.MasterData.EmployeeStatuses;
using SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.CreateEmployeesStatus;
using SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses;
using SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.CreateEmployeeTaxStatus;
using SkeletonApi.Application.Features.MasterData.Items;
using SkeletonApi.Application.Features.MasterData.Items.Commands.CreateItem;
using SkeletonApi.Application.Features.MasterData.MasterProjects;
using SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.CreateMasterProject;
using SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.UpdateMasterProject;
using SkeletonApi.Application.Features.MasterData.Merks;
using SkeletonApi.Application.Features.MasterData.Merks.Commands.CreateMerk;
using SkeletonApi.Application.Features.MasterData.PaymentSchedules;
using SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.CreatePaymentSchedule;
using SkeletonApi.Application.Features.MasterData.PaymentStatuses;
using SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.CreatePaymentStatus;
using SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts;
using SkeletonApi.Application.Features.MasterData.PensiunCosts.Commands.CreatePensiunCost;
using SkeletonApi.Application.Features.MasterData.PettyCashs;
using SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.CreateCodePettyCash;
using SkeletonApi.Application.Features.MasterData.Positions;
using SkeletonApi.Application.Features.MasterData.Positions.Commands.CreatePositions;
using SkeletonApi.Application.Features.MasterData.Suppliers;
using SkeletonApi.Application.Features.MasterData.Suppliers.Commands.CreateSupplier;
using SkeletonApi.Application.Features.MasterData.Suppliers.Commands.Import;
using SkeletonApi.Application.Features.MasterData.TaxMethodes;
using SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.CreateTaxMethod;
using SkeletonApi.Application.Features.MasterData.Teams;
using SkeletonApi.Application.Features.MasterData.Teams.Commands.CreateTeam;
using SkeletonApi.Application.Features.MasterData.Title;
using SkeletonApi.Application.Features.MasterData.Title.Commands.CreateSettingTitle;
using SkeletonApi.Application.Features.MasterData.Units;
using SkeletonApi.Application.Features.MasterData.Units.Commands.CreateUnit;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN.Commands.CreateReportNonPPN;
using SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import;
using SkeletonApi.Application.Features.Purchases.PPN;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.Import;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.UpdatePpn;
using SkeletonApi.Application.Features.Sales.NonProjects;
using SkeletonApi.Application.Features.Sales.NonProjects.Commands.CreateNonProject;
using SkeletonApi.Application.Features.Sales.ProjectInformations;
using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks;
using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Commands.CreateMainTask;
using SkeletonApi.Application.Features.Sales.ProjectInformations.Commands.Import;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.CreateProjectInformation;
using SkeletonApi.Application.Features.Users;
using SkeletonApi.Domain.Entities;
using System.Reflection;

namespace SkeletonApi.Application.Common.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            ApplyMappingsFromAssembly(Assembly.GetExecutingAssembly());

            CreateMap<CreateUserRequest, User>();
            CreateMap<User, CreateUserResponseDto>();

            CreateMap<CreateRolesRequest, Role>();
            CreateMap<Role, CreateRolesResponseDto>();

            CreateMap<CreatePermissionsRequest, Permission>();
            CreateMap<Permission, CreatePermissionsResponseDto>();

            CreateMap<UpdatePermissionsRequest, Permission>();

            CreateMap<CreateAccountRequest, Account>();
            CreateMap<Account, CreateAccountResponseDto>();

            CreateMap<CreateCustomerRequest, Customer>();
            CreateMap<Customer, CreateCustomerResponseDto>();

            CreateMap<CreatePositionRequest, Position>();
            CreateMap<Position, CreatePositionResponseDto>();

            CreateMap<CreateMasterProjectRequest, MasterProject>();
            CreateMap<MasterProject, CreateProjectResponseDto>();

            CreateMap<UpdateMasterProjectRequest, MasterProject>();
            CreateMap<MasterProject, UpdateProjectResponseDto>();

            CreateMap<CreateTeamRequest, Team>();
            CreateMap<Team, CreateTeamResponseDto>();

            CreateMap<CreateMerkRequest, Merk>();
            CreateMap<Merk, CreateMerkResponseDto>();

            CreateMap<CreateEmployeeStatusRequest, EmployeeStatus>();
            CreateMap<EmployeeStatus, CreateEmployeeStatusResponseDto>();

            CreateMap<CreateEmployeeLevelRequest, EmployeeLevel>();
            CreateMap<EmployeeLevel, CreateEmployeeLevelResponseDto>();

            CreateMap<CreateBankRequest, Bank>();
            CreateMap<Bank, CreateBankResponseDto>();

            CreateMap<CreateBankBranchRequest, BranchBank>();
            CreateMap<BranchBank, CreateBankBranchResponseDto>();

            CreateMap<CreateEmployeeTaxStatusRequest, EmployeeTaxStatus>();
            CreateMap<EmployeeTaxStatus, CreateEmployeeTaxStatusResponseDto>();

            CreateMap<CreatePaymentScheduleRequest, PaymentSchedule>();
            CreateMap<PaymentSchedule, CreatePaymentScheduleResponseDto>();

            CreateMap<CreatePaymentStatusRequest, PaymentStatus>();
            CreateMap<PaymentStatus, CreatePaymentStatusResponseDto>();

            CreateMap<CreateItemRequest, Item>();
            CreateMap<Item, CreateItemResponseDto>();

            CreateMap<CreateUnitRequest, MasterUnit>();
            CreateMap<MasterUnit, CreateUnitResponnseDto>();

            CreateMap<CreateTaxMethodRequest, TaxMethode>();
            CreateMap<TaxMethode, CreateTaxMethodResponseDto>();

            CreateMap<CreateSupplierRequest, Supplier>();
            CreateMap<Supplier, CreateSupplierResponseDto>();
            CreateMap<Supplier, SupplierImportDto>().ReverseMap();
            CreateMap<SupplierImportDto, CreateSupplierResponseDto>().ReverseMap();

            CreateMap<CreateApprovalStatusRequest, ApprovalStatus>();
            CreateMap<ApprovalStatus, CreateApprovalStatusResponseDto>();

            CreateMap<CreatePensionInsuranceCostsRequest, PensionInsuranceCost>();
            CreateMap<PensionInsuranceCost, CreatePensionInsuranceCostResponseDto>();

            CreateMap<CreatePpnRequest, PurchasePpn>();
            CreateMap<PurchasePpn, CreatePpnResponseDto>();
            CreateMap<UpdatePpnRequest, PurchasePpn>();
            CreateMap<PurchasePpn, PpnImportDto>().ReverseMap();

            CreateMap<CreateCodePettyCashRequest, CodePettyCash>();
            CreateMap<CodePettyCash, CreateCodePettyCashResponseDto>();

            CreateMap<CreateSettingTitleRequest, SettingTitle>();
            CreateMap<SettingTitle, CreateSettingTitleResponseDto>();

            CreateMap<CreateCategoryRequest, Category>();
            CreateMap<Category, CreateCategoryResponseDto>();

            CreateMap<CreateNonPpnRequest, PurchaseNonPpn>();
            CreateMap<PurchaseNonPpn, CreateNonPpnResponseDto>();
            CreateMap<PurchaseNonPpn, NonPpnImportDto>().ReverseMap();
            CreateMap<NonPpnImportDto, CreateNonPpnResponseDto>().ReverseMap();

            CreateMap<CreateProjectInformationRequest, Project>();
            CreateMap<Project, CreateProjectInformationResponseDto>();
            CreateMap<Project, ProjectImportDto>().ReverseMap();
            CreateMap<ProjectImportDto, CreateProjectInformationResponseDto>().ReverseMap();

            CreateMap<CreateMainTaskRequest, CreateMainTaskResponseDto>();

            CreateMap<CreateNonProjectRequest, NonProject>();
            CreateMap<NonProject, CreateNonProjectResponseDto>();
        }

        private void ApplyMappingsFromAssembly(Assembly assembly)
        {
            var mapFromType = typeof(IMapFrom<>);

            var mappingMethodName = nameof(IMapFrom<object>.Mapping);

            bool HasInterface(Type t) => t.IsGenericType && t.GetGenericTypeDefinition() == mapFromType;

            var types = assembly.GetExportedTypes().Where(t => t.GetInterfaces().Any(HasInterface)).ToList();

            var argumentTypes = new Type[] { typeof(Profile) };

            foreach (var type in types)
            {
                var instance = Activator.CreateInstance(type);

                var methodInfo = type.GetMethod(mappingMethodName);

                if (methodInfo != null)
                {
                    methodInfo.Invoke(instance, new object[] { this });
                }
                else
                {
                    var interfaces = type.GetInterfaces().Where(HasInterface).ToList();

                    if (interfaces.Count > 0)
                    {
                        foreach (var @interface in interfaces)
                        {
                            var interfaceMethodInfo = @interface.GetMethod(mappingMethodName, argumentTypes);

                            interfaceMethodInfo.Invoke(instance, new object[] { this });
                        }
                    }
                }
            }
        }
    }
}