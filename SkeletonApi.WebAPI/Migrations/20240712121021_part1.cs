﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace SkeletonApi.WebAPI.Migrations
{
    /// <inheritdoc />
    public partial class part1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
        //    migrationBuilder.CreateTable(
        //        name: "Accounts",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            username = table.Column<string>(type: "text", nullable: true),
        //            photo_url = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Accounts", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "ActivityUsers",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            username = table.Column<string>(type: "text", nullable: true),
        //            logtype = table.Column<string>(type: "text", nullable: true),
        //            datetime = table.Column<DateTime>(type: "timestamp", nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_ActivityUsers", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "ApprovalStatuses",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            status = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_ApprovalStatuses", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "AspNetRoles",
        //        columns: table => new
        //        {
        //            Id = table.Column<string>(type: "text", nullable: false),
        //            CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
        //            UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
        //            DeletedBy = table.Column<Guid>(type: "uuid", nullable: true),
        //            CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            DeletedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
        //            NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
        //            ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_AspNetRoles", x => x.Id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "AspNetUsers",
        //        columns: table => new
        //        {
        //            Id = table.Column<string>(type: "text", nullable: false),
        //            RefreshToken = table.Column<string>(type: "text", nullable: true),
        //            RefreshTokenExpiryTime = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
        //            CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
        //            UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
        //            DeletedBy = table.Column<Guid>(type: "uuid", nullable: true),
        //            CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            DeletedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
        //            NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
        //            Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
        //            NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
        //            EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
        //            PasswordHash = table.Column<string>(type: "text", nullable: true),
        //            SecurityStamp = table.Column<string>(type: "text", nullable: true),
        //            ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
        //            PhoneNumber = table.Column<string>(type: "text", nullable: true),
        //            PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
        //            TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
        //            LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
        //            LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
        //            AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_AspNetUsers", x => x.Id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Banks",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            bank_name = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Banks", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Categories",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            category_name = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Categories", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "CodePettyCashes",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            code_name = table.Column<string>(type: "text", nullable: false),
        //            item = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_CodePettyCashes", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Customers",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            customer_name = table.Column<string>(type: "text", nullable: false),
        //            npwp = table.Column<string>(type: "text", nullable: false),
        //            address = table.Column<string>(type: "text", nullable: false),
        //            contact_person = table.Column<string>(type: "text", nullable: false),
        //            phone_number = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Customers", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "EmployeeLevels",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            level = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_EmployeeLevels", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Employees",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            first_name = table.Column<string>(type: "text", nullable: false),
        //            last_name = table.Column<string>(type: "text", nullable: false),
        //            gender = table.Column<string>(type: "text", nullable: false),
        //            email = table.Column<string>(type: "text", nullable: false),
        //            mobile_phone = table.Column<string>(type: "text", nullable: false),
        //            phone = table.Column<string>(type: "text", nullable: false),
        //            place_of_birth = table.Column<string>(type: "text", nullable: false),
        //            birth_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
        //            blood_type = table.Column<string>(type: "text", nullable: false),
        //            marital_status = table.Column<string>(type: "text", nullable: false),
        //            religion = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Employees", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "EmployeeStatuses",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            status = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_EmployeeStatuses", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "EmployeeTaxStatuses",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            Status = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_EmployeeTaxStatuses", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Merks",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            Name = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Merks", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "PaymentSchedules",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            schedule = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_PaymentSchedules", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "PaymentStatuses",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            status = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_PaymentStatuses", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Positions",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            position_name = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Positions", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "ProrateSettings",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            setting = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_ProrateSettings", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "SettingTitles",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            title_name = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_SettingTitles", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Suppliers",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            supplier_name = table.Column<string>(type: "text", nullable: false),
        //            npwp = table.Column<string>(type: "text", nullable: false),
        //            address = table.Column<string>(type: "text", nullable: false),
        //            contact_person = table.Column<string>(type: "text", nullable: false),
        //            phone_number = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Suppliers", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "TaxMethodes",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            status = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_TaxMethodes", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Teams",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            team_name = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Teams", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Units",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            unit_name = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Units", x => x.id);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "AspNetRoleClaims",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(type: "integer", nullable: false)
        //                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
        //            RoleId = table.Column<string>(type: "text", nullable: false),
        //            ClaimType = table.Column<string>(type: "text", nullable: true),
        //            ClaimValue = table.Column<string>(type: "text", nullable: true),
        //            Discriminator = table.Column<string>(type: "text", nullable: false),
        //            CreatedBy = table.Column<Guid>(type: "uuid", nullable: true),
        //            UpdatedBy = table.Column<Guid>(type: "uuid", nullable: true),
        //            DeletedBy = table.Column<Guid>(type: "uuid", nullable: true),
        //            CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            DeletedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
        //                column: x => x.RoleId,
        //                principalTable: "AspNetRoles",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "AspNetUserClaims",
        //        columns: table => new
        //        {
        //            Id = table.Column<int>(type: "integer", nullable: false)
        //                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
        //            UserId = table.Column<string>(type: "text", nullable: false),
        //            ClaimType = table.Column<string>(type: "text", nullable: true),
        //            ClaimValue = table.Column<string>(type: "text", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
        //            table.ForeignKey(
        //                name: "FK_AspNetUserClaims_AspNetUsers_UserId",
        //                column: x => x.UserId,
        //                principalTable: "AspNetUsers",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "AspNetUserLogins",
        //        columns: table => new
        //        {
        //            LoginProvider = table.Column<string>(type: "text", nullable: false),
        //            ProviderKey = table.Column<string>(type: "text", nullable: false),
        //            ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
        //            UserId = table.Column<string>(type: "text", nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
        //            table.ForeignKey(
        //                name: "FK_AspNetUserLogins_AspNetUsers_UserId",
        //                column: x => x.UserId,
        //                principalTable: "AspNetUsers",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "AspNetUserRoles",
        //        columns: table => new
        //        {
        //            UserId = table.Column<string>(type: "text", nullable: false),
        //            RoleId = table.Column<string>(type: "text", nullable: false)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
        //            table.ForeignKey(
        //                name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
        //                column: x => x.RoleId,
        //                principalTable: "AspNetRoles",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_AspNetUserRoles_AspNetUsers_UserId",
        //                column: x => x.UserId,
        //                principalTable: "AspNetUsers",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "AspNetUserTokens",
        //        columns: table => new
        //        {
        //            UserId = table.Column<string>(type: "text", nullable: false),
        //            LoginProvider = table.Column<string>(type: "text", nullable: false),
        //            Name = table.Column<string>(type: "text", nullable: false),
        //            Value = table.Column<string>(type: "text", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
        //            table.ForeignKey(
        //                name: "FK_AspNetUserTokens_AspNetUsers_UserId",
        //                column: x => x.UserId,
        //                principalTable: "AspNetUsers",
        //                principalColumn: "Id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "BranchBanks",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            branch_bank_name = table.Column<string>(type: "text", nullable: false),
        //            bank_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_BranchBanks", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_BranchBanks_Banks_bank_id",
        //                column: x => x.bank_id,
        //                principalTable: "Banks",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "MasterProjects",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            project_name = table.Column<string>(type: "text", nullable: false),
        //            customer_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_MasterProjects", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_MasterProjects_Customers_customer_id",
        //                column: x => x.customer_id,
        //                principalTable: "Customers",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Items",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            barcode_item = table.Column<string>(type: "text", nullable: false),
        //            type_item = table.Column<string>(type: "text", nullable: false),
        //            item_name = table.Column<string>(type: "text", nullable: false),
        //            merk_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            unit_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Items", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_Items_Merks_merk_id",
        //                column: x => x.merk_id,
        //                principalTable: "Merks",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_Items_Units_unit_id",
        //                column: x => x.unit_id,
        //                principalTable: "Units",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Projects",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            number_spk = table.Column<string>(type: "text", nullable: false),
        //            master_project_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            project_start_date = table.Column<DateOnly>(type: "date", nullable: false),
        //            project_end_date = table.Column<DateOnly>(type: "date", nullable: false),
        //            pic_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Projects", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_Projects_Employees_pic_id",
        //                column: x => x.pic_id,
        //                principalTable: "Employees",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_Projects_MasterProjects_master_project_id",
        //                column: x => x.master_project_id,
        //                principalTable: "MasterProjects",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "PurchaseNonPpns",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            master_project_id = table.Column<Guid>(type: "uuid", nullable: true),
        //            code_id = table.Column<Guid>(type: "uuid", nullable: true),
        //            category_id = table.Column<Guid>(type: "uuid", nullable: true),
        //            bon_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            bon_number = table.Column<string>(type: "text", nullable: true),
        //            transaction = table.Column<string>(type: "text", nullable: true),
        //            debit = table.Column<double>(type: "double precision", nullable: true),
        //            kredit = table.Column<double>(type: "double precision", nullable: true),
        //            payment_method = table.Column<string>(type: "text", nullable: true),
        //            approval_by = table.Column<string>(type: "text", nullable: true),
        //            approval_status = table.Column<string>(type: "text", nullable: true),
        //            note = table.Column<string>(type: "text", nullable: true),
        //            file_path = table.Column<string>(type: "text", nullable: true),
        //            type = table.Column<string>(type: "text", nullable: true),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_PurchaseNonPpns", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_PurchaseNonPpns_Categories_category_id",
        //                column: x => x.category_id,
        //                principalTable: "Categories",
        //                principalColumn: "id");
        //            table.ForeignKey(
        //                name: "FK_PurchaseNonPpns_CodePettyCashes_code_id",
        //                column: x => x.code_id,
        //                principalTable: "CodePettyCashes",
        //                principalColumn: "id");
        //            table.ForeignKey(
        //                name: "FK_PurchaseNonPpns_MasterProjects_master_project_id",
        //                column: x => x.master_project_id,
        //                principalTable: "MasterProjects",
        //                principalColumn: "id");
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "RequestItemWarehouses",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            item_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            request_employee_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            pic_employee_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            project_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            qty_request = table.Column<int>(type: "integer", nullable: false),
        //            is_finish = table.Column<bool>(type: "boolean", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_RequestItemWarehouses", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_RequestItemWarehouses_Employees_pic_employee_id",
        //                column: x => x.pic_employee_id,
        //                principalTable: "Employees",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_RequestItemWarehouses_Employees_request_employee_id",
        //                column: x => x.request_employee_id,
        //                principalTable: "Employees",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_RequestItemWarehouses_Items_item_id",
        //                column: x => x.item_id,
        //                principalTable: "Items",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_RequestItemWarehouses_MasterProjects_project_id",
        //                column: x => x.project_id,
        //                principalTable: "MasterProjects",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "Warehouse",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            item_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            request_employee_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            pic_employee_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            project_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            qty_item = table.Column<int>(type: "integer", nullable: false),
        //            note = table.Column<string>(type: "text", nullable: false),
        //            path_document = table.Column<string>(type: "text", nullable: true),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_Warehouse", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_Warehouse_Employees_pic_employee_id",
        //                column: x => x.pic_employee_id,
        //                principalTable: "Employees",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_Warehouse_Employees_request_employee_id",
        //                column: x => x.request_employee_id,
        //                principalTable: "Employees",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_Warehouse_Items_item_id",
        //                column: x => x.item_id,
        //                principalTable: "Items",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_Warehouse_MasterProjects_project_id",
        //                column: x => x.project_id,
        //                principalTable: "MasterProjects",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "PurchasePpns",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            unique_code = table.Column<string>(type: "text", nullable: false),
        //            payment_status = table.Column<string>(type: "text", nullable: false),
        //            supplier_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            project_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            pre_order_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
        //            pre_order_number = table.Column<string>(type: "text", nullable: false),
        //            pi_number = table.Column<string>(type: "text", nullable: false),
        //            inv_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
        //            inv_receipt_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
        //            inv_number = table.Column<string>(type: "text", nullable: false),
        //            faktur_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
        //            fp_number = table.Column<string>(type: "text", nullable: false),
        //            travel_doc_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
        //            travel_doc_number = table.Column<string>(type: "text", nullable: false),
        //            masa = table.Column<string>(type: "text", nullable: false),
        //            dpp = table.Column<double>(type: "double precision", nullable: false),
        //            ppn = table.Column<double>(type: "double precision", nullable: false),
        //            note = table.Column<string>(type: "text", nullable: true),
        //            paid_of_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
        //            file_path = table.Column<string>(type: "text", nullable: true),
        //            pph23 = table.Column<double>(type: "double precision", nullable: false),
        //            discount = table.Column<double>(type: "double precision", nullable: true),
        //            approval_by = table.Column<string>(type: "text", nullable: true),
        //            approval_status = table.Column<string>(type: "text", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_PurchasePpns", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_PurchasePpns_Projects_project_id",
        //                column: x => x.project_id,
        //                principalTable: "Projects",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //            table.ForeignKey(
        //                name: "FK_PurchasePpns_Suppliers_supplier_id",
        //                column: x => x.supplier_id,
        //                principalTable: "Suppliers",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "SectionProject",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            section_name = table.Column<string>(type: "text", nullable: false),
        //            project_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_SectionProject", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_SectionProject_Projects_project_id",
        //                column: x => x.project_id,
        //                principalTable: "Projects",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateTable(
        //        name: "TaskProject",
        //        columns: table => new
        //        {
        //            id = table.Column<Guid>(type: "uuid", nullable: false),
        //            task_name = table.Column<string>(type: "text", nullable: false),
        //            status = table.Column<string>(type: "text", nullable: false),
        //            assign_to = table.Column<string>(type: "text", nullable: false),
        //            due_date = table.Column<string>(type: "text", nullable: false),
        //            section_project_id = table.Column<Guid>(type: "uuid", nullable: false),
        //            created_by = table.Column<string>(type: "text", nullable: true),
        //            update_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            deleted_by = table.Column<Guid>(type: "uuid", nullable: true),
        //            created_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            update_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
        //            deleted_at = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
        //        },
        //        constraints: table =>
        //        {
        //            table.PrimaryKey("PK_TaskProject", x => x.id);
        //            table.ForeignKey(
        //                name: "FK_TaskProject_SectionProject_section_project_id",
        //                column: x => x.section_project_id,
        //                principalTable: "SectionProject",
        //                principalColumn: "id",
        //                onDelete: ReferentialAction.Cascade);
        //        });

        //    migrationBuilder.CreateIndex(
        //        name: "IX_AspNetRoleClaims_RoleId",
        //        table: "AspNetRoleClaims",
        //        column: "RoleId");

        //    migrationBuilder.CreateIndex(
        //        name: "RoleNameIndex",
        //        table: "AspNetRoles",
        //        column: "NormalizedName",
        //        unique: true);

        //    migrationBuilder.CreateIndex(
        //        name: "IX_AspNetUserClaims_UserId",
        //        table: "AspNetUserClaims",
        //        column: "UserId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_AspNetUserLogins_UserId",
        //        table: "AspNetUserLogins",
        //        column: "UserId");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_AspNetUserRoles_RoleId",
        //        table: "AspNetUserRoles",
        //        column: "RoleId");

        //    migrationBuilder.CreateIndex(
        //        name: "EmailIndex",
        //        table: "AspNetUsers",
        //        column: "NormalizedEmail");

        //    migrationBuilder.CreateIndex(
        //        name: "UserNameIndex",
        //        table: "AspNetUsers",
        //        column: "NormalizedUserName",
        //        unique: true);

        //    migrationBuilder.CreateIndex(
        //        name: "IX_BranchBanks_bank_id",
        //        table: "BranchBanks",
        //        column: "bank_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Items_merk_id",
        //        table: "Items",
        //        column: "merk_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Items_unit_id",
        //        table: "Items",
        //        column: "unit_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_MasterProjects_customer_id",
        //        table: "MasterProjects",
        //        column: "customer_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Projects_master_project_id",
        //        table: "Projects",
        //        column: "master_project_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Projects_pic_id",
        //        table: "Projects",
        //        column: "pic_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_PurchaseNonPpns_category_id",
        //        table: "PurchaseNonPpns",
        //        column: "category_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_PurchaseNonPpns_code_id",
        //        table: "PurchaseNonPpns",
        //        column: "code_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_PurchaseNonPpns_master_project_id",
        //        table: "PurchaseNonPpns",
        //        column: "master_project_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_PurchasePpns_project_id",
        //        table: "PurchasePpns",
        //        column: "project_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_PurchasePpns_supplier_id",
        //        table: "PurchasePpns",
        //        column: "supplier_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_RequestItemWarehouses_item_id",
        //        table: "RequestItemWarehouses",
        //        column: "item_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_RequestItemWarehouses_pic_employee_id",
        //        table: "RequestItemWarehouses",
        //        column: "pic_employee_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_RequestItemWarehouses_project_id",
        //        table: "RequestItemWarehouses",
        //        column: "project_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_RequestItemWarehouses_request_employee_id",
        //        table: "RequestItemWarehouses",
        //        column: "request_employee_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_SectionProject_project_id",
        //        table: "SectionProject",
        //        column: "project_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_TaskProject_section_project_id",
        //        table: "TaskProject",
        //        column: "section_project_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Warehouse_item_id",
        //        table: "Warehouse",
        //        column: "item_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Warehouse_pic_employee_id",
        //        table: "Warehouse",
        //        column: "pic_employee_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Warehouse_project_id",
        //        table: "Warehouse",
        //        column: "project_id");

        //    migrationBuilder.CreateIndex(
        //        name: "IX_Warehouse_request_employee_id",
        //        table: "Warehouse",
        //        column: "request_employee_id");
        //}

        ///// <inheritdoc />
        //protected override void Down(MigrationBuilder migrationBuilder)
        //{
        //    migrationBuilder.DropTable(
        //        name: "Accounts");

        //    migrationBuilder.DropTable(
        //        name: "ActivityUsers");

        //    migrationBuilder.DropTable(
        //        name: "ApprovalStatuses");

        //    migrationBuilder.DropTable(
        //        name: "AspNetRoleClaims");

        //    migrationBuilder.DropTable(
        //        name: "AspNetUserClaims");

        //    migrationBuilder.DropTable(
        //        name: "AspNetUserLogins");

        //    migrationBuilder.DropTable(
        //        name: "AspNetUserRoles");

        //    migrationBuilder.DropTable(
        //        name: "AspNetUserTokens");

        //    migrationBuilder.DropTable(
        //        name: "BranchBanks");

        //    migrationBuilder.DropTable(
        //        name: "EmployeeLevels");

        //    migrationBuilder.DropTable(
        //        name: "EmployeeStatuses");

        //    migrationBuilder.DropTable(
        //        name: "EmployeeTaxStatuses");

        //    migrationBuilder.DropTable(
        //        name: "PaymentSchedules");

        //    migrationBuilder.DropTable(
        //        name: "PaymentStatuses");

        //    migrationBuilder.DropTable(
        //        name: "Positions");

        //    migrationBuilder.DropTable(
        //        name: "ProrateSettings");

        //    migrationBuilder.DropTable(
        //        name: "PurchaseNonPpns");

        //    migrationBuilder.DropTable(
        //        name: "PurchasePpns");

        //    migrationBuilder.DropTable(
        //        name: "RequestItemWarehouses");

        //    migrationBuilder.DropTable(
        //        name: "SettingTitles");

        //    migrationBuilder.DropTable(
        //        name: "TaskProject");

        //    migrationBuilder.DropTable(
        //        name: "TaxMethodes");

        //    migrationBuilder.DropTable(
        //        name: "Teams");

        //    migrationBuilder.DropTable(
        //        name: "Warehouse");

        //    migrationBuilder.DropTable(
        //        name: "AspNetRoles");

        //    migrationBuilder.DropTable(
        //        name: "AspNetUsers");

        //    migrationBuilder.DropTable(
        //        name: "Banks");

        //    migrationBuilder.DropTable(
        //        name: "Categories");

        //    migrationBuilder.DropTable(
        //        name: "CodePettyCashes");

        //    migrationBuilder.DropTable(
        //        name: "Suppliers");

        //    migrationBuilder.DropTable(
        //        name: "SectionProject");

        //    migrationBuilder.DropTable(
        //        name: "Items");

        //    migrationBuilder.DropTable(
        //        name: "Projects");

        //    migrationBuilder.DropTable(
        //        name: "Merks");

        //    migrationBuilder.DropTable(
        //        name: "Units");

        //    migrationBuilder.DropTable(
        //        name: "Employees");

        //    migrationBuilder.DropTable(
        //        name: "MasterProjects");

        //    migrationBuilder.DropTable(
        //        name: "Customers");
        }
    }
}
