﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SkeletonApi.WebAPI.Migrations
{
    /// <inheritdoc />
    public partial class part5 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_Units_unit_id",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "unit_id",
                table: "Items",
                newName: "master_unit_id");

            migrationBuilder.RenameColumn(
                name: "type_item",
                table: "Items",
                newName: "type");

            migrationBuilder.RenameColumn(
                name: "item_name",
                table: "Items",
                newName: "name");

            migrationBuilder.RenameIndex(
                name: "IX_Items_unit_id",
                table: "Items",
                newName: "IX_Items_master_unit_id");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Units_master_unit_id",
                table: "Items",
                column: "master_unit_id",
                principalTable: "Units",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_Units_master_unit_id",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "type",
                table: "Items",
                newName: "type_item");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "Items",
                newName: "item_name");

            migrationBuilder.RenameColumn(
                name: "master_unit_id",
                table: "Items",
                newName: "unit_id");

            migrationBuilder.RenameIndex(
                name: "IX_Items_master_unit_id",
                table: "Items",
                newName: "IX_Items_unit_id");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Units_unit_id",
                table: "Items",
                column: "unit_id",
                principalTable: "Units",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
