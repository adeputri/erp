﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SkeletonApi.Application.Interfaces;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Application.Interfaces.Repositories.Configuration;
using SkeletonApi.Application.Interfaces.Repositories.Configuration.Dapper;
using SkeletonApi.Persistence.Contexts;
using SkeletonApi.Persistence.Repositories;
using SkeletonApi.Persistence.Repositories.Dapper;

namespace SkeletonApi.Persistence.IServiceCollectionExtensions
{
    public static class IServiceCollectionExtensions
    {
        public static void AddPersistenceLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext(configuration);
            services.AddRepositories();
        }

        public static void AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("sqlConnection");

            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseNpgsql(connectionString,
                   builder => builder.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));
        }

        private static void AddRepositories(this IServiceCollection services)
        {
            services
                .AddTransient(typeof(IUnitOfWork), typeof(UnitOfWork))
                .AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>))
                .AddTransient(typeof(IGenRepository<>), typeof(GenRepository<>))
                .AddTransient(typeof(IDataRepository<>), typeof(DataRepository<>))
                .AddScoped<DapperUnitOfWorkContext>()
                .AddTransient<IRoleRepository, RoleRepository>()
                .AddTransient<IEmployeeRepository, EmployeeRepository>()
                //.AddTransient<IStorageLocationRepository, StorageLocationRepository>()
                .AddTransient<IAuthenticationUserRepository, AuthenticationRepository>()
                .AddTransient<IAccountRepository, AccountRepository>()
                .AddTransient<INotificationRepository, NotificationRepository>()
                .AddScoped<IDiviceDateRepository, DeviceDataRepository>()
                .AddScoped<ICustomerRepository, CustomerRepository>()
                .AddScoped<IPositionRepository, PositionRepository>()
                .AddScoped<IProjectRepository, ProjectRepository>()
                .AddScoped<ITeamRepository, TeamRepository>()
                .AddScoped<IEmployeeStatusRepository, EmployeeStatusRepository>()
                .AddScoped<IEmployeeLevelRepository, EmployeeLevelRepository>()
                .AddScoped<IBankRepository, BankRepository>()
                .AddScoped<IBankBranchRepository, BankBranchRepository>()
                .AddScoped<IEmployeeTaxStatusRespository, EmployeetaxStatusRepository>()
                .AddScoped<IPaymentScheduleRepository, PaymentScheduleRepository>()
                .AddScoped<IPaymentStatusRepository, PaymentStatusRepository>()
                .AddScoped<IApprovalStatusRepository, ApprovalStatusRepository>()
                .AddScoped<IUnitRepository, UnitRepository>()
                .AddScoped<IProrateSettingRespository, ProrateSettingRespository>()
                .AddScoped<IItemRepository, ItemRepository>()
                .AddScoped<ITaxMethodRepository, TaxMethodRepository>()
                .AddScoped<IPensionInsuranceCostRepository, PensionInsuranceCostRepository>()
                .AddScoped<ISupplierRepository, SupplierRepository>()
                .AddScoped<IPpnRepository, PpnRepository>()
                .AddScoped<IPettyCashRepository, PettyCashRepository>()
                .AddScoped<ICategoryRepository, CategoryRepository>()
                .AddScoped<INonPpnRepository, NonPpnRepository>()
                .AddScoped<IPpnRepository, PpnRepository>()
                .AddScoped<IMasterProjectRepository, MasterProjectRepository>()
                .AddScoped<IMerkRepository, MerkRepository>()
                .AddTransient<IUserRepository, UserRepository>();
        }
    }
}