﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Common.Exceptions;
using SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnById;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnInformation;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnWithPagination;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetPreviewFileNonPpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class NonPpnRepository : INonPpnRepository
    {
        private readonly IGenericRepository<PurchaseNonPpn> _purchaseNonPpnRepository;
        private readonly IGenericRepository<CodePettyCash> _codeRepository;
        private readonly IGenericRepository<MasterProject> _masterProjectRepository;
        private readonly IGenericRepository<Category> _categoryRepository;
        private readonly IWebHostEnvironment _env;

        public NonPpnRepository(IGenericRepository<PurchaseNonPpn> purchaseNonRepository, IWebHostEnvironment env, IGenericRepository<MasterProject> masterProjectRepository, IGenericRepository<Category> categoryRepository, IGenericRepository<CodePettyCash> codeRepository)
        {
            _purchaseNonPpnRepository = purchaseNonRepository;
            _env = env;
            _masterProjectRepository = masterProjectRepository;
            _categoryRepository = categoryRepository;
            _codeRepository = codeRepository;
        }

        //mendapatkan mime type file yg diijinkan
        private string GetMimeType(string filePath)
        {
            var ext = Path.GetExtension(filePath).ToLowerInvariant();
            return ext switch
            {
                ".jpg" => "image/jpeg",
                ".pdf" => "application/pdf",
                _ => "application/octet-stream",
            };
        }

        public async Task<GetPreviewFileNonPpnDto> GetFile(string path)
        {
            //folder public
            string _photoDirectory = Path.Combine(_env.WebRootPath, "FilePurchaseNonPPN");
            var filePath = Path.Combine(_photoDirectory, path);
            var mimeType = GetMimeType(filePath);
            var fileContent = await System.IO.File.ReadAllBytesAsync(filePath);
            var data = new GetPreviewFileNonPpnDto
            {
                MimeType = mimeType,
                Path = filePath,
                Content = fileContent,
            };
            return data;
        }

        public async Task<GetNonPpnByIdDto> GetNonPpnById(Guid id)
        {
            var file = _purchaseNonPpnRepository.FindByCondition(o => o.Id == id)
                                           .Select(p => p.FilePath)
                                           .Distinct();

            string combinedFilePaths = string.Join(",", file);
            string[] files = combinedFilePaths.Split(',');

            var data = await _purchaseNonPpnRepository.FindByCondition(j => j.Id == id && j.DeletedAt == null)
            .OrderBy(d => d.Type).Select(g => new GetNonPpnByIdDto
            {
                Id = g.Id,
                BonDate = g.BonDate.Value.ToString("dd-MM-yyyy"),
                BonNumber = g.BonNumber,
                CodeId = g.CodeId,
                CodeName = g.CodePettyCash.CodeName,
                Item = g.CodePettyCash.Item,
                Transaction = g.Transaction,
                Debit = g.Debit,
                Kredit = g.Kredit,
                Saldo = 0,
                PaymentMethod = g.PaymentMethod,
                Note = g.Note,
                MasterProjectId = g.MasterProjectId,
                MasterProjectName = g.MasterProject.ProjectName,
                CategoryId = g.CategoryId,
                CategoryName = g.Category.CategoryName,
                ApprovalStatus = g.ApprovalStatus,
                FilePath = files,
                Type = g.Type,
            }).FirstOrDefaultAsync();
            return data;
        }

        public async Task<GetNonPpnInformationDto> GetNonPpnInformation(int month, int year, Guid categoryId)
        {
            //mengambil bulan sebelumnya untuk mendapatkan sisa saldo dari bulan sebelumnya
            int monthLast = month - 1;
            var dataLastMonth = await _purchaseNonPpnRepository.FindByCondition(o => o.BonDate.Value.Month == monthLast && o.BonDate.Value.Year == year && o.CategoryId == categoryId && o.DeletedAt == null)
                               .GroupBy(p => new { p.Type })
                               .Select(p => new DataSaldoDto
                               {
                                   TotalDebit = p.Sum(o => o.Debit),
                                   TotalKredit = p.Sum(o => o.Kredit)
                               }).ToListAsync();

            var totalDebit = dataLastMonth.Sum(g => g.TotalDebit);
            var totalKredit = dataLastMonth.Sum(g => g.TotalKredit);

            double? sisaSaldoLast = totalDebit - totalKredit;

            //menghitung total debit dan total kredit bulan ini
            var dataThisMonth = await _purchaseNonPpnRepository.FindByCondition(o => o.BonDate.Value.Month == month && o.BonDate.Value.Year == year && o.CategoryId == categoryId && o.DeletedAt == null)
                              .GroupBy(p => new { p.Type })
                              .Select(p => new DataSaldoDto
                              {
                                  TotalDebit = p.Sum(o => o.Debit),
                                  TotalKredit = p.Sum(o => o.Kredit)
                              }).ToListAsync();

            var debit = dataThisMonth.Sum(g => g.TotalDebit);
            var kredit = dataThisMonth.Sum(g => g.TotalKredit);

            var data = new GetNonPpnInformationDto
            {
                SaldoAwal = sisaSaldoLast,
                Debit = debit,
                Kredit = kredit,
                SaldoAkhir = debit - kredit,
            };
            return data;
        }

        public async Task<List<GetNonPpnWithPaginationDto>> GetNonPpnWithPagination(string searchTerm, int month, int year, Guid categoryId)
        {
            int monthLast = month - 1;
            var dataLastMonth = await _purchaseNonPpnRepository.FindByCondition(o => o.BonDate.Value.Month == monthLast && o.BonDate.Value.Year == year && o.CategoryId == categoryId)
                               .GroupBy(p => new { p.Type })
                               .Select(p => new DataSaldoDto
                               {
                                   TotalDebit = p.Sum(o => o.Debit),
                                   TotalKredit = p.Sum(o => o.Kredit)
                               }).ToListAsync();

            var totalDebit = dataLastMonth.Sum(g => g.TotalDebit);
            var totalKredit = dataLastMonth.Sum(g => g.TotalKredit);
            //sisa saldo bulan sebelumnya
            double? sisaSaldoLast = totalDebit - totalKredit;
            //search by bon number , code name, transaction
            var data = await _purchaseNonPpnRepository.FindByCondition(j => (j.BonDate.Value.Month == month) && (j.BonDate.Value.Year == year) && (j.CategoryId == categoryId) && (j.DeletedAt == null)
                                && (searchTerm == null || j.BonNumber.ToLower().Contains(searchTerm.ToLower()) || j.CodePettyCash.CodeName.ToLower().Contains(searchTerm.ToLower()) || j.Transaction.ToLower().Contains(searchTerm.ToLower())))
                                .OrderBy(d => d.Type).Select(g => new GetNonPpnWithPaginationDto
                                {
                                    Id = g.Id,
                                    BonDate = g.BonDate.Value.ToString("dd-MM-yyyy"),
                                    BonNumber = g.BonNumber,
                                    CodeId = g.CodeId,
                                    CodeName = g.CodePettyCash.CodeName,
                                    Item = g.CodePettyCash.Item,
                                    Transaction = g.Transaction,
                                    Debit = g.Debit,
                                    Kredit = g.Kredit,
                                    Saldo = 0,
                                    PaymentMethod = g.PaymentMethod,
                                    Note = g.Note,
                                    MasterProjectId = g.MasterProjectId,
                                    MasterProjectName = g.MasterProject.ProjectName,
                                    CategoryId = g.CategoryId,
                                    CategoryName = g.Category.CategoryName,
                                    ApprovalStatus = g.ApprovalStatus,
                                    FilePath = g.FilePath,
                                    Type = g.Type,
                                    CreatedAt = g.CreatedAt.Value,
                                }).ToListAsync();

            double? currentSaldo = 0;
            //untuk mendapatkan data saldo jika transaction type nya debit maka current saldo akan di tambah nominal debit
            //jika transaction type nya kredit maka current saldo akan di kurangi nominal kredit
            // setelah itu akan di tambah sisa saldo bulan lalu
            foreach (var transaction in data)
            {
                if (transaction.Type == "debit")
                {
                    currentSaldo += transaction.Debit;
                }
                else if (transaction.Type == "kredit")
                {
                    currentSaldo -= transaction.Kredit;
                }
                transaction.Saldo = currentSaldo + sisaSaldoLast;
            }

            return data.OrderByDescending(o => o.CreatedAt).ToList();
        }

        public async Task<(bool, string)> UploadFile(FileDto file)
        {
            var dataFile = file.Attachment.ToArray();
            var allowedExtensions = new[] { ".jpg", ".pdf" }; // allowed extensions
            var maxFileSizeInBytes = 3 * 1024 * 1024; // 3 MB maximum size in bytes
            var fileNames = new List<string>();

            for (var i = 0; i < dataFile.Length; i++)
            {
                var fileExtension = Path.GetExtension(dataFile[i].FileName).ToLower();
                if (dataFile[i].Length > maxFileSizeInBytes || !allowedExtensions.Contains(fileExtension))
                {
                    return (false, string.Empty);
                }
                string photoDirectory = @"wwwroot/FilePurchaseNonPPN";
                if (!Directory.Exists(photoDirectory))
                {
                    Directory.CreateDirectory(photoDirectory);
                }
                var path = Path.Combine(photoDirectory, dataFile[i].FileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await dataFile[i].CopyToAsync(stream);
                }

                fileNames.Add(dataFile[i].FileName);
            }

            var concatenatedFileNames = string.Join(",", fileNames);
            return (true, concatenatedFileNames);
        }

        //upload excel
        public async Task<List<NonPpnImportDto>> UploadExcel(IFormFile file)
        {
            var nonPpn = new List<NonPpnImportDto>();
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                stream.Position = 0;

                using (var workbook = new XLWorkbook(stream))
                {
                    var worksheet = workbook.Worksheet(1);
                    var rows = worksheet.RowsUsed().Skip(1);

                    foreach (var row in rows)
                    {
                        var checkCode = await _codeRepository.FindByCondition(o => o.CodeName == row.Cell(3).Value.ToString()).FirstOrDefaultAsync();
                        if (checkCode == null)
                        {
                            CodeNotFoundException.Throw(row.Cell(3).Value.ToString());
                        }
                        var checkProject = await _masterProjectRepository.FindByCondition(p => p.ProjectName == row.Cell(9).Value.ToString()).FirstOrDefaultAsync();
                        if (checkProject == null)
                        {
                            ProjectNameNotFoundException.Throw(row.Cell(9).Value.ToString());
                        }
                        var checkCategory = await _categoryRepository.FindByCondition(o => o.CategoryName == row.Cell(10).Value.ToString()).FirstOrDefaultAsync();
                        if (checkCategory == null)
                        {
                            CategoryNotFoundException.Throw(row.Cell(10).Value.ToString());
                        }
                        var data = new NonPpnImportDto
                        {
                            MasterProjectId = checkProject.Id,
                            CodeId = checkCode.Id,
                            CategoryId = checkCategory.Id,
                            BonDate = row.Cell(1).IsEmpty() ? null : row.Cell(1).GetValue<DateTime>(),
                            BonNumber = row.Cell(2).IsEmpty() ? null : row.Cell(2).GetValue<string>(),
                            Transaction = row.Cell(4).IsEmpty() ? null : row.Cell(4).GetValue<string>(),
                            Debit = row.Cell(5).IsEmpty() ? null : row.Cell(5).GetValue<double>(),
                            Kredit = row.Cell(6).IsEmpty() ? null : row.Cell(6).GetValue<double>(),
                            PaymentMethod = row.Cell(7).IsEmpty() ? null : row.Cell(7).GetValue<string>(),
                            Note = row.Cell(8).IsEmpty() ? null : row.Cell(8).GetValue<string>(),
                            CreatedBy = row.Cell(11).GetValue<string>(),
                        };

                        nonPpn.Add(data);
                    }
                }
            }
            return nonPpn;
        }
    }
}