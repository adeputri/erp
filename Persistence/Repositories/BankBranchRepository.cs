﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class BankBranchRepository : IBankBranchRepository
    {
        private readonly IGenericRepository<BranchBank> _repository;

        public BankBranchRepository(IGenericRepository<BranchBank> repository)
        {
            _repository = repository;
        }

        public async Task<bool> ValidateData(BranchBank branchBank)
        {
            var x = await _repository.FindByCondition(o => o.BranchBankName.ToLower() == branchBank.BranchBankName.ToLower() && o.BankId == branchBank.BankId).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}