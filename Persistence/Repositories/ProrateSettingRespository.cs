﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class ProrateSettingRespository : IProrateSettingRespository
    {
        private readonly IGenericRepository<ProrateSetting> _repository;

        public ProrateSettingRespository(IGenericRepository<ProrateSetting> repository)
        {
            _repository = repository;
        }

        public async Task<bool> ValidateData(ProrateSetting prorateSetting)
        {
            var x = await _repository.FindByCondition(o => o.Setting.ToLower() == prorateSetting.Setting.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}