﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class PensionInsuranceCostRepository : IPensionInsuranceCostRepository
    {
        private readonly IGenericRepository<PensionInsuranceCost> _repository;

        public PensionInsuranceCostRepository(IGenericRepository<PensionInsuranceCost> repository)
        {
            _repository = repository;
        }

        public async Task<bool> ValidateData(PensionInsuranceCost pensionInsuranceCost)
        {
            var x = await _repository.FindByCondition(o => o.Status.ToLower() == pensionInsuranceCost.Status.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}