﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class TaxMethodRepository : ITaxMethodRepository
    {
        private readonly IGenericRepository<TaxMethode> _repository;

        public TaxMethodRepository(IGenericRepository<TaxMethode> repository)
        {
            _repository = repository;
        }

        public async Task<bool> Validatedata(TaxMethode taxMethode)
        {
            var x = await _repository.FindByCondition(o => o.Status.ToLower() == taxMethode.Status.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}