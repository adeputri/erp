﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Common.Exceptions;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.Import;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnById;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnWithPagination;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPreviewFilePpn;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;
using System.Data;

namespace SkeletonApi.Persistence.Repositories
{
    public class PpnRepository : IPpnRepository
    {
        private readonly IGenericRepository<PurchasePpn> _ppnRepository;
        private readonly IGenericRepository<Supplier> _supplierRepository;
        private readonly IGenericRepository<Project> _projectRepository;
        private readonly IDapperReadDbConnection _dbConnection;
        private readonly IWebHostEnvironment _env;

        public PpnRepository(IGenericRepository<PurchasePpn> castAtBankRepository, IDapperReadDbConnection dbConnection, IWebHostEnvironment env, IGenericRepository<Supplier> supplierRepository, IGenericRepository<Project> projectRepository)
        {
            _ppnRepository = castAtBankRepository;
            _dbConnection = dbConnection;
            _env = env;
            _supplierRepository = supplierRepository;
            _projectRepository = projectRepository;
        }

        public async Task<GetPreviewFilePpnDto> GetFile(string path)
        {
            //folder public
            string _photoDirectory = System.IO.Path.Combine(_env.WebRootPath, "FilePurchasePPN");
            var filePath = System.IO.Path.Combine(_photoDirectory, path);
            var mimeType = GetMimeType(filePath);
            var fileContent = await System.IO.File.ReadAllBytesAsync(filePath);
            var data = new GetPreviewFilePpnDto
            {
                MimeType = mimeType,
                Path = filePath,
                Content = fileContent,
            };
            return data;
        }

        //mendapatkan mime type file yg diijinkan
        private string GetMimeType(string filePath)
        {
            var ext = System.IO.Path.GetExtension(filePath).ToLowerInvariant();
            return ext switch
            {
                ".jpg" => "image/jpeg",
                ".pdf" => "application/pdf",
                _ => "application/octet-stream",
            };
        }

        public async Task<(bool, string)> UploadFile(FileDto files)
        {
            var dataFile = files.Attachment.ToArray();
            var allowedExtensions = new[] { ".jpg", ".pdf" }; // allowed extensions
            var maxFileSizeInBytes = 3 * 1024 * 1024; // 3 MB maximum size in bytes
            var fileNames = new List<string>();

            for (var i = 0; i < dataFile.Length; i++)
            {
                var fileExtension = System.IO.Path.GetExtension(dataFile[i].FileName).ToLower();
                if (dataFile[i].Length > maxFileSizeInBytes || !allowedExtensions.Contains(fileExtension))
                {
                    return (false, string.Empty);
                }
                string photoDirectory = @"wwwroot/FilePurchasePPN";
                if (!Directory.Exists(photoDirectory))
                {
                    Directory.CreateDirectory(photoDirectory);
                }
                var path = System.IO.Path.Combine(photoDirectory, dataFile[i].FileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await dataFile[i].CopyToAsync(stream);
                }

                fileNames.Add(dataFile[i].FileName);
            }
            var concatenatedFileNames = string.Join(",", fileNames);
            return (true, concatenatedFileNames);
        }

        public async Task<List<GetPpnWithPaginationDto>> GetAll(string searchTerm, int month, int year)
        {
            //search by po number,supplier name, masa,
            var dt = await _ppnRepository.FindByCondition(x => (x.DeletedAt == null) && (x.UpdatedAt.Value.Month == DateTime.Now.Month) && (x.UpdatedAt.Value.Year == DateTime.Now.Year)
                         && (searchTerm == null || x.Supplier.SupplierName.ToLower().Contains(searchTerm.ToLower()) || x.PreOrderNumber.ToLower().Contains(searchTerm.ToLower()) || x.Masa.ToLower().Contains(searchTerm.ToLower()))
                         || (month == null || year == null || (x.UpdatedAt.Value.Month == month) && (x.UpdatedAt.Value.Year <= year)))
                         .AsSplitQuery()
                         .GroupBy(p => new
                         {
                             p.Project.NumberSpk,
                             p.CreatedBy,
                             p.Project.MasterProject.ProjectName,
                             p.Dpp,
                             p.Ppn,
                             p.Pph23,
                             p.Discount,
                             p.PiNumber,
                             p.FpDate,
                             p.FakturNumber,
                             p.TravelDocDate,
                             p.TravelDocNumber,
                             p.Masa,
                             p.Note,
                             p.PaidOfDate,
                             p.FilePath,
                             p.UniqueCode,
                             p.ProjectId,
                             p.SupplierId,
                             p.PaymentStatus,
                             p.Supplier.SupplierName,
                             p.Supplier.Npwp,
                             p.ApprovalBy,
                             p.ApprovalStatus,
                             p.UpdatedAt.Value.Date
                         })
                         .Select(o => new GetPpnWithPaginationDto
                         {
                             UniqueCode = o.Key.UniqueCode ?? string.Empty,
                             Masa = o.Key.Masa ?? string.Empty,
                             DataPo = o.GroupBy(j => new { j.PreOrderNumber, j.PreOrderDate }).Select(k => new DataPreOrder
                             {
                                 PreOrderNumber = k.Key.PreOrderNumber ?? string.Empty,
                                 PreOrderDate = k.Key.PreOrderDate.Value.ToString("dd-MM-yyyy") ?? string.Empty,
                             }).ToList(),
                             SupplierId = o.Key.SupplierId,
                             SupplierName = o.Key.SupplierName ?? string.Empty,
                             SupplierNpwp = o.Key.Npwp ?? string.Empty,
                             Total = (o.Key.Pph23 ?? 0.0) + (o.Key.Dpp ?? 0.0) + (o.Key.Ppn ?? 0.0) - (o.Key.Discount ?? 0.0),
                             CreatedBy = o.Key.CreatedBy ?? string.Empty,
                             ApprovalStatus = o.Key.ApprovalStatus ?? string.Empty,
                             CreatedAt = (o.Key.Date.AddHours(7).ToString("dd-MM-yyyy")),
                             FilePath = o.Key.FilePath ?? string.Empty,
                             PiNumber = o.Key.PiNumber ?? string.Empty,
                             FpDate = o.Key.FpDate ?? DateTime.MinValue,
                             FakturNumber = o.Key.FakturNumber ?? string.Empty,
                             TravelDocDate = o.Key.TravelDocDate ?? DateTime.MinValue,
                             TravelDocNumber = o.Key.TravelDocNumber ?? string.Empty,
                             Note = o.Key.Note ?? string.Empty,
                             PaidOfDate = o.Key.PaidOfDate ?? DateTime.MinValue,
                             PaymentStatus = o.Key.PaymentStatus ?? string.Empty,
                             ProjectName = o.Key.ProjectName ?? string.Empty,
                             NumberSpk = o.Key.NumberSpk ?? string.Empty,
                             ProjectId = o.Key.ProjectId,
                             Dpp = o.Key.Dpp ?? 0.0,
                             Ppn = o.Key.Ppn ?? 0.0,
                             Pph23 = o.Key.Pph23 ?? 0.0,
                             Discount = o.Key.Discount ?? 0.0,
                             Approval = o.Key.ApprovalBy ?? string.Empty,
                             UpdateAt = o.Key.Date,
                         }).OrderByDescending(o => o.UniqueCode).ToListAsync();
            return dt;
        }

        //get by id purchase ppn
        public async Task<GetPpnByIdDto> GetById(string uniqueCode)
        {
            var file = _ppnRepository.FindByCondition(o => o.UniqueCode == uniqueCode)
                         .Select(p => p.FilePath)
                         .Distinct();

            string combinedFilePaths = string.Join(",", file);
            string[] files = combinedFilePaths.Split(',');

            var dt = await _ppnRepository.FindByCondition(o => o.UniqueCode == uniqueCode)
                   .GroupBy(p => new { p.Project.MasterProject.ProjectName, p.Dpp, p.Ppn, p.Pph23, p.Discount, p.PiNumber, p.FpDate, p.FakturNumber, p.TravelDocDate, p.TravelDocNumber, p.Masa, p.Note, p.PaidOfDate, p.FilePath, p.UniqueCode, p.ProjectId, p.SupplierId, p.PaymentStatus, p.Supplier.SupplierName, p.Supplier.Npwp, p.ApprovalBy, p.ApprovalStatus, UpdatedAt = p.UpdatedAt.Value.Date })
                   .Select(o => new GetPpnByIdDto
                   {
                       UniqueCode = o.Key.UniqueCode,
                       FilePath = files,
                       PiNumber = o.Key.PiNumber ?? string.Empty,
                       FpDate = o.Key.FpDate ?? DateTime.MinValue,
                       FakturNumber = o.Key.FakturNumber ?? string.Empty,
                       TravelDocDate = o.Key.TravelDocDate ?? DateTime.MinValue,
                       TravelDocNumber = o.Key.TravelDocNumber ?? string.Empty,
                       Masa = o.Key.Masa ?? string.Empty,
                       Note = o.Key.Note ?? string.Empty,
                       PaidOfDate = o.Key.PaidOfDate ?? DateTime.MinValue,
                       PaymentStatus = o.Key.PaymentStatus ?? string.Empty,
                       SupplierName = o.Key.SupplierName ?? string.Empty,
                       Npwp = o.Key.Npwp ?? string.Empty,
                       ProjectName = o.Key.ProjectName ?? string.Empty,
                       SupplierId = o.Key.SupplierId,
                       ProjectId = o.Key.ProjectId,
                       Dpp = o.Key.Dpp ?? 0.0,
                       Ppn = o.Key.Ppn ?? 0.0,
                       Pph23 = o.Key.Pph23 ?? 0.0,
                       Discount = o.Key.Discount ?? 0.0,
                       Total = (o.Key.Pph23 ?? 0.0) + (o.Key.Dpp ?? 0.0) + (o.Key.Ppn ?? 0.0) - (o.Key.Discount ?? 0.0),
                       Approval = o.Key.ApprovalBy ?? string.Empty,
                       ApprovalStatus = o.Key.ApprovalStatus ?? string.Empty,
                       UpdateAt = o.Key.UpdatedAt.AddHours(7),
                       DataPo = o.GroupBy(j => new { j.PreOrderNumber, j.PreOrderDate }).Select(k => new DataPreOrder
                       {
                           PreOrderNumber = k.Key.PreOrderNumber ?? string.Empty,
                           PreOrderDate = k.Key.PreOrderDate.Value.ToString("dd-MM-yyyy") ?? string.Empty,
                       }).ToList(),
                       DataInv = o.GroupBy(j => new { j.InvDate, j.InvNumber, j.InvReceiptDate }).Select(g => new DataInvoice
                       {
                           InvDate = g.Key.InvDate.Value.ToString("dd-MM-yyyy") ?? string.Empty,
                           InvNumber = g.Key.InvNumber ?? string.Empty,
                           InvRecieptDate = g.Key.InvReceiptDate.Value.ToString("dd-MM-yyyy") ?? string.Empty
                       }).ToList(),
                   }).FirstOrDefaultAsync();

            return dt;
        }

        //import excel
        public async Task<List<PpnImportDto>> UploadExcel(IFormFile file)
        {
            var ppnImport = new List<PpnImportDto>();
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                stream.Position = 0;

                using (var workbook = new XLWorkbook(stream))
                {
                    var worksheet = workbook.Worksheet(1);
                    var rows = worksheet.RowsUsed().Skip(1);

                    foreach (var row in rows)
                    {
                        var checkSupplier = await _supplierRepository.FindByCondition(o => o.SupplierName.ToLower() == row.Cell(1).Value.ToString().ToLower()).FirstOrDefaultAsync();
                        if (checkSupplier == null)
                        {
                            SupplierNameNotFoundException.Throw(row.Cell(1).Value.ToString());
                        }
                        var checkProject = await _projectRepository.FindByCondition(o => o.NumberSpk.ToLower() == row.Cell(3).Value.ToString().ToLower()).FirstOrDefaultAsync();
                        if (checkProject == null)
                        {
                            SpkNumberNotFoundException.Throw(row.Cell(3).ToString());
                        }

                        var data = new PpnImportDto
                        {
                            UniqueCode = DateTime.UtcNow.ToString("yyMMddhhmmss"),
                            SupplierId = checkSupplier.Id,
                            PiNumber = row.Cell(2).IsEmpty() ? "-" : row.Cell(2).GetValue<string>(),
                            ProjectId = checkProject.Id,
                            Masa = row.Cell(6).IsEmpty() ? "-" : row.Cell(6).GetValue<string>(),
                            FpDate = row.Cell(7).IsEmpty() ? (DateTime?)null : row.Cell(7).GetValue<DateTime>(),
                            FakturNumber = row.Cell(8).IsEmpty() ? "-" : row.Cell(8).GetValue<string>(),
                            TravelDocDate = row.Cell(9).IsEmpty() ? (DateTime?)null : row.Cell(9).GetValue<DateTime>(),
                            TravelDocNumber = row.Cell(10).IsEmpty() ? "-" : row.Cell(10).GetValue<string>(),
                            Dpp = row.Cell(14).IsEmpty() ? 0.0 : row.Cell(14).GetValue<double>(),
                            Ppn = row.Cell(15).IsEmpty() ? 0.0 : row.Cell(15).GetValue<double>(),
                            Pph23 = row.Cell(16).IsEmpty() ? 0.0 : row.Cell(16).GetValue<double>(),
                            Discount = row.Cell(17).IsEmpty() ? 0.0 : row.Cell(17).GetValue<double>(),
                            PaymentStatus = row.Cell(18).IsEmpty() ? "-" : row.Cell(18).GetValue<string>(),
                            Note = row.Cell(19).IsEmpty() ? "-" : row.Cell(19).GetValue<string>(),
                            PaidOfDate = row.Cell(20).IsEmpty() ? (DateTime?)null : row.Cell(20).GetValue<DateTime>(),
                            CreatedBy = row.Cell(21).IsEmpty() ? "-" : row.Cell(21).GetValue<string>(),
                            ApprovalStatus = "Waiting",
                            DataPo = new List<DataPo>(),
                            DataInv = new List<DataInv>()
                        };
                        var preOrderNumbers = row.Cell(4).IsEmpty() ? null : row.Cell(4).GetValue<string>().Split(',');
                        var preOrderDates = row.Cell(5).IsEmpty() ? null : row.Cell(5).GetValue<string>().Split(',');

                        //jika po number dan po date tidak null dan lebih dari 1
                        if (preOrderNumbers != null && preOrderDates != null && preOrderDates.Length > 1 && preOrderNumbers.Length > 1)
                        {
                            for (int i = 0; i < preOrderNumbers.Length && i < preOrderDates.Length; i++)
                            {
                                data.DataPo.Add(new DataPo
                                {
                                    PreOrderNumber = preOrderNumbers[i],
                                    PreOrderDate = DateTime.TryParse(preOrderDates[i], out DateTime preOrderDate) ? preOrderDate : (DateTime?)null
                                });
                            }
                        }
                        else
                        {
                            data.DataPo.Add(new DataPo
                            {
                                PreOrderNumber = row.Cell(4).IsEmpty() ? null : row.Cell(4).GetValue<string>(),
                                PreOrderDate = row.Cell(5).IsEmpty() ? null : row.Cell(5).GetValue<DateTime>()
                            });
                        }

                        var invNumbers = row.Cell(11).IsEmpty() ? null : row.Cell(11).GetValue<string>().Split(',');
                        var invDates = row.Cell(12).IsEmpty() ? null : row.Cell(12).GetValue<string>().Split(',');
                        var invReceiptDates = row.Cell(13).IsEmpty() ? null : row.Cell(13).GetValue<string>().Split(',');

                        //jika inv number, inv date dan inv receipt date tidak null dan lebih dari 1
                        if ((invNumbers != null && invNumbers.Length > 1) || (invDates != null && invDates.Length > 1) || (invReceiptDates != null && invReceiptDates.Length > 1))
                        {
                            if (invNumbers != null && invDates != null && invReceiptDates != null)
                            {
                                for (int i = 0; i < invNumbers.Length && i < invDates.Length && i < invReceiptDates.Length; i++)
                                {
                                    data.DataInv.Add(new DataInv
                                    {
                                        InvNumber = invNumbers[i],
                                        InvDate = DateTime.TryParse(invDates[i], out DateTime invDate) ? invDate : (DateTime?)null,
                                        InvRecieptDate = DateTime.TryParse(invReceiptDates[i], out DateTime invReceiptDate) ? invReceiptDate : (DateTime?)null
                                    });
                                }
                            }
                        }
                        else
                        {
                            data.DataInv.Add(new DataInv
                            {
                                InvNumber = row.Cell(11).IsEmpty() ? null : row.Cell(11).GetValue<string>(),
                                InvDate = row.Cell(12).IsEmpty() ? null : row.Cell(12).GetValue<DateTime>(),
                                InvRecieptDate = row.Cell(13).IsEmpty() ? null : row.Cell(13).GetValue<DateTime>(),
                            });
                        }

                        ppnImport.Add(data);
                    }
                    return ppnImport;
                }
            }
        }
    }
}