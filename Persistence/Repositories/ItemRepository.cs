﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class ItemRepository : IItemRepository
    {
        private readonly IGenericRepository<Item> _repository;

        public ItemRepository(IGenericRepository<Item> repository)
        {
            _repository = repository;
        }

        public async Task<Guid> GetIdByBarcodeAsync(string barcode) =>
            await _repository
            .Entities
            .Where(o => o.BarcodeItem == barcode)
            .Select(o => o.Id)
            .FirstOrDefaultAsync();

        public async Task<Item> GetItemAsync(Guid id) =>
            await _repository.FindAsync(id);

        public async Task<bool> ValidateData(Item item)
        {
            var x = await _repository.FindByCondition(o => o.BarcodeItem == item.BarcodeItem).CountAsync();

            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}