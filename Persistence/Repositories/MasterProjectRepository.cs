﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class MasterProjectRepository : IMasterProjectRepository
    {
        private readonly IGenericRepository<MasterProject> _masterProjectRepository;

        public MasterProjectRepository(IGenericRepository<MasterProject> masterProjectRepository = null)
        {
            _masterProjectRepository = masterProjectRepository;
        }

        public async Task<MasterProject> GetProjectById(Guid id) =>
         await _masterProjectRepository.FindAsync(id);

        public async Task<bool> ValidateData(MasterProject project)
        {
            var x = await _masterProjectRepository.
                FindByCondition(o => o.DeletedAt == null && o.ProjectName.ToLower() == project.ProjectName.ToLower() &&
                o.CustomerId == o.CustomerId)
                .CountAsync();

            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}