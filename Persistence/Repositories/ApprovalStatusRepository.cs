﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class ApprovalStatusRepository : IApprovalStatusRepository
    {
        private readonly IGenericRepository<ApprovalStatus> _repository;

        public ApprovalStatusRepository(IGenericRepository<ApprovalStatus> repository)
        {
            _repository = repository;
        }

        public async Task<bool> Validatedata(ApprovalStatus approvalStatus)
        {
            var x = await _repository.FindByCondition(o => o.Status.ToLower() == approvalStatus.Status.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}