﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Common.Exceptions;
using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Commands.CreateMainTask;
using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Queries.GetAllTaskByIdProject;
using SkeletonApi.Application.Features.Sales.ProjectInformations.Commands.Import;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Queries.GetProjectWithPagination;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly IGenericRepository<SectionProject> _sectionRepository;
        private readonly IGenericRepository<Project> _projectRepository;
        private readonly IGenericRepository<TaskProject> _taskProject;
        private readonly IGenericRepository<MasterProject> _masterProject;
        private readonly IGenericRepository<Employee> _employeeRepository;

        public ProjectRepository(IGenericRepository<SectionProject> sectionRepository, IGenericRepository<Project> projectRepository, IGenericRepository<TaskProject> taskRepository, IGenericRepository<MasterProject> masterProject, IGenericRepository<Employee> employeeRepository)
        {
            _sectionRepository = sectionRepository;
            _projectRepository = projectRepository;
            _taskProject = taskRepository;
            _masterProject = masterProject;
            _employeeRepository = employeeRepository;
        }

        //menghitung progress
        public async Task<List<GetProjectWithPaginationDto>> GetProgress()
        {
            var data = await _projectRepository.FindByCondition(p => p.DeletedAt == null).Include(e => e.Employee).Include(p => p.MasterProject).Include(o => o.MasterProject.Customer).OrderByDescending(p => p.UpdatedAt.Value).ToListAsync();
            var projectDtos = new List<GetProjectWithPaginationDto>();

            foreach (var item in data)
            {
                //menghitung task yang status nya done
                decimal taskDone = await _taskProject.FindByCondition(p => p.SectionProject.ProjectId == item.Id && p.Status == "done").CountAsync();
                //menghitung total task
                int totalTask = await _taskProject.FindByCondition(p => p.DeletedAt == null && p.SectionProject.ProjectId == item.Id).CountAsync();
                //rumus mendapatkan nilai presentase
                decimal progress = taskDone > 0 ? taskDone / totalTask * 100 : 0;
                string projectStatus;
                var projectDto = new GetProjectWithPaginationDto
                {
                    Id = item.Id,
                    NumberSpk = item.NumberSpk,
                    MasterProjectId = item.MasterProjectId,
                    ProjectName = item.MasterProject.ProjectName,
                    Progress = Math.Round(progress, 1),
                    StartProject = item.ProjectStartDate,
                    EndProject = item.ProjectEndDate,
                    CustomerName = item.MasterProject.Customer.CustomerName,
                    PicId = item.PicId,
                    PicName = item.Employee.FirstName + " " + item.Employee.LastName,
                    CreatedAt = item.UpdatedAt.Value.AddHours(7),
                };

                if (progress <= 0)
                {
                    projectStatus = "not-started";
                }
                else if (progress < 100)
                {
                    projectStatus = "in-progress";
                }
                else
                {
                    projectStatus = "done";
                }
                var today = DateOnly.FromDateTime(DateTime.Now.Date);
                //menghitung project end date dari project yang kurang dari 2 minggu dari hr ini
                if (item.ProjectEndDate < today.AddDays(14))
                {
                    var daysLeft = (item.ProjectEndDate.ToDateTime(TimeOnly.MinValue) - DateTime.Now).Days;
                    projectDto.DueDate = $"{daysLeft} Days Left";
                }
                projectDto.ProjectStatus = projectStatus;
                projectDtos.Add(projectDto);
            }

            return projectDtos;
        }

        public async Task<IEnumerable<GetAllTaskByIdProjectDto>> GetAllTaskByIdProject(Guid projectId)
        {
            var data = await _sectionRepository.FindByCondition(o => o.ProjectId == projectId).GroupBy(o => new { o.ProjectId, o.UpdatedAt.Value })
                .Select(section => new GetAllTaskByIdProjectDto
                {
                    ProjectId = projectId,
                    CreatedAt = section.Key.Value.AddHours(7),
                    CreateSections = section.Select(j => new CreateSection
                    {
                        SectionName = j.SectionName,
                        CreateTasks = j.TaskProjects.Select(task => new CreateTask
                        {
                            TaskName = task.TaskName,
                            Status = task.Status,
                            AssignTo = task.AssignTo,
                            DueDate = task.DueDate,
                        }).ToList(),
                    }).ToList(),
                }).OrderByDescending(p => p.CreatedAt).ToListAsync();

            return data;
        }

        public async Task<bool> CheckMasterProjectId(Guid MasterProjectId)
        {
            var idMasterProject = await _projectRepository.FindByCondition(p => p.MasterProjectId == MasterProjectId).CountAsync();
            if (idMasterProject > 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //import excel
        public async Task<List<ProjectImportDto>> UploadExcel(IFormFile file)
        {
            var project = new List<ProjectImportDto>();
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                stream.Position = 0;

                using (var workbook = new XLWorkbook(stream))
                {
                    var worksheet = workbook.Worksheet(1);
                    var rows = worksheet.RowsUsed().Skip(1);

                    foreach (var row in rows)
                    {
                        var checkProject = await _masterProject.FindByCondition(p => p.ProjectName == row.Cell(2).Value.ToString()).FirstOrDefaultAsync();
                        if (checkProject == null)
                        {
                            ProjectNameNotFoundException.Throw(row.Cell(2).Value.ToString());
                        }
                        var checkPic = await _employeeRepository.FindByCondition(p => p.FirstName + " " + p.LastName == row.Cell(5).Value.ToString()).FirstOrDefaultAsync();
                        if (checkPic == null)
                        {
                            EmployeeNameNotFound.Throw(row.Cell(5).Value.ToString());
                        }
                        var data = new ProjectImportDto
                        {
                            NumberSpk = row.Cell(1).IsEmpty() ? "-" : row.Cell(1).GetValue<string>(),
                            MasterProjectId = checkProject.Id,
                            ProjectStartDate = row.Cell(3).IsEmpty() ? null : (DateOnly?)DateOnly.FromDateTime(row.Cell(3).GetValue<DateTime>()),
                            ProjectEndDate = row.Cell(4).IsEmpty() ? null : (DateOnly?)DateOnly.FromDateTime(row.Cell(4).GetValue<DateTime>()),
                            PicId = checkPic.Id,
                        };
                        project.Add(data);
                    }
                }
            }
            return project;
        }

        public async Task<bool> ValidateData(Project project)
        {
            var x = await _projectRepository.FindByCondition(o => o.DeletedAt == null && o.NumberSpk.ToLower() == project.NumberSpk.ToLower() && o.MasterProjectId == project.MasterProjectId).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}