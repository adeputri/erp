﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class UnitRepository : IUnitRepository
    {
        private readonly IGenericRepository<MasterUnit> _unitRepository;

        public UnitRepository(IGenericRepository<MasterUnit> repository)
        {
            _unitRepository = repository;
        }

        public async Task<bool> ValidateData(MasterUnit masterUnit)
        {
            var x = await _unitRepository.FindByCondition(o => o.UnitName.ToLower() == masterUnit.UnitName.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}