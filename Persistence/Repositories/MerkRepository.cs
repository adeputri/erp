﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Application.Interfaces.Repositories.Configuration;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class MerkRepository : IMerkRepository
    {
        private readonly IGenericRepository<Merk> _merkRepository;

        public MerkRepository(IGenericRepository<Merk> merkRepository)
        {
            _merkRepository = merkRepository;
        }

        public async Task<bool> ValidateData(Merk merk)
        {
            var x = await _merkRepository.FindByCondition(o => o.DeletedAt == null && o.Name.ToLower() == merk.Name.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}