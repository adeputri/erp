﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class PositionRepository : IPositionRepository
    {
        private readonly IGenericRepository<Position> _positionRepository;

        public PositionRepository(IGenericRepository<Position> repository)
        {
            _positionRepository = repository;
        }

        public async Task<bool> ValidateData(Position position)
        {
            var x = await _positionRepository.FindByCondition(o => o.PositionName.ToLower() == position.PositionName.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}