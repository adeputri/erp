﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class PaymentScheduleRepository : IPaymentScheduleRepository
    {
        private readonly IGenericRepository<PaymentSchedule> _repository;

        public PaymentScheduleRepository(IGenericRepository<PaymentSchedule> repository)
        {
            _repository = repository;
        }

        public async Task<bool> ValidateData(PaymentSchedule schedule)
        {
            var x = await _repository.FindByCondition(o => o.Schedule.ToLower() == schedule.Schedule.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}