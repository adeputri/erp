﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class PaymentStatusRepository : IPaymentStatusRepository
    {
        private readonly IGenericRepository<PaymentStatus> _paymentStatusRepository;

        public PaymentStatusRepository(IGenericRepository<PaymentStatus> repository)
        {
            _paymentStatusRepository = repository;
        }

        public async Task<bool> ValidateData(PaymentStatus paymentStatus)
        {
            var x = await _paymentStatusRepository.FindByCondition(o => o.Status.ToLower() == paymentStatus.Status.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}