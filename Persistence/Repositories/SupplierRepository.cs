﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Features.MasterData.Suppliers.Commands.Import;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class SupplierRepository : ISupplierRepository
    {
        private readonly IGenericRepository<Supplier> _repository;

        public SupplierRepository(IGenericRepository<Supplier> repository)
        {
            _repository = repository;
        }

        public async Task<List<SupplierImportDto>> UploadExcel(IFormFile file)
        {
            var supplier = new List<SupplierImportDto>();
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                stream.Position = 0;

                using (var workbook = new XLWorkbook(stream))
                {
                    var worksheet = workbook.Worksheet(1);
                    var rows = worksheet.RowsUsed().Skip(1);

                    foreach (var row in rows)
                    {
                        var data = new SupplierImportDto
                        {
                            SupplierName = row.Cell(1).IsEmpty() ? "-" : row.Cell(1).GetValue<string>(),
                            Npwp = row.Cell(2).IsEmpty() ? "-" : row.Cell(2).GetValue<string>(),
                            Address = row.Cell(3).IsEmpty() ? "-" : row.Cell(3).GetValue<string>(),
                            ContactPerson = row.Cell(4).IsEmpty() ? "-" : row.Cell(4).GetValue<string>(),
                            PhoneNumber = row.Cell(5).IsEmpty() ? "-" : row.Cell(5).GetValue<string>(),
                        };
                        supplier.Add(data);
                    }
                }
            }
            return supplier;
        }

        public async Task<bool> ValidateData(Supplier supplier)
        {
            var x = await _repository.FindByCondition(o => o.DeletedAt == null && o.SupplierName.ToLower() == supplier.SupplierName.ToLower() && o.Npwp.ToLower() == supplier.Npwp.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}