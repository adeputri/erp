﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class EmployeeLevelRepository : IEmployeeLevelRepository
    {
        private readonly IGenericRepository<EmployeeLevel> _repository;

        public EmployeeLevelRepository(IGenericRepository<EmployeeLevel> repository)
        {
            _repository = repository;
        }

        public async Task<bool> ValidateData(EmployeeLevel employeeLevel)
        {
            var x = await _repository.FindByCondition(o => o.Level.ToLower() == employeeLevel.Level.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}