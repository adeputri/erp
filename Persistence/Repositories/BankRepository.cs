﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class BankRepository : IBankRepository
    {
        private readonly IGenericRepository<Bank> _bankRepository;

        public BankRepository(IGenericRepository<Bank> repository)
        {
            _bankRepository = repository;
        }

        public async Task<bool> ValidateData(Bank bank)
        {
            var x = await _bankRepository.FindByCondition(o => o.BankName.ToLower() == bank.BankName.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}