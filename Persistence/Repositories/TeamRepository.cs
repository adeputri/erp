﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private readonly IGenericRepository<Team> _teamRepository;

        public TeamRepository(IGenericRepository<Team> repository)
        {
            _teamRepository = repository;
        }

        public async Task<bool> ValidateData(Team team)
        {
            var x = await _teamRepository.FindByCondition(o => o.TeamName.ToLower() == team.TeamName.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}