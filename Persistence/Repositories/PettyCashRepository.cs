﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class PettyCashRepository : IPettyCashRepository
    {
        private readonly IGenericRepository<CodePettyCash> _pettyCashRepository;

        public PettyCashRepository(IGenericRepository<CodePettyCash> pettyCashRepository)
        {
            _pettyCashRepository = pettyCashRepository;
        }

        public async Task<bool> ValidateDate(CodePettyCash codePettyCash)
        {
            var x = await _pettyCashRepository.FindByCondition(o => o.CodeName.ToLower() == codePettyCash.CodeName.ToLower() && o.Item.ToLower() == codePettyCash.Item.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}