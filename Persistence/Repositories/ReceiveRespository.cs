﻿//using Microsoft.AspNetCore.Http;
//using SkeletonApi.Application.Features.Warehouse.Receives.Commands.CreateReceipt;
//using SkeletonApi.Application.Interfaces.Repositories;
//using SkeletonApi.Domain.Entities;

//namespace SkeletonApi.Persistence.Repositories
//{
//    public class ReceiveRespository : IReceiveRepository
//    {
//        private readonly IGenericRepository<Receive> _receiveRepository;

//        public ReceiveRespository(IGenericRepository<Receive> receiveRepository)
//        {
//            _receiveRepository = receiveRepository;
//        }

//        public async Task<bool> CheckQtyItemId(CreateReceiveRequest request)
//        {
//            var qty = _receiveRepository.FindByCondition(x => x.ItemId == request.ItemId).FirstOrDefault();
//            if (qty != null)
//            {
//                qty.ItemId = request.ItemId;
//                qty.Sender = request.Sender;
//                qty.ArrivalDate = request.ArrivalDate;
//                qty.CostEachItem = request.CostEachItem;
//                qty.Path = request.Path.FileName;
//                qty.Note = request.Note;
//                qty.QtyItem += request.QtyItem;
//                qty.UpdatedAt = DateTime.UtcNow;
//                await _receiveRepository.UpdateAsync(qty);
//                return true;
//            }
//            return false;
//        }

//        public async Task<bool> UploadFile(IFormFile file)
//        {
//            var allowedExtensions = new[] { ".jpg", ".pdf" }; // ekstensi yang diizinkan
//            var fileExtension = Path.GetExtension(file.FileName).ToLower();
//            var maxFileSizeInBytes = 3 * 1024 * 1024; // ukuran maksimum (dalam byte)

//            if (file.Length > maxFileSizeInBytes & !allowedExtensions.Contains(fileExtension))
//            {
//                return false;
//            }
//            string photoDirectory = @"D:\FileReceive";
//            if (!Directory.Exists(photoDirectory))
//            {
//                Directory.CreateDirectory(photoDirectory);
//            }
//            var path = Path.Combine(photoDirectory, file.FileName);

//            using (var stream = new FileStream(path, FileMode.Create))
//            {
//                await file.CopyToAsync(stream);
//            }
//            return true;
//        }
//    }
//}