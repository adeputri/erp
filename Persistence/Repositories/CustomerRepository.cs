﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IGenericRepository<Customer> _customerRepository;

        public CustomerRepository(IGenericRepository<Customer> repository)
        {
            _customerRepository = repository;
        }

        public async Task<bool> ValidateData(Customer customer)
        {
            var x = await _customerRepository.FindByCondition(o => o.CustomerName.ToLower() == customer.CustomerName.ToLower() && o.Npwp.ToLower() == customer.Npwp.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}