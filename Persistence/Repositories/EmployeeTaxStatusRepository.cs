﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class EmployeetaxStatusRepository : IEmployeeTaxStatusRespository
    {
        private readonly IGenericRepository<EmployeeTaxStatus> _repository;

        public EmployeetaxStatusRepository(IGenericRepository<EmployeeTaxStatus> repository)
        {
            _repository = repository;
        }

        public async Task<bool> ValidateData(EmployeeTaxStatus employeeTaxStatus)
        {
            var x = await _repository.FindByCondition(o => o.Status.ToLower() == employeeTaxStatus.Status.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}