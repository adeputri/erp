﻿using Microsoft.EntityFrameworkCore;
using SkeletonApi.Application.Interfaces.Repositories;
using SkeletonApi.Domain.Entities;

namespace SkeletonApi.Persistence.Repositories
{
    public class EmployeeStatusRepository : IEmployeeStatusRepository
    {
        private readonly IGenericRepository<EmployeeStatus> _repositoryEmployeeStatus;

        public EmployeeStatusRepository(IGenericRepository<EmployeeStatus> repository)
        {
            _repositoryEmployeeStatus = repository;
        }

        public async Task<bool> ValidateData(EmployeeStatus employeeStatus)
        {
            var x = await _repositoryEmployeeStatus.FindByCondition(o => o.Status.ToLower() == employeeStatus.Status.ToLower()).CountAsync();
            if (x > 0)
            {
                return false;
            }
            return true;
        }
    }
}