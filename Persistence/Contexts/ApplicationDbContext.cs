﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SkeletonApi.Domain.Common.Abstracts;
using SkeletonApi.Domain.Common.Interfaces;
using SkeletonApi.Domain.Entities;
using System.Data;

namespace SkeletonApi.Persistence.Contexts
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, string, IdentityUserClaim<string>,
    UserRole, IdentityUserLogin<string>,
    IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        private readonly IDomainEventDispatcher _dispatcher;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IDomainEventDispatcher dispatcher = null)
            : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            _dispatcher = dispatcher;
        }

        public DbSet<Account> Accounts => Set<Account>();
        public DbSet<User> Users => Set<User>();
        public DbSet<Role> Roles => Set<Role>();
        public DbSet<UserRole> UserRoles => Set<UserRole>();
        public DbSet<Permission> Permissions => Set<Permission>();
        public DbSet<ActivityUser> ActivityUsers => Set<ActivityUser>();
        public DbSet<MasterProject> MasterProjects => Set<MasterProject>();
        public DbSet<Team> Teams => Set<Team>();
        public DbSet<Position> Positions => Set<Position>();
        public DbSet<Customer> Customers => Set<Customer>();
        public DbSet<EmployeeLevel> EmployeeLevels => Set<EmployeeLevel>();
        public DbSet<EmployeeStatus> EmployeeStatuses => Set<EmployeeStatus>();
        public DbSet<EmployeeTaxStatus> EmployeeTaxStatuses => Set<EmployeeTaxStatus>();
        public DbSet<Bank> Banks => Set<Bank>();
        public DbSet<BranchBank> BranchBanks => Set<BranchBank>();
        public DbSet<MasterUnit> Units => Set<MasterUnit>();
        public DbSet<Item> Items => Set<Item>();
        public DbSet<RequestItemWarehouse> RequestItemWarehouses => Set<RequestItemWarehouse>();
        public DbSet<PaymentSchedule> PaymentSchedules => Set<PaymentSchedule>();
        public DbSet<PaymentStatus> PaymentStatuses => Set<PaymentStatus>();
        public DbSet<ApprovalStatus> ApprovalStatuses => Set<ApprovalStatus>();
        public DbSet<ProrateSetting> ProrateSettings => Set<ProrateSetting>();
        public DbSet<Supplier> Suppliers => Set<Supplier>();
        public DbSet<TaxMethode> TaxMethodes => Set<TaxMethode>();
        public DbSet<PurchasePpn> PurchasePpns => Set<PurchasePpn>();
        public DbSet<CodePettyCash> CodePettyCashes => Set<CodePettyCash>();
        public DbSet<Employee> Employees => Set<Employee>();
        public DbSet<Merk> Merks => Set<Merk>();
        public DbSet<SettingTitle> SettingTitles => Set<SettingTitle>();
        public DbSet<Category> Categories => Set<Category>();
        public DbSet<Project> Projects => Set<Project>();
        public DbSet<PurchaseNonPpn> PurchaseNonPpns => Set<PurchaseNonPpn>();
        public DbSet<NonProject> NonProjects => Set<NonProject>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MasterUnit>()
                .HasQueryFilter(x => x.DeletedAt == null);

            modelBuilder.Entity<Item>()
                .HasQueryFilter(x => x.DeletedAt == null);

            modelBuilder.Entity<MasterProject>()
                .HasQueryFilter(x => x.DeletedAt == null);

            modelBuilder.Entity<UserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId);

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId);
            });

            modelBuilder.Entity<Item>()
                .HasOne(o => o.Merk)
                .WithMany(o => o.Items)
                .HasForeignKey(w => w.MerkId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Warehouse>()
                .HasOne(w => w.RequestEmployee)
                .WithMany(w => w.WarehouseRequest)
                .HasForeignKey(w => w.RequestEmployeeId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Warehouse>()
                .HasOne(w => w.PICEmployee)
                .WithMany(w => w.WarehousePIC)
                .HasForeignKey(w => w.PICEmployeeId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Warehouse>()
                .HasOne(w => w.Project)
                .WithMany(w => w.Warehouses)
                .HasForeignKey(w => w.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<RequestItemWarehouse>()
                .HasOne(w => w.RequestEmployee)
                .WithMany(w => w.RequestItemWarehouseRequest)
                .HasForeignKey(w => w.RequestEmployeeId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<RequestItemWarehouse>()
                .HasOne(w => w.PICEmployee)
                .WithMany(w => w.RequestItemWarehousePIC)
                .HasForeignKey(w => w.PICEmployeeId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<RequestItemWarehouse>()
                .HasOne(w => w.Project)
                .WithMany(w => w.RequestItemWarehouses)
                .HasForeignKey(w => w.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Role>()
               .HasMany(e => e.Permissions)
               .WithOne(e => e.Role)
               .HasForeignKey(e => e.RoleId)
               .IsRequired(true);

            modelBuilder.Entity<Bank>()
              .HasMany(e => e.BranchBank)
              .WithOne(e => e.Bank)
              .HasForeignKey(e => e.BankId)
              .IsRequired(true);

            modelBuilder.Entity<MasterUnit>()
              .HasMany(e => e.Item)
              .WithOne(e => e.MasterUnit)
              .HasForeignKey(e => e.MasterUnitId)
              .IsRequired(true);

            modelBuilder.Entity<Supplier>()
             .HasMany(e => e.CashAtBank)
             .WithOne(e => e.Supplier)
             .HasForeignKey(e => e.SupplierId)
             .IsRequired(true);

            modelBuilder.Entity<Project>()
             .HasMany(e => e.PurchasePpn)
             .WithOne(e => e.Project)
             .HasForeignKey(e => e.ProjectId)
             .IsRequired(true);

            modelBuilder.Entity<CodePettyCash>()
           .HasMany(e => e.ReportNonPpns)
           .WithOne(e => e.CodePettyCash)
           .HasForeignKey(e => e.CodeId)
           .IsRequired(false);

            modelBuilder.Entity<MasterProject>()
            .HasMany(e => e.ReportNonPpns)
            .WithOne(e => e.MasterProject)
            .HasForeignKey(e => e.MasterProjectId)
            .IsRequired(false);

            modelBuilder.Entity<Category>()
            .HasMany(e => e.PurchaseNonPpn)
            .WithOne(e => e.Category)
            .HasForeignKey(e => e.CategoryId)
            .IsRequired(false);

            modelBuilder.Entity<Employee>()
           .HasMany(e => e.Projects)
           .WithOne(e => e.Employee)
           .HasForeignKey(e => e.PicId)
           .IsRequired(true);

            modelBuilder.Entity<MasterProject>()
           .HasMany(e => e.Projects)
           .WithOne(e => e.MasterProject)
           .HasForeignKey(b => b.MasterProjectId)
           .IsRequired(true);

            modelBuilder.Entity<Project>()
            .HasMany(e => e.SectionProjects)
            .WithOne(e => e.Project)
            .HasForeignKey(b => b.ProjectId)
            .IsRequired(true);

            modelBuilder.Entity<SectionProject>()
            .HasMany(e => e.TaskProjects)
            .WithOne(e => e.SectionProject)
            .HasForeignKey(b => b.SectionProjectId)
            .IsRequired(true);

            modelBuilder.Entity<Project>()
            .HasMany(e => e.NonProjects)
            .WithOne(e => e.Project)
            .HasForeignKey(b => b.ProjectId)
            .IsRequired(true);

            modelBuilder.Entity<Employee>()
          .HasMany(e => e.NonProjects)
          .WithOne(e => e.Employee)
          .HasForeignKey(b => b.PicId)
          .IsRequired(true);

            modelBuilder.Entity<Item>()
          .HasMany(e => e.NonProjects)
          .WithOne(e => e.Item)
          .HasForeignKey(b => b.ItemId)
          .IsRequired(true);

            modelBuilder.Entity<ActivityUser>(
            eb =>
            {
                eb.Property(b => b.Id).HasColumnName("id").HasColumnType("uuid");
                eb.Property(b => b.UserName).HasColumnName("username").HasColumnType("text");
                eb.Property(b => b.LogType).HasColumnName("logtype").HasColumnType("text");
                eb.Property(b => b.DateTime).HasColumnName("datetime").HasColumnType("timestamp");
            });

            modelBuilder.Entity<Account>(
            eb =>
            {
                eb.Property(b => b.Username).HasColumnName("username").HasColumnType("text");
                eb.Property(b => b.PhotoURL).HasColumnName("photo_url").HasColumnType("text");
            });
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            int result = await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

            // ignore events if no dispatcher provided
            if (_dispatcher == null) return result;

            // dispatch events only if save was successful
            var entitiesWithEvents = ChangeTracker.Entries<BaseEntity>()
                .Select(e => e.Entity)
                .Where(e => e.DomainEvents.Any())
                .ToArray();

            await _dispatcher.DispatchAndClearEvents(entitiesWithEvents);

            return result;
        }

        public override int SaveChanges()
        {
            return SaveChangesAsync().GetAwaiter().GetResult();
        }

        public IDbConnection Connection { get; }
    }
}