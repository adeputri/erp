﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.ApprovalStatuses;
using SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.CreateApprovalStatus;
using SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.DeleteApprovalStatus;
using SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Commands.UpdateApprovalStatus;
using SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Queries.GetAllApprovalStatus;
using SkeletonApi.Application.Features.MasterData.ApprovalStatuses.Queries.GetApprovalStatusWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    public class ApprovalStatusController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public ApprovalStatusController(IMediator mediator, ILogger<ApprovalStatusController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateApprovalStatusResponseDto>>> Create(CreateApprovalStatusRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteApprovalStatusRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<ApprovalStatus>>> Update(Guid id, UpdateApprovalStatusRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetApprovalStatusWithPaginationDto>>> GetWithPagination([FromQuery] GetApprovalStatusWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetApprovalStatusWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-approval")]
        public async Task<ActionResult<Result<List<GetAllApprovalStatusDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllApprovalStatusQuery());
        }
    }
}