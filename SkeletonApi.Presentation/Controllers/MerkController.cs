﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Merks;
using SkeletonApi.Application.Features.MasterData.Merks.Commands.CreateMerk;
using SkeletonApi.Application.Features.MasterData.Merks.Commands.Delete;
using SkeletonApi.Application.Features.MasterData.Merks.Commands.Update;
using SkeletonApi.Application.Features.MasterData.Merks.Queries.GetAllMerk;
using SkeletonApi.Application.Features.MasterData.Merks.Queries.GetMerkWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/merk")]
    public class MerkController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public MerkController(IMediator mediator, ILogger<MerkController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateMerkResponseDto>>> Create(CreateMerkRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteMerkRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<Merk>>> Update(Guid id, UpdateMerkRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetMerkWithPaginationDto>>> GetWithPagination([FromQuery] GetMerkWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetMerkWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-merk")]
        public async Task<ActionResult<Result<List<GetAllMerkDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllMerkQuery());
        }
    }
}