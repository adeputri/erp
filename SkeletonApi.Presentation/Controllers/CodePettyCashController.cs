﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.PettyCashs;
using SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.CreateCodePettyCash;
using SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.DeleteCodePettyCash;
using SkeletonApi.Application.Features.MasterData.PettyCashs.Commands.UpdateCodePettyCash;
using SkeletonApi.Application.Features.MasterData.PettyCashs.Queries.GetAllCodePettyCash;
using SkeletonApi.Application.Features.MasterData.PettyCashs.Queries.GetPettyCashWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/code-petty-cash")]
    public class CodePettyCashController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public CodePettyCashController(IMediator mediator, ILogger<CodePettyCashController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateCodePettyCashResponseDto>>> Create(CreateCodePettyCashRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteCodePettyCashRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<CodePettyCash>>> Update(Guid id, UpdatePettyCashRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetPettyCashWithPaginationDto>>> GetWithPagination([FromQuery] GetPettyCashWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetPettyCashWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-code")]
        public async Task<ActionResult<Result<List<GetAllCodePettyCashDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllCodePettyCashQuery());
        }
    }
}