﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.PaymentStatuses;
using SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.CreatePaymentStatus;
using SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.DeletePaymentStatus;
using SkeletonApi.Application.Features.MasterData.PaymentStatuses.Commands.UpdatePaymentStatus;
using SkeletonApi.Application.Features.MasterData.PaymentStatuses.Queries.GetAllPaymentStatus;
using SkeletonApi.Application.Features.MasterData.PaymentStatuses.Queries.GetPaymentStatusWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/payment-status")]
    public class PaymentStatusController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public PaymentStatusController(IMediator mediator, ILogger<PaymentStatusController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreatePaymentStatusResponseDto>>> Create(CreatePaymentStatusRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeletePaymentStatusRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<PaymentStatus>>> Update(Guid id, UpdatePaymentStatusRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetPaymentStatusWithPaginationDto>>> GetWithPagination([FromQuery] GetPaymentStatusWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetPaymentStatusWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-payment-status")]
        public async Task<ActionResult<Result<List<GetAllPaymentStatusDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllPaymentStatusQuery());
        }
    }
}