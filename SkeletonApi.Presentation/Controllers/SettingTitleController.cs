﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Title;
using SkeletonApi.Application.Features.MasterData.Title.Commands.CreateSettingTitle;
using SkeletonApi.Application.Features.MasterData.Title.Commands.UpdateSettingTitle;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/setting-title")]
    public class SettingTitleController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public SettingTitleController(IMediator mediator, ILogger<SettingTitleController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateSettingTitleResponseDto>>> Create(CreateSettingTitleRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<SettingTitle>>> Update(Guid id, UpdateSettingTitleRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }
    }
}