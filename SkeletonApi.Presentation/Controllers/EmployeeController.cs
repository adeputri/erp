﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Employees.Queries.GetAllEmployee;
using SkeletonApi.Shared;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/employee")]
    public class EmployeeController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public EmployeeController(IMediator mediator, ILogger<EmployeeController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet("get-all-employee")]
        public async Task<ActionResult<Result<List<GetAllEmployeeDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllEmployeeQuery());
        }
    }
}