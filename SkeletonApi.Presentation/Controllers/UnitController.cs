﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Units;
using SkeletonApi.Application.Features.MasterData.Units.Commands.CreateUnit;
using SkeletonApi.Application.Features.MasterData.Units.Commands.DeleteUnit;
using SkeletonApi.Application.Features.MasterData.Units.Commands.UpdateUnit;
using SkeletonApi.Application.Features.MasterData.Units.Queries.GetAllUnit;
using SkeletonApi.Application.Features.MasterData.Units.Queries.GetUnitWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/unit")]
    public class UnitController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public UnitController(IMediator mediator, ILogger<UnitController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateUnitResponnseDto>>> Create(CreateUnitRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteUnitRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<MasterUnit>>> Update(Guid id, UpdateUnitRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetUnitWithPaginationDto>>> GetWithPagination([FromQuery] GetUnitWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetUnitWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-unit")]
        public async Task<ActionResult<Result<List<GetAllUnitDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllUnitQuery());
        }
    }
}