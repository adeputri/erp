﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.TaxMethodes;
using SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.CreateTaxMethod;
using SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.DeleteTaxmethod;
using SkeletonApi.Application.Features.MasterData.TaxMethodes.Commands.UpdateTaxMethod;
using SkeletonApi.Application.Features.MasterData.TaxMethodes.Queries.GetAllTaxMethod;
using SkeletonApi.Application.Features.MasterData.TaxMethodes.Queries.GetAllTaxMethodWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/tax-method")]
    public class TaxMethodController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public TaxMethodController(IMediator mediator, ILogger<TaxMethodController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateTaxMethodResponseDto>>> Create(CreateTaxMethodRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteTaxmethodRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<TaxMethode>>> Update(Guid id, UpdateTaxMethodRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetTaxMethodWithPaginationDto>>> GetWithPagination([FromQuery] GetTaxMethodWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetTaxMethodWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-customer")]
        public async Task<ActionResult<Result<List<GetAllTaxMethodDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllTaxMethodQuery());
        }
    }
}