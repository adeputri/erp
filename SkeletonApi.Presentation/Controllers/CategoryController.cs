﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Categories;
using SkeletonApi.Application.Features.MasterData.Categories.Commands.CreateCategory;
using SkeletonApi.Application.Features.MasterData.Categories.Commands.DeleteCategory;
using SkeletonApi.Application.Features.MasterData.Categories.Commands.UpdateCategory;
using SkeletonApi.Application.Features.MasterData.Categories.Queries.GetAllCategory;
using SkeletonApi.Application.Features.MasterData.Categories.Queries.GetCategoryWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/category")]
    public class CategoryController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public CategoryController(IMediator mediator, ILogger<CategoryController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateCategoryResponseDto>>> Create(CreateCategoryRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteCategoryRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<Category>>> Update(Guid id, UpdateCategoryRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetCategoryWithPaginationDto>>> GetWithPagination([FromQuery] GetCategoryWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetCategoryWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-category")]
        public async Task<ActionResult<Result<List<GetAllCategoryDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllCategoryQuery());
        }
    }
}