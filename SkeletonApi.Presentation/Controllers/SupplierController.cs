﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Suppliers;
using SkeletonApi.Application.Features.MasterData.Suppliers.Commands.CreateSupplier;
using SkeletonApi.Application.Features.MasterData.Suppliers.Commands.DeleteSupplier;
using SkeletonApi.Application.Features.MasterData.Suppliers.Commands.Import;
using SkeletonApi.Application.Features.MasterData.Suppliers.Commands.UpdateSupplier;
using SkeletonApi.Application.Features.MasterData.Suppliers.Queries.GetAllSupplier;
using SkeletonApi.Application.Features.MasterData.Suppliers.Queries.GetSupplierWithPagination;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN;
using SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/supplier")]
    public class SupplierController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public SupplierController(IMediator mediator, ILogger<SupplierController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateSupplierResponseDto>>> Create(CreateSupplierRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteSupplierRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<Supplier>>> Update(Guid id, UpdateSupplierRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetSupplierWithPaginationDto>>> GetWithPagination([FromQuery] GetSupplierWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetSupplierWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-supplier")]
        public async Task<ActionResult<Result<List<GetAllSupplierDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllSupplierQuery());
        }
        [HttpPost("upload-file")]
        public async Task<ActionResult<Result<CreateSupplierResponseDto>>> Upload(IFormFile file)
        {
            var upload = new ImportExcelSupplierRequest(file);
            return await _mediator.Send(upload);
        }
    }
}