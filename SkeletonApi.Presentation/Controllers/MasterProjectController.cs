﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.MasterProjects;
using SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.CreateMasterProject;
using SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.DeleteMasterProject;
using SkeletonApi.Application.Features.MasterData.MasterProjects.Commands.UpdateMasterProject;
using SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetAllMasterProject;
using SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetCustomerByIdMasterProject;
using SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetMasterProjectForSalesProject;
using SkeletonApi.Application.Features.MasterData.MasterProjects.Queries.GetMasterProjectWithPagination;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/master-project")]
    public class MasterProjectController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public MasterProjectController(IMediator mediator, ILogger<MasterProjectController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateProjectResponseDto>>> Create(CreateMasterProjectRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteMasterProjectRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<UpdateProjectResponseDto>>> Update(Guid id, UpdateMasterProjectRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetMasterProjectWithPaginationDto>>> GetWithPagination([FromQuery] GetMasterProjectWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetMasterProjectWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-master-project")]
        public async Task<ActionResult<Result<List<GetAllMasterProjectDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllMasterProjectQuery());
        }

        [HttpGet("get-customer/{id:guid}")]
        public async Task<ActionResult<Result<GetCustomerByIdMasterProjectDto>>> Get(Guid id)
        {
            return await _mediator.Send(new GetCustomerByIdMasterProjectQuery(id));
        }
        [HttpGet("master-project-sales-project")]
        public async Task<ActionResult<Result<List<GetMasterProjectForSalesProjectDto>>>> Get()
        {
            return await _mediator.Send(new GetMasterProjectForSalesProjectQuery());
        }


    }
}