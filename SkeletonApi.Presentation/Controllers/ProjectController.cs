﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Suppliers.Commands.Import;
using SkeletonApi.Application.Features.MasterData.Suppliers;
using SkeletonApi.Application.Features.Sales.ProjectInformations;
using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks;
using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Commands.CreateMainTask;
using SkeletonApi.Application.Features.Sales.ProjectInformations.AddTasks.Queries.GetAllTaskByIdProject;
using SkeletonApi.Application.Features.Sales.ProjectInformations.Queries.GetAllProject;
using SkeletonApi.Application.Features.Sales.ProjectInformations.Queries.GetProjectById;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.CreateProjectInformation;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.DeleteProjectInformation;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Commands.UpdateProjectInformation;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Queries.GetAllProject;
using SkeletonApi.Application.Features.Seller.ProjectInformations.Queries.GetProjectWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;
using SkeletonApi.Application.Features.Sales.ProjectInformations.Commands.Import;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/project")]
    public class ProjectController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public ProjectController(IMediator mediator, ILogger<ProjectController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost("main-task")]
        public async Task<ActionResult<Result<CreateMainTaskResponseDto>>> CreateMainTask(CreateMainTaskRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateProjectInformationResponseDto>>> Create(CreateProjectInformationRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteProjectInformationRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<Project>>> Update(Guid id, UpdateProjectInformationRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("get-all-project")]
        public async Task<ActionResult<Result<List<GetAllProjectDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllProjectQuery());
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetProjectWithPaginationDto>>> GetWithPagination([FromQuery] GetProjectWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetProjectWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("{projectId:guid}")]
        public async Task<ActionResult<Result<IEnumerable<GetAllTaskByIdProjectDto>>>> Get(Guid projectId)
        {
            return await _mediator.Send(new GetAllTaskByIdProjectQuery(projectId));
        }

        [HttpGet("get-by-id/{id:guid}")]
        public async Task<ActionResult<Result<GetProjectByIdDto>>> GetProject(Guid id)
        {
            return await _mediator.Send(new GetProjectByIdQuery(id));
        }
        [HttpPost("upload-file")]
        public async Task<ActionResult<Result<CreateProjectInformationResponseDto>>> Upload(IFormFile file)
        {
            var upload = new ImportExcelProjectRequest(file);
            return await _mediator.Send(upload);
        }
    }
}