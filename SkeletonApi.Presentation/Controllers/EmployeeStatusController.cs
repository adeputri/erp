﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.EmployeeStatuses;
using SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.CreateEmployeesStatus;
using SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.DeleteEmployeeStatus;
using SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Commands.UpdateEmployeeStatus;
using SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Queries.GetAllEmployeeStatus;
using SkeletonApi.Application.Features.MasterData.EmployeeStatuses.Queries.GetEmployeeStatusWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/employee-status")]
    public class EmployeeStatusController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public EmployeeStatusController(IMediator mediator, ILogger<EmployeeStatusController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateEmployeeStatusResponseDto>>> Create(CreateEmployeeStatusRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteEmployeeStatusRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<EmployeeStatus>>> Update(Guid id, UpdateEmployeeStatusRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetEmployeeStatusWithPaginationDto>>> GetWithPagination([FromQuery] GetEmployeeStatusWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetEmployeeStatusWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-employee-status")]
        public async Task<ActionResult<Result<List<GetAllEmployeeStatusDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllEmployeeStatusQuery());
        }
    }
}