﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/history")]
    public class HistoryController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public HistoryController(IMediator mediator, ILogger<HistoryController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        //    [HttpGet("paged")]
        //    public async Task<ActionResult<PaginatedResult<GetHistoryWithPaginationDto>>> GetWithPagination([FromQuery] GetHistoryWithPaginationQuery query)
        //    {
        //        // Call Validate or ValidateAsync and pass the object which needs to be validated
        //        var validator = new GetHistoryWithPaginationValidator();

        //        var result = validator.Validate(query);

        //        if (result.IsValid)
        //        {
        //            var pg = await _mediator.Send(query);
        //            var paginationData = new
        //            {
        //                pg.page_number,
        //                pg.total_pages,
        //                pg.page_size,
        //                pg.total_count,
        //                pg.has_previous,
        //                pg.has_next
        //            };
        //            Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
        //            return Ok(pg);
        //        }

        //        var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
        //        return BadRequest(errorMessages);
        //    }

        //    [HttpGet("download")]
        //    public async Task<ActionResult<PaginatedResult<GetHistoryWithPaginationDto>>> DownloadToExcel([FromQuery] GetHistoryWithPaginationQuery query)
        //    {
        //        var validator = new GetHistoryWithPaginationValidator();

        //        // Call Validate or ValidateAsync and pass the object which needs to be validated
        //        var result = validator.Validate(query);

        //        if (result.IsValid)
        //        {
        //            var pg = await _mediator.Send(query);
        //            byte[]? bytes = null;
        //            string filename = string.Empty;
        //            try
        //            {
        //                if (pg != null)
        //                {
        //                    var Export = new DownloadHistoryToExcel(pg);
        //                    Export.GetListExcel(ref bytes, ref filename);
        //                }
        //                Response.Headers.Add("x-download", filename);
        //                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
        //            }
        //            catch (Exception ex)
        //            {
        //                if (ex.InnerException == null) { return Problem(ex.Message); }
        //                return Problem(ex.InnerException.Message);
        //            }
        //        }
        //        var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
        //        return BadRequest(errorMessages);
        //    }
        //}
    }
}