﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts;
using SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Commands.DeletePensiunCost;
using SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Commands.UpdatePensiunCost;
using SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Queries.GetAllPensionInsuranceCost;
using SkeletonApi.Application.Features.MasterData.PensionInsuranceCosts.Queries.GetPensionInsuranceCostWtihPagination;
using SkeletonApi.Application.Features.MasterData.PensiunCosts.Commands.CreatePensiunCost;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/pension-insurance-cost")]
    public class PensionInsuranceCostController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public PensionInsuranceCostController(IMediator mediator, ILogger<PensionInsuranceCostController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreatePensionInsuranceCostResponseDto>>> Create(CreatePensionInsuranceCostsRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeletePensionCostRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<PensionInsuranceCost>>> Update(Guid id, UpdatePensionInsuranceCostRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetPensionInsuranceCostWtihPaginationDto>>> GetWithPagination([FromQuery] GetPensionInsuranceCostWtihPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetPensionInsuranceCostWtihPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-pension-cost")]
        public async Task<ActionResult<Result<List<GetAllPensionInsuranceCostDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllPensionInsuranceCostQuery());
        }
    }
}