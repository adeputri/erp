﻿//using MediatR;
//using Microsoft.AspNetCore.Hosting;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
//using SkeletonApi.Domain.Entities;
//using SkeletonApi.Shared;

//namespace SkeletonApi.Presentation.Controllers
//{
//    [Route("api/receive")]
//    public class ReceiveController : ApiControllerBase
//    {
//        private readonly IMediator _mediator;
//        private readonly IWebHostEnvironment _webHostEnvironment;
//        private ILogger _logger;

//        public ReceiveController(IMediator mediator, ILogger<ReceiveController> logger, IWebHostEnvironment webHostEnvironment)
//        {
//            _mediator = mediator;
//            _logger = logger;
//            _webHostEnvironment = webHostEnvironment;
//        }

//        [HttpPost("incoming-item")]
//        public async Task<ActionResult<Result<Receive>>> Create([FromForm] CreateIncomingItemRequest command, IFormFile file)
//        {
//            if (file != null && file.Length > 0)
//            {
//                var uploadFolderPath = "D:\\UploadedFiles";

//                if (!Directory.Exists(uploadFolderPath))
//                {
//                    Directory.CreateDirectory(uploadFolderPath);
//                }
//                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
//                var extensionsFile = Path.GetExtension(file.FileName);
//                var filePath = Path.Combine(uploadFolderPath, $"{fileName}{DateTime.Now.ToString("dd_MM_yyyy HH_mm_ss")}{extensionsFile}");

//                using (var stream = new FileStream(filePath, FileMode.Create))
//                {
//                    await file.CopyToAsync(stream);
//                }
//                command.PathDocument = filePath;
//            }
//            return await _mediator.Send(command);
//        }

//        //[HttpDelete("{id:guid}")]
//        //public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
//        //{
//        //    return await _mediator.Send(new DeleteReceiveRequest(id));
//        //}

//        //[HttpGet("paged")]
//        //public async Task<ActionResult<PaginatedResult<GetReceiveWithPaginationDto>>> GetWithPagination([FromQuery] GetReceiveWithPaginationQuery query)
//        //{
//        //    // Call Validate or ValidateAsync and pass the object which needs to be validated
//        //    var validator = new GetReceiveWithPaginationValidator();

//        //    var result = validator.Validate(query);

//        //    if (result.IsValid)
//        //    {
//        //        var pg = await _mediator.Send(query);
//        //        var paginationData = new
//        //        {
//        //            pg.page_number,
//        //            pg.total_pages,
//        //            pg.page_size,
//        //            pg.total_count,
//        //            pg.has_previous,
//        //            pg.has_next
//        //        };
//        //        Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
//        //        return Ok(pg);
//        //    }

//        //    var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
//        //    return BadRequest(errorMessages);
//        //}
//    }
//}