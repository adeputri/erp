﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.Sales.NonProjects;
using SkeletonApi.Application.Features.Sales.NonProjects.Commands.CreateNonProject;
using SkeletonApi.Application.Features.Sales.NonProjects.Commands.DeleteNonProject;
using SkeletonApi.Application.Features.Sales.NonProjects.Commands.UpdateNonProject;
using SkeletonApi.Application.Features.Sales.NonProjects.Queries.GetNonProjectById;
using SkeletonApi.Application.Features.Sales.NonProjects.Queries.GetNonProjectWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/non-project")]
    public class NonProjectController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public NonProjectController(IMediator mediator, ILogger<NonPpnController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateNonProjectResponseDto>>> Create(CreateNonProjectRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteNonProjectRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<NonProject>>> Update(Guid id, UpdateNonProjectRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetNonProjectWithPaginationDto>>> GetWithPagination([FromQuery] GetNonProjectWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetNonProjectWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Result<GetNonProjectByIdDto>>> GetProject(Guid id)
        {
            return await _mediator.Send(new GetNonProjectByIdQuery(id));
        }
    }
}