﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Teams;
using SkeletonApi.Application.Features.MasterData.Teams.Commands.CreateTeam;
using SkeletonApi.Application.Features.MasterData.Teams.Commands.DeleteTeam;
using SkeletonApi.Application.Features.MasterData.Teams.Commands.UpdateTeam;
using SkeletonApi.Application.Features.MasterData.Teams.Queries.GetAllTeam;
using SkeletonApi.Application.Features.MasterData.Teams.Queries.GetTeamWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/team")]
    public class TeamController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logeer;

        public TeamController(IMediator mediator, ILogger<TeamController> logger)
        {
            _mediator = mediator;
            _logeer = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateTeamResponseDto>>> Create(CreateTeamRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteTeamRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<Team>>> Update(Guid id, UpdateTeamRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetTeamWithPaginationDto>>> GetWithPagination([FromQuery] GetTeamWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetTeamWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-team")]
        public async Task<ActionResult<Result<List<GetAllTeamDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllTeamQuery());
        }
    }
}