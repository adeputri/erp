﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.BankBranchs;
using SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.CreateBankBranch;
using SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.DeleteBankBranch;
using SkeletonApi.Application.Features.MasterData.BankBranchs.Commands.UpdateBankBranch;
using SkeletonApi.Application.Features.MasterData.BankBranchs.Queries.GetAllBankBranch;
using SkeletonApi.Application.Features.MasterData.BankBranchs.Queries.GetBankBranchWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/bank-branch")]
    public class BankBranchController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public BankBranchController(IMediator mediator, ILogger<BankBranchController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateBankBranchResponseDto>>> Create(CreateBankBranchRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteBankBranchRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<BranchBank>>> Update(Guid id, UpdateBankBranchRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetBankBranchWithPaginationDto>>> GetWithPagination([FromQuery] GetBankBranchWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetBankBranchWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-bank-branch")]
        public async Task<ActionResult<Result<List<GetAllBankBranchDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllBankBranchQuery());
        }
    }
}