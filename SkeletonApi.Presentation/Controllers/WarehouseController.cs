﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SkeletonApi.Application.Features.Warehouses.IncomingItems.Command.CreateIncomingItem;
using SkeletonApi.Application.Features.Warehouses.RequestItems.Command.CreateRequestItem;
using SkeletonApi.Shared;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/warehouse")]
    public class WarehouseController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public WarehouseController(IMediator mediator, IWebHostEnvironment webHostEnvironment)
        {
            _mediator = mediator;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost("incoming-item")]
        public async Task<ActionResult<Result<Guid>>> CreateIncomingItem([FromForm] CreateIncomingItemRequest request, IFormFile file)
        {
            if (file != null && file.Length > 0)
            {
                var uploadFolderPath = "D:\\UploadedFiles";

                if (!Directory.Exists(uploadFolderPath))
                {
                    Directory.CreateDirectory(uploadFolderPath);
                }

                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var extensionsFile = Path.GetExtension(file.FileName);
                var filePath = Path.Combine(uploadFolderPath, $"{fileName}{DateTime.Now.ToString("dd_MM_yyyy HH_mm_ss")}{extensionsFile}");

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                request.PathDocument = filePath;
            }
            return await _mediator.Send(request);
        }

        [HttpPost("request-item")]
        public async Task<ActionResult<Result<Guid>>> CreateRequestItem([FromBody] CreateRequestItemCommand request)
        {
            return await _mediator.Send(request);
        }
    }
}
