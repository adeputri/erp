﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.Purchases.PPN;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.ConfirmApprovePpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.CreatePpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.DeletePpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.Import;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.UpdatePpn;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.Download;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnById;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPpnWithPagination;
using SkeletonApi.Application.Features.Purchases.PPN.Queries.GetPreviewFilePpn;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/ppn")]
    public class PpnController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;
        public PpnController(IMediator mediator, ILogger<PpnController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreatePpnResponseDto>>> Create([FromForm] CreatePpnRequest command)

        {
            command.DataInv = JsonSerializer.Deserialize<List<DataInv>>(command.DataInvoice);
            command.DataPo = JsonSerializer.Deserialize<List<DataPo>>(command.DataPreOrder);
            return await _mediator.Send(command);
        }

        [HttpGet("{uniqueCode}")]
        public async Task<ActionResult<Result<GetPpnByIdDto>>> Get(string uniqueCode)
        {
            return await _mediator.Send(new GetPpnByIdQuery(uniqueCode));
        }

        [HttpDelete("{uniqueCode}")]
        public async Task<ActionResult<Result<string>>> Delete(string uniqueCode)
        {
            return await _mediator.Send(new DeletePpnRequest(uniqueCode));
        }

        [HttpPut("{uniqueCode}")]
        public async Task<ActionResult<Result<PurchasePpn>>> Update(string uniqueCode, [FromForm] UpdatePpnRequest command)
        {
            if (uniqueCode != command.UniqueCode)
            {
                return BadRequest();
            }
            command.DataInv = JsonSerializer.Deserialize<List<DataInv>>(command.DataInvoice);
            command.DataPo = JsonSerializer.Deserialize<List<DataPo>>(command.DataPreOrder);
            return await _mediator.Send(command);
        }

        [HttpPut("approval/{month:int}")]
        public async Task<ActionResult<Result<PurchasePpn>>> Confirm(int month, ConfirmApproveCashAtBankRequest command)
        {
            if (month != command.Month)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("preview/{path}")]
        public async Task<ActionResult<Result<GetPreviewFilePpnDto>>> GetFile(string path)
        {
            var file = await _mediator.Send(new GetPreviewFilePpnQuery(path));
            return PhysicalFile(file.Data.Path, file.Data.MimeType);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetPpnWithPaginationDto>>> GetWithPagination([FromQuery] GetPpnWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetPpnWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("download")]
        public async Task<ActionResult<PaginatedResult<GetPpnWithPaginationDto>>> DownloadToExcel([FromQuery] GetPpnWithPaginationQuery query)
        {
            var validator = new GetPpnWithPaginationValidator();

            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                byte[]? bytes = null;
                string filename = string.Empty;
                try
                {
                    if (pg != null)
                    {
                        var Export = new DownloadPpnToExcel(pg);
                        Export.GetListExcel(ref bytes, ref filename);
                    }
                    Response.Headers.Add("x-download", filename);
                    return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null) { return Problem(ex.Message); }
                    return Problem(ex.InnerException.Message);
                }
            }
            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpPost("upload-file")]
        public async Task<ActionResult<Result<CreatePpnResponseDto>>> Upload(IFormFile file)
        {
            var upload = new ImportExcelPurchasePpnRequest(file);
            return await _mediator.Send(upload);
        }

        //[HttpGet("get-template-excel")]
        //public async Task<ActionResult> DownloadTemplate()
        //{
        //    byte[] content = null;
        //    string fileName = null;
        //    try
        //    {
        //        _downloadTemplateExcel.GetTemplateExcel(ref content, ref fileName);
        //        return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        //    }
        //    catch (FileNotFoundException ex)
        //    {
        //        return NotFound(ex.Message);
        //    }
        //}
    }
}