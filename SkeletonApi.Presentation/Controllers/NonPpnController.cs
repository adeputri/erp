﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN;
using SkeletonApi.Application.Features.PurchaseNonPPN.ReportNonPPN.Commands.CreateReportNonPPN;
using SkeletonApi.Application.Features.Purchases.NonPPN.Commands.DeleteNonPpn;
using SkeletonApi.Application.Features.Purchases.NonPPN.Commands.UpdateNonPpn;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.Download;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnById;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnInformation;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetNonPpnWithPagination;
using SkeletonApi.Application.Features.Purchases.NonPPN.Queries.GetPreviewFileNonPpn;
using SkeletonApi.Application.Features.Purchases.PPN.Commands.Import;
using SkeletonApi.Application.Features.Purchases.PPN;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;
using SkeletonApi.Application.Features.Purchases.NonPPN.Commands.Import;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/non-ppn")]
    public class NonPpnController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public NonPpnController(IMediator mediator, ILogger<NonPpnController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpGet("information")]
        public async Task<ActionResult<Result<GetNonPpnInformationDto>>> GetInformation(int month, int year, Guid categoryId)
        {
            return await _mediator.Send(new GetNonPpnInformationQuery(month, year, categoryId));
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Result<GetNonPpnByIdDto>>> GetById(Guid id)
        {
            return await _mediator.Send(new GetNonPpnByIdQuery(id));
        }

        [HttpGet("preview/{path}")]
        public async Task<ActionResult<Result<GetPreviewFileNonPpnDto>>> GetFile(string path)
        {
            var file = await _mediator.Send(new GetPreviewFileNonPpnQuery(path));
            return PhysicalFile(file.Data.Path, file.Data.MimeType);
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateNonPpnResponseDto>>> Create([FromForm] CreateNonPpnRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteNonPpnRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<PurchaseNonPpn>>> Update(Guid id, [FromForm] UpdateNonPpnRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetNonPpnWithPaginationDto>>> GetWithPagination([FromQuery] GetNonPpnWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetNonPpnWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("download")]
        public async Task<ActionResult<PaginatedResult<GetNonPpnWithPaginationDto>>> DownloadToExcel([FromQuery] GetNonPpnWithPaginationQuery query)
        {
            var validator = new GetNonPpnWithPaginationValidator();

            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                byte[]? bytes = null;
                string filename = string.Empty;
                try
                {
                    if (pg != null)
                    {
                        var Export = new DownloadNonPpnToExcel(pg);
                        Export.GetListExcel(ref bytes, ref filename);
                    }
                    Response.Headers.Add("x-download", filename);
                    return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null) { return Problem(ex.Message); }
                    return Problem(ex.InnerException.Message);
                }
            }
            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }
        [HttpPost("upload-file")]
        public async Task<ActionResult<Result<CreateNonPpnResponseDto>>> Upload(IFormFile file)
        {
            var upload = new ImportExcelNonPpnRequest(file);
            return await _mediator.Send(upload);
        }
    }
}