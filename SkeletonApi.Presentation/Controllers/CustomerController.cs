﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Customers;
using SkeletonApi.Application.Features.MasterData.Customers.Commands.CreateCustomer;
using SkeletonApi.Application.Features.MasterData.Customers.Commands.DeleteCustomer;
using SkeletonApi.Application.Features.MasterData.Customers.Commands.UpdateCustomer;
using SkeletonApi.Application.Features.MasterData.Customers.Queries.GetAllCustomer;
using SkeletonApi.Application.Features.MasterData.Customers.Queries.GetCustomerWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/customer")]
    public class CustomerController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public CustomerController(IMediator mediator, ILogger<CustomerController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateCustomerResponseDto>>> Create(CreateCustomerRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteCustomerRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<Customer>>> Update(Guid id, UpdateCustomerRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetCustomerWithPaginationDto>>> GetWithPagination([FromQuery] GetCustomerWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetCustomerWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-customer")]
        public async Task<ActionResult<Result<List<GetAllCustomerDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllCustomerQuery());
        }
    }
}