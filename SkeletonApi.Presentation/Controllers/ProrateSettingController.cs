﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.ProrateSettings;
using SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.CreateProrateSetting;
using SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.DeleteProrateSetting;
using SkeletonApi.Application.Features.MasterData.ProrateSettings.Commands.UpdateProrateSetting;
using SkeletonApi.Application.Features.MasterData.ProrateSettings.Queries.GetAllProrateSetting;
using SkeletonApi.Application.Features.MasterData.ProrateSettings.Queries.GetProrateSettingWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/prorate-setting")]
    public class ProrateSettingController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public ProrateSettingController(IMediator mediator, ILogger<ProrateSettingController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateProrateSettingResponseDto>>> Create(CreateProrateSettingRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteProrateSettingRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<ProrateSetting>>> Update(Guid id, UpdateProrateSettingRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetProrateSettingWithPaginationDto>>> GetWithPagination([FromQuery] GetProrateSettingWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetProrateSettingWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-prorate-setting")]
        public async Task<ActionResult<Result<List<GetAllProrateSettingDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllProrateSettingQuery());
        }
    }
}