﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Items;
using SkeletonApi.Application.Features.MasterData.Items.Commands.CreateItem;
using SkeletonApi.Application.Features.MasterData.Items.Commands.DeleteItem;
using SkeletonApi.Application.Features.MasterData.Items.Commands.UpdateItem;
using SkeletonApi.Application.Features.MasterData.Items.Queries.GetAllItem;
using SkeletonApi.Application.Features.MasterData.Items.Queries.GetItemWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/item")]
    public class ItemController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public ItemController(IMediator mediator, ILogger<ItemController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateItemResponseDto>>> Create(CreateItemRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<string>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteItemRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<Item>>> Update(Guid id, UpdateItemRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetItemWithPaginationDto>>> GetWithPagination([FromQuery] GetItemWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetItemWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-item")]
        public async Task<ActionResult<Result<List<GetAllItemDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllItemQuery());
        }
    }
}