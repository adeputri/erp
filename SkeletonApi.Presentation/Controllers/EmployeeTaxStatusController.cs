﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses;
using SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.CreateEmployeeTaxStatus;
using SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.DeleteEmployeeTaxStatus;
using SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Commands.UpdateEmployeeTaxStatus;
using SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Queries.GetAllEmployeeTaxStatus;
using SkeletonApi.Application.Features.MasterData.EmployeeTaxStatuses.Queries.GetEmployeeTaxStatusWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/employee-tax-status")]
    public class EmployeeTaxStatusController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public EmployeeTaxStatusController(IMediator mediator, ILogger<EmployeeTaxStatusController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateEmployeeTaxStatusResponseDto>>> Create(CreateEmployeeTaxStatusRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteEmployeeTaxStatusRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<EmployeeTaxStatus>>> Update(Guid id, UpdateEmployeeTaxStatusRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetEmployeeTaxStatusWithPaginationDto>>> GetWithPagination([FromQuery] GetEmployeeTaxStatusWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetEmployeetaxStatusWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-employee-tax-status")]
        public async Task<ActionResult<Result<List<GetAllEmployeeTaxStatusDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllEmployeeTaxStatusQuery());
        }
    }
}