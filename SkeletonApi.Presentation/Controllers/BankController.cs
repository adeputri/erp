﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.Banks;
using SkeletonApi.Application.Features.MasterData.Banks.Commands.CreateBank;
using SkeletonApi.Application.Features.MasterData.Banks.Commands.DeleteBank;
using SkeletonApi.Application.Features.MasterData.Banks.Commands.UpdateBank;
using SkeletonApi.Application.Features.MasterData.Banks.Queries.GetAllBank;
using SkeletonApi.Application.Features.MasterData.Banks.Queries.GetBankWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/bank")]
    public class BankController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public BankController(IMediator mediator, ILogger<BankController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateBankResponseDto>>> Create(CreateBankRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteBankRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<Bank>>> Update(Guid id, UpdateBankRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetBankWithPaginationDto>>> GetWithPagination([FromQuery] GetBankWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetBankWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-bank")]
        public async Task<ActionResult<Result<List<GetAllBankDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllBankQuery());
        }
    }
}