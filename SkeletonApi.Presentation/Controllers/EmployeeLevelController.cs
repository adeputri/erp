﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.EmployeeLevels;
using SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.CreateEmployeeLevel;
using SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.DeleteEmployeeLevel;
using SkeletonApi.Application.Features.MasterData.EmployeeLevels.Commands.UpdateEmployeeLevel;
using SkeletonApi.Application.Features.MasterData.EmployeeLevels.Queries.GetAllEmployeeLevel;
using SkeletonApi.Application.Features.MasterData.EmployeeLevels.Queries.GetEmployeeLevelWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/employee-level")]
    public class EmployeeLevelController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private ILogger _logger;

        public EmployeeLevelController(IMediator mediator, ILogger<EmployeeLevelController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreateEmployeeLevelResponseDto>>> Create(CreateEmployeeLevelRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteEmployeeLevelRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<EmployeeLevel>>> Update(Guid id, UpdateEmployeeLevelRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetEmployeeLevelWithPaginationDto>>> GetWithPagination([FromQuery] GetEmployeeLevelWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetEmployeeLevelWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-employee-level")]
        public async Task<ActionResult<Result<List<GetAllEmployeeLevelDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllEmployeeLevelQuery());
        }
    }
}