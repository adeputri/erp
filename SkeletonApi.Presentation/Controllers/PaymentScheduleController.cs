﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SkeletonApi.Application.Features.MasterData.PaymentSchedules;
using SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.CreatePaymentSchedule;
using SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.DeletePaymentSchedule;
using SkeletonApi.Application.Features.MasterData.PaymentSchedules.Commands.UpdatePaymentSchedule;
using SkeletonApi.Application.Features.MasterData.PaymentSchedules.Queries.GetAllPaymentSchedule;
using SkeletonApi.Application.Features.MasterData.PaymentSchedules.Queries.GetPaymentScheduleWithPagination;
using SkeletonApi.Domain.Entities;
using SkeletonApi.Shared;
using System.Text.Json;

namespace SkeletonApi.Presentation.Controllers
{
    [Route("api/payment-schedule")]
    public class PaymentScheduleController : ApiControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public PaymentScheduleController(IMediator mediator, ILogger<PaymentScheduleController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult<Result<CreatePaymentScheduleResponseDto>>> Create(CreatePaymentScheduleRequest command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<Result<Guid>>> Delete(Guid id)
        {
            return await _mediator.Send(new DeletePaymentScheduleRequest(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<Result<PaymentSchedule>>> Update(Guid id, UpdatePaymentScheduleRequest command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return await _mediator.Send(command);
        }

        [HttpGet("paged")]
        public async Task<ActionResult<PaginatedResult<GetPaymentShceduleWithPaginationDto>>> GetWithPagination([FromQuery] GetPaymentShceduleWithPaginationQuery query)
        {
            // Call Validate or ValidateAsync and pass the object which needs to be validated
            var validator = new GetPaymentShceduleWithPaginationValidator();

            var result = validator.Validate(query);

            if (result.IsValid)
            {
                var pg = await _mediator.Send(query);
                var paginationData = new
                {
                    pg.page_number,
                    pg.total_pages,
                    pg.page_size,
                    pg.total_count,
                    pg.has_previous,
                    pg.has_next
                };
                Response.Headers.Add("x-pagination", JsonSerializer.Serialize(paginationData));
                return Ok(pg);
            }

            var errorMessages = result.Errors.Select(x => x.ErrorMessage).ToList();
            return BadRequest(errorMessages);
        }

        [HttpGet("get-all-payment-schedule")]
        public async Task<ActionResult<Result<List<GetAllPaymentScheduleDto>>>> GetAll()
        {
            return await _mediator.Send(new GetAllPaymentScheduleQuery());
        }
    }
}