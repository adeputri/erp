﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Customer : BaseAuditableEntity
    {
        [Column("customer_name")]
        public string CustomerName { get; set; }

        [Column("npwp")]
        public string Npwp { get; set; }

        [Column("address")]
        public string Address { get; set; }

        [Column("contact_person")]
        public string ContactPerson { get; set; }

        [Column("phone_number")]
        public string PhoneNumber { get; set; }

        public ICollection<MasterProject> Projects { get; set; } = new List<MasterProject>();
    }
}