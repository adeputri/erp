﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class NonProject : BaseAuditableEntity
    {
        [Column("project_id")]
        public Guid ProjectId { get; set; }

        [Column("item_id")]
        public Guid ItemId { get; set; }

        [Column("pic_id")]
        public Guid PicId { get; set; }

        [Column("status_item")]
        public string StatusItem { get; set; }

        [Column("order_date")]
        public DateOnly OrderDate { get; set; }

        [Column("order_qty")]
        public int OrderQty { get; set; }

        [Column("etd")]
        public DateOnly Etd { get; set; }

        [Column("send_date")]
        public DateOnly SendDate { get; set; }

        [Column("send_qty")]
        public int SendQty { get; set; }

        [Column("balance")]
        public int BalanceQty { get; set; }
        public Project Project { get; set; }
        public Item Item { get; set; }
        public Employee Employee { get; set; }
    }
}