﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class SectionProject : BaseAuditableEntity
    {
        [Column("section_name")]
        public string SectionName { get; set; }
        [Column("project_id")]
        public Guid ProjectId { get; set; }
        public Project Project { get; set; }
        public ICollection<TaskProject> TaskProjects { get; } = new List<TaskProject>();
    }
}