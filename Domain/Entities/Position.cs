﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Position : BaseAuditableEntity
    {
        [Column("position_name")]
        public string PositionName { get; set; }
    }
}