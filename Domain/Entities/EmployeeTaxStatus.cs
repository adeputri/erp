﻿using SkeletonApi.Domain.Common.Abstracts;
using System.Text.Json.Serialization;

namespace SkeletonApi.Domain.Entities
{
    public class EmployeeTaxStatus : BaseAuditableEntity
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}