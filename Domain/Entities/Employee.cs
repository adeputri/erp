﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Employee : BaseAuditableEntity
    {
        [Column("first_name")]
        public string FirstName { get; set; }

        [Column("last_name")]
        public string LastName { get; set; }

        [Column("gender")]
        public string Gender { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("mobile_phone")]
        public string MobilePhone { get; set; }

        [Column("phone")]
        public string Phone { get; set; }

        [Column("place_of_birth")]
        public string PlaceOfBirth { get; set; }

        [Column("birth_date")]
        public DateTime BirthDate { get; set; }

        [Column("blood_type")]
        public string BloodType { get; set; }

        [Column("marital_status")]
        public string MaritalStatus { get; set; }

        [Column("religion")]
        public string Religion { get; set; }

        public ICollection<Warehouse> WarehouseRequest { get; } = new List<Warehouse>();
        public ICollection<Warehouse> WarehousePIC { get; } = new List<Warehouse>();
        public ICollection<RequestItemWarehouse> RequestItemWarehouseRequest { get; } = new List<RequestItemWarehouse>();
        public ICollection<RequestItemWarehouse> RequestItemWarehousePIC { get; } = new List<RequestItemWarehouse>();
        public ICollection<Project> Projects { get; } = new List<Project>();
        public ICollection<NonProject> NonProjects { get; } = new List<NonProject>();
    }
}