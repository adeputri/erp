﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Category : BaseAuditableEntity
    {
        [Column("category_name")]
        public string CategoryName { get; set; }

        public ICollection<PurchaseNonPpn> PurchaseNonPpn { get; set; } = new List<PurchaseNonPpn>();
    }
}