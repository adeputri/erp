﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class ProrateSetting : BaseAuditableEntity
    {
        [Column("setting")]
        public string Setting { get; set; }
    }
}