﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class MasterUnit : BaseAuditableEntity
    {
        [Column("unit_name")]
        public string UnitName { get; set; }

        public ICollection<Item> Item { get; } = new List<Item>();
    }
}