﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class RequestItemWarehouse : BaseAuditableEntity
    {
        [Column("item_id")]
        public Guid ItemId { get; set; }
        public Item Item { get; set; }

        [Column("request_employee_id")]
        public Guid RequestEmployeeId { get; set; }
        public Employee RequestEmployee { get; set; }

        [Column("pic_employee_id")]
        public Guid PICEmployeeId { get; set; }
        public Employee PICEmployee { get; set; }

        [Column("project_id")]
        public Guid ProjectId { get; set; }
        public MasterProject Project { get; set; }

        [Column("qty_request")]
        public int QtyRequest { get; set; }
        [Column("is_finish")]
        public bool IsFinish { get; set; }

    }
}
