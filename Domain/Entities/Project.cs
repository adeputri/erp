﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace SkeletonApi.Domain.Entities
{
    public class Project : BaseAuditableEntity
    {
        [Column("number_spk")]
        public string NumberSpk { get; set; }

        [Column("master_project_id")]
        public Guid MasterProjectId { get; set; }

        [Column("project_start_date")]
        public DateOnly ProjectStartDate { get; set; }

        [Column("project_end_date")]
        public DateOnly ProjectEndDate { get; set; }

        [Column("pic_id")]
        public Guid PicId { get; set; }

        [JsonIgnore]
        public MasterProject MasterProject { get; set; }

        [JsonIgnore]
        public Employee Employee { get; set; }

        [JsonIgnore]
        public ICollection<PurchasePpn> PurchasePpn { get; } = new List<PurchasePpn>();

        public ICollection<SectionProject> SectionProjects { get; } = new List<SectionProject>();
        public ICollection<NonProject> NonProjects { get; } = new List<NonProject>();
    }
}