﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Bank : BaseAuditableEntity
    {
        [Column("bank_name")]
        public string BankName { get; set; }

        public ICollection<BranchBank> BranchBank { get; } = new List<BranchBank>();
    }
}