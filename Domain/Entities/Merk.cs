﻿using SkeletonApi.Domain.Common.Abstracts;

namespace SkeletonApi.Domain.Entities
{
    public class Merk : BaseAuditableEntity
    {
        public string Name { get; set; }
        public ICollection<Item> Items { get; set; } = new List<Item>();
    }
}
