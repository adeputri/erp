﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Item : BaseAuditableEntity
    {
        [Column("barcode_item")]
        public string BarcodeItem { get; set; }

        [Column("type")]
        public string Type { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("merk_id")]
        public Guid MerkId { get; set; }

        public Merk Merk { get; set; }

        [Column("master_unit_id")]
        public Guid MasterUnitId { get; set; }

        public MasterUnit MasterUnit { get; set; }
        public ICollection<Warehouse> Warehouses { get; set; } = new List<Warehouse>();
        public ICollection<NonProject> NonProjects { get; } = new List<NonProject>();
    }
}