﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class MasterProject : BaseAuditableEntity
    {
        [Column("project_name")]
        public string ProjectName { get; set; }

        [Column("customer_id")]
        public Guid CustomerId { get; set; }

        public Customer Customer { get; set; }

        public ICollection<Warehouse> Warehouses { get; } = new List<Warehouse>();
        public ICollection<RequestItemWarehouse> RequestItemWarehouses { get; } = new List<RequestItemWarehouse>();
        public ICollection<PurchaseNonPpn> ReportNonPpns { get; } = new List<PurchaseNonPpn>();
        public ICollection<Project> Projects { get; } = new List<Project>();
    }
}