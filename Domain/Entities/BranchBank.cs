﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class BranchBank : BaseAuditableEntity
    {
        [Column("branch_bank_name")]
        public string BranchBankName { get; set; }

        [Column("bank_id")]
        public Guid BankId { get; set; }

        public Bank Bank { get; set; }
    }
}