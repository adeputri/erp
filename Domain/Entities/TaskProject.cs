﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class TaskProject : BaseAuditableEntity
    {
        [Column("task_name")]
        public string TaskName { get; set; }

        [Column("status")]
        public string Status { get; set; }

        [Column("assign_to")]
        public string AssignTo { get; set; }

        [Column("due_date")]
        public string DueDate { get; set; }

        [Column("section_project_id")]
        public Guid SectionProjectId { get; set; }

        public SectionProject SectionProject { get; set; }
    }
}