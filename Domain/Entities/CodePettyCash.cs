﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class CodePettyCash : BaseAuditableEntity
    {
        [Column("code_name")]
        public string CodeName { get; set; }

        [Column("item")]
        public string Item { get; set; }

        public ICollection<PurchaseNonPpn> ReportNonPpns { get; } = new List<PurchaseNonPpn>();
    }
}