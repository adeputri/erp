﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class SettingTitle : BaseAuditableEntity
    {
        [Column("title_name")]
        public string TitleName { get; set; }
    }
}