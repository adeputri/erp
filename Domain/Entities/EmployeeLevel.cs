﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class EmployeeLevel : BaseAuditableEntity
    {
        [Column("level")]
        public string Level { get; set; }
    }
}