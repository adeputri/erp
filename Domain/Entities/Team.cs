﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Team : BaseAuditableEntity
    {
        [Column("team_name")]
        public string TeamName { get; set; }
    }
}