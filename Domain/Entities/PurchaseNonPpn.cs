﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class PurchaseNonPpn : BaseAuditableEntity
    {
        [Column("master_project_id")]
        public Guid? MasterProjectId { get; set; }

        [Column("code_id")]
        public Guid? CodeId { get; set; }

        [Column("category_id")]
        public Guid? CategoryId { get; set; }

        [Column("bon_date")]
        public DateTime? BonDate { get; set; }

        [Column("bon_number")]
        public string? BonNumber { get; set; }

        [Column("transaction")]
        public string? Transaction { get; set; }

        [Column("debit")]
        public double? Debit { get; set; }

        [Column("kredit")]
        public double? Kredit { get; set; }

        [Column("payment_method")]
        public string? PaymentMethod { get; set; }

        [Column("approval_by")]
        public string? ApprovalBy { get; set; }

        [Column("approval_status")]
        public string? ApprovalStatus { get; set; }

        [Column("note")]
        public string? Note { get; set; }

        [Column("file_path")]
        public string? FilePath { get; set; }
        [Column("type")]
        public string? Type { get; set; }

        public MasterProject MasterProject { get; set; }
        public CodePettyCash CodePettyCash { get; set; }
        public Category Category { get; set; }
    }
}