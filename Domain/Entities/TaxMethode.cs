﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class TaxMethode : BaseAuditableEntity
    {
        [Column("status")]
        public string Status { get; set; }
    }
}