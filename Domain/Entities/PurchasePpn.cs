﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class PurchasePpn : BaseAuditableEntity
    {
        [Column("unique_code")]
        public string UniqueCode { get; set; }

        [Column("payment_status")]
        public string? PaymentStatus { get; set; }

        [Column("supplier_id")]
        public Guid SupplierId { get; set; }

        [Column("project_id")]
        public Guid ProjectId { get; set; }

        [Column("pre_order_date")]
        public DateTime? PreOrderDate { get; set; }

        [Column("pre_order_number")]
        public string? PreOrderNumber { get; set; }

        [Column("pi_number")]
        public string? PiNumber { get; set; }

        [Column("inv_date")]
        public DateTime? InvDate { get; set; }

        [Column("inv_receipt_date")]
        public DateTime? InvReceiptDate { get; set; }

        [Column("inv_number")]
        public string? InvNumber { get; set; }

        [Column("faktur_date")]
        public DateTime? FpDate { get; set; }

        [Column("fp_number")]
        public string? FakturNumber { get; set; }

        [Column("travel_doc_date")]
        public DateTime? TravelDocDate { get; set; }

        [Column("travel_doc_number")]
        public string? TravelDocNumber { get; set; }

        [Column("masa")]
        public string? Masa { get; set; }

        [Column("dpp")]
        public double? Dpp { get; set; }

        [Column("ppn")]
        public double? Ppn { get; set; }

        [Column("note")]
        public string? Note { get; set; }

        [Column("paid_of_date")]
        public DateTime? PaidOfDate { get; set; }

        [Column("file_path")]
        public string? FilePath { get; set; }

        [Column("pph23")]
        public double? Pph23 { get; set; }

        [Column("discount")]
        public double? Discount { get; set; }

        [Column("approval_by")]
        public string? ApprovalBy { get; set; }

        [Column("approval_status")]
        public string? ApprovalStatus { get; set; }

        public Supplier Supplier { get; set; }
        public Project Project { get; set; }
    }
}