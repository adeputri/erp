﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Warehouse : BaseAuditableEntity
    {
        [Column("item_id")]
        public Guid ItemId { get; set; }
        public Item Item { get; set; }

        [Column("request_employee_id")]
        public Guid RequestEmployeeId { get; set; }
        public Employee RequestEmployee { get; set; }

        [Column("pic_employee_id")]
        public Guid PICEmployeeId { get; set; }
        public Employee PICEmployee { get; set; }

        [Column("project_id")]
        public Guid ProjectId { get; set; }
        public MasterProject Project { get; set; }

        [Column("qty_item")]
        public int QtyItem { get; set; }

        [Column("note")]
        public string Note { get; set; }

        [Column("path_document")]
        public string? PathDocument { get; set; }


    }
}
