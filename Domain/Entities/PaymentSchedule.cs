﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class PaymentSchedule : BaseAuditableEntity
    {
        [Column("schedule")]
        public string Schedule { get; set; }
    }
}