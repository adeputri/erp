﻿using SkeletonApi.Domain.Common.Abstracts;
using System.ComponentModel.DataAnnotations.Schema;

namespace SkeletonApi.Domain.Entities
{
    public class Supplier : BaseAuditableEntity
    {
        [Column("supplier_name")]
        public string SupplierName { get; set; }

        [Column("npwp")]
        public string? Npwp { get; set; }

        [Column("address")]
        public string? Address { get; set; }

        [Column("contact_person")]
        public string? ContactPerson { get; set; }

        [Column("phone_number")]
        public string? PhoneNumber { get; set; }

        public ICollection<PurchasePpn> CashAtBank { get; } = new List<PurchasePpn>();
    }
}